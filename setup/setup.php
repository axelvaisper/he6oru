<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
error_reporting(0);
define('STATUS_DIR', realpath(dirname(dirname(__FILE__))));
define('PE', PHP_EOL);
require_once STATUS_DIR . '/setup/class/class.SX.php';
SX::getInit();

$_REQUEST['step'] = isset($_REQUEST['step']) ? $_REQUEST['step'] : '';
switch ($_REQUEST['step']) {
    default:
        Setup::get();
        break;

    case '1':
        Setup::getStep1();
        break;

    case '2':
        Setup::getStep2();
        break;

    case '3':
        Setup::getStep3();
        break;

    case '4':
        Setup::getStep4();
        break;
}
