<script>
{include file="$theme/validate.txt"}
$.validator.setDefaults({
  submitHandler: function() {
    document.forms['Form'].submit();
  }
});
$(document).ready(function() {
  $('#Form').validate({
    rules: {
      dbhost:   { required placeholder=" ": true },
      dbuser:   { required placeholder=" ": true },
      dbname:   { required placeholder=" ": true },
      dbprefix: { required placeholder=" ": true }
    },
    messages: { }
  });
});
</script>

<h1>{#Step2#}</h1>

{if !empty($errors_path)}

<div class="errorbox">
  <h3>{#Errors#}</h3>
  <br />
   {#Step1ErrInf#}
</div>

<div class="eula">
{foreach from=$error_not_writables item=nw}
  <div class="error_nw">{$nw}</div>
{/foreach}
</div>

<form method="post" action="{$setupdir}/setup.php">
  <input type="hidden" name="step" value="1" />
  <input type="submit" value="{#Step1ErrB#}" btnx/>
</form>

{else}
{$dfg}
<form autocomplete="off" name="Form" id="Form"  method="post" action="{$setupdir}/setup.php" step2>

{if isset($db_no_connection) && $db_no_connection == 1}
  <div class="error_conn">
  {#icon_alert#} {#Step1_NoConn#}
  </div>
{/if}

<label title="{$lang.Step1_ainf|sanitize}">
<small>{#Step1_a#}</small>
<input  type="text" name="dbhost" value="{$smarty.post.dbhost|sanitize|default: 'localhost'}"/>
</label>

<label title="{$lang.Step1_pinf|sanitize}">
<small>{#Step1_p#}</small>
<input type="text" name="dbport" value="{$smarty.post.dbport|sanitize|default: '3306'}" />
</label>

<label title="{$lang.Step1_binf|sanitize}">
<small>{#Step1_b#}</small>
<input type="text" name="dbuser" value="{$smarty.post.dbuser|sanitize|default: 'root'}" />
</label>

<label title="{$lang.Step1_cinf|sanitize}">
<small>{#Step1_c#}</small>
<input type="text" name="dbpass" value="{if isset($smarty.post.dbpass)}{$smarty.post.dbpass|sanitize}{/if}" />
</label>

<label title="{$lang.Step1_dinf|sanitize}">
<small>{#Step1_d#}</small>
<input type="text" name="dbname" value="{$smarty.post.dbname|sanitize|default: 'phpmyadmin'}" />
</label>

<label title="{$lang.Step1_einf|sanitize}">
<small>{#Step1_e#}</small>
<input type="text" name="dbprefix" value="{$smarty.post.dbprefix|sanitize|default: 'sx011'}" />
</label>

<label title="{$lang.Step1_ginf|sanitize}" selectx>
<small>{#Step1_g#}</small>
<select name="type_sess">
<option value="auto" {if !isset($smarty.post.type_sess)}selected="selected"{/if}>{#AutoSessions#}</option>
<option value="base" {if isset($smarty.post.type_sess) && $smarty.post.type_sess == 'base'}selected="selected"{/if}>{#BaseSessions#}</option>
<option value="file" {if isset($smarty.post.type_sess) && $smarty.post.type_sess == 'file'}selected="selected"{/if}>{#FileSessions#}</option>
</select>
</label>

<p>
<input type="hidden" name="step" value="3" />
<input type="submit" value="{#Step1_Button#}"/>
</p>

</form>
{/if}
