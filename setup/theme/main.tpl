<!DOCTYPE html>
<html lang="ru">
<head>
<title>{$title}</title>

<meta charset="windows-1251" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="pragma" content="no-cache" />
<meta name="generator" content="CMS Status-X {$Version}" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel="stylesheet" href="{$setupdir}/theme/setup.css{#version_file#}" />

{if $browser == 'ie7' || $browser == 'ie8' || $browser == 'ie9'}
<script src="{$homedir}/js/html5shiv.js{#version_file#}"></script>
<script src="{$homedir}/js/jquery-1.11.1.js{#version_file#}"></script>
{else}
<script src="{$homedir}/js/jquery-2.1.1.js{#version_file#}"></script>
{/if}

<script src="{$homedir}/js/jvalidate.js{#version_file#}"></script>
</head>
<body gridx="row center middle">

<main gridx="col 8@m 4@b">

<figure><a href="http://www.status-x.ru" target="_blank" title="����������� ����">
<img src="{$setupdir}/images/logo.png{#version_file#}"/></a></figure>

{$content}

<br>
<div align="right">
<a href="http://www.status-x.ru/" target="_blank">
CMS Status-X 2009-{$smarty.now|date_format:"%Y"} {#icon_copyright#}</a>
</div>

</main>

</body>
</html>
