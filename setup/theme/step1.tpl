<form autocomplete="off" name="Form" id="Form"  method="post" action="{$setupdir}/setup.php" step1>

<h1>{#Step1#}</h1>

  {if $params.php == 1}
    <span title="{$lang.Test_Ok}">
      {#icon_check#} {#Load_php#}
    </span>
  {else}
    <em title="{$lang.Test_Fail}">
      {#icon_alert#} {#Load_php#} {#SetSetup#}
    </em>
  {/if}
  <br>

  {if $params.mbstring == 1}
    <span title="{$lang.Test_Ok}">
      {#icon_check#} {#Load_Ext#} mbstring
    </span>
  {else}
    <em title="{$lang.Test_Fail}">
      {#icon_alert#} {#Load_Ext#} mbstring {#SetSetup#}
    </em>
  {/if}
  <br>

  {if $params.spl == 1}
    <span title="{$lang.Test_Ok}">
      {#icon_check#} {#Load_Ext#} spl
    </span>
  {else}
    <em title="{$lang.Test_Fail}">
      {#icon_alert#} {#Load_Ext#} spl {#SetSetup#}
    </em>
  {/if}
  <br>

  {if $params.mysqli == 1}
    <span title="{$lang.Test_Ok}">
      {#icon_check#} {#Load_Ext#} mysqli
    </span>
  {else}
    <em title="{$lang.Test_Fail}">
      {#icon_alert#} {#Load_Ext#} mysqli {#SetSetup#}
    </em>
  {/if}
  <br>

  {if $params.iconv == 1}
    <span title="{$lang.Test_Ok}">
      {#icon_check#}
    </span>
  {else}
    <span title="{$lang.Test_Fail}" class="red">
      {#icon_alert#}
    </span>
  {/if}
  {#Load_Ext#} iconv
  <br>

  {if $params.gd == 1}
    <span title="{$lang.Test_Ok}">
      {#icon_check#}
    </span>
  {else}
    <em title="{$lang.Test_Fail}">
      {#icon_alert#}
    </em>
  {/if}
  {#Load_Ext#} gd
  <br>

  {if $params.zlib == 1}
    <span title="{$lang.Test_Ok}">
      {#icon_check#}
    </span>
  {else}
     <em title="{$lang.Test_Fail}">
      {#icon_alert#}
    </em>
  {/if}
  {#Load_Ext#} zlib
  <br>

  {if $params.locale == 1}
    <span title="{$lang.Test_Ok}">
      {#icon_check#}
    </span>
  {else}
    <em title="{$lang.Test_Fail}">
      {#icon_alert#}
    </em>
  {/if}
  {#Load_Locale#}
  <br>

  {if $params.safemode == 0}
    <span title="{$lang.Test_Ok}">
    {#icon_check#}
    </span>
  {else}
    <em title="{$lang.Test_Fail}">
      {#icon_alert#}
    </em>
  {/if}
  {#Load_Param#} safe_mode
  <br>

  {if $params.magic_quotes_gpc == 0}
    <span title="{$lang.Test_Ok}">
      {#icon_check#}
    </span>
  {else}
    <em title="{$lang.Test_Fail}">
      {#icon_alert#}
    </em>
  {/if}
  {#Load_Param#} magic_quotes_gpc
  <br>

  {if $params.magic_quotes_runtime == 0}
    <span title="{$lang.Test_Ok}">
    {#icon_check#}
    </span>
  {else}
    <em title="{$lang.Test_Fail}">
    {#icon_alert#}
    </em>
  {/if}
  {#Load_Param#} magic_quotes_runtime
  <br>

  {if $params.magic_quotes_sybase == 0}
    <span title="{$lang.Test_Ok}">
      {#icon_check#}
    </span>
  {else}
    <em title="{$lang.Test_Fail}">
      {#icon_alert#}
    </em>
  {/if}
  {#Load_Param#} magic_quotes_sybase
  <br>

  {if $params.memory_limit > 16}
    <span title="{$lang.Test_Ok}">
      {#icon_check#}
    </span>
  {else}
    <em title="{$lang.Test_Fail}">
      {#icon_alert#}
    </em>
  {/if}
  {#Mem_limit#}

<br>

<p>
{if $params.php == 1 && $params.mbstring == 1 && $params.spl == 1 && $params.mysqli == 1}
  <input type="hidden" name="step" value="2" />
  <input type="submit" value="{#Step_Button#}"/>
{else}
  <h2><span>{#ErrorSetup#}</span></h2>
{/if}
</p>

</form>
