<script>
{include file="$theme/validate.txt"}
$.validator.setDefaults({
  submitHandler: function() {
      document.forms['Form'].submit();
  }
});
$(document).ready(function() {
  $('#Form').validate({
      rules: {
          first: { required: false },
          last: { required: false },
          username: { required: true },
          pass: { required: true, minlength: 3 },
          email: { required: true, email: true },
          street: { required: false },
          zip: { required: false, number: false },
          town: { required: false },
          company: { required: false },
          websitename: { required: true }
      },
      messages: { }
  });
});
</script>


<h1>{#Step3#}</h1>

<form name="Form" id="Form" method="post" action="{$setupdir}/setup.php" step3>

<label title="{$lang.s2_3inf|sanitize}">
<small>{#s2_3#}</small>
<input name="username" type="text" value="{$smarty.post.username|sanitize|default: ''}" />
</label>

<label title="{$lang.s2_5inf|sanitize}">
<small>{#s2_5#}</small>
<input name="email" type="email" value="{$smarty.post.email|sanitize|default: ''}" />
</label>

<label title="{$lang.s2_4inf|sanitize}">
<small>{#s2_4#}</small>
<input name="pass" type="password" value="{$smarty.post.pass|sanitize|default: ''}" />
</label>

<label><small>{#s2_1#}</small>
<input name="first" type="text" value="{$smarty.post.first|sanitize|default: ''}" />
</label>

<label for="last"><small>{#s2_2#}</small>
<input name="last" type="text" value="{$smarty.post.last|sanitize|default: ''}" />
</label>

<label><small>{#s2_6#}</small>
<input name="street" type="text"  value="{$smarty.post.street|sanitize|default: ''}" />
</label>

<label><small>{#s2_7#}</small>
<input name="zip" type="text" value="{$smarty.post.zip|sanitize|default: ''}" />
</label>

<label><small>{#s2_8#}</small>
<input name="town" type="text" value="{$smarty.post.town|sanitize|default: ''}" />
</label>

<label><small>{#s2_9#}</small>
<input name="company" type="text" value="{$smarty.post.company|sanitize|default: ''}" />
</label>

<label><small>{#s2_11#}</small>
<input name="websitename" type="text"  value="{$smarty.post.websitename|sanitize|default: ''}" />
</label>

<label><small>{#s2_12#}</small>
<input name="phone" type="text" value="{$smarty.post.phone|sanitize|default: ''}" />
</label>

<label><small>{#s2_13#}</small>
<input name="fax" type="text" value="{$smarty.post.fax|sanitize|default: ''}" />
</label>

<p>
<input type="hidden" name="step" value="4" />
<input type="submit" value="{#Step_Button#}"/>
</p>

</form>
