<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (!defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}

class View extends Smarty {

    protected static $_instance;

    public static function get() {
        if (self::$_instance === NULL) {
            self::$_instance = new self;
        }
        return self::$_instance;
    }

    public function __construct() {
        parent::__construct();
        $this->compile_dir = STATUS_DIR . '/temp/compiled/1/main';
        $this->cache_dir = STATUS_DIR . '/temp/compiled/1/main';
        $this->config_dir = STATUS_DIR . '/setup/lang';
        $this->template_dir = STATUS_DIR . '/setup/theme';
        $this->plugins_dir = array(SMARTY_DIR . 'plugins', SMARTY_DIR . 'statusplugins');

        $this->debugging = false;
        $this->error_unassigned = false; // ���������� ��������� � ������������� ����������
        $this->registerPlugin('modifier', 'sanitize', 'sanitize');

        $this->configLoad(STATUS_DIR . '/setup/lang/lang.txt');
        SX::$lang = $this->getConfigVars();
        $this->assign('lang', SX::$lang);
        $this->assign('theme', THEME);
        $this->assign('browser', SX::getAgent());
        $this->assign('homedir', 'http://' . $_SERVER['HTTP_HOST'] . str_replace('/setup/setup.php', '', $_SERVER['PHP_SELF']));
        $this->assign('setupdir', 'http://' . $_SERVER['HTTP_HOST'] . str_replace('/setup.php', '', $_SERVER['PHP_SELF']));
        $this->assign('Version', '1.05.011');
    }

}
