<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (!defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}
function getAll($var, $val = '') {
    return preg_replace('/[^\w-' . $val . ']/i', '', $var);
}

function sanitize($text) {
    static $arr = NULL;
    if ($arr === NULL) {
        $arr['search'] = array('\'', ' & ', '<', '>', '"', '�', '�', '�', '�', '�', '�', '�', '�');
        $arr['replace'] = array('&#039;', ' &amp; ', '&lt;', '&gt;', '&quot;', '&euro;', '&raquo;', '&laquo;', '&copy;', '&reg;', '&trade;', '&bdquo;', '&ldquo;');
    }
    return str_replace($arr['search'], $arr['replace'], $text);
}

abstract class Tool {

    /* ����� �������� ���������� ������� apache */
    public static function apacheModul($module) {
        if (function_exists('apache_get_modules')) {
            static $modules = NULL;
            if (empty($modules)) {
                $modules = apache_get_modules();
            }
            return in_array($module, $modules) ? true : false;
        }
        return true;
    }

    /* ����� ���������� �� ���������� ����� �������� */
    public static function cleanAllow($value, $mask = NULL) {
        return trim(preg_replace('/[^\w-' . preg_quote($mask, '/') . ']/i', '', $value));
    }
}

abstract class SX {

    public static $lang = array();
    protected static $_param = array();

    /* ����� ��������� ���������� */
    public static function get($key, $default = NULL) {
        list($a, $count) = self::_parse($key);
        switch ($count) {
            case 1: return isset(self::$_param[$a[0]]) ? self::$_param[$a[0]] : $default;
            case 2: return isset(self::$_param[$a[0]][$a[1]]) ? self::$_param[$a[0]][$a[1]] : $default;
            case 3: return isset(self::$_param[$a[0]][$a[1]][$a[2]]) ? self::$_param[$a[0]][$a[1]][$a[2]] : $default;
            case 4: return isset(self::$_param[$a[0]][$a[1]][$a[2]][$a[3]]) ? self::$_param[$a[0]][$a[1]][$a[2]][$a[3]] : $default;
            case 5: return isset(self::$_param[$a[0]][$a[1]][$a[2]][$a[3]][$a[4]]) ? self::$_param[$a[0]][$a[1]][$a[2]][$a[3]][$a[4]] : $default;
            case 6: return isset(self::$_param[$a[0]][$a[1]][$a[2]][$a[3]][$a[4]][$a[5]]) ? self::$_param[$a[0]][$a[1]][$a[2]][$a[3]][$a[4]][$a[5]] : $default;
        }
        return $default;
    }

    /* ����� ���������� ���������� */
    public static function set($key, $value = NULL) {
        list($a, $count) = self::_parse($key);
        switch ($count) {
            case 1: self::$_param[$a[0]] = $value; break;
            case 2: self::$_param[$a[0]][$a[1]] = $value; break;
            case 3: self::$_param[$a[0]][$a[1]][$a[2]] = $value; break;
            case 4: self::$_param[$a[0]][$a[1]][$a[2]][$a[3]] = $value; break;
            case 5: self::$_param[$a[0]][$a[1]][$a[2]][$a[3]][$a[4]] = $value; break;
            case 6: self::$_param[$a[0]][$a[1]][$a[2]][$a[3]][$a[4]][$a[5]] = $value; break;
        }
    }

    /* ����� �������� ������������� ��������� */
    public static function has($key) {
        list($a, $count) = self::_parse($key);
        switch ($count) {
            case 1: return isset(self::$_param[$a[0]]);
            case 2: return isset(self::$_param[$a[0]][$a[1]]);
            case 3: return isset(self::$_param[$a[0]][$a[1]][$a[2]]);
            case 4: return isset(self::$_param[$a[0]][$a[1]][$a[2]][$a[3]]);
            case 5: return isset(self::$_param[$a[0]][$a[1]][$a[2]][$a[3]][$a[4]]);
            case 6: return isset(self::$_param[$a[0]][$a[1]][$a[2]][$a[3]][$a[4]][$a[5]]);
        }
        return false;
    }

    /* ����� ��������� ���������� */
    public static function del($key) {
        list($a, $count) = self::_parse($key);
        switch ($count) {
            case 1: unset(self::$_param[$a[0]]); break;
            case 2: unset(self::$_param[$a[0]][$a[1]]); break;
            case 3: unset(self::$_param[$a[0]][$a[1]][$a[2]]); break;
            case 4: unset(self::$_param[$a[0]][$a[1]][$a[2]][$a[3]]); break;
            case 5: unset(self::$_param[$a[0]][$a[1]][$a[2]][$a[3]][$a[4]]); break;
            case 6: unset(self::$_param[$a[0]][$a[1]][$a[2]][$a[3]][$a[4]][$a[5]]); break;
        }
    }

    /* ����� ����������� ���������� ������� ������ � ����������� */
    protected static function _parse($key) {
        static $cache = array();
        if (!isset($cache[$key])) {
            $array = explode('.', $key);
            $cache[$key] = array($array, count($array));
        }
        return $cache[$key];
    }

    /* ����� ������������� ��������� */
    public static function getInit() {
        self::getLocale();
        self::versionPhp();
        self::getCheckCompiled();
        self::getStripslashesArray();
        self::set('database', self::getConfig('db.config'));
        self::setDefine('PE', PHP_EOL);
        self::setDefine('BASE_PATH', self::basePatch());
        self::setDefine('THEME', STATUS_DIR . '/setup/theme');
        self::setDefine('SMARTY_DIR', STATUS_DIR . '/lib/smarty/');
        self::setDefine('SMARTY_RESOURCE_CHAR_SET', 'windows-1251');
        self::setDefine('SMARTY_RESOURCE_DATE_FORMAT', '%d.%m.%Y, %H:%M');
        spl_autoload_register(array('SX', 'setup_autoload'));
    }

    /* ����� �������� ���� ����������� ����� � �������� */
    protected static function basePatch() {
        return str_replace('//', '/', str_replace('setup/setup.php', '/', $_SERVER['PHP_SELF']));
    }

    /* ������������� ������ PHP */
    public static function getLocale() {
        header('Content-type: text/html; charset=windows-1251');
        setlocale(LC_ALL, 'ru_RU.CP1251', 'ru_RU.cp1251', 'rus_RUS.CP1251', 'Russian_Russia.1251', 'ru_RU', 'ru', 'russian');
    }

    /* ����� ������������ ������� */
    public static function setup_autoload($class) {
        switch ($class) {
            case 'DB':
                include (STATUS_DIR . '/class/class.DB.php');
                break;
            case 'Smarty':
                include (SMARTY_DIR . 'Smarty.class.php');
                break;
            default:
                if (substr(strtolower($class), 0, 7) != 'smarty_') {
                    include (STATUS_DIR . '/setup/class/class.' . $class . '.php');
                }
                break;
        }
        return class_exists($class, false);
    }

    /* ����� ����������� ������ ����� */
    public static function getConfig($name) {
        $config = '';
        include (STATUS_DIR . '/config/' . $name . '.php');
        return !empty($config) ? $config : '';
    }

    /* ����� ��������� define */
    public static function setDefine($define, $value = '') {
        if (!defined($define)) {
            define($define, $value);
        }
    }

    /* ����� ����������� �������� */
    public static function getAgent() {
        if (stristr($_SERVER['HTTP_USER_AGENT'], 'firefox')) {
            return 'firefox';
        } elseif (stristr($_SERVER['HTTP_USER_AGENT'], 'safari')) {
            return 'safari';
        } elseif (stristr($_SERVER['HTTP_USER_AGENT'], 'Trident')) {
            return 'ie11';
        } elseif (stristr($_SERVER['HTTP_USER_AGENT'], 'msie 10')) {
            return 'ie10';
        } elseif (stristr($_SERVER['HTTP_USER_AGENT'], 'msie 8')) {
            return 'ie8';
        } elseif (stristr($_SERVER['HTTP_USER_AGENT'], 'opera')) {
            return 'opera';
        } else {
            return '';
        }
    }

    /* ����� ����������� ����������� �� ������� */
    public static function getCheckInstall() {
        $config = self::getConfig('db.config');
        if (self::getConnect($config)) {
            self::setDefine('PREFIX', $config['dbprefix']);
            $check = DB::get()->fetch_object("SELECT Id FROM " . PREFIX . "_benutzer WHERE Id='1' LIMIT 1");
            if (is_object($check)) {
                View::get()->assign('title', self::$lang['NameSite']);
                View::get()->assign('content', View::get()->fetch(THEME . '/error.tpl'));
                View::get()->display(THEME . '/main.tpl');
                exit;
            }
        }
    }

    /* ����� �������� ������� ���� */
    public static function getCreateBase($config) {
        $link = mysqli_connect($config['dbhost'], $config['dbuser'], $config['dbpass']);
        if (mysqli_connect_errno()) {
            mysqli_query($link, "CREATE DATABASE IF NOT EXISTS " . $config['dbname'] . " DEFAULT CHARACTER SET cp1251 COLLATE cp1251_general_ci");
            mysqli_close($link);
            return true;
        }
        return false;
    }

    /* ����� �������� ����������� � ���� */
    public static function getConnect($config) {
        if (!empty($config['dbhost']) && !empty($config['dbname'])) {
            if (mysqli_connect($config['dbhost'], $config['dbuser'], $config['dbpass'], $config['dbname'], $config['dbport'])) {
                return true;
            }
        }
        return false;
    }

    /* ����� �������� ����������� ��������� ����� �������� ������ */
    public static function checkSession() {
        ini_set('session.save_handler', 'user');
        return ini_get('session.save_handler') == 'user' ? true : false;
    }

    /* ����� �������� ������ php */
    public static function checkVersion() {
        return version_compare(PHP_VERSION, '5.2.0', '<') ? true : false;
    }

    /* ����� ����������� ����������� �� ������� */
    public static function versionPhp() {
        if (self::checkVersion()) {
            self::output('<pre><font color="#FF0000"><h2>�������!</h2></font> ������� �� ����� ���� �����������, ������ ���������� PHP-������ ' . PHP_VERSION . '<br>����������� ����������� ������ PHP ��� ���������� ������ ������� <strong>5.2</strong></pre>', true);
        }
    }

    /* ����� �������� ����������� ����� ���� �������� �� ������ */
    public static function getCheckCompiled() {
        if (!is_writable(STATUS_DIR . '/temp/compiled/1/main')) {
            chmod(STATUS_DIR . '/temp/compiled/1/main', 0777);
        }
        if (!is_writable(STATUS_DIR . '/temp/compiled/1/main')) {
            self::output('<pre><font color="#FF0000"><h2>�������! / ������!</h2></font>������� <strong>&quot;/temp/compiled/1/main/&quot;</strong> �� ����� ���� �� ������!
		���������� �������� ����� ������� �� 777<br /></pre>', true);
        }
    }

    /* ��������� ������ �������� stripslashes ���������� �������� $_POST, $_GET, $_REQUEST, $_COOKIE */
    public static function getStripslashesArray() {
        if (function_exists('get_magic_quotes_gpc') && get_magic_quotes_gpc()) {
            $function = create_function('&$value', '$value = stripslashes($value);');
            if (!empty($_POST)) {
                array_walk_recursive($_POST, $function);
            }
            if (!empty($_GET)) {
                array_walk_recursive($_GET, $function);
            }
            if (!empty($_REQUEST)) {
                array_walk_recursive($_REQUEST, $function);
            }
            if (!empty($_COOKIE)) {
                array_walk_recursive($_COOKIE, $function);
            }
        }
    }

    /* ����� ������ */
    public static function output($content, $exit = false) {
        echo $content;
        if ($exit === true) {
            if (!headers_sent()) {
                header('Date: ' . gmdate('D, d M Y H:i:s \G\M\T'));
                header('Server: Protected by CMS Status-X');
                header('X-Powered-By: CMS Status-X');
                header('Expires: ' . gmdate('D, d M Y H:i:s \G\M\T', gmmktime(0, 4, 0, 11, 1, 1974)));
            }
            exit;
        }
    }

    /* ����� �������� */
    public static function setLog($action, $type = 1, $benutzer = 0) {

    }

}
