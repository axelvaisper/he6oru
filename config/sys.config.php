<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2016  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (!defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}

// ���������� �����, ��������� ��� ������ � �����
// 1-���� ��������, 0-���� ��������
$config['site']['aktiv'] = '1';

// ��������� �������� ����� �������
// 1-��������, 0-���� � ������� ������
$config['debug'] = '1';

// ����� ������ �����
// 0-�� �����, 1-����� � ����� temp/logs, 2-����� �� ���������� PHP, example@mail.ru - ��� �� ��������� �����
$config['loger'] = '0';

// ��������� ������ ������� �� ����������
// 1-����������� ����, 0-����� cron �������
$config['cron']  = '1';

// ��������� �������� �� ������ SSL
// 0-http://, 1-https://
$config['ssl']   = '0';

// �������� https ������ �� ������������ ���������.
// �������� array('shop:callback') ������� https ������ �� �������� index.php?p=shop&action=callback,
// * - ����� ��������. ������ array('shop:*', '*:callback','*:*')
$config['https'] = array('*:*');

// ��������� �������� �� ����� � ������� �� �������� ��������
// <!-- ���/���� ������� CMS Status-X --> � <!-- End ���/���� ������� CMS Status-X -->
// 1-������� ��, 0-�������, ��� ��������� ������� ���� ������� �����������!
$config['tplcleanid']   = '0';

// ����� ��������� ��� ����������� ����� ���� ��������
$config['site']['time']  = '���� �� �������������, ������ ����� ����� ����� ������ :)';

// IP ����� ��� �������� ���� �������� ��������, ��������� ������� ����������� ������� ��� ��������
$config['site']['ip']    = '127.0.0.1';

// �������� �� ������� ��������� ������������ �������
$config['shop'] = array('showforums', 'showforum', 'showtopic', 'forum', 'forums', 'members', 'newpost', 'pn', 'addpost', 'user');


// �������� �� ������� ��������� ������������ ���������
$config['kalendar'] = array('shop', 'showforums', 'showforum', 'showtopic', 'forum', 'forums', 'members', 'newpost', 'pn', 'addpost', 'user');

// ����������������� ������� ������� ���������, ��� ���������� ���������� �������� ������
// $config['hosts'] = array('http://www.status-x.ru/', 'http://sx-studio.ru/');

// ����������� ��������, ��� ������ � ������� ���������� ����������� {$configs.test}
// ��������, ������ ���� ��� ������� � ������ ����� {$configs.v} ����������� � v1703
// $config['v'] = 'v1703';

error_reporting($config['debug'] != '1' ? 0 : E_ALL);
ini_set('magic_quotes_runtime', 0);
ini_set('magic_quotes_sybase', 0);
ini_set('arg_separator.input', '&amp;');
ini_set('arg_separator.output', '&amp;');
ini_set('url_rewriter.tags', '1');
// ������ session id �����
ini_set('session.use_cookies', 1);
// ������ session id ������ � �����
ini_set('session.use_only_cookies', 1);
// ��������� ���������� ��������� sid
ini_set('session.use_trans_sid', 0);
