<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}
error_reporting(0);                                                                      // ����� ������ php: 0 - ���������, E_ALL - ��������
define('STATUS_DIR', realpath(dirname(__FILE__)));
require_once STATUS_DIR . '/class/class.SX.php';                                         // ���������� �������� ����� �������
SX::initStatus('user');                                                                  // �������������� �������
$Core          = SX::object('Core');
$Core->getSection();                                                                     // �������� � ��������� � $_REQUEST['area'] � $_SESSION['area'] ������ ������
$Core->section();                                                                        // ���������� ������������ ������ ��� ���������� ��������� ������
$Core->modules();                                                                        // ������������� ��� �������� ������
$Core->aktiveLangs();                                                                    // �������� ������ �������� ������
$Core->selectLangs();                                                                    // ������������� ������ �� �����
$Core->getLangcode();                                                                    // ������������� ��� ����� � $_SESSION['Langcode']
$lang_settings = $Core->langSettings();                                                  // �������� ��������� �������� �����
SX::getLocale($lang_settings['Sprachcode']);                                             // ������������� ������ PHP
$Core->selectTemplate();                                                                 // ������������� ������ � ������� � ������ �������� ������� �������
SX::setDefine('THEME', STATUS_DIR . '/theme/' . SX::get('options.theme'));               // ������������� ���� � �������
SX::loadLang(LANG_DIR . '/' . $_SESSION['lang'] . '/main.txt');                          // ��������� ������ �� ��������� ���� �����
SX::object('Login')->authorize();                                                        // ��������� �����������
$settings      = SX::get('system');                                                      // �������� ���������� ��������� �������
$user_group    = $_SESSION['user_group'];
function RnBr($text) {
    $text = str_replace("\r\n", '<br />', $text);
    return str_replace(array('<br /><br /><br /><br /><br />', '<br /><br /><br /><br />', '<br /><br /><br />', '<br /><br />'), '<br />', $text);
}

function yarss($text) {
    $text = str_replace(array('&amp;', '&nbsp;', '&lt;', '&gt;', '&quot;', '&euro;', '&raquo;', '&laquo;', '&copy;', '&reg;', '&trade;', '&bdquo;', '&ldquo;'), array('&', ' ', '<', '>', '"', '�', '�', '�', '�', '�', '�', '�', '�'), $text);
    $text = str_replace(array('&', '<', '>', '"'), array('&amp;', '&lt;', '&gt;', '&quot;'), $text);
    return $text;
}

function Revrite($out) {
    $out = preg_replace('/index.php([?])p=showtopic&amp;toid=([\d]*)&amp;fid=([\d]*)&amp;page=([\d]*)&amp;t=([\w-]*)/i', 'topic' . '/\\2/\\3/\\4/\\5/', $out);
    $out = preg_replace('/index.php([?])p=showtopic&amp;print_post=([\d]*)&amp;toid=([\d]*)&amp;t=([\w-]*)/i', 'postprint' . '/\\2/\\3/\\4/', $out);
    $out = preg_replace('/index.php([?])p=user&amp;id=([\d]*)&amp;area=([\d]*)/i', 'userprofile' . '/\\2/\\3/', $out);
    $out = preg_replace('/index.php([?])p=showforum&amp;fid=([\d]*)&amp;t=([\w-]*)/i', 'forum' . '/\\2/\\3/', $out);
    return $out;
}

function getForum() {
    return DB::get()->query("SELECT
	  c.id AS cid,
	  c.title AS ctitle,
	  c.group_id AS cgroup,
	  f.id AS fid,
	  f.title AS ftitle,
	  f.group_id AS fgroup
	FROM
	  " . PREFIX . "_f_category AS c,
	  " . PREFIX . "_f_forum AS f
	WHERE
	  f.active = '1'
	AND
	  f.password = ''
	AND
	  f.category_id = c.id
	ORDER BY c.position ASC");
}

$forum = intval(Arr::getGet('forum'));
$topic = intval(Arr::getGet('topic'));
$post  = intval(Arr::getGet('post'));
$mode  = Arr::getGet('mode');
$link  = 'http://' . $_SERVER['HTTP_HOST'] . str_replace('/yarss.php', '', $_SERVER['PHP_SELF']);
$limit = '40';
$out   = '';

if ($mode == 'opml') {
    header('Content-Type: text/xml');
    $out .= "<?xml version=\"1.0\" encoding=\"" . SX::$lang['Charset'] . "\"\r\n";
    $out .= "<opml version=\"1.0\">\r\n";
    $out .= "<head>\r\n";
    $out .= "<title>Ya.rss opml file</title>\r\n";
    $out .= "</head>\r\n";
    $out .= "<body>\r\n";
    $sql = getForum();
    while ($row = $sql->fetch_object()) {
        if (in_array($user_group, explode(',', $row->cgroup)) && in_array($user_group, explode(',', $row->fgroup))) {
            $row->title = $row->ctitle . ' :: ' . $row->ftitle;
            $out .= "<outline text='" . $row->title . "' type='rss' htmlUrl='" . $link . "/index.php?p=showforum&amp;fid=" . $row->fid . "&amp;t=" . translit($row->ftitle) . "' xmlUrl='" . $link . "/yarss.php?forum=" . $row->fid . "' />\r\n";
        }
    }
    $out .= "</body>\r\n";
    $out .= "</opml>\r\n";
    if ($settings['use_seo'] == 1) {
        $out = Revrite($out);
    }
    SX::output($out, true);
}

if ($forum == 0) {
    $out .= "<html>\r\n";
    $out .= "<body>\r\n";
    $out .= "<strong>������ ����������� � ���������� ������� � RSS:</strong> (<a href=\"/yarss.php?mode=opml\">opml</a>)<p>\r\n";
    $sql = getForum();
    while ($row = $sql->fetch_object()) {
        if (in_array($user_group, explode(',', $row->cgroup)) && in_array($user_group, explode(',', $row->fgroup))) {
            $row->title = $row->ctitle . ' :: ' . $row->ftitle;
            $num        = DB::get()->cache_fetch_object("SELECT COUNT(id) AS num, SUM(replies) AS repl FROM " . PREFIX . "_f_topic WHERE forum_id = '" . $row->fid . "'");
            $num->repl  = (!empty($num->repl)) ? $num->repl : 0;
            $out .= '<a href="' . $link . '/yarss.php?forum=' . $row->fid . '">' . $row->title . '</a> <small title="������/��������">(' . $num->num . '/' . $num->repl . ')</small><br />' . "\r\n";
        }
    }
    $out .= "</body>\r\n";
    $out .= "</html>\r\n";
    SX::output($out, true);
}

$xout      = $where     = '';
$now_datum = 0;
$order     = "t.id DESC, p.id DESC";

if (empty($mode)) {
    $simbol = "=";
} else {
    $simbol = "<>";
    $order  = "p.id DESC";
}
if ($topic != 0 && empty($mode)) {
    $where = "AND t.id < '$topic'";
    $order = "t.id DESC";
}
if ($post != 0 && $mode == 'comments') {
    $where = "AND p.id < '$post'";
    $order = "p.id DESC";
}
$class_post = SX::object('Post');
$query      = "SELECT
	p.id AS pid,
	p.topic_id,
	p.datum,
	p.message,
	p.title AS post_title,
	p.uid AS puid,
	t.id AS tid,
	t.title AS topic_title,
	f.title AS ftitle,
	f.group_id AS fgroup
FROM
	" . PREFIX . "_f_post AS p,
	" . PREFIX . "_f_topic AS t ,
	" . PREFIX . "_f_forum AS f
WHERE
	first_post_id " . $simbol . " p.id
AND
	p.topic_id = t.id
AND
	f.id = t.forum_id
AND
	f.active = '1'
AND
	f.password = ''
AND
	t.forum_id = '" . $forum . "' " . $where . "
ORDER by $order LIMIT $limit";

$result = DB::get()->query($query);
while ($row    = $result->fetch_object()) {
    if (in_array($user_group, explode(',', $row->fgroup))) {
        $title      = $row->ftitle;
        $page_post  = $row->pid;
        $page_topic = $row->tid;
        $row->datum = strtotime($row->datum);
        if ($now_datum < $row->datum) {
            $now_datum = $row->datum;
        }
        $row->message    = RnBr($row->message);
        $row->message    = preg_replace('/[\x00-\x08\x0B-\x0C\x0E-\x19]/', '', $row->message);
        $row->post_title = (!empty($row->post_title)) ? $row->post_title : Tool::chars(strip_tags($row->message), 60);
        $row->post_title = $class_post->clean($row->post_title);
        $xout .= "<item>\r\n";
        if (empty($mode)) {
            $xout .= "<title>" . sanitize($row->topic_title) . "</title>\r\n";
        } else {
            $xout .= "<title>" . sanitize($row->post_title) . "</title>\r\n";
        }
        if ($mode != 'comments') {
            $xout .= "<link>" . $link . "/index.php?p=showtopic&amp;toid=" . $row->tid . "&amp;fid=" . $forum . "&amp;page=1&amp;t=" . translit($row->topic_title) . "</link>\r\n";
        } else {
            $xout .= "<link>" . $link . "/index.php?p=showtopic&amp;print_post=" . $row->pid . "&amp;toid=" . $row->tid . "&amp;t=" . translit($row->post_title) . "</link>\r\n";
        }
        if ($mode == 'comments') {
            $xout .= "<ya:post>" . $link . "/index.php?p=showtopic&amp;toid=" . $row->tid . "&amp;fid=" . $forum . "&amp;page=1&amp;t=" . translit($row->topic_title) . "</ya:post>\r\n";
        }
        $xout .= "<guid isPermaLink='true'>" . $link . "/index.php?p=showtopic&amp;print_post=" . $row->pid . "&amp;toid=" . $row->tid . "&amp;t=" . translit($row->post_title) . "</guid>\r\n";
        $xout .= "<description>" . yarss($class_post->smilies($class_post->bbcode($row->message, '', 1))) . "</description>\r\n";
        $xout .= "<pubDate>" . (date('r', $row->datum)) . "</pubDate>\r\n";
        $xout .= "<author>" . $link . "/index.php?p=user&amp;id=" . $row->puid . "&amp;area=" . AREA . "</author>\r\n";
        $xout .= "</item>" . "\r\n";
    }
}

$out .= "<?xml version=\"1.0\" encoding=\"" . SX::$lang['Charset'] . "\"\r\n";
$out .= "<rss version=\"2.0\" xmlns:ya=\"http://blogs.yandex.ru/\" xmlns:wfw=\"http://wellformedweb.org/CommentAPI/\">\r\n";
$out .= "<channel>\r\n";
$out .= "<title>" . sanitize((!empty($title) ? $title . " :: " : "") . $settings['Seitenname']) . "</title>\r\n";
$out .= "<link>" . $link . "</link>\r\n";
$out .= "<description>" . sanitize($settings['Seitenname']) . "</description>\r\n";
$out .= "<language>" . $_SESSION['lang'] . "</language>\r\n";
$out .= "<managingEditor>" . $settings['Mail_Absender'] . "</managingEditor>\r\n";
$out .= "<generator>CMS Status-X</generator>\r\n";
$out .= $xout;

if ((!empty($page_post) && $post != $page_post) && !empty($page_topic)) {
    if ($mode != 'comments') {
        $out .= "<ya:more>" . $link . "/yarss.php?forum=" . $forum . "&amp;topic=" . $page_topic . "</ya:more>\r\n";
    } else {
        $out .= "<ya:more>" . $link . "/yarss.php?forum=" . $forum . "&amp;post=" . $page_post . "&amp;mode=" . $mode . "</ya:more>\r\n";
    }
}

if ($mode != 'comments') {
    $out .= "<wfw:commentRss>" . $link . "/yarss.php?forum=" . $forum . "&amp;mode=comments</wfw:commentRss>\r\n";
}
$out .= "</channel>\r\n";
$out .= "</rss>\r\n";
if ($settings['use_seo'] == 1) {
    $out = Revrite($out);
}
header('Content-Type: text/xml');
SX::output($out, true, time(), $now_datum);
