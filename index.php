<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
error_reporting(0);                                                           // ����� ������ php: 0 - ���������, E_ALL - ��������
define('STATUS_DIR', realpath(dirname(__FILE__)));                            // ������������� ������ ���� �� �������
require_once STATUS_DIR . '/class/class.SX.php';                              // ���������� �������� ����� �������
SX::initStatus('user');                                                       // �������������� �������                                                                 // �������� �����������
$Core = SX::object('Core');
$Core->ssl();                                                                 // ������������� �������� ����������
$Core->getSection();                                                          // �������� � ��������� � $_REQUEST['area'] � $_SESSION['area'] ������ ������
$Core->section();                                                             // ���������� ������������ ������ ��� ���������� ��������� ������
$Core->modules();                                                             // ������������� ��� �������� ������
$Core->aktiveLangs();                                                         // �������� ������ �������� ������
$Core->selectLangs();                                                         // ������������� ������ �� �����
$Core->getLangcode();                                                         // ������������� ��� ����� � $_SESSION['Langcode']
$lang_settings = $Core->langSettings();                                       // �������� ��������� �������� �����
SX::getLocale($lang_settings['Sprachcode']);                                  // ������������� ������ PHP
$Core->compileDir($_SESSION['area']);                                         // ��������� �� ������ ����� ����������
$Core->selectTemplate();                                                      // ������������� ������ � ������� � ������ �������� ������� �������
SX::setDefine('THEME', STATUS_DIR . '/theme/' . SX::get('options.theme'));    // ������������� ���� � �������
SX::loadLang(LANG_DIR . '/' . $_SESSION['lang'] . '/main.txt');               // ��������� ������ �� ��������� ���� �����
SX::loadLang(LANG_DIR . '/' . $_SESSION['lang'] . '/mail.txt');               // ��������� ������ �� ���� ����� ������� �����
$_SESSION['Charset'] = SX::$lang['Charset'];                                  // ������������� ������ � ������� ����������
header('Content-type: text/html; charset=' . $_SESSION['Charset']);           // ������������� ��������� � ������� ����������
$Core->getModules($lang_settings);                                            // ����������� �������
$Core->extensions();                                                          // ����������� ������� �������

$content = NULL;
if (!defined('AJAX_OUTPUT')) {
    $tpl = 'forums.tpl';
    if ($_SESSION['banned'] != 1) {
        if (Arr::getRequest('blanc') == 1) {
            $tpl = 'popup.tpl';
        } else {
            $tpl = SX::get('options.is_print') ? 'print.tpl' : View::get()->template($_REQUEST['p']);
        }
    }
    $content = View::get()->fetch(THEME . '/page/' . $tpl);
    $content = $Core->outReplace($content);
    $content = SX::object('CodeWidget')->get($content);
    if (SX::get('system.use_seo') == 1) {
        $content = SX::object('Rewrite')->get($content);
    }
    if (get_active('highlighter') && in_array($_REQUEST['p'], array('articles', 'news', 'content', 'shop'))) {
        $content = SX::object('Highlight')->get($content);
    }
    $content = $Core->ignoreRewrite($content);
}
SX::output($content, true);
