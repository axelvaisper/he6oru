<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (!defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}

class Redir {

    public function __construct() {
        if (!defined('BASE_PATH')) {
            define('BASE_PATH', SX::basePatch());
        }
    }

    public function location($link, $code = 301) {
        SX::object('Response')->get($code);
        header('Location: ' . $link);
        exit;
    }

    protected function index($link) {
        if (empty($link) || $link == 'index.php') {
            $this->location(BASE_PATH);
        }
    }

    protected function normalize($link) {
        return str_replace('&amp;', '&', $link);
    }

    /* ����� ��������� � ��������� */
    public function seoRedirect($link, $code = 301) {
        $domain = '';
        $this->index($link);
        $link = $this->normalize($link);
        if (SX::get('system.use_seo') == 1) {
            $domain = strpos($_SERVER['SCRIPT_NAME'], 'index.php') ? str_replace('/index.php', '', $_SERVER['SCRIPT_NAME']) : '';
            $url = $_SERVER['HTTP_HOST'] . BASE_PATH;
            $link = $this->normalize(SX::object('Rewrite')->get(str_replace('&', '&amp;', $link)));
            $link = $domain . '/' . str_replace(array('http://' . $url, 'https://' . $url), '', $link);
        }
        $link = str_replace(array($domain . '/http://', $domain . '/https://'), array('http://', 'https://'), $link);
        $this->location($link, $code);
    }

    /* ����� ��������� ��� �������� */
    public function redirect($link, $code = 301) {
        $this->index($link);
        $link = $this->normalize($link);
        if (stripos($link, '://') === false) {
            $link = BASE_PATH . $link;
        }
        $this->location($link, $code);
    }

    /* ����� ��������� ������ ������� �������� */
    public function link() {
        static $cache = NULL;
        if ($cache === NULL) {
            $uri = $_SERVER['PHP_SELF'];
            if (!empty($_GET)) {
                $params = array();
                foreach ($_GET as $key => $value) {
                    $params[] = urlencode($key) . '=' . urlencode($value);
                }
                $uri .= '?' . implode('&amp;', $params);
            }
            $cache = SX::protocol() . $_SERVER['HTTP_HOST'] . $uri;
        }
        return $cache;
    }

    public function printLink() {
        $link  = $this->link();
        $array = explode('?', $link);
        $link .= !empty($array[1]) ? '&amp;print=1' : '?print=1';
        return $link;
    }

    public function noprintLink() {
        return str_replace(array('&print=1', '&amp;print=1', '?print=1', 'print.html'), '', $this->link());
    }

    public function referer($link = false) {
        static $cache = array();
        if (empty($cache)) {
            $value = Arr::getServer('HTTP_REFERER');
            if (stripos($value, BASE_URL) !== false) {
                $array['bool'] = true;
                $array['link'] = $value;
            } else {
                $array['bool'] = false;
                $array['link'] = BASE_URL;
            }
        }
        return $link === true ? $array['link'] : $array['bool'];
    }

}
