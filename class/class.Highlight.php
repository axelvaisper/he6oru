<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (!defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}

class Highlight {

    public function get($text) {
        if (preg_match('#\[sx_code lang=(php|html|js|css|mysql|java|delphi)\](.*?)\[/sx_code\]#si', $text)) {
            $jscode = '<script type="text/javascript" src="' . JS_PATH . '/chili/jquery.chili-2.2.js></script>
		       <script type="text/javascript" src="' . JS_PATH . '/chili/recipes.js></script>
		       <script type="text/javascript"> ChiliBook.automatic = true; ChiliBook.lineNumbers = true; </script>';
            $text = str_replace("</head>", "\n" . $jscode . "\n</head>", $text);

            $agent = Tool::browser();
            if ($agent == 'IE9' || $agent == 'IE8' || $agent == 'IE7' || $agent == 'IE6') {
                $text = preg_replace('#\[sx_code lang=(.*?)\](.*?)\[/sx_code\]#si', "<code class=\"\\1\" style=\"\"><pre>\\2</pre></code>", $text);
            } else {
                $text = preg_replace('#\[sx_code lang=(.*?)\](.*?)\[/sx_code\]#si', "<code class=\"\\1\" style=\"\">\\2</code>", $text);
            }
        }
        return $text;
    }

}
