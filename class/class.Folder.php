<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (!defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}

abstract class Folder {

    /* �������� ����� ��������� � ���� � �������� ����� */
    public static function fullpath($dirs, $chmod) {
        $item = '';
        $dirs = explode('/', $dirs);
        foreach ($dirs as $dir) {
            $item .= $dir . '/';
            self::create($item, $chmod);
        }
    }

    /* �������� ����� */
    public static function create($folder, $chmod = 0777) {
        if (!is_dir($folder)) {
            if (!mkdir($folder)) {
                SX::setLog('��������� ������! �� ������� ������� ���������� ' . $folder, '5', $_SESSION['benutzer_id']);
                return false;
            } else {
                chmod($folder, $chmod);
            }
        }
        return true;
    }

    /* �������� ����� */
    public static function delete($folder) {
        if (!is_dir($folder)) {
            chmod($folder, 0777);
            if (!mkdir($folder)) {
                SX::setLog('��������� ������! �� ������� ������� ���������� ' . $folder, '5', $_SESSION['benutzer_id']);
                return false;
            }
        }
    }

    /* �������� ���������� ����� �� ����������� ������� ������ */
    public static function protection($file, $chmod = 0777) {
        self::create($file, $chmod);
        File::set($file . '.htaccess', 'deny from all');
        File::set($file . 'index.php', "<?php\r\nheader('Location: /index.php?p=banned&action=bann');\r\nexit;");
    }

    /* ����� �������� ����������� ����� */
    public static function clean($folder) {
        if (!empty($folder)) {
            set_time_limit(60);
            $handle = opendir($folder);
            while (false !== ($datei = readdir($handle))) {
                if (!in_array($datei, array('.', '..', '.htaccess', 'index.php'))) {
                    File::delete($folder . $datei);
                }
            }
            closedir($handle);
        }
    }

}