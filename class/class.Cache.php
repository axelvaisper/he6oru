<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (!defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}

class Cache {

    protected $_dir;
    protected $_ext = '.txt'; // ���������� ������ � �����
    protected $_life = 86400; // ����� �������� �� ���������

    /* ����� ����������� ������ */
    public function __construct() {
        $this->_dir = TEMP_DIR . '/private/';
    }

    /* ����� ��������� ���� �� ����� */
    public function get($key) {
        $file = $this->file($key);
        if (is_file($file) && filemtime($file) > time()) {
            return unserialize(file_get_contents($file));
        }
        return false;
    }

    /* ����� ���������� ���� �� ����� */
    public function set($key, $value, $life = 0) {
        $file = $this->file($key);
        if (file_put_contents($file, serialize($value), LOCK_EX) !== false) {
            chmod($file, 0777);
            return touch($file, $this->life($life, true));
        }
        return false;
    }

    /* ����� �������� ���� �� ����� */
    public function del($key) {
        $file = $this->file($key);
        return is_file($file) ? unlink($file) : false;
    }

    /* ����� �������� ����� ���� */
    public function clear() {
        $files = glob($this->_dir . '*' . $this->_ext);
        foreach ($files as &$file) {
            unlink($file);
        }
        return true;
    }

    /* ����� ��������� ����� ����� */
    protected function file($key) {
        return $this->_dir . md5($key) . $this->_ext;
    }

    /* ����� ��������� ����� �������� ���� */
    protected function life($life = 0, $time = false) {
        if ($life <= 0) {
            $life = $this->_life;
        }
        return $time === true ? $life + time() : $life;
    }

}