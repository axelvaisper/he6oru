<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (!defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}

class Session {

    protected $_db;

    public function __construct() {
        $this->_db = DB::get();
        session_set_save_handler(
            array($this, 'open'), array($this, 'close'), array($this, 'read'), array($this, 'write'), array($this, 'destroy'), array($this, 'gc')
        );
    }

    /* ������������� ������, ����������� � ���� */
    public function open($path, $name) {
        return true;
    }

    public function close() {
        return true;
    }

    /* ������ ������ ������ */
    public function read($key) {
        $sql = $this->_db->query("SELECT Wert FROM " . PREFIX . "_sessions WHERE Schluessel = '" . $this->_db->escape($key) . "' AND Ip = '" . IP_USER . "' AND Ablauf > " . time());
        if ((list($session) = $sql->fetch_row())) {
            return $session;
        }
        $this->_db->query("DELETE FROM " . PREFIX . "_sessions WHERE Ablauf < " . time());
        return false;
    }

    /* ���������� ��� ��������� ������ */
    public function write($key, $val) {
        $expire = time() + SX::get('database.dbsesslife');
        $val = $this->_db->escape($val);
        $sql = $this->_db->query("INSERT INTO " . PREFIX . "_sessions (
                Schluessel,
                Ablauf,
                Wert,
                Ip
        ) VALUES (
                '" . $this->_db->escape($key) . "',
                '" . $expire . "',
                '" . $val . "',
                '" . IP_USER . "'
        ) ON DUPLICATE KEY UPDATE
                Ablauf = '" . $expire . "',
                Wert = '" . $val . "',
                Ip = '" . IP_USER . "'");
        return !$sql ? false : true;
    }

    /* ���������� ������ */
    public function destroy($key) {
        $sql = $this->_db->query("DELETE FROM " . PREFIX . "_sessions WHERE Schluessel = '" . $this->_db->escape($key) . "'");
        return !$sql ? false : true;
    }

    /* ������ �������� ������ */
    public function gc() {
        $this->_db->query("DELETE FROM " . PREFIX . "_sessions WHERE Ablauf < " . time());
        return true;
    }

}