<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (!defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}

abstract class Magic {

    protected static $__storage_objects = array();

    protected function __object($key) {
        if (!isset(self::$__storage_objects[$key])) {
            SX::object($key);
        }
        return self::$__storage_objects[$key];
    }

    public function __get($key) {
        static $objects = array('_view' => false, '_db' => false, '_lang' => false);
        if (isset($objects[$key])) {
            if ($objects[$key] === false) {
                switch ($key) {
                    case '_view': $objects[$key] = View::get(); break;
                    case '_db' : $objects[$key] = DB::get(); break;
                    case '_lang': $objects[$key] = SX::$lang; break;
                }
            }
            return $objects[$key];
        }
        return SX::get('classes.' . $key);
    }

    public function __set($key, $value) {
        SX::set('classes.' . $key, $value);
    }

    public function __isset($key) {
        return SX::has('classes.' . $key);
    }

    public function __unset($key) {
        SX::del('classes.' . $key);
    }

    public function __call($method, $args) {
        trigger_error('������ �������������� ����� ' . $method . ' � ������ ' . get_class($this));
    }

}

abstract class SX extends Magic {

    public static $lang = array();
    protected static $_mail = array();
    protected static $_param = array();

    /* ����� ������������� ������� */
    public static function initStatus($type) {
        spl_autoload_register(array('SX', $type . 'Autoload'));   // ��������� ������������� �������
        self::unsetGlobals();                                     // ������� ���������� �������
        $configs = self::getConfig('sys.config');                 // ���������� ���� � ����������� �������
        self::set('configs', $configs);
        self::cleanArray();                                       // ��������� ���������� ���������� ��������
        self::stripslashesArray();                                // ��������� ������ �������� stripslashes ���������� ��������
        self::checkDomain();                                      // �������� �� ������� ����������� �������� � $_SERVER['HTTP_HOST']
        self::systemDefines();                                    // ������������� �������� ��������� �������
        if ($configs['site']['aktiv'] == '0') {                   // ���������� ����� ��� �������������
            self::object('Repair');
        }
        $database = self::getConfig('db.config');                 // ���������� ���� � ����������� ���� ������
        $database['load'] = false;
        self::set('database', $database);
        self::checkInstall($database);                            // ��������� ����������� �� �������
        self::setDefine('PREFIX', $database['dbprefix']);         // ������������� ��������� � ��������� ���� ������
        self::load();                                             // ��������� ��������� �������
        self::setTimeZone();                                      // ������������� ��������� ���� �� ���������
        require STATUS_DIR . '/lib/functions.php';                // ���������� �������
        self::getDebug();                                         // ����������� ������
        self::startSession($database['type_sess']);               // �������������� � �������� ������
        self::setDefaults();                                      // ������������� ������������ ��������� ��� ����������
    }

    /* ����� ��������� �������� �������� */
    public static function systemDefines() {
        self::setDefine('STATUS', 'Status-X 1.05.011');               // ������ �������
        self::setDefine('PE', PHP_EOL);                           // ������������� ��������� c ��������� �����
        self::setDefine('IP_USER', self::getIp());                // ������������� ��������� � IP ������������
        self::setDefine('BASE_PATH', self::basePatch());          // ������������� ��������� �����, ��������� ���������� ���� � �������� ��� ���
        self::setDefine('BASE_URL', self::baseUrl());             // ������������� ��������� ������ ����� �������� ����������� �����
        self::setDefine('UPLOADS_DIR', STATUS_DIR . '/uploads');  // ������������� ������ ���� �� ����� uploads
        self::setDefine('LANG_DIR', STATUS_DIR . '/lang');        // ������������� ������ ���� �� ����� lang
        self::setDefine('TEMP_DIR', STATUS_DIR . '/temp');        // ������������� ������ ���� �� ����� temp
        self::setDefine('MODUL_DIR', STATUS_DIR . '/modules');    // ������������� ������ ���� �� ����� � ��������
        self::setDefine('WIDGET_DIR', STATUS_DIR . '/widgets');   // ������������� ������ ���� �� ����� � ���������
        self::setDefine('JS_PATH', BASE_PATH . 'js');             // ������������� ������ ���� �� ����� � js
        self::setDefine('HTTP', self::httpProtocol());            // ������������� �������� �������
    }

    /* ����� ������������� ��������� ��� ������������ ���������� */
    public static function setDefaults() {
        $_REQUEST['p'] = Arr::getRequest('p', 'index');
        $_REQUEST['t'] = Arr::getRequest('t', '-');
        $_SESSION['loggedin'] = Arr::getSession('loggedin', 0);
        $_SESSION['user_group'] = Arr::getSession('user_group', 2);
        $_SESSION['benutzer_id'] = Arr::getSession('benutzer_id', 0);
        $_SESSION['Charset'] = Arr::getSession('Charset', 'windows-1251');
    }

    /* ����� ������������� ��������� ���� */
    public static function setTimeZone() {
        $timezone = self::get('system.timezone');
        date_default_timezone_set(!empty($timezone) ? $timezone : 'Europe/Moscow');
    }

    /* �������������� ��������� ������� */
    public static function httpProtocol() {
        return isset($_SERVER['SERVER_PROTOCOL']) && $_SERVER['SERVER_PROTOCOL'] == 'HTTP/1.1' ? 'HTTP/1.1' : 'HTTP/1.0';
    }

    /* ����� �������� ���� ����������� ����� � �������� */
    public static function basePatch() {
        return str_replace('//', '/', str_replace('/index.php', '/', $_SERVER['PHP_SELF']));
    }

    /* ����� �������� ����� ����� �������� ����������� ����� */
    public static function baseUrl() {
        $patch = str_replace(array('/admin/index.php', '/index.php'), '', $_SERVER['PHP_SELF']);
        return self::protocol() . $_SERVER['HTTP_HOST'] . $patch;
    }

    /* ����� ����������� ������ ����� */
    public static function getConfig($name) {
        $config = array();
        include (STATUS_DIR . '/config/' . $name . '.php');
        return $config;
    }

    /* ����� �������� ������� */
    public static function object($key, $params = NULL, $new = false) {
        if (!isset(self::$__storage_objects[$key]) || $new === true) {
            self::$__storage_objects[$key] = new $key($params);
        }
        return self::$__storage_objects[$key];
    }

    /* ����� �������� �������� ���������� */
    public static function loadLang($file) {
        $object = View::get();
        $object->configLoad($file);
        self::$lang = $object->getConfigVars();
        $object->assign('lang', self::$lang);
    }

    /* ����� ������ ���� �������� ������ */
    public static function startSession($type = 'base') {
        if ($type == 'file') {
            ini_set('session.save_handler', 'files');
        } else {
            ini_set('session.save_handler', 'user');
            if (ini_get('session.save_handler') == 'user') {
                new Session;
            }
        }
        session_set_cookie_params(0, BASE_PATH);
        session_name('STATUS');
        session_start();
    }

    /* ����� ��������� ������ PHP */
    public static function getLocale($locale = 'ru') {
        if ($locale == 'ru') {
            setlocale(LC_ALL, 'ru_RU.CP1251', 'ru_RU.cp1251', 'rus_RUS.CP1251', 'Russian_Russia.1251', 'ru_RU', 'ru', 'russian');
        } else {
            setlocale(LC_ALL, $locale . '_' . strtoupper($locale), $locale);
        }
    }

    /* ����� ��������� ���������� */
    public static function get($key, $default = NULL) {
        list($a, $count) = self::parse($key);
        switch ($count) {
            case 1: return isset(self::$_param[$a[0]]) ? self::$_param[$a[0]] : $default;
            case 2: return isset(self::$_param[$a[0]][$a[1]]) ? self::$_param[$a[0]][$a[1]] : $default;
            case 3: return isset(self::$_param[$a[0]][$a[1]][$a[2]]) ? self::$_param[$a[0]][$a[1]][$a[2]] : $default;
            case 4: return isset(self::$_param[$a[0]][$a[1]][$a[2]][$a[3]]) ? self::$_param[$a[0]][$a[1]][$a[2]][$a[3]] : $default;
            case 5: return isset(self::$_param[$a[0]][$a[1]][$a[2]][$a[3]][$a[4]]) ? self::$_param[$a[0]][$a[1]][$a[2]][$a[3]][$a[4]] : $default;
            case 6: return isset(self::$_param[$a[0]][$a[1]][$a[2]][$a[3]][$a[4]][$a[5]]) ? self::$_param[$a[0]][$a[1]][$a[2]][$a[3]][$a[4]][$a[5]] : $default;
        }
        return $default;
    }

    /* ����� ���������� ���������� */
    public static function set($key, $value = NULL) {
        list($a, $count) = self::parse($key);
        switch ($count) {
            case 1: self::$_param[$a[0]] = $value; break;
            case 2: self::$_param[$a[0]][$a[1]] = $value; break;
            case 3: self::$_param[$a[0]][$a[1]][$a[2]] = $value; break;
            case 4: self::$_param[$a[0]][$a[1]][$a[2]][$a[3]] = $value; break;
            case 5: self::$_param[$a[0]][$a[1]][$a[2]][$a[3]][$a[4]] = $value; break;
            case 6: self::$_param[$a[0]][$a[1]][$a[2]][$a[3]][$a[4]][$a[5]] = $value; break;
        }
    }

    /* ����� �������� ������������� ��������� */
    public static function has($key) {
        list($a, $count) = self::parse($key);
        switch ($count) {
            case 1: return isset(self::$_param[$a[0]]);
            case 2: return isset(self::$_param[$a[0]][$a[1]]);
            case 3: return isset(self::$_param[$a[0]][$a[1]][$a[2]]);
            case 4: return isset(self::$_param[$a[0]][$a[1]][$a[2]][$a[3]]);
            case 5: return isset(self::$_param[$a[0]][$a[1]][$a[2]][$a[3]][$a[4]]);
            case 6: return isset(self::$_param[$a[0]][$a[1]][$a[2]][$a[3]][$a[4]][$a[5]]);
        }
        return false;
    }

    /* ����� ��������� ���������� */
    public static function del($key) {
        list($a, $count) = self::parse($key);
        switch ($count) {
            case 1: unset(self::$_param[$a[0]]); break;
            case 2: unset(self::$_param[$a[0]][$a[1]]); break;
            case 3: unset(self::$_param[$a[0]][$a[1]][$a[2]]); break;
            case 4: unset(self::$_param[$a[0]][$a[1]][$a[2]][$a[3]]); break;
            case 5: unset(self::$_param[$a[0]][$a[1]][$a[2]][$a[3]][$a[4]]); break;
            case 6: unset(self::$_param[$a[0]][$a[1]][$a[2]][$a[3]][$a[4]][$a[5]]); break;
        }
    }

    /* ����� ����������� ���������� ������� ������ � ����������� */
    public static function parse($key) {
        static $cache = array();
        if (!isset($cache[$key])) {
            $array = explode('.', $key);
            $cache[$key] = array($array, count($array));
        }
        return $cache[$key];
    }

    /* ����� ���������� �������� � ������ */
    public static function load($array = NULL) {
        $where = '';
        if (!empty($array)) {
            $array = is_array($array) ? $array : array($array);
            foreach ($array as $value) {
                $where[] = "'" . DB::get()->escape(trim($value)) . "'";
            }
            $where = " WHERE `Modul` IN(" . implode(',', $where) . ")";
        }
        $sql = DB::get()->query("SELECT SQL_CACHE `Modul`, `Name`, `Type`, `Value` FROM `" . PREFIX . "_settings`" . $where);
        while ($row = $sql->fetch_assoc()) {
            self::$_param[$row['Modul']][$row['Name']] = ($row['Type'] == 'int') ? (0 + $row['Value']) : $row['Value'];
        }
        $sql->close();
    }

    /* ����� ���������� - ���������� �������� */
    public static function save($table, $array = array()) {
        if (!empty($array)) {
            $DB = DB::get();
            foreach ($array as $name => $value) {
                $name = $DB->escape(trim($name));
                $table = $DB->escape(trim($table));
                $value = $DB->escape(trim($value));
                $type = is_numeric($value) ? 'int' : 'string';
                $id = strtolower($table . '_' . $name);
                $DB->query("INSERT INTO `" . PREFIX . "_settings` (
                        `Id`,
                        `Modul`,
                        `Name`,
                        `Type`,
                        `Value`
                ) VALUES (
                        '" . $id . "',
                        '" . $table . "',
                        '" . $name . "',
                        '" . $type . "',
                        '" . $value . "'
                ) ON DUPLICATE KEY UPDATE
                        `Modul` = '" . $table . "',
                        `Name` = '" . $name . "',
                        `Value` = '" . $value . "'");
            }
        }
    }

    /* ����� ������������ ������� */
    public static function userAutoload($class) {
        if (strncasecmp($class, 'smarty_', 7) !== 0) {
            include STATUS_DIR . '/class/class.' . $class . '.php';
        }
        return class_exists($class, false);
    }

    /* ����������������� ������� ������� */
    public static function adminAutoload($class) {
        if (strncasecmp($class, 'smarty_', 7) !== 0) {
            $patch = is_file(STATUS_DIR . '/admin/class/class.' . $class . '.php') ? 'admin/' : '';
            include STATUS_DIR . '/' . $patch . 'class/class.' . $class . '.php';
        }
        return class_exists($class, false);
    }

    /* ����� ������ ��������� ��������� � ���� */
    public static function setLog($action, $type = 1, $benutzer = 0) {
        self::fileLogs($action, $type);
        self::errorBrouser($action, $type);
        if (self::get('database.load') === true && self::get('system.Logging') == 1) {
            $insert_array = array(
                'Datum'    => time(),
                'Benutzer' => $benutzer,
                'Aktion'   => $action,
                'Typ'      => $type,
                'Ip'       => IP_USER,
                'Agent'    => Arr::getServer('HTTP_USER_AGENT'));
            DB::get()->insert_query('log', $insert_array);
        }
    }

    /* ����� ������ � ������ */
    protected static function fileLogs($action, $type) {
        $array = array(
            '0' => 'admin',
            '1' => 'adminshop',
            '2' => 'payment',
            '3' => 'sys',
            '4' => 'mysql',
            '5' => 'erphp',
            '6' => 'seite');
        switch (self::get('configs.loger')) {
            case '0': return;
            case '1':
                $file = isset($array[$type]) ? $array[$type] : 'other';
                error_log($action . PE . str_repeat('-', 80) . PE, 3, STATUS_DIR . '/temp/logs/' . $file . '_errors.log');
                break;
            case '2':
                if ($type == '5') {
                    error_log($action, 0);
                }
                break;
            default:
                if (Tool::isMail(self::get('configs.loger'))) {
                    error_log($action . PE . str_repeat('-', 80) . PE, 1, self::get('configs.loger'));
                }
                break;
        }
    }

    /* ����� ������ ��������� �� ����� */
    public static function errorBrouser($action, $type) {
        if (self::get('configs.debug') == '1' && ($type == '4' || $type == '5')) {
            self::output('<div style="height:140px; padding:8px; width:800px; overflow:auto; background:#ffff00">
			  <pre style="font-size:13px">��������� ���������:<br />' . $action . '</pre></div>');
        }
    }

    /* ����� ��������� ��������� ������ */
    public static function fatalError() {
        $error = error_get_last();
        if ($error && in_array($error['type'], array(E_ERROR, E_PARSE, E_COMPILE_ERROR))) {
            if (stripos($error['message'], 'Allowed memory size') !== false) {
                ini_set('memory_limit', (int) ini_get('memory_limit') + 32 . 'M');
            }
            self::errorHandler($error['type'], $error['message'], $error['file'], $error['line']);
        }
    }

    /* ����� ��������� ������ PHP ����������������� ������ */
    public static function errorHandler($errno, $errmsg, $filename, $linenum) {
        if (!in_array($errno, array(E_NOTICE, E_STRICT, E_WARNING))) {
            self::setLog('������ PHP!' . PE . '������ �: ' . $errno . PE . '���������: ' . $errmsg . PE . '����: ' . $filename . PE . '������: ' . $linenum, 5);
        }
    }

    /* ����� ������ � ��� ���������� php */
    public static function exceptionHandler($exception) {
        self::setLog('��������� PHP!' . PE . '�����: ' . $exception, 5);
    }

    /* ����� ��������� ������ ������ */
    public static function getDebug() {
        register_shutdown_function(array('SX', 'fatalError'));
        set_error_handler(array('SX', 'errorHandler'), E_ALL);
        set_exception_handler(array('SX', 'exceptionHandler'));
    }

    /* ����� �������� ���������� �������� */
    public static function unsetGlobals() {
        if (ini_get('register_globals')) {
            if (isset($_REQUEST['GLOBALS']) || isset($_FILES['GLOBALS'])) {
                self::output('������� ���������� ������������������� ��������! ������ ���������...', true);
            }
            $global = array_keys($GLOBALS);
            $global = array_diff($global, array(
                '_COOKIE',
                '_ENV',
                '_GET',
                '_FILES',
                '_POST',
                '_REQUEST',
                '_SERVER',
                '_SESSION',
                'GLOBALS'));
            foreach ($global as $value) {
                unset($GLOBALS[$value]);
            }
        }
    }

    /* ����� ��������� ���������� ���������� �������� $_POST, $_GET, $_REQUEST, $_COOKIE, $_SERVER */
    public static function cleanArray() {
        if (!empty($_SERVER)) {
            self::queryString();
            array_walk_recursive($_SERVER, array(__CLASS__, 'sanitArray'));
        }
        if (!empty($_POST)) {
            array_walk_recursive($_POST, array(__CLASS__, 'sanitArray'));
        }
        if (!empty($_GET)) {
            array_walk_recursive($_GET, array(__CLASS__, 'sanitArray'));
        }
        if (!empty($_REQUEST)) {
            array_walk_recursive($_REQUEST, array(__CLASS__, 'sanitArray'));
        }
        if (!empty($_COOKIE)) {
            array_walk_recursive($_COOKIE, array(__CLASS__, 'sanitArray'));
        }
        if (!empty($_FILES)) {
            array_walk_recursive($_FILES, array(__CLASS__, 'sanitArray'));
        }
    }

    /* ����� ������� �������� stripslashes ���������� �������� $_POST, $_GET, $_REQUEST, $_COOKIE */
    public static function stripslashesArray() {
        if (function_exists('get_magic_quotes_gpc') && get_magic_quotes_gpc()) {
            $function = create_function('&$value', '$value = stripslashes($value);');
            if (!empty($_POST)) {
                array_walk_recursive($_POST, $function);
            }
            if (!empty($_GET)) {
                array_walk_recursive($_GET, $function);
            }
            if (!empty($_REQUEST)) {
                array_walk_recursive($_REQUEST, $function);
            }
            if (!empty($_COOKIE)) {
                array_walk_recursive($_COOKIE, $function);
            }
        }
    }

    /* ����� �������� �� �������� ���� */
    protected static function queryString() {
        if (!empty($_SERVER['QUERY_STRING'])) {
            $val = $_SERVER['QUERY_STRING'];
            if (preg_match("/'|`|concat|union|c2nyaxb0|cmd=|&cmd|etc\/passwd|proc\/self\/environ|\+Result:\+|%5BPLM|\+ADw-|\+AD4-|http:\/\/|\/\*|\"|%27/i", $val) ||
                    preg_match("/<[^>]*[script|object|iframe|applet|meta|style|form]*\"?[^>]*>/i", $val) ||
                    preg_match("/\([^>]*\"?[^)]*\)/i", $val)) {
                self::object('Redir')->redirect('index.php?p=banned&action=bann');
            }
        }
    }

    /* ����� ������� ���������� */
    protected static function sanitArray(&$value, $key) {
        static $array = array(
            'area'            => array('int'),
            'gid'             => array('int'),
            'categ'           => array('int'),
            'langcode'        => array('int'),
            'galid'           => array('int'),
            'hid'             => array('int'),
            'limit'           => array('int'),
            'pp'              => array('int'),
            'uid'             => array('int'),
            'fid'             => array('int'),
            'pid'             => array('int'),
            'toid'            => array('int'),
            'cid'             => array('int'),
            't_id'            => array('int'),
            'period'          => array('int'),
            'forum_id'        => array('int'),
            'group'           => array('int'),
            'posticon'        => array('int'),
            'img_id'          => array('preg', '/[^\d]/'),
            'p'               => array('preg', '/[^a-z]/i'),
            't'               => array('preg', '/[^\da-z-_]/i'),
            'id'              => array('preg', '/[^\da-z-_]/i'),
            'sort'            => array('preg', '/[^\da-z-_]/i'),
            'unit'            => array('preg', '/[^\da-z-_]/i'),
            'listpn'          => array('preg', '/[^\da-z-_]/i'),
            'action'          => array('preg', '/[^\da-z-_]/i'),
            'do'              => array('preg', '/[^\da-z-_]/i'),
            'sub'             => array('preg', '/[^\da-z-_]/i'),
            'phpsessid'       => array('preg', '/[^\da-z-_]/i'),
            'high'            => array('preg', '/[^\w-. ]/i'),
            'http_referer'    => array('preg', '![^\w-,.:;\/&=?#%+ ]!i'),
            'http_user_agent' => array('preg', '![^\w-,.:;\/&=?#%+ ]!i'),
            'request_uri'     => array('preg', '![^\w-,.:;\/&=?#%+ ]!i'),
            'query_string'    => array('preg', '![^\w-,.:;\/&=?#%+ ]!i'),
            'page'            => array('empty', 1),
            'newsid'          => array('empty', 1),
            'artpage'         => array('empty', 1),
        );

        $key = strtolower($key);
        if (isset($array[$key])) {
            if ($array[$key][0] == 'int') {
                $value = (0 + $value);
            } elseif ($array[$key][0] == 'preg') {
                $value = preg_replace($array[$key][1], '', $value);
            } elseif ($array[$key][0] == 'empty') {
                $value = !empty($value) ? intval($value) : $array[$key][1];
            }
        }
    }

    /* ����� �������� ����������� �� ������� */
    public static function checkInstall($config) {
        if (empty($config['dbhost']) || empty($config['dbname'])) {
            if (is_file(STATUS_DIR . '/setup/setup.php')) {
                header('Expires: ' . gmdate('D, d M Y H:i:s \G\M\T', gmmktime(0, 4, 0, 11, 1, 1974)));
                self::object('Redir')->redirect('setup/setup.php');
            }
            self::output('<li> ���� �� ������������, �������� �����!', true);
        }
    }

    /* ����� ����������� IP ������������ */
    public static function getIp() {
        $addr = $_SERVER['REMOTE_ADDR'];
        $addr = filter_var($addr, FILTER_VALIDATE_IP) !== false ? $addr : '0.0.0.0';
        $array = array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED');
        foreach ($array as $value) {
            if (!empty($_SERVER[$value])) {
                $arr = explode(',', $_SERVER[$value]);
                $ip = trim(end($arr));
                if ($addr != $ip && filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE) !== false) {
                    return $ip;
                }
            }
        }
        return $addr;
    }

    /* ����� �������� �� ������� ����������� �������� � $_SERVER['HTTP_HOST'] */
    public static function checkDomain() {
        if (isset($_SERVER['HTTP_HOST'])) {
            $_SERVER['HTTP_HOST'] = strtolower($_SERVER['HTTP_HOST']);
            if (!preg_match('/^\[?(?:[\w-:\]_]+\.?)+$/i', $_SERVER['HTTP_HOST'])) {
                self::object('Response')->get(400);
                exit;
            }
        } else {
            $_SERVER['HTTP_HOST'] = '';
        }
    }

    /* ����� ���������� �������� */
    public static function protocol() {
        static $cache = NULL;
        if (empty($cache)) {
            $cache = 'http://';
            $server = array(
                'HTTPS'                  => '',
                'SERVER_PORT'            => '',
                'HTTP_X_HTTPS'           => '',
                'HTTP_X_FORWARDED_PROTO' => '',
            );
            $server = Arr::getServer($server);
            if (
                    $server['HTTPS'] == 'on' ||
                    $server['SERVER_PORT'] == 443 ||
                    $server['HTTP_X_HTTPS'] == '1' ||
                    $server['HTTP_X_FORWARDED_PROTO'] == 'https'
            ) {
                $cache = 'https://';
            }
        }
        return $cache;
    }

    /* ����� ������ */
    public static function output($content, $exit = false, $expires = NULL, $modified = NULL) {
        if (!headers_sent()) {
            header('Date: ' . gmdate('D, d M Y H:i:s \G\M\T'));
            header('Server: Protected by CMS Status-X');
            header('X-Powered-By: CMS Status-X');
            header('Expires: ' . gmdate('D, d M Y H:i:s \G\M\T', !empty($expires) ? $expires : gmmktime(0, 4, 0, 11, 1, 1974)));
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s \G\M\T', !empty($modified) ? $modified : time() - mt_rand(1000, 10000)));
        }
        if ($exit === true) {
            $content = Tool::prefixPatch($content);
            $content = SX::object('Binder')->execute($content);
            echo($content);
            exit;
        } else {
            echo($content);
        }
    }

    /* ����� ��������� define */
    public static function setDefine($define, $value = '') {
        if (!defined($define)) {
            define($define, $value);
        }
    }

    /* ����� ���������� ������ � ������� */
    public static function setMail($array) {
        self::$_mail[] = $array;
    }

    /* ����� �������� ����� �� ������� */
    public static function sendMail() {
        $mails = self::$_mail;
        if (!empty($mails)) {
            $Mail = self::object('Mail');
            foreach ($mails as $x) {
                $Mail->send($x['globs'], $x['to'], $x['to_name'], $x['text'], $x['subject'], $x['fromemail'], $x['from'], $x['type'], $x['attach'], $x['html'], $x['prio']);
            }
        }
    }

    /* ����� �������� �� ��� */
    public static function checkBanned() {
        if (Arr::getSession('user_group') == 1) {
            $_SESSION['banned'] = 0;
        } else {
            $where = array();
            DB::get()->query("DELETE FROM `" . PREFIX . "_banned` WHERE `TimeEnd` <= '" . time() . "'");
            $array = Arr::getSession(array('user_name' => '', 'login_email' => '', 'benutzer_id' => 0));
            $array['cookie_ip'] = preg_replace('/[^\d.]/', '', Arr::getCookie('welcome'));
            if (!empty($array['benutzer_id'])) {
                $where[] = "`User_id` = '" . $array['benutzer_id'] . "'";
            }
            if (!empty($array['user_name'])) {
                $where[] = "`Name` = '" . $array['user_name'] . "'";
            }
            if (Tool::isMail($array['login_email'])) {
                $domain = explode('@', $array['login_email']);
                $where[] = "`Email` = '" . $array['login_email'] . "'";
                $where[] = "`Email` = '*@" . $domain[1] . "'";
            }
            $ip = explode('.', IP_USER);
            $where[] = "`Ip` = '" . IP_USER . "'";
            $where[] = "`Ip` = '" . $ip[0] . "." . $ip[1] . "." . $ip[2] . ".*'";
            $where[] = "`Ip` = '" . $ip[0] . "." . $ip[1] . ".*.*'";
            $where[] = "`Ip` = '" . $ip[0] . "*.*.*'";
            if (!empty($array['cookie_ip'])) {
                $ip = explode('.', $array['cookie_ip']);
                $where[] = "`Ip` = '" . $array['cookie_ip'] . "'";
                $where[] = "`Ip` = '" . $ip[0] . "." . $ip[1] . "." . $ip[2] . ".*'";
                $where[] = "`Ip` = '" . $ip[0] . "." . $ip[1] . ".*.*'";
                $where[] = "`Ip` = '" . $ip[0] . "*.*.*'";
            }
            $sql = DB::get()->fetch_object("SELECT SQL_CACHE `Id` FROM `" . PREFIX . "_banned` WHERE " . implode(' OR ', $where) . " AND `Aktiv` = '1' LIMIT 1");
            $_SESSION['banned'] = is_object($sql) ? 1 : 0;
        }
    }

}