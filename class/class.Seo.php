<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (!defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}

class Seo extends Magic {

    protected $words;
    protected $content;
    protected $paramseo;
    protected $seo;

    /* $paramseo['pagetitle']        ��� <title> </title>
     * $paramseo['headernav']        ������� ������
     * $paramseo['tags_keywords']    ����� �������������� ����� ��������
     * $paramseo['tags_description'] ����� �������������� ����� ���������
     * $paramseo['generate']         ����� ������ ��� ���������
     * $paramseo['canonical']        ����� �������� ������ � ��������� */
    public function create($paramseo) {
        $pagetitle = '';
        $this->seo = SX::get('seo');
        $this->paramseo = $paramseo;
        $headernav = '<a href="index.php?area=' . $_SESSION['area'] . '">' . $this->_lang['Startpage'] . '</a>';
        if ($_REQUEST['p'] != 'index') {
            $headernav .= $this->_lang['PageSep'] . $this->paramseo['headernav'];
        }
        if (!empty($this->seo['title'])) {
            $pagetitle = $this->clean($this->seo['title']);
        } else {
            $value = SX::get('options.is_print') ? $this->_lang['PrintVersion'] : SX::get('system.Seitenname');
            $pagetitle = Tool::cleanAllow($this->sanit($this->paramseo['pagetitle']), ' .,:?!\/\|()�-') . $this->_lang['PageSep'] . $value;
        }
        $tpl_array = array(
            'pagetitle'   => $pagetitle,
            'headernav'   => $headernav,
            'keywords'    => $this->keywords(150),
            'description' => $this->description(200),
            'canonical'   => $this->canonical());
        $this->_view->assign($tpl_array);
    }

    /* ����� ��������� ������� �������� ��� ������ */
    protected function canonical() {
        $result = NULL;
        if (!empty($this->seo['canonical'])) {
            $result = $this->seo['canonical'];
        } else {
            $uri = 'index.php?' . str_replace('&', '&amp;', strtolower($_SERVER['QUERY_STRING']));
            if (isset($this->paramseo['canonical']) && $this->paramseo['canonical'] != $uri) {
                $result = $this->paramseo['canonical'];
            } elseif (SX::get('options.is_print')) {
                $result = str_replace(array('&print=1', '&amp;print=1', '?print=1', 'print.html'), '', $uri);
            }
        }
        return $result;
    }

    /* ����� ��������� ������ ���� keywords */
    protected function keywords($count = 150) {
        if (!empty($this->seo['keywords'])) {
            return $this->clean($this->seo['keywords']);
        } elseif (!empty($this->paramseo['tags_keywords'])) {
            $text = strtolower($this->sanit($this->paramseo['tags_keywords']));
        } elseif (!empty($this->paramseo['generate'])) {
            $text = $this->__object('Keywords')->create($this->sanit($this->paramseo['generate']));
            if (($strlen = strlen($text)) < $count) {
                $strlen = $count - $strlen;
                $text .= ', ' . ($this->baseContent() ? $this->baseWords($strlen) : $this->__object('Keywords')->create($this->content()));
            }
        } else {
            $text = $this->baseContent() ? $this->baseWords() : $this->__object('Keywords')->create($this->content());
        }
        $text = Tool::chars(Tool::cleanAllow($text, ' ,'), SX::get('system.CountKeywords'), '');
        return trim($text, ',');
    }

    /* ����� ��������� ������ ���� keywords */
    protected function description($count = 200) {
        if (!empty($this->seo['description'])) {
            return $this->clean($this->seo['description']);
        } elseif (!empty($this->paramseo['tags_description'])) {
            $text = strtolower($this->sanit($this->paramseo['tags_description']));
        } elseif (!empty($this->paramseo['generate'])) {
            $text = $this->setkey($this->sanit($this->paramseo['generate'], false));
            if (($strlen = strlen($text)) < $count) {
                $strlen = $count - $strlen;
                $text .= '. ' . $this->setkey(($this->baseContent() ? $this->baseWords($strlen) : $this->content()), $strlen);
            }
        } else {
            $text = $this->setkey($this->baseContent() ? $this->baseWords() : $this->content());
        }
        $text = Tool::chars(Tool::cleanAllow($text, ' .'), SX::get('system.CountDescription'), '');
        return trim($text);
    }

    /* ����� ��������� ������ �� ���� */
    protected function baseContent() {
        static $aktiv = false;
        if ($aktiv === false) {
            $aktiv = true;
            $this->base(25);
        }
        return !empty($this->words) ? true : false;
    }

    protected function baseWords($num = 200) {
        $words = explode(',', $this->words);
        shuffle($words);
        $count = 0;
        $result = array();
        foreach ($words as $word) {
            $count += strlen($word);
            $result[] = trim($word);
            if ($count > $num) {
                break;
            }
        }
        return implode(', ', $result);
    }

    protected function content() {
        if (empty($this->content)) {
            $this->content = $this->sanit($this->paramseo['content']);
        }
        return $this->content;
    }

    /* ����� ��������� description */
    protected function setkey($str, $num = 200) {
        if (!empty($str)) {
            $str = str_replace(array("\r\n", "\r", "\n", ',', '?', '!', ':', ';', '.'), PHP_EOL, $str);
            $result = $words = array();
            foreach (explode(PHP_EOL, $str) as $word) {
                if (isset($word{15}) && !is_numeric($word)) {
                    $words[] = ucfirst(trim(preg_replace(array('/[^\w- ]/i', '/\s+/'), ' ', strtolower($word))));
                }
            }
            $words = array_unique($words);
            shuffle($words);
            $count = 0;
            foreach ($words as $word) {
                if (!empty($word)) {
                    $count += strlen($word);
                    $result[] = $word;
                    if ($count > $num) {
                        break;
                    }
                }
            }
            return implode('. ', $result);
        }
        return '';
    }

    /* ����� ��������� ������� ������ */
    protected function sanit($text, $param = true) {
        $text = strip_tags($text);
        $text = Tool::cleanTags($text, array('codewidget', 'screen', 'contact', 'audio', 'video', 'neu'));
        $search = array(
            '!\[code\].*?\[/code\]!si',
            '!\[php\].*?\[/php\]!si',
            '!\[reg\].*?\[/reg\]!si',
            '!\[url[^\]]*?\].*?\[/url\]!si',
            '!\[hide[^\]]*?\].*?\[/hide\]!si',
            '!\[[^\]]*?\]!si',
            '!<script[^>]*?>.*?</script>!si',
            '!<[\/\!]*?[^<>]*?>!si',
            '/&[#a-z0-9]{2,6};/i'
        );
        $text = preg_replace($search, ' ', $text);
        return $param ? Tool::cleanSpace($text) : $text;
    }

    /* ����� ������ �������� description �� ���� */
    protected function base($val = '12') {
        if (get_active('seomod')) {
            $array = $this->__object('Cache')->get('full_description');
            if ($array === false) {
                $array = $this->query();
                $this->__object('Cache')->set('full_description', $array);
            }
            $array = $this->rand($val, $array);
        }
        $this->words = !empty($array) ? implode(', ', $array) : '';
    }

    /* ����� ������ �������� description �� ���� */
    protected function query() {
        $array = array();
        $sql = DB::get()->query("SELECT Text FROM " . PREFIX . "_description WHERE Aktiv = '1'");
        while ($row = $sql->fetch_object()) {
            $array[] = trim($row->Text);
        }
        $sql->close();
        return $array;
    }

    /* ����� ������ ��������� �������� �� ������� */
    protected function rand($val, $array) {
        $rand = array();
        if (!empty($array)) {
            $array_rand = array_rand($array, $val);
            if (is_array($array_rand)) {
                foreach ($array_rand as $val) {
                    $rand[] = $array[$val];
                }
            } else {
                $rand[] = $array[$array_rand];
            }
        }
        return $rand;
    }

    protected function clean($text) {
        return sanitize(strip_tags($text));
    }

}
