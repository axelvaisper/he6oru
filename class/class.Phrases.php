<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (!defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}

class Phrases extends Magic {

    /* ����� ���������� �������� */
    public function get() {
        $sql = $this->_db->query("SELECT SQL_CACHE id, phrase FROM " . PREFIX . "_phrases WHERE active = '1'");
        $i = 0;
        while ($row = $sql->fetch_object()) {
            $content[$i] = '%%' . $row->id . '%%' . $row->phrase;
            $i++;
        }
        $sql->close();
        if (empty($content)) {
            return '';
        }
        $random = mt_rand(0, count($content) - 1);
        $content_out = explode('%%', $content[$random]);
        return $content_out[2];
    }

}
