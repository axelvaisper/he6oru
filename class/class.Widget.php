<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (!defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}

class Widget extends Magic {

    protected $_cache = array();
    protected $_object = array();

    /* ����� ��������� �������� ������� */
    public function settings($widget, $default = array()) {
        $array = array();
        if (!empty($widget)) {
            $settings = unserialize(SX::get('widgets.' . $widget));
            if ($settings !== false) {
                $array += (array) $settings;
            }
            $array += (array) $default;
        }
        return $array;
    }

    /* ����� ����������� ������� */
    public function get($params = array()) {
        if (get_active($params['name'])) {
            $key = $this->key($params);
            if (isset($this->_cache[$key])) {
                return $this->_cache[$key];
            }
            $class = $this->name($params['name']);
            if ($this->load($params['name'], $class) && is_callable(array($class, 'get'))) {
                return $this->_cache[$key] = $this->object($class)->get($params);
            }
        }
        return NULL;
    }

    /* ����� ��������� ����� �� ����������� ������� */
    protected function key($params) {
        array_multisort($params);
        return md5(serialize($params));
    }

    /* ����� ��������� ����� ������ */
    protected function name($value) {
        return 'Widget' . ucfirst($value);
    }

    /* ����� ��������� ����� ������ */
    protected function load($widget, $class) {
        $file = WIDGET_DIR . '/' . $widget . '/class/class.' . $class . '.php';
        if (false === include_once $file) {
            return false;
        }
        return true;
    }

    /* ����� ��������� ������� */
    protected function object($class) {
        if (!isset($this->_object[$class])) {
            $this->_object[$class] = $this->__object($class);
        }
        return $this->_object[$class];
    }

}
