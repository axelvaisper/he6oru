<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (!defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}

class Captcha extends Magic {

    protected $_options = array(
        'max_calc1' => 9, // ������������ ����� ������� �������� � �������
        'max_calc2' => 9, // ������������ ����� ������� �������� � �������
        'min_text'  => 3, // ����������� ���������� ��������, ��������� ������
        'max_text'  => 4, // ������������ ���������� ��������, ��������� ������
        'type'      => 'auto', // auto - ����������� ��������� ����� � ����������, text - ��������� �����, calc - ����������
        'text'      => '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ', // ����� �������� ��� ��������� ������
        'ignore'    => array('=' => 'army', '+' => 'army', '-' => 'army'), // ������� �� ������������ � ��������� ������ ������
        'fonts'     => array('alien', 'army', 'davinci', 'dustismo', 'linear', 'simpler', 'jasper', 'plakat', 'titul', 'demish', 'actionj', 'lexo'),
    );

    public function __construct() {
        $config = SX::get('secure');
        $this->_options = $config + $this->_options;
    }

    /* ����� ������������� ������ */
    public function start($out = NULL, $uniq_key = NULL) {
        if (!empty($out) && $out == 'ajax') {
            $this->ajax($uniq_key);
        } else {
            $this->assign($uniq_key);
        }
    }

    /* ����� �������� ������������ ����� ������ */
    public function validate() {
        $uniq_key = Arr::getRequest('uniq_key');
        $scode = Arr::getRequest('scode' . $uniq_key);
        $secure = Arr::getSession('secure_result' . $uniq_key);
        $valid = (empty($scode) || empty($secure) || $scode != $secure) ? 'false' : 'true';
        SX::output($valid, true);
    }

    /* ����� ������ ������ ajax */
    public function ajax() {
        if ($this->aktive()) {
            $uniq_key = Arr::getRequest('uniq_key');
            $this->create($uniq_key);
            SX::output($this->choice($uniq_key), true);
        }
        exit;
    }

    /* ����� ������� ��������� ������ */
    public function load() {
        if (!extension_loaded('gd')) {
            SX::output('�� ����������� ���������� ��� ������ � �������������', true);
        }
        $string = $this->receive();
        $width = strlen($string) * 30;
        $image = imagecreate($width, 44);
        if ($image === false) {
            SX::output('������ ��������� �����������', true);
        }
        imagecolorallocate($image, 0xff, 0xff, 0xff);
        if ($string == 'ERROR' || $this->_options['ttf_font'] != 1 || $this->ttf($image, $string) === false) {
            $this->string($image, $string);
        }
        $this->send($image);
    }

    /* ����� �������� ������������ ����� ������ ����� �������� */
    public function check($error = NULL, $spam = true) {
        $uniq_key = Arr::getRequest('uniq_key');
        if ($this->aktive()) {
            $secure = Arr::getSession('secure_result' . $uniq_key);
            $scode = Arr::getPost('scode' . $uniq_key);
            $scode = strtolower($scode);
            if (empty($scode) || empty($secure) || $scode != $secure) {
                $error[] = $this->_lang['Reg_SecurecodeWrong'];
            }
        }
        return $this->error($error, $spam, $uniq_key);
    }

    /* ����� ������ ������ � ������ */
    protected function assign($uniq_key = NULL) {
        if ($this->aktive()) {
            $this->create($uniq_key);
            $this->_view->assign('use_code' . $uniq_key, 1);
            $this->_view->assign('captcha_img' . $uniq_key, $this->choice($uniq_key));
        } else {
            $this->_view->assign('use_code' . $uniq_key, 0);
        }
    }

    /* ����� ������ � �������� */
    protected function error($error, $spam, $uniq_key = NULL) {
        if ($spam === true) {
            foreach ($_POST as $key) {
                if (strlen($key) >= 2 && Tool::checkSpam($key) === false) {
                    $error[] = $this->_lang['SpamUsed'];
                    break;
                }
            }
        }
        $this->_view->assign('error' . $uniq_key, $error);
        return !empty($error) ? false : true;
    }

    /* ����� �������� ���������� ������ */
    protected function aktive() {
        $check = $this->_options['active'];
        return $check == 1 || ($check == 2 && Arr::getSession('loggedin') != 1);
    }

    /* ����� �������� �������� ������ */
    protected function create($uniq_key = NULL) {
        unset($_SESSION['secure_result' . $uniq_key], $_SESSION['secure_question' . $uniq_key]);
        $method = $this->_options['type'];
        if (!is_callable(array($this, $method))) {
            $method = 'auto';
        }
        list($result, $question) = $this->$method();
        Arr::setSession(array('secure_result' . $uniq_key => $result, 'secure_question' . $uniq_key => $question));
    }

    /* ����� ��������������� ������ ���� ������ */
    protected function auto() {
        $method = Arr::rand(array('calc', 'text'));
        return $this->$method();
    }

    /* ����� ��������� �������� �������������� ������ */
    protected function calc() {
        $operator = Arr::rand(array('+', '-'));
        list($val1, $val2) = $this->rand($operator);
        $result = $operator == '+' ? $val1 + $val2 : $val1 - $val2;
        $question = $val1 . $operator . $val2 . '=';
        return array($result, $question);
    }

    /* ����� ��������� ���� ����� ��� �������������� ������ */
    protected function rand($operator = '') {
        do {
            $array = array(mt_rand(1, $this->_options['max_calc1']), mt_rand(1, $this->_options['max_calc2']));
        } while ($array[0] == $array[1]);
        if ($operator == '-' && $array[0] < $array[1]) {
            $array = array_reverse($array);
        }
        return $array;
    }

    /* ����� ��������� �������� ��������� ������ */
    protected function text() {
        $array = str_split($this->_options['text']);
        $array = Arr::rand($array, mt_rand($this->_options['min_text'], $this->_options['max_text']));
        $string = implode('', $array);
        return array(strtolower($string), $string);
    }

    /* ����� ������ ���� ������ */
    protected function choice($uniq_key = NULL) {
        if ($this->_options['gd'] == '1') {
            if (!empty($uniq_key)) {
                $uniq_key = 'uniq_key=' . $uniq_key . '&';
            }
            return '<img class="absmiddle" src="' . BASE_URL . '/lib/secure.php?' . $uniq_key . time() . '" alt="" />';
        } else {
            return $_SESSION['secure_question' . $uniq_key];
        }
    }

    /* ����� ��������� ������ � ������ */
    protected function receive() {
        $uniq_key = Arr::getRequest('uniq_key');
        $string = Arr::getSession('secure_question' . $uniq_key);
        return !empty($string) ? $string : 'ERROR';
    }

    /* ����� ��������� ����� ������ */
    protected function color($image) {
        return imagecolorallocate($image, mt_rand(0, 255), mt_rand(0, 250), mt_rand(0, 250));
    }

    /* ����� ��������� ������� ������ */
    protected function size() {
        return mt_rand(18, 28);
    }

    /* ����� ��������� ������� ������ */
    protected function angle() {
        return mt_rand(-25, 25);
    }

    /* ����� �������� ������ */
    protected function font($font = NULL) {
        if (empty($font)) {
            $font = Arr::rand($this->_options['fonts']);
        }
        return STATUS_DIR . '/lib/fonts/' . $font . '.ttf';
    }

    /* ����� ��������� ������� */
    protected function ttf($image, $string) {
        if (function_exists('imagettftext')) {
            $next = 10;
            foreach (str_split($string) as $value) {
                $size = $this->size();
                $font = isset($this->_options['ignore'][$value]) ? $this->font($this->_options['ignore'][$value]) : $this->font();
                if (!imagettftext($image, $size, $this->angle(), $next, 32, $this->color($image), $font, $value)) {
                    return false;
                }
                $next += $size;
            }
            return true;
        }
        return false;
    }

    /* ����� �������� ������ ��� ������������� ttf */
    protected function string($image, $string) {
        $y_old = '';
        $font = 5;
        $x = mt_rand(2, 35);
        for ($i = 0; $i < strlen($string); $i++) {
            if (($y1 = $y_old - 7) < 2) {
                $y1 = 2;
            }
            if (($y2 = $y_old + 7) > 15) {
                $y2 = 15;
            }
            $y = mt_rand($y1, $y2);
            imagestring($image, $font, $x, $y, $string{$i}, $this->color($image));
            $x += 11;
            $y_old = $y;
        }
    }

    /* ����� ������ ������ */
    protected function send($image) {
        header('Expires: Mon, 1 Jan 2006 00:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0', false);
        header('Pragma: no-cache');
        if (function_exists('imagepng')) {
            header('Content-type: image/png');
            imagealphablending($image, false);
            $color = imagecolorallocatealpha($image, 0, 0, 0, 127);
            imagefill($image, 0, 0, $color);
            imagesavealpha($image, true);
            imagepng($image);
        } elseif (function_exists('imagegif')) {
            header('Content-type: image/gif');
            imagegif($image);
        } elseif (function_exists('imagejpeg')) {
            header('Content-type: image/jpeg');
            imagejpeg($image, NULL, 50);
        } else {
            SX::output('�� ������� ������� �����������...', true);
        }
        imagedestroy($image);
    }

}
