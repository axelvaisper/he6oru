<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (!defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}

final class Result {

    protected $_result;

    public function __construct($result) {
        $this->_result = $result;
    }

    /* ����� �������� ��� �� ������ ������ � ���� �������������� �������, ������������� �������, ��� ����� */
    public function fetch_array() {
        return $this->_result ? $this->_result->fetch_array() : false;
    }

    /* ����� �������� ��� �� ������ ������ � ���� ������� */
    public function fetch_object() {
        return $this->_result ? $this->_result->fetch_object() : false;
    }

    /* ����� �������� ��� �� ������ ������ � ���� ������������� ������� */
    public function fetch_row() {
        return $this->_result ? $this->_result->fetch_row() : false;
    }

    /* ����� �������� ��� �� ������ ������ � ���� �������������� ������� */
    public function fetch_assoc() {
        return $this->_result ? $this->_result->fetch_assoc() : false;
    }

    /* ����� ���������� ���������� ����� � ������ ������ */
    public function num_rows() {
        return $this->_result ? $this->_result->num_rows : false;
    }

    /* ����� ���������� ���������� ����� � ������ ������ */
    public function field_count() {
        return $this->_result ? $this->_result->field_count : false;
    }

    /* ����� ������������� ��������� �� ��������� ��������� �� ������������ ���������� ����� */
    public function data_seek() {
        return $this->_result ? $this->_result->data_seek() : false;
    }

    /* ����� ���������� �������� ������� ������� */
    public function field_name($i) {
        if (!$this->_result) {
            return false;
        }
        $this->_result->field_seek($i);
        $field = $this->_result->fetch_field();
        return $field->name;
    }

    /* ����� ����������� ������, ���������� ����������� ������� */
    public function close() {
        if (!$this->_result) {
            return false;
        }
        $result = $this->_result->close();
        unset($this);
        return $result;
    }

    /* ����� ������������� ������� ������ */
    public function __get($key) {
        if (in_array($key, array('lengths', 'field_count', 'num_rows'))) {
            return $this->$key();
        }
    }

}

class DB {

    protected $_mysqli;
    protected $_cache;
    protected $_prefix;

    protected function __clone() {

    }

    /* ������������� ������ � ��������� ���������� � ����� */
    protected function __construct($config) {
        $port = !empty($config['dbport']) ? $config['dbport'] : 3306;
        $this->_prefix = $config['dbprefix'];
        $this->_mysqli = new mysqli($config['dbhost'], $config['dbuser'], $config['dbpass'], $config['dbname'], $port);
        SX::set('database.load', $this->connect_base($config['dbcharset']));
    }

    /* ����� ���������� �� ��������� ������ ��������� ������ ��� ������� */
    public static function get() {
        static $object = NULL;
        if ($object === NULL) {
            $object = new self(SX::get('database'));
        }
        return $object;
    }

    /* ����� ��� ������������� �������������� ��� */
    public static function create($config = NULL) {
        static $object = array();
        if (empty($config)) {
            return self::get();
        }
        $name = $config['dbname'] . $config['dbprefix'];
        if (!isset($object[$name])) {
            $object[$name] = new self($config);
        }
        return $object[$name];
    }

    /* ����������� � ���� ������ */
    protected function connect_base($charset = 'cp1251') {
        if (!$this->_mysqli->connect_errno) {
            if ($this->server_info() && substr($this->server_info(), 0, 1) > 4) {
                $this->_mysqli->query("SET SESSION SQL_MODE = ''");
            }
            if (!$this->_mysqli->set_charset($charset)) {
                $this->_mysqli->query("SET NAMES " . $charset);
            }
            return true;
        }
        $this->error_connect();
        return false;
    }

    /* ����� ���������� ������ ���������� ����� � ������� � SQL_CALC_FOUND_ROWS */
    public function found_rows() {
        $sql = $this->fetch_object("SELECT FOUND_ROWS() AS total");
        return $sql->total;
    }

    /* ������ multi_query */
    public function multi_query($query) {
        if ($this->_mysqli->multi_query($query)) {
            return true;
        }
        $this->error_query($query);
        return false;
    }

    /* �������� ������ ��������� ������� multi_query */
    public function store_result() {
        if (($result = $this->_mysqli->store_result())) {
            return new Result($result);
        }
        return false;
    }

    /* �������� �� ������������� ���������� ���������� ������� multi_query */
    public function more_results() {
        return $this->_mysqli->more_results();
    }

    /* ������� � ���������� ���������� ������� multi_query */
    public function next_result() {
        return $this->_mysqli->next_result();
    }

    /* ���� ���������� ���� �������, ��� ��������� ������� � ����������� ����������� ������� multi_query */
    public function store_next_result() {
        if ($this->more_results() !== false) {
            if ($this->next_result() !== false) {
                return $this->store_result();
            }
        }
        return false;
    }

    /* ����� ��������� ������ */
    public function query($query) {
        $result = $this->_mysqli->query($query);
        if ($result) {
            return new Result($result);
        }
        $this->error_query($query);
        return false;
    }

    /* ����� ������ � �������, � �������� ���������� ������������� ������ */
    public function insert_query($table, $array) {
        $arr = array();
        foreach ($array as $key => $val) {
            $arr['key'][] = '`' . $key . '`';
            $arr['val'][] = '\'' . $this->escape(trim($val)) . '\'';
        }
        $arr['key'] = implode(',', $arr['key']);
        $arr['val'] = implode(',', $arr['val']);
        $query = 'INSERT INTO `' . $this->_prefix . '_' . $table . '` (' . $arr['key'] . ') VALUES (' . $arr['val'] . ')';
        return !empty($arr['key']) ? $this->query($query) : false;
    }

    /* ����� ���������� �������, � �������� ���������� ������������� ������ */
    public function update_query($table, $array, $where = NULL) {
        $arr = array();
        foreach ($array as $key => $val) {
            $arr[] = '`' . $key . '` = \'' . $this->escape(trim($val)) . '\'';
        }
        if (!empty($where)) {
            $where = ' WHERE ' . $where;
        }
        $query = 'UPDATE `' . $this->_prefix . '_' . $table . '` SET ' . implode(', ', $arr) . $where;
        return !empty($arr) ? $this->query($query) : false;
    }

    /* ���������� ���������� ����� ���������� ������� */
    public function num_rows($query) {
        return $this->query_type($query, 'num_rows');
    }

    /* ������������ ��� ���������� ������� � ���������� ������ */
    public function fetch_object($query) {
        return $this->query_type($query, 'fetch_object');
    }

    /* ����� ���������� ���������� ����� � ������ ������ */
    public function fetch_assoc($query) {
        return $this->query_type($query, 'fetch_assoc');
    }

    /* ������������ ��� ���� ���������� ������� � ���������� ������ */
    public function fetch_object_all($query) {
        return $this->query_all($query, 'fetch_object');
    }

    /* ����� ���������� ���������� ����� � ������ ������ */
    public function fetch_assoc_all($query) {
        return $this->query_all($query, 'fetch_assoc');
    }

    /* ������������ ��� ���� ���������� ������� � ���������� ������ � ������������ */
    public function cache_fetch_object_all($query) {
        return $this->cache_query_all($query, 'fetch_object');
    }

    /* ����� ���������� ���������� ���� ������ ������ � ������������ */
    public function cache_fetch_assoc_all($query) {
        return $this->cache_query_all($query, 'fetch_assoc');
    }

    /* ���������� ���������� ����� ���������� �������, ������� � ������������ */
    public function cache_num_rows($query) {
        return $this->cache_query($query, 'num_rows');
    }

    /* ������������ ��� ���������� ������� � ���������� ������, ������� � ������������ */
    public function cache_fetch_object($query) {
        return $this->cache_query($query, 'fetch_object');
    }

    /* ������������ ��� ���������� �������, ��������� ������������� ������, ������� � ������������ */
    public function cache_fetch_assoc($query) {
        return $this->cache_query($query, 'fetch_assoc');
    }

    /* �������� ID ���������� ������������ ������� INSERT */
    public function insert_id() {
        return $this->_mysqli->insert_id;
    }

    /* ���������� ���������� �����, ���������� ��������� INSERT, UPDATE, DELETE */
    public function affected_rows() {
        return $this->_mysqli->affected_rows;
    }

    /* ����� ��������� ���������� � ������ ���� */
    public function server_info() {
        return $this->_mysqli->server_info;
    }

    /* ���������� ����������� ������� � ������, ������������ � SQL-�������, �������� �� �������� ��������� ���������� */
    public function escape($query) {
        return $this->_mysqli->real_escape_string($query);
    }

    /* ����� ���������� ������� ������� ���� */
    public function prefix() {
        return $this->_prefix;
    }

    /* ��������� ���������� � ����� */
    public function close() {
        $this->_mysqli->close();
    }

    /* ����� ������ �������������� �������� ��������� ���������� */
    public function begin() {
        $this->_mysqli->autocommit(false);
    }

    /* ����� ��������� ��������� ������� ���������� */
    public function commit() {
        $this->_mysqli->commit();
        $this->_mysqli->autocommit(true);
    }

    /* ����� �������� ��������� ������� ���������� */
    public function rollback() {
        $this->_mysqli->rollback();
        $this->_mysqli->autocommit(true);
    }

    protected function query_type($query, $type) {
        $result = $this->query($query);
        if ($result) {
            $return = $result->$type();
            $result->close();
            return $return;
        }
        return false;
    }

    protected function query_all($query, $type) {
        $array = array();
        $result = $this->query($query);
        if ($result) {
            while ($row = $result->$type()) {
                $array[] = $row;
            }
            $result->close();
        }
        return $array;
    }

    protected function cache_query_all($query, $type) {
        $array = array();
        $key = md5($query, $type);
        if (isset($this->_cache['all'][$key])) {
            $array = unserialize($this->_cache['all'][$key]);
        } else {
            $array = $this->query_all($query, $type);
            $this->_cache['all'][$key] = serialize($array);
        }
        return $array;
    }

    /* ����� ����������� ��������� �������� SELECT */
    protected function cache_query($query, $array) {
        $data = md5($query);
        if (isset($this->_cache[$array][$data])) {
            return unserialize($this->_cache[$array][$data]);
        } else {
            return $this->cache_result($query, $array, $data);
        }
    }

    /* ����� ������� �� ���� � ������ � ��� */
    protected function cache_result($query, $array, $data) {
        $result = $this->query($query);
        if ($result) {
            $result = $this->query($query);
            $res = $result->$array();
            $result->close();
            $this->_cache[$array][$data] = serialize($res);
            return $res;
        }
        return false;
    }

    /* ����� ������ ��������� �� ������ ���������� � ����� */
    protected function error_connect() {
        $text = '������ ����������� � ����!' . PE . '������ �:' . $this->_mysqli->connect_errno . PE . '���������: ' . $this->_mysqli->connect_error;
        SX::setLog($text, '5', 0);
        SX::output('<li> ���� �������� ����������', true);
    }

    /* ����� ��������� ��������� �������� � ���� */
    protected function error_query($query = '') {
        $my_error = $this->_mysqli->error;
        $my_errno = $this->_mysqli->errno;
        $query = preg_replace('/\s+/', ' ', $query);
        if (!defined('NOLOGGED')) {
            SX::setLog('������ � MySQL!' . PE . '������ �:' . $my_errno . PE . '������: ' . $query . PE . '������: ' . $my_error . PE . '��������: http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], 4);
        }
        if (defined('SQLERROR_WIDTH')) {
            SX::output('<div class="info_red" style="width:' . SQLERROR_WIDTH . '%;height:100px"><div style="margin-bottom:5px; width:98%">������ �: ' . $my_errno . '<br />������: ' . $my_error . '<br />������: <br /><em>' . $query . '</em></div></div>');
        }
    }

}
