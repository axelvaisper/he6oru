<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (!defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}

class Editor {

    /* ������������� ��������� */
    public function load($sys = '', $value = '', $fieldname = '', $height = '', $type = '') {
        switch ($sys) {
            case 'admin':
                $type = empty($type) ? 'Full' : $type;
                return $this->select(SX::get('admin.Type_Redaktor'), $value, $fieldname, $height, $type);
            case 'user':
                $type = empty($type) ? 'User' : $type;
                return $this->select(SX::get('system.SiteEditor'), $value, $fieldname, $height, $type);
            default:
                return $this->textarea($value, $fieldname, $height);
        }
    }

    /* ����� ��������� */
    protected function select($select, $value, $fieldname, $height, $type) {
        if ($select == '1') {
            return $this->cke($value, $fieldname, $height, $type);
        }
        return $this->textarea($value, $fieldname, $height);
    }

    /* ������������� textarea */
    protected function textarea($value, $fieldname, $height) {
        $height = empty($height) ? 350 : $height;
        return '<textarea style="width:100%; height:' . $height . 'px" name="' . $fieldname . '" wrap="virtual">' . sanitize($value) . '</textarea>';
    }

    /* ������������� CKE ��������� */
    protected function cke($value, $fieldname, $height, $type) {
        static $CKEditor = NULL;
        $config['toolbar'] = $type;
        $config['height'] = empty($height) ? 450 : $height;
        if (empty($CKEditor)) {
            include_once STATUS_DIR . '/lib/editor/ckeditor_php5.php';
            $CKEditor = SX::object('CKEditor');
        }
        $CKEditor->returnOutput = true;
        $CKEditor->basePath = BASE_URL . '/lib/editor/';
        return $CKEditor->editor($fieldname, $value, $config);
    }

}
