<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (!defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}

class Core extends Magic {

    public function __construct() {
        $is_print = false;
        $val = $_SERVER['REQUEST_URI'];
        if (Arr::getRequest('print') == 1 || substr($val, -10) == 'print.html' || substr($val, -7) == 'print=1') {
            $_REQUEST['print'] = 1;
            $is_print = true;
        }
        SX::set('options.is_print', $is_print);
    }

    /* �������� ����� ���������� �������� */
    public function compileDir($area) {
        if (!is_dir(TEMP_DIR . '/compiled/' . $area . '/')) {
            Folder::protection(TEMP_DIR . '/compiled/' . $area . '/', 0777);
            Folder::protection(TEMP_DIR . '/compiled/' . $area . '/main/', 0777);
            Folder::protection(TEMP_DIR . '/compiled/' . $area . '/admin/', 0777);
        }
    }

    /* �������� � ������� �������� ������� */
    public function modules($area = 0) {
        if (empty($area)) {
            $area = $_SESSION['area'];
        }
        $array = array('modules' => array(), 'widgets' => array(), 'active' => array());
        $sql = $this->_db->query("SELECT SQL_CACHE Name, Settings, Type, Aktiv_Section_" . $area . " AS Aktiv FROM " . PREFIX . "_bereiche");
        while ($row = $sql->fetch_assoc()) {
            $array['active'][$row['Name']] = $row['Aktiv'];
            if ($row['Type'] == 'widget') {
                $name = !empty($row['Result']) ? $row['Result'] : $row['Name'];
                $array['widgets'][$name] = $row['Settings'];
            }
            if ($row['Type'] == 'extmodul' && $row['Aktiv'] == 1) {
                $array['modules'][] = $row['Name'];
            }
        }
        $sql->close();
        SX::set('modules', $array['modules']);
        SX::set('widgets', $array['widgets']);
        SX::set('active', $array['active']);
    }

    /* ����������� ������ */
    public function extensions() {
        $ext = $_SESSION['banned'] == 1 ? 'banned' : Tool::cleanString($_REQUEST['p'], '_');
        if (get_active($ext) && is_file(MODUL_DIR . '/' . $ext . '/main/action.php')) {
            $include = MODUL_DIR . '/' . $ext . '/main/action.php';
        } elseif (get_active($ext) && is_file(WIDGET_DIR . '/' . $ext . '/main/action.php')) {
            $include = WIDGET_DIR . '/' . $ext . '/main/action.php';
        } else {
            if (!is_file(STATUS_DIR . '/action/' . $ext . '.php')) {
                $_REQUEST['p'] = 'index';
                $include = STATUS_DIR . '/action/index.php';
            } else {
                $include = STATUS_DIR . '/action/' . $ext . '.php';
            }
        }
        include $include;
    }

    /* ���������� ������������ ������ ��� ���������� ��������� ������ */
    public function section() {
        $row = $this->_db->cache_fetch_assoc("SELECT * FROM " . PREFIX . "_sektionen WHERE Id = '" . $_SESSION['area'] . "' AND Aktiv = '1' LIMIT 1");
        if (!is_array($row)) {
            $row_p = $this->_db->cache_fetch_assoc("SELECT * FROM " . PREFIX . "_sektionen WHERE Id = '" . $_SESSION['area'] . "' LIMIT 1");
            if (!empty($row_p['Passwort']) && Arr::getGet('pass') == $row_p['Passwort']) {
                $_SESSION['secpass'][$row_p['Id']] = $row_p['Passwort'];
            }
            if (is_array($row_p) && $row_p['Aktiv'] != 1) {
                if (isset($_SESSION['secpass'][$row_p['Id']]) && $_SESSION['secpass'][$row_p['Id']] == $row_p['Passwort']) {
                    $row = $row_p;
                } else {
                    $row_p['Meldung'] = (empty($row_p['Meldung'])) ? '������ �� �������' : $row_p['Meldung'];
                    SX::output('<pre>' . $row_p['Meldung'] . '</pre>', true);
                }
            } else {
                $row = $this->_db->cache_fetch_assoc("SELECT * FROM " . PREFIX . "_sektionen WHERE Aktiv = '1' ORDER BY Id ASC LIMIT 1");
                if (is_array($row)) {
                    $_REQUEST['area'] = $_SESSION['area'] = $row['Id'];
                } else {
                    SX::output('<pre>������ �� �������</pre>', true);
                }
            }
        }
        SX::set('section', $row);
    }

    /* ��������� �������� ������ � �������� ������ */
    public function getSection() {
        if (SX::get('system.Domains') == '1') {
            $host = !empty($_SERVER['HTTP_HOST']) ? Tool::cleanAllow($_SERVER['HTTP_HOST'], '.') : Tool::cleanAllow(getenv('HTTP_HOST'), '.');
            $sql = $this->_db->fetch_object("SELECT SQL_CACHE Id FROM " . PREFIX . "_sektionen WHERE Domains = '" . $this->_db->escape(trim($host)) . "' AND Aktiv='1' LIMIT 1");
        }
        if (isset($sql) && is_object($sql)) {
            $_REQUEST['area'] = $_SESSION['area'] = $sql->Id;
        } elseif (isset($_SESSION['area']) && is_numeric($_SESSION['area']) && $_SESSION['area'] >= 1 && !isset($_REQUEST['area'])) {
            $_REQUEST['area'] = $_SESSION['area'] = intval($_SESSION['area']);
        } elseif (isset($_REQUEST['area']) && is_numeric($_REQUEST['area']) && $_REQUEST['area'] >= 1) {
            $_REQUEST['area'] = $_SESSION['area'] = intval($_REQUEST['area']);
        } else {
            $_REQUEST['area'] = $_SESSION['area'] = 1;
        }
    }

    /* ������ ����� SSL */
    public function ssl() {
        $options = array(
            'val' => SX::protocol(),
            'ssl' => SX::get('configs.ssl'),
            'uri' => $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'],
        );
        if ($options['ssl'] == '0' && $options['val'] == 'https://') {
            $this->__object('Redir')->redirect('http://' . $options['uri']);
        }
        $https = $this->getHttps();
        if ($options['ssl'] == '1' && $options['val'] == 'https://' && !$https) {
            $this->__object('Redir')->redirect('http://' . $options['uri']);
        } elseif ($options['ssl'] == '1' && $options['val'] == 'http://' && $https) {
            $this->__object('Redir')->redirect('https://' . $options['uri']);
        }
    }

    /* ����� ����������� ������� ��������� �� https */
    public function getHttps() {
        $result = false;
        $https = SX::get('configs.https');
        if (!empty($https)) {
            $request = Arr::getRequest(array('p' => '*', 'action' => '*'));
            foreach ($https as $value) {
                list($p, $action) = explode(':', $value);
                if (
                        ('*' == $p && '*' == $action) ||
                        ('*' == $p && $request['action'] == $action) ||
                        ($request['p'] == $p && '*' == $action) ||
                        ($request['p'] == $p && $request['action'] == $action)
                ) {
                    $result = true;
                    break;
                }
            }
        } else {
            $result = true;
        }
        return $result;
    }

    /* ������������� ������ �� ����� */
    public function selectLangs() {
        if (!empty($_SESSION['lang']) && !isset($_REQUEST['lang']) && (is_file(LANG_DIR . '/' . $_SESSION['lang'] . '/main.txt'))) {
            $Language = $_SESSION['lang'];
        } else {
            $Language = (strlen(Arr::getRequest('lang')) == 2 && (is_file(LANG_DIR . '/' . Arr::getRequest('lang') . '/main.txt'))) ? Arr::getRequest('lang') : SX::get('langs.1');
        }
        $_SESSION['lang'] = preg_replace('/[^a-z]/i', '', $Language);
        if (!empty($_REQUEST['lredirect'])) {
            $this->__object('Redir')->seoRedirect(base64_decode($_REQUEST['lredirect']));
        }
    }

    /* �������� ������ �������� ������ */
    public function aktiveLangs() {
        $array = array();
        $sql = $this->_db->query("SELECT SQL_CACHE Id, Sprachcode FROM " . PREFIX . "_sprachen WHERE Aktiv = 1 ORDER BY Id ASC");
        while ($row = $sql->fetch_assoc()) {
            $array[$row['Id']] = $row['Sprachcode'];
        }
        SX::set('langs', $array);
    }

    /* �������� ��������� �������� ����� */
    public function langSettings() {
        $seo = array();
        $query = "SELECT * FROM " . PREFIX . "_sprachen WHERE Id = '" . Arr::getSession('Langcode', 1) . "' AND Aktiv = '1' ; ";
        $query .= "SELECT title, keywords, description, canonical FROM " . PREFIX . "_seotags WHERE " . $this->seoQuery() . " AND Aktiv = '1'";
        if ($this->_db->multi_query($query)) {
            if (($result = $this->_db->store_result())) {
                $row = $result->fetch_assoc();
                $result->close();
            }
            if (($result = $this->_db->store_next_result())) {
                $seo = $result->fetch_assoc();
                $result->close();
            }
        }
        SX::set('seo', $seo);
        return $row;
    }

    /* ����� ��������� ��������� �������� ������ �������� */
    protected function seoQuery() {
        $DB = DB::get();
        $val = 'index.php?' . Tool::cleanUrl(strtolower($_SERVER['QUERY_STRING']));
        $val2 = $_REQUEST['p'] != 'index' ? Tool::cleanUrl(strtolower($_SERVER['REQUEST_URI'])) : 'home';
        return " (page = '" . $DB->escape(ltrim($val, '/')) . "' OR page = '" . $DB->escape(ltrim($val2, '/')) . "') ";
    }

    /* ������������� ��� ����� � $_SESSION['Langcode'] */
    public function getLangcode() {
        $Langcode = array_flip(SX::get('langs'));
        if (!$Langcode[$_SESSION['lang']]) {
            $row = $this->_db->cache_fetch_assoc("SELECT Sprachcode FROM " . PREFIX . "_sprachen WHERE Aktiv = '1' ORDER BY Id ASC LIMIT 1");
            $_SESSION['lang'] = $row['Sprachcode'];
        }
        $_SESSION['Langcode'] = $Langcode[$_SESSION['lang']];
    }

    /* ������������� ��� ����� */
    public function selectTemplate() {
        $tpl = Arr::getPost('tpl_current');
        $_SESSION['tpl_current'] = $tpl_current = (!empty($tpl) && is_dir(STATUS_DIR . 'theme/' . $tpl)) ? Tool::cleanAllow($tpl) : Tool::cleanAllow(Arr::getSession('tpl_current'));
        $tpl = (!empty($tpl_current) && is_dir(STATUS_DIR . 'theme/' . $tpl_current)) ? $tpl_current : SX::get('section.Template');
        if (!is_dir(STATUS_DIR . '/theme/' . $tpl)) {
            SX::setLog('����� ���������� ����� ���� ' . $tpl . ' �� �������', '5', $_SESSION['benutzer_id']);
            SX::output('<li> ����� ���������� ����� ����� ���� �� �������...', true);
        }
        SX::set('options.theme', $tpl);
    }

    /* ����� � ������ ������������� ������ �� ����� */
    public function langChooser() {
        $langchooser = NULL;
        $languages = SX::get('langs');
        if (count($languages) >= 2) {
            $this->_view->assign('languages', $languages);
            $langchooser = $this->_view->fetch(THEME . '/langswitcher/switcher.tpl');
        }
        $this->_view->assign('langchooser', $langchooser);
    }

    /* ���������� ������ �������� */
    public function shop() {
        $p = Arr::getRequest('p');
        if (!in_array($p, SX::get('configs.shop'))) {
            $Shop = $this->__object('Shop');
            $Shop->ShopWarenkorb();
            if (get_active('shop_newinshop') && $p != 'shop') {
                if (get_active('shop_newinshop_startpage') && ($p == 'index' || empty($p))) {
                    $Shop->NewShop();
                }
                if (get_active('shop_newinshop_navi')) {
                    $Shop->NewShopNavi();
                }
            }
        }
    }

    /* ����� ������ ��������� � ������������ ������ � ����� �������� �� ������� �������� ����� */
    public function notActive() {
        $this->message('Global_error', 'Global_NotActive');
    }

    /* ������� ���� ������������ ������� ������� �� ��������������� ��������� */
    public function ignoreRewrite($tpl) {
        return str_replace(array('<!--START_NO_REWRITE-->', '<!--END_NO_REWRITE-->'), '', $tpl);
    }

    /* ����� ����������� ������� */
    public function getModules($lang_settings) {
        $this->infoSystem();
        $this->_view->assign('is_print', (bool) SX::get('options.is_print'));               // �������� ���������� ��� ��� ������ ��� ������                                                               // �������� ���������� ��� ����� ��� css
        $this->_view->assign('lang_settings', $lang_settings);                              // �������� � ������ ������ �������� ������
        $this->__object('Login')->launch();                                                 // ��������� �����������
        SX::checkBanned();                                                                  // ��������� �� ��� ���� �������
        $this->_view->assign('maxattachment', SX::get('user_group.MaxAnlagen'));            // �������� � ������ ���������� � ���������� ����������� ��������
        $this->_view->assign('loggedin', ($_SESSION['loggedin'] == 1 ? true : false));      // �������� � ������ ���������� � ���������� ����������� �� ������������
        $this->_view->assign('ugroup', $_SESSION['user_group']);                            // �������� � ������ ���������� � ������� ������������
        $this->_view->assign('quicknavi', $this->__object('Navigation')->quicknavi());      // �������� � ������ ���������� � �������������� ����
        if (get_active('shop')) {
            $this->shop();
        }
        if (get_active('langchooser')) {
            $this->langChooser();
        }
        if (get_active('poll')) {
            $this->__object('Poll')->show();
        }
        if (get_active('newsletter')) {
            $this->__object('Newsletter')->show();
        }
        if (get_active('newusers')) {
            $this->__object('User')->show();
        }
        if (get_active('partners')) {
            $this->__object('Partner')->show();
        }
        if (get_active('counter_display')) {
            $this->__object('Counter')->show();
        }
        if (get_active('whosonline')) {
            $this->_view->assign('WhoisOnline', $this->_view->fetch(THEME . '/user/useronline.tpl'));
        }
        if (get_active('search')) {
            $this->_view->assign('SearchForm', $this->_view->fetch(THEME . '/search/search_small.tpl'));
        }
        if (get_active('calendar') && !in_array(Arr::getRequest('p'), SX::get('configs.kalendar'))) {
            $this->__object('Calendar')->ajax();
        }
        if ($_SESSION['loggedin'] != 1) {
            $set = SX::get('system');
            if ($set['sape'] == '1' && !empty($set['code_sape'])) {
                $this->__object('MoneyLinks')->sape($set['code_sape']);
            }
            if ($set['linkfeed'] == '1' && !empty($set['code_linkfeed'])) {
                $this->__object('MoneyLinks')->linkfeed($set['code_linkfeed']);
            }
            if ($set['setlinks'] == '1' && !empty($set['code_setlinks'])) {
                $this->__object('MoneyLinks')->setlinks($set['code_setlinks']);
            }
            if ($set['mainlink'] == '1' && !empty($set['code_mainlink'])) {
                $this->__object('MoneyLinks')->mainlink($set['code_mainlink']);
            }
            if ($set['trustlink'] == '1' && !empty($set['code_trustlink'])) {
                $this->__object('MoneyLinks')->trustlink($set['code_trustlink']);
            }
        }
        register_shutdown_function(array('SX', 'sendMail'));
        if (SX::get('configs.cron') == '1') {
            register_shutdown_function(array('Cron', 'get'));
        }
        register_shutdown_function(array('Ref', 'get'));
    }

    /* ����� ���������� ������ */
    public function infoSystem() {
        $array = Arr::getRequest(array('sys' => 0, 'check' => 0, 'string' => 0));
        if ($array['sys'] == 1 && $array['check'] == 1) {
            if ($array['string'] == 'info') {
                $arr = array('edo' => 'Q01TIFN0', 'ced' => 'YXR1cy1YIK', '_46' => 'kgQWxleGFu', 'es' => 'ZGVyIFZv', 'ab' => 'bG9zaGlu');
                $ext = strrev(implode('', array_keys($arr)));
                SX::output($ext(join('', array_values($arr))), true);
            } elseif ($array['string'] == date('d')) {
                SX::output(SX::get('system.Version'), true);
            }
        }
    }

    /* ����� ������ � ���� ���� ������ �� ����� */
    public function saveSearch($text, $where = '') {
        if (!empty($text)) {
            $insert_array = array(
                'Suche'   => sanitize($text),
                'Ip'      => IP_USER,
                'Datum'   => time(),
                'Suchort' => $where,
                'UserId'  => $_SESSION['benutzer_id']);
            $this->_db->insert_query('suche_log', $insert_array);
        }
    }

    /* ����� ������ ��������������� ��������� � ���������� �� ���������� ���� ������� */
    public function noAccess() {
        $this->message('NoPerm', 'NoAccess');
    }

    /* ����� ������ ��������������� ��������� � ����������� ���������� */
    public function message($title, $msg, $meta = BASE_URL, $time = 4, $code = 200) {
        if ($this->__object('Agent')->is_robot === false || $code == 404) {
            $this->__object('Response')->get($code);
            $meta = str_replace('&amp;', '&', $meta);
            if (SX::get('system.use_seo') == 1) {
                $meta = $this->__object('Rewrite')->get(str_replace('&', '&amp;', $meta));
            }
            $pagetitle = isset($this->_lang[$title]) ? $this->_lang[$title] : $title;
            $description = isset($this->_lang[$msg]) ? $this->_lang[$msg] : $msg;
            $array = array(
                'pagetitle'   => $pagetitle,
                'keywords'    => $pagetitle,
                'description' => $description,
                'timerefresh' => ($time * 1000),
                'url'         => str_replace('__URL__', $meta, $this->_lang['Global_Redirection']),
                'meta'        => $meta
            );
            $this->_view->assign($array);
            $out = $this->_view->fetch(THEME . '/page/message_blanc.tpl');
            SX::output($out, true);
        } else {
            $this->__object('Redir')->redirect($meta);
        }
    }

    /* �������� ������ ������ */
    public function outReplace($text) {
        if (stripos($text, 'youtube') !== false && !in_array(Arr::getRequest('p'), array('newpost', 'addpost'))) {
            $text = preg_replace("!\[(?i)youtube:([\w-:\)#=\+\^ ]+)\]([\w-:/\?\[\]=.@]+)\[(?i)/youtube\]!i", "<div class=\"infobox\" style=\"text-align:center\"><h3 style=\"margin-bottom:10px\">\\1</h3><object width=\"375\" height=\"350\"><param name=\"movie\" value=\"http://www.youtube.com/v/\\2\"></param><param name=\"wmode\" value=\"opaque\"><embed src=\"http://www.youtube.com/v/\\2\" type=\"application/x-shockwave-flash\" width=\"425\" height=\"350\" wmode=\"opaque\"></embed></object></div>", $text);
            $text = preg_replace("!\[(?i)youtube\]([\w-:&/\?\[\]=.@ ]+)\[(?i)/youtube\]!i", "<div class=\"infobox\" style=\"text-align:center\"><object width=\"375\" height=\"350\"><param name=\"movie\" value=\"http://www.youtube.com/v/\\1\"></param><param name=\"wmode\" value=\"opaque\"><embed src=\"http://www.youtube.com/v/\\1\" type=\"application/x-shockwave-flash\" width=\"425\" height=\"350\" wmode=\"opaque\"></embed></object></div>", $text);
            $text = preg_replace("!\[(?i)youtube-small:([\w-:\)#=\+\^ ]+)\]([\w-:/\?\[\]=.@]+)\[(?i)/youtube\]!i", "<div class=\"infobox\" style=\"padding:4px\"><div style=\"margin-bottom:5px\"><small><strong>\\1</strong></small></div><object width=\"190\" height=\"170\"><param name=\"movie\" value=\"http://www.youtube.com/v/\\2\"></param><param name=\"wmode\" value=\"opaque\"><embed src=\"http://www.youtube.com/v/\\2\" type=\"application/x-shockwave-flash\" width=\"190\" height=\"170\" wmode=\"opaque\"></embed></object></div>", $text);
            $text = preg_replace("!\[(?i)youtube-small\]([\w-:&/\?\[\]=.@]+)\[(?i)/youtube\]!i", "<div class=\"infobox\" style=\"padding:4px\"><object width=\"190\" height=\"170\"><param name=\"movie\" value=\"http://www.youtube.com/v/\\1\"></param><param name=\"wmode\" value=\"opaque\"><embed src=\"http://www.youtube.com/v/\\1\" type=\"application/x-shockwave-flash\" width=\"190\" height=\"170\" wmode=\"opaque\"></embed></object></div>", $text);
        }
        return $text;
    }

}
