<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (!defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}

abstract class Arr {

    /* ����� ���������� ����� ������� */
    public static function request($type = NULL) {
        if (empty($type)) {
            $result = strtoupper(self::getServer('REQUEST_METHOD'));
        } else {
            $type = strtoupper($type);
            if ($type == 'AJAX') {
                $result = self::getServer('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest' ? true : false;
            } else {
                $result = strtoupper(self::getServer('REQUEST_METHOD')) == $type ? true : false;
            }
        }
        return $result;
    }

    /* ����� ���������� ��������� �������� �� ������� */
    public static function rand(array $array, $limit = 1) {
        return array_rand(array_flip($array), $limit);
    }

    /* ����� ���������� �������� ������� �������� ��������� ���������� �������� */
    public static function get($array, $key, $default = NULL) {
        return !is_array($key) ? self::_get($array, $key, $default) : self::_getArr($array, $key, $default);
    }

    /* ����� ������������� �������� �������, �������� ��������� ���������� �������� */
    public static function set($array, $key, $value = NULL) {
        return !is_array($key) ? self::_set($array, $key, $value) : self::_setArr($array, $key);
    }

    /* ����� ��������� �� ������� �������� �������, �������� ������ ���������� */
    public static function nil($array, $key) {
        return !is_array($key) ? self::_nil($array, $key) : self::_nilArr($array, $key);
    }

    /* ����� ��������� �� ������������� �������� �������, �������� ������ ���������� */
    public static function has($array, $key) {
        return !is_array($key) ? self::_has($array, $key) : self::_hasArr($array, $key);
    }

    /* ����� ������� �������� �������, �������� ������ ���������� */
    public static function del(&$array, $key) {
        foreach ((array) $key as $value) {
            unset($array[$value]);
        }
    }

    /* ����� ���������� �������� ������� $_POST, �������� ��������� ���������� �������� */
    public static function getPost($key, $default = NULL) {
        return !is_array($key) ? self::_get($_POST, $key, $default) : self::_getArr($_POST, $key, $default);
    }

    /* ����� ������������� �������� ������� $_POST, �������� ��������� ���������� �������� */
    public static function setPost($key, $value = NULL) {
        return !is_array($key) ? self::_set($_POST, $key, $value) : self::_setArr($_POST, $key);
    }

    /* ����� ��������� �� ������� ��������� ������� $_POST, �������� ������ ���������� */
    public static function nilPost($key) {
        return !is_array($key) ? self::_nil($_POST, $key) : self::_nilArr($_POST, $key);
    }

    /* ����� ��������� �� ������������� �������� ������� $_POST, �������� ������ ���������� */
    public static function hasPost($key) {
        return !is_array($key) ? self::_has($_POST, $key) : self::_hasArr($_POST, $key);
    }

    /* ����� ������� �������� ������� $_POST, �������� ������ ���������� */
    public static function delPost($key) {
        foreach ((array) $key as $value) {
            unset($_POST[$value]);
        }
    }

    /* ����� ���������� �������� ������� $_GET, �������� ��������� ���������� �������� */
    public static function getGet($key, $default = NULL) {
        return !is_array($key) ? self::_get($_GET, $key, $default) : self::_getArr($_GET, $key, $default);
    }

    /* ����� ������������� �������� ������� $_GET, �������� ��������� ���������� �������� */
    public static function setGet($key, $value = NULL) {
        return !is_array($key) ? self::_set($_GET, $key, $value) : self::_setArr($_GET, $key);
    }

    /* ����� ��������� �� ������� ��������� ������� $_GET, �������� ������ ���������� */
    public static function nilGet($key) {
        return !is_array($key) ? self::_nil($_GET, $key) : self::_nilArr($_GET, $key);
    }

    /* ����� ��������� �� ������������� �������� ������� $_GET, �������� ������ ���������� */
    public static function hasGet($key) {
        return !is_array($key) ? self::_has($_GET, $key) : self::_hasArr($_GET, $key);
    }

    /* ����� ������� �������� ������� $_GET, �������� ������ ���������� */
    public static function delGet($key) {
        foreach ((array) $key as $value) {
            unset($_GET[$value]);
        }
    }

    /* ����� ���������� �������� ������� $_REQUEST, �������� ��������� ���������� �������� */
    public static function getRequest($key, $default = NULL) {
        return !is_array($key) ? self::_get($_REQUEST, $key, $default) : self::_getArr($_REQUEST, $key, $default);
    }

    /* ����� ������������� �������� ������� $_REQUEST, �������� ��������� ���������� �������� */
    public static function setRequest($key, $value = NULL) {
        return !is_array($key) ? self::_set($_REQUEST, $key, $value) : self::_setArr($_REQUEST, $key);
    }

    /* ����� ��������� �� ������� ��������� ������� $_REQUEST, �������� ������ ���������� */
    public static function nilRequest($key) {
        return !is_array($key) ? self::_nil($_REQUEST, $key) : self::_nilArr($_GET, $_REQUEST);
    }

    /* ����� ��������� �� ������������� �������� ������� $_REQUEST, �������� ������ ���������� */
    public static function hasRequest($key) {
        return !is_array($key) ? self::_has($_REQUEST, $key) : self::_hasArr($_REQUEST, $key);
    }

    /* ����� ������� �������� ������� $_REQUEST, �������� ������ ���������� */
    public static function delRequest($key) {
        foreach ((array) $key as $value) {
            unset($_REQUEST[$value]);
        }
    }

    /* ����� ���������� �������� ������� $_SESSION, �������� ��������� ���������� �������� */
    public static function getSession($key, $default = NULL) {
        return !is_array($key) ? self::_get($_SESSION, $key, $default) : self::_getArr($_SESSION, $key, $default);
    }

    /* ����� ������������� �������� ������� $_SESSION, �������� ��������� ���������� �������� */
    public static function setSession($key, $value = NULL) {
        return !is_array($key) ? self::_set($_SESSION, $key, $value) : self::_setArr($_SESSION, $key);
    }

    /* ����� ��������� �� ������� ��������� ������� $_SESSION, �������� ������ ���������� */
    public static function nilSession($key) {
        return !is_array($key) ? self::_nil($_SESSION, $key) : self::_nilArr($_GET, $_SESSION);
    }

    /* ����� ��������� �� ������������� �������� ������� $_SESSION, �������� ������ ���������� */
    public static function hasSession($key) {
        return !is_array($key) ? self::_has($_SESSION, $key) : self::_hasArr($_SESSION, $key);
    }

    /* ����� ������� �������� ������� $_SESSION, �������� ������ ���������� */
    public static function delSession($key) {
        foreach ((array) $key as $value) {
            unset($_SESSION[$value]);
        }
    }

    /* ����� ���������� �������� ������� $_SERVER, �������� ��������� ���������� �������� */
    public static function getServer($key, $default = NULL) {
        return !is_array($key) ? self::_get($_SERVER, $key, $default) : self::_getArr($_SERVER, $key, $default);
    }

    /* ����� ������������� �������� ������� $_SERVER, �������� ��������� ���������� �������� */
    public static function setServer($key, $value = NULL) {
        return !is_array($key) ? self::_set($_SERVER, $key, $value) : self::_setArr($_SERVER, $key);
    }

    /* ����� ��������� �� ������� ��������� ������� $_SERVER, �������� ������ ���������� */
    public static function nilServer($key) {
        return !is_array($key) ? self::_nil($_SERVER, $key) : self::_nilArr($_GET, $key);
    }

    /* ����� ��������� �� ������������� �������� ������� $_SERVER, �������� ������ ���������� */
    public static function hasServer($key) {
        return !is_array($key) ? self::_has($_SERVER, $key) : self::_hasArr($_SERVER, $key);
    }

    /* ����� ������� �������� ������� $_SERVER, �������� ������ ���������� */
    public static function delServer($key) {
        foreach ((array) $key as $value) {
            unset($_SERVER[$value]);
        }
    }

    /* ����� ��������� ��������� ������� $_COOKIE, �������� ��������� ���������� �������� */
    public static function getCookie($key, $default = NULL) {
        return !is_array($key) ? self::_get($_COOKIE, $key, $default) : self::_getArr($_COOKIE, $key, $default);
    }

    /* ����� ��������� ��������� ������� $_COOKIE */
    public static function setCookie($key, $value, $time = 86400, $path = NULL, $domain = NULL, $secure = false, $httponly = false) {
        if (is_numeric($time)) {
            $time += time();
        } else {
            $time = strtotime($time);
            $time = is_numeric($time) ? $time : 86400;
        }
        if (empty($path)) {
            $path = BASE_PATH;
        }
        $send = setcookie($key, $value, $time, $path, $domain, $secure, $httponly);
        if ($send) {
            $_COOKIE[$key] = $value;
        }
        return $send;
    }

    /* ����� ��������� �� ������� ��������� ������� $_COOKIE, �������� ������ ���������� */
    public static function nilCookie($key) {
        return !is_array($key) ? self::_nil($_COOKIE, $key) : self::_nilArr($_COOKIE, $key);
    }

    /* ����� ��������� �� ������������� �������� ������� $_COOKIE, �������� ������ ���������� */
    public static function hasCookie($key) {
        return !is_array($key) ? self::_has($_COOKIE, $key) : self::_hasArr($_COOKIE, $key);
    }

    /* ����� �������� ��������� ������� $_COOKIE */
    public static function delCookie($key, $path = NULL, $domain = NULL, $secure = false, $httponly = false) {
        $time = (3600 * 24 * 365) + time();
        if (empty($path)) {
            $path = BASE_PATH;
        }
        $send = setcookie($key, NULL, $time, $path, $domain, $secure, $httponly);
        if ($send) {
            unset($_COOKIE[$key]);
        }
        return $send;
    }

    /* ����� ���������� �������� ������� �� ����� */
    protected static function _get($array, $key, $default = NULL) {
        return isset($array[$key]) ? $array[$key] : $default;
    }

    /* ����� ���������� ������ �������� �� ������� ������� */
    protected static function _getArr($array, $keys, $default = NULL) {
        $result = array();
        foreach ((array) $keys as $key => $val) {
            if (is_string($key)) {
                $result[$key] = self::_get($array, $key, $val);
            } else {
                $result[$val] = self::_get($array, $val, $default);
            }
        }
        return $result;
    }

    /* ����� ������������� �������� ������� �� ����� */
    protected static function _set(&$array, $key, $value = NULL) {
        $array[$key] = $value;
    }

    /* ����� ������������� ������ �������� �� ������� ������� */
    protected static function _setArr(&$array, $keys) {
        foreach ($keys as $key => $val) {
            self::_set($array, $key, $val);
        }
    }

    /* ����� �������� �� ������� ��������� � ������� */
    protected static function _nil($array, $key) {
        return empty($array[$key]);
    }

    /* ����� �������� �������� �� ������� ���������� � ������� */
    protected static function _nilArr($array, $keys) {
        $result = false;
        foreach ((array) $keys as $value) {
            if (($result = self::_nil($array, $value)) !== true) {
                return false;
            }
        }
        return $result;
    }

    /* ����� �������� ������������� ��������� � ������� */
    protected static function _has($array, $key) {
        return isset($array[$key]);
    }

    /* ����� �������� �������� ������������� ���������� � ������� */
    protected static function _hasArr($array, $keys) {
        $result = false;
        foreach ((array) $keys as $value) {
            if (($result = self::_has($array, $value)) !== true) {
                return false;
            }
        }
        return $result;
    }

}
