<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (!defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}

class MoneyLinks extends Magic {

    /* ������� ������� ������ Sape */
    public function sape($code) {
        SX::setDefine('_SAPE_USER', $code);
        $this->_view->assign('Sape_links', $this->aktive($this->__object('SAPE_client')->return_links()));
    }

    /* ������� ������� ������ Linkfeed */
    public function linkfeed($code) {
        SX::setDefine('LINKFEED_USER', $code);
        $this->_view->assign('Linkfeed_links', $this->aktive($this->__object('LinkfeedClient')->return_links()));
    }

    /* ������� ������� ������ Setlinks */
    public function setlinks($code) {
        SX::setDefine('SETLINKS_USER', $code);
        $this->_view->assign('setlinks_links', $this->aktive($this->__object('SLClient')->GetLinks(0, '<br />')));
    }

    /* ������� ������� ������ Mainlink */
    public function mainlink($code) {
        SX::setDefine('SECURE_CODE', $code);
        $this->_view->assign('mainlink_links', $this->aktive($this->__object('ML')->Get_Links()));
    }

    /* ������� ������� ������ Trustlink */
    public function trustlink($code) {
        SX::setDefine('TRUSTLINK_USER', $code);
        $o['charset'] = $_SESSION['Charset'];
        $trustlink = new TrustlinkClient($o);
        unset($o);
        $this->_view->assign('trustlink_links', $this->aktive(Tool::win1251($trustlink->build_links())));
    }

    /* ��������� �� ������� �������� */
    protected function aktive($value) {
        static $load = false;
        if (!empty($value) && $load === false) {
            $load = true;
            $this->_view->assign('AktiveLink', 1);
        }
        return $value;
    }

}
