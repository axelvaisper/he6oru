<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (!defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}

class PaymentBank extends Magic {

    public function pd4($ref_url) {
        $error = true;
        if (stristr($_SERVER['HTTP_REFERER'], $ref_url)) {
            $error = false;
        }
        if ($error == true) {
            $this->_view->assign('payment_error', 1);
            SX::setDefine('PAYMENT_ERROR', 1);
        }

        $seo_array = array(
            'headernav' => $this->_lang['Shop_thankyou_title'],
            'pagetitle' => $this->_lang['Shop_thankyou_title'],
            'content'   => $this->_view->fetch(THEME . '/payment/payment_sberbank.tpl'));
        $this->_view->finish($seo_array);
    }

    public function account($ref_url) {
        $error = true;
        if (stristr($_SERVER['HTTP_REFERER'], $ref_url)) {
            $error = false;
        }
        if ($error == true) {
            $this->_view->assign('payment_error', 1);
            SX::setDefine('PAYMENT_ERROR', 1);
        }

        $seo_array = array(
            'headernav' => $this->_lang['Shop_thankyou_title'],
            'pagetitle' => $this->_lang['Shop_thankyou_title'],
            'content'   => $this->_view->fetch(THEME . '/payment/payment_bank.tpl'));
        $this->_view->finish($seo_array);
    }

    protected function number2string($n, $rod) {
        $a = floor($n / 100);
        $b = floor(($n - $a * 100) / 10);
        $c = $n % 10;
        $s = '';
        switch ($a) {
            case 1:
                $s = '���';
                break;
            case 2:
                $s = '������';
                break;
            case 3:
                $s = '������';
                break;
            case 4:
                $s = '���������';
                break;
            case 5:
                $s = '�������';
                break;
            case 6:
                $s = '��������';
                break;
            case 7:
                $s = '�������';
                break;
            case 8:
                $s = '���������';
                break;
            case 9:
                $s = '���������';
                break;
        }
        $s .= ' ';
        if ($b != 1) {
            switch ($b) {
                case 1:
                    $s .= '������';
                    break;
                case 2:
                    $s .= '��������';
                    break;
                case 3:
                    $s .= '��������';
                    break;
                case 4:
                    $s .= '�����';
                    break;
                case 5:
                    $s .= '���������';
                    break;
                case 6:
                    $s .= '����������';
                    break;
                case 7:
                    $s .= '���������';
                    break;
                case 8:
                    $s .= '�����������';
                    break;
                case 9:
                    $s .= '���������';
                    break;
            }
            $s .= ' ';
            switch ($c) {
                case 1:
                    $s .= $rod ? '����' :
                            '����';
                    break;
                case 2:
                    $s .= $rod ? '���' :
                            '���';
                    break;
                case 3:
                    $s .= '���';
                    break;
                case 4:
                    $s .= '������';
                    break;
                case 5:
                    $s .= '����';
                    break;
                case 6:
                    $s .= '�����';
                    break;
                case 7:
                    $s .= '����';
                    break;
                case 8:
                    $s .= '������';
                    break;
                case 9:
                    $s .= '������';
                    break;
            }
        } else {
            switch ($c) {
                case 0:
                    $s .= '������';
                    break;
                case 1:
                    $s .= '�����������';
                    break;
                case 2:
                    $s .= '����������';
                    break;
                case 3:
                    $s .= '����������';
                    break;
                case 4:
                    $s .= '������������';
                    break;
                case 5:
                    $s .= '�����������';
                    break;
                case 6:
                    $s .= '������������';
                    break;
                case 7:
                    $s .= '�����������';
                    break;
                case 8:
                    $s .= '�������������';
                    break;
                case 9:
                    $s .= '�������������';
                    break;
            }
        }
        return $s;
    }

    public function convert($n) {
        $billions = floor($n / 1000000000);
        $millions = floor(($n - $billions * 1000000000) / 1000000);
        $grands = floor(($n - $billions * 1000000000 - $millions * 1000000) / 1000);
        $roubles = floor(($n - $billions * 1000000000 - $millions * 1000000 - $grands * 1000));
        $kop = round($n * 100 - round(floor($n) * 100));
        if ($kop < 10) {
            $kop = '0' . (string) $kop;
        }
        $s = '';
        if ($billions > 0) {
            $t = '��';
            $temp = $billions % 10;
            if (floor(($billions % 100) / 10) != 1) {
                if ($temp == 1) {
                    $t = '';
                } elseif ($temp >= 2 && $temp <= 4) {
                    $t = '�';
                }
            }
            $s .= $this->number2string($billions, 1) . " ��������$t ";
        }
        if ($millions > 0) {
            $t = '��';
            $temp = $millions % 10;
            if (floor(($millions % 100) / 10) != 1) {
                if ($temp == 1) {
                    $t = '';
                } elseif ($temp >= 2 && $temp <= 4) {
                    $t = '�';
                }
            }
            $s .= $this->number2string($millions, 1) . " �������$t ";
        }
        if ($grands > 0) {
            $t = '';
            $temp = $grands % 10;
            if (floor(($grands % 100) / 10) != 1) {
                if ($temp == 1) {
                    $t = '�';
                } elseif ($temp >= 2 && $temp <= 4) {
                    $t = '�';
                }
            }
            $s .= $this->number2string($grands, 0) . " �����$t ";
        }
        if ($roubles > 0) {
            $rub = '��';
            $temp = $roubles % 10;
            if (floor(($roubles % 100) / 10) != 1) {
                if ($temp == 1) {
                    $rub = '�';
                } elseif ($temp >= 2 && $temp <= 4) {
                    $rub = '�';
                }
            }
            $s .= $this->number2string($roubles, 1) . " ����$rub ";
        } {
            $kp = '��';
            $temp = $kop % 10;
            if (floor(($kop % 100) / 10) != 1) {
                if ($temp == 1) {
                    $kp = '���';
                } elseif ($temp >= 2 && $temp <= 4) {
                    $kp = '���';
                }
            }
            $s .= "$kop ����$kp";
        }

        if ($roubles > 0 || $grands > 0 || $millions > 0 || $billions > 0) {
            $cnt = 0;
            while ($s[$cnt] == ' ') {
                $cnt++;
            }
            $s[$cnt] = chr(ord($s[$cnt]) - 32);
        }
        return $s;
    }

}
