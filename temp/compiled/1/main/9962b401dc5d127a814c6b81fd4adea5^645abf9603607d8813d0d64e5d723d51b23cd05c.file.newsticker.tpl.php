<?php /* Smarty version Smarty-3.1.18, created on 2017-02-24 16:12:23
         compiled from "/home/vampireos/www/he6oru.localhost/theme/standard/news/newsticker.tpl" */ ?>
<?php /*%%SmartyHeaderCode:197334321458b03137992878-53419241%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '645abf9603607d8813d0d64e5d723d51b23cd05c' => 
    array (
      0 => '/home/vampireos/www/he6oru.localhost/theme/standard/news/newsticker.tpl',
      1 => 1407622609,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '197334321458b03137992878-53419241',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'newsitems' => 0,
    'news_count' => 0,
    'news' => 0,
    'DateTemp' => 0,
    'lang_settings' => 0,
    'length' => 0,
    'area' => 0,
    'imgpath' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_58b031379d1220_63009952',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58b031379d1220_63009952')) {function content_58b031379d1220_63009952($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home/vampireos/www/he6oru.localhost/lib/smarty/plugins/modifier.date_format.php';
if (!is_callable('smarty_modifier_truncate')) include '/home/vampireos/www/he6oru.localhost/lib/smarty/plugins/modifier.truncate.php';
if (!is_callable('smarty_modifier_sslash')) include '/home/vampireos/www/he6oru.localhost/lib/smarty/statusplugins/modifier.sslash.php';
?><div class="box_innerhead"><?php echo $_smarty_tpl->getConfigVariable('Newsarchive');?>
</div>
<?php $_smarty_tpl->tpl_vars['news_count'] = new Smarty_variable(0, null, 0);?>
<?php  $_smarty_tpl->tpl_vars['news'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['news']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['newsitems']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['news']->key => $_smarty_tpl->tpl_vars['news']->value) {
$_smarty_tpl->tpl_vars['news']->_loop = true;
?>
  <?php $_smarty_tpl->tpl_vars['length'] = new Smarty_variable(400, null, 0);?>
  <?php $_smarty_tpl->tpl_vars['news_count'] = new Smarty_variable($_smarty_tpl->tpl_vars['news_count']->value+1, null, 0);?>
  <?php $_smarty_tpl->tpl_vars['year'] = new Smarty_variable(smarty_modifier_date_format($_smarty_tpl->tpl_vars['news']->value['ZeitStart'],"%Y"), null, 0);?>
  <?php if (smarty_modifier_date_format($_smarty_tpl->tpl_vars['news']->value['ZeitStart'],"%d.%Y")!=(($tmp = @$_smarty_tpl->tpl_vars['DateTemp']->value)===null||$tmp==='' ? '' : $tmp)) {?>
    <div class="newsticker_header"><strong> <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['news']->value['ZeitStart'],$_smarty_tpl->tpl_vars['lang_settings']->value['Zeitformat']);?>
</strong></div>
  <?php }?>
  <div class="news_startpage">
    <h3><a class="ticker" href="index.php?p=news&amp;area=<?php echo $_smarty_tpl->tpl_vars['news']->value['Sektion'];?>
&amp;newsid=<?php echo $_smarty_tpl->tpl_vars['news']->value['Id'];?>
&amp;name=<?php echo translit($_smarty_tpl->tpl_vars['news']->value['LinkTitle']);?>
"><?php echo sanitize($_smarty_tpl->tpl_vars['news']->value['Titel']);?>
</a></h3>
    <br />
    <?php if (!empty($_smarty_tpl->tpl_vars['news']->value['Bild'])) {?>
      <?php $_smarty_tpl->tpl_vars['length'] = new Smarty_variable(220, null, 0);?>
      <span class="newsstart_icon"><a href="index.php?p=news&amp;area=<?php echo $_smarty_tpl->tpl_vars['news']->value['Sektion'];?>
&amp;newsid=<?php echo $_smarty_tpl->tpl_vars['news']->value['Id'];?>
&amp;name=<?php echo translit($_smarty_tpl->tpl_vars['news']->value['LinkTitle']);?>
"><img  style="<?php if ($_smarty_tpl->tpl_vars['news']->value['BildAusrichtung']=='right') {?>margin: 0 0 5px 5px<?php } else { ?>margin: 0 5px 5px 0<?php }?>" src="<?php echo $_smarty_tpl->tpl_vars['news']->value['Thumb'];?>
" alt="" align="<?php echo (($tmp = @$_smarty_tpl->tpl_vars['news']->value['BildAusrichtung'])===null||$tmp==='' ? 'left' : $tmp);?>
" /></a></span>
        <?php }?>
    <div class="justify news_startpage_text">
      <?php if ($_smarty_tpl->tpl_vars['news']->value['Intro']) {?>
        <?php echo smarty_modifier_sslash(smarty_modifier_truncate($_smarty_tpl->tpl_vars['news']->value['Intro'],$_smarty_tpl->tpl_vars['length']->value));?>

      <?php } else { ?>
        <?php echo smarty_modifier_sslash(smarty_modifier_truncate($_smarty_tpl->tpl_vars['news']->value['News'],$_smarty_tpl->tpl_vars['length']->value));?>

      <?php }?>
    </div>
    <div class="clear"></div>
    <div class="newsstart_footer">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td align="left">
            <?php echo $_smarty_tpl->getConfigVariable('GlobalAutor');?>
: <a href="index.php?p=user&amp;id=<?php echo $_smarty_tpl->tpl_vars['news']->value['Autor'];?>
&amp;area=<?php echo $_smarty_tpl->tpl_vars['area']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['news']->value['User'];?>
</a>,
            <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['news']->value['Zeit'],'%H:%M');?>
 | <?php echo $_smarty_tpl->tpl_vars['news']->value['Hits'];?>
 <?php echo $_smarty_tpl->getConfigVariable('Hits');?>

            <?php if (!empty($_smarty_tpl->tpl_vars['news']->value['Kommentare'])) {?> | <a title="<?php echo $_smarty_tpl->getConfigVariable('Comments');?>
" href="index.php?p=news&amp;area=<?php echo $_smarty_tpl->tpl_vars['news']->value['Sektion'];?>
&amp;newsid=<?php echo $_smarty_tpl->tpl_vars['news']->value['Id'];?>
&amp;name=<?php echo translit($_smarty_tpl->tpl_vars['news']->value['LinkTitle']);?>
#comments"><?php echo $_smarty_tpl->getConfigVariable('Comments');?>
</a><?php }?>
          </td>
          <td align="right">
            <img class="absmiddle" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/page/arrow_right_small.png" alt="<?php echo $_smarty_tpl->getConfigVariable('ReadAll');?>
" />
            <a title="<?php echo $_smarty_tpl->getConfigVariable('ReadAll');?>
" href="index.php?p=news&amp;area=<?php echo $_smarty_tpl->tpl_vars['news']->value['Sektion'];?>
&amp;newsid=<?php echo $_smarty_tpl->tpl_vars['news']->value['Id'];?>
&amp;name=<?php echo translit($_smarty_tpl->tpl_vars['news']->value['LinkTitle']);?>
"><?php echo $_smarty_tpl->getConfigVariable('ReadAll');?>
</a>&nbsp;
            <img class="absmiddle" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/page/print.png" alt="<?php echo $_smarty_tpl->getConfigVariable('Print');?>
" />
            <a title="<?php echo $_smarty_tpl->getConfigVariable('Print');?>
" target="_blank" href="index.php?p=news&amp;area=<?php echo $_smarty_tpl->tpl_vars['news']->value['Sektion'];?>
&amp;newsid=<?php echo $_smarty_tpl->tpl_vars['news']->value['Id'];?>
&amp;name=<?php echo translit($_smarty_tpl->tpl_vars['news']->value['LinkTitle']);?>
&amp;print=1"><?php echo $_smarty_tpl->getConfigVariable('Print');?>
</a>
          </td>
        </tr>
      </table>
    </div>
  </div>
  <?php $_smarty_tpl->tpl_vars['DateTemp'] = new Smarty_variable(smarty_modifier_date_format($_smarty_tpl->tpl_vars['news']->value['ZeitStart'],"%d.%Y"), null, 0);?>
  <?php $_smarty_tpl->tpl_vars['YearTemp'] = new Smarty_variable(smarty_modifier_date_format($_smarty_tpl->tpl_vars['news']->value['ZeitStart'],"%Y"), null, 0);?>
<?php } ?>
<br style="clear: both" />
<?php }} ?>
