<?php /* Smarty version Smarty-3.1.18, created on 2017-02-25 15:10:10
         compiled from "/home/vampireos/www/he6oru.localhost/setup/theme/step2.tpl" */ ?>
<?php /*%%SmartyHeaderCode:148524834858aef730e17f94-92362767%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '364849b18b6399452bdd76a6769d2c8523e83d2c' => 
    array (
      0 => '/home/vampireos/www/he6oru.localhost/setup/theme/step2.tpl',
      1 => 1488031807,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '148524834858aef730e17f94-92362767',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_58aef730efa1c9_70773408',
  'variables' => 
  array (
    'errors_path' => 0,
    'error_not_writables' => 0,
    'nw' => 0,
    'setupdir' => 0,
    'dfg' => 0,
    'db_no_connection' => 0,
    'lang' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58aef730efa1c9_70773408')) {function content_58aef730efa1c9_70773408($_smarty_tpl) {?><script>
<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['theme']->value)."/validate.txt", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

$.validator.setDefaults({
  submitHandler: function() {
    document.forms['Form'].submit();
  }
});
$(document).ready(function() {
  $('#Form').validate({
    rules: {
      dbhost:   { required placeholder=" ": true },
      dbuser:   { required placeholder=" ": true },
      dbname:   { required placeholder=" ": true },
      dbprefix: { required placeholder=" ": true }
    },
    messages: { }
  });
});
</script>

<h1><?php echo $_smarty_tpl->getConfigVariable('Step2');?>
</h1>

<?php if (!empty($_smarty_tpl->tpl_vars['errors_path']->value)) {?>

<div class="errorbox">
  <h3><?php echo $_smarty_tpl->getConfigVariable('Errors');?>
</h3>
  <br />
   <?php echo $_smarty_tpl->getConfigVariable('Step1ErrInf');?>

</div>

<div class="eula">
<?php  $_smarty_tpl->tpl_vars['nw'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['nw']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['error_not_writables']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['nw']->key => $_smarty_tpl->tpl_vars['nw']->value) {
$_smarty_tpl->tpl_vars['nw']->_loop = true;
?>
  <div class="error_nw"><?php echo $_smarty_tpl->tpl_vars['nw']->value;?>
</div>
<?php } ?>
</div>

<form method="post" action="<?php echo $_smarty_tpl->tpl_vars['setupdir']->value;?>
/setup.php">
  <input type="hidden" name="step" value="1" />
  <input type="submit" value="<?php echo $_smarty_tpl->getConfigVariable('Step1ErrB');?>
" btnx/>
</form>

<?php } else { ?>
<?php echo $_smarty_tpl->tpl_vars['dfg']->value;?>

<form autocomplete="off" name="Form" id="Form"  method="post" action="<?php echo $_smarty_tpl->tpl_vars['setupdir']->value;?>
/setup.php" cardx style="width:370px">

<?php if (isset($_smarty_tpl->tpl_vars['db_no_connection']->value)&&$_smarty_tpl->tpl_vars['db_no_connection']->value==1) {?>
  <div class="error_conn">
  <?php echo $_smarty_tpl->getConfigVariable('icon_alert');?>
 <?php echo $_smarty_tpl->getConfigVariable('Step1_NoConn');?>

  </div>
<?php }?>

<label title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Step1_ainf']);?>
">
<small><?php echo $_smarty_tpl->getConfigVariable('Step1_a');?>
</small>
<input  type="text" name="dbhost" value="<?php echo (($tmp = @sanitize($_POST['dbhost']))===null||$tmp==='' ? 'localhost' : $tmp);?>
"/>
</label>

<label title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Step1_pinf']);?>
">
<small><?php echo $_smarty_tpl->getConfigVariable('Step1_p');?>
</small>
<input type="text" name="dbport" value="<?php echo (($tmp = @sanitize($_POST['dbport']))===null||$tmp==='' ? '3306' : $tmp);?>
" />
</label>

<label title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Step1_binf']);?>
">
<small><?php echo $_smarty_tpl->getConfigVariable('Step1_b');?>
</small>
<input type="text" name="dbuser" value="<?php echo (($tmp = @sanitize($_POST['dbuser']))===null||$tmp==='' ? 'root' : $tmp);?>
" />
</label>

<label title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Step1_cinf']);?>
">
<small><?php echo $_smarty_tpl->getConfigVariable('Step1_c');?>
</small>
<input type="text" name="dbpass" value="<?php if (isset($_POST['dbpass'])) {?><?php echo sanitize($_POST['dbpass']);?>
<?php }?>" />
</label>

<label title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Step1_dinf']);?>
">
<small><?php echo $_smarty_tpl->getConfigVariable('Step1_d');?>
</small>
<input type="text" name="dbname" value="<?php echo (($tmp = @sanitize($_POST['dbname']))===null||$tmp==='' ? 'phpmyadmin' : $tmp);?>
" />
</label>

<label title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Step1_einf']);?>
">
<small><?php echo $_smarty_tpl->getConfigVariable('Step1_e');?>
</small>
<input type="text" name="dbprefix" value="<?php echo (($tmp = @sanitize($_POST['dbprefix']))===null||$tmp==='' ? 'sx011' : $tmp);?>
" />
</label>

<label title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Step1_ginf']);?>
" selectx>
<small><?php echo $_smarty_tpl->getConfigVariable('Step1_g');?>
</small>
<select name="type_sess">
<option value="auto" <?php if (!isset($_POST['type_sess'])) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->getConfigVariable('AutoSessions');?>
</option>
<option value="base" <?php if (isset($_POST['type_sess'])&&$_POST['type_sess']=='base') {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->getConfigVariable('BaseSessions');?>
</option>
<option value="file" <?php if (isset($_POST['type_sess'])&&$_POST['type_sess']=='file') {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->getConfigVariable('FileSessions');?>
</option>
</select>
</label>

<p>
<input type="hidden" name="step" value="3" />
<input type="submit" value="<?php echo $_smarty_tpl->getConfigVariable('Step1_Button');?>
" btnx/>
</p>

</form>
<?php }?>
<?php }} ?>
