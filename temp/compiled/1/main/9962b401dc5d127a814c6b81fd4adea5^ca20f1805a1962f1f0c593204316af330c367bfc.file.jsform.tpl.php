<?php /* Smarty version Smarty-3.1.18, created on 2017-02-24 16:12:37
         compiled from "/home/vampireos/www/he6oru.localhost/theme/standard/other/jsform.tpl" */ ?>
<?php /*%%SmartyHeaderCode:204947361558b0314587ac74-45514064%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ca20f1805a1962f1f0c593204316af330c367bfc' => 
    array (
      0 => '/home/vampireos/www/he6oru.localhost/theme/standard/other/jsform.tpl',
      1 => 1407104673,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '204947361558b0314587ac74-45514064',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_58b03145895380_60058715',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58b03145895380_60058715')) {function content_58b03145895380_60058715($_smarty_tpl) {?>$(document).ready(function() { // ����� ������� �������
    $('#msgform').on('click select keyup', function() {
        saveCaret();
    });
});

function toggleSmiles(click, target) { // ���� ������ �������
    $(document).ready(function() {
        var timer = 0;
        $('#' + target).slideToggle(300);
        $(document).on('click', function(e) {
            if ($(e.target).closest('#' + click + ', #' + target).length === 0) {
                setCaret(0);
                $('#' + target).slideUp(300);
                e.stopPropagation();
            }
        });
        $('#' + target).on('click', function() {
            setCaret(0);
            $('#' + target).slideUp(300);
        });
        $('#' + click + ', #' + target).mouseover(function() {
            clearTimeout(timer);
        }).mouseout(function() {
            setCaret(0);
            clearTimeout(timer);
            timer = setTimeout(function() {
                $('#' + target).slideUp(300);
            }, 1000);
        });
    });
}

var BBcode   = new Array();
BBcode.array = new Array(); // �������� �������� ����
BBcode.caret = 0;           // �������� ������� �������
BBcode.elem  = 'msgform';   // ������������� �����

function saveCaret() { // �������� ������� ������� � ���������
    var elem = document.getElementById(BBcode.elem);
    if (document.selection) {
        var select = document.selection.createRange();
        var duplic = select.duplicate();
        select.collapse(true);
        duplic.moveToElementText(elem);
        duplic.setEndPoint('EndToEnd', select);
        BBcode.caret = duplic.text.length;
    } else if (elem.selectionStart || elem.selectionStart === '0') {
        BBcode.caret = elem.selectionStart;
    } else {
        BBcode.caret = elem.value.length;
    }
}
function setCaret(pos, set) { // ������ ������� ��������� �������
    if (set === true) {
        BBcode.caret = 0;
    }
    BBcode.caret += pos;
    var elem = document.getElementById(BBcode.elem);
    if (elem.setSelectionRange) {
        elem.focus();
        elem.setSelectionRange(BBcode.caret, BBcode.caret);
    } else if (elem.createTextRange) {
        var range = elem.createTextRange();
        range.collapse(true);
        range.moveEnd('character', BBcode.caret);
        range.moveStart('character', BBcode.caret);
        range.select();
    }
    elem.focus();
}
function loadCaret(load) { // ��������� � ������� �������
    var elem = document.getElementById(BBcode.elem);
    var length = elem.value.length;
    var before = elem.value.substring(0, BBcode.caret);
    var ending = elem.value.substring(BBcode.caret, length);
    elem.value = before + load + ending;
}
function isSelect(open, close) { // ��� ������� ��������� �������� ������
    var select = '';
    var elem = document.getElementById(BBcode.elem);
    if (document.selection) {
        var select = document.selection.createRange().text;
        if (select.length > 0) {
            document.selection.createRange().text = open + select + close;
            setCaret(open.length);
        }
    } else if (elem.selectionStart) {
        var end = elem.selectionEnd;
        var start = elem.selectionStart;
        var length = elem.textLength;
        var before = elem.value.substring(0, start);
        var select = elem.value.substring(start, end);
        var ending = elem.value.substring(end, length);
        if (select.length > 0) {
            elem.value = before + open + select + close + ending;
            setCaret(open.length);
        }
    }
    return select.length > 0 ? true : false;
}
function loadSelect(open, close) { // �������� ������ ���������� �����
    if (isSelect(open, close) === false) {
        loadCaret(open + close);
        setCaret(open.length);
    }
}
function loadCode(code) { // ��������� ����
    loadCaret(code);
    setCaret(code.length);
}
function closeCode(code) { // ��������� ���
    if (BBcode.array.length > 0) {
        for (var key in BBcode.array) {
            if (BBcode.array[key] == code) {
                delete BBcode.array[key];
                loadCode('[/' + code + '] ');
                return true;
            }
        }
    }
    return false;
}
function closeCodes() { // ��������� ��� �������� ����
    var ende = '';
    var elem = document.getElementById(BBcode.elem);
    while (BBcode.array[0]) {
        ende = ' ';
        elem.value += '[/' + BBcode.array.pop() + ']';
    }
    elem.value += ende;
    setCaret(elem.value.length + 1, true);
}
function addCode(code) { // �������� ����� ������ � ������
    code = code.toLowerCase();
    switch (true) {
        case closeCode(code):                               // ���� ��� ������ ���������
            break;
        case isSelect('[' + code + ']', '[/' + code + ']'): // ���� ���� ��������� �������� ������
            break;
        default:
            switch (code) {
                case 'list':
                    addList();
                    break;
                case 'img':
                    addImg();
                    break;
                case 'url':
                    addUrl();
                    break;
                case 'hide':
                    addHide();
                    break;
                case 'email':
                case 'mail':
                    addMail();
                    break;
                case 'video':
                    addVideo();
                    break;
                default:
                    BBcode.array.push(code);
                    loadCode('[' + code + ']');
            }
            break;
    }
}
function addMail() {
    var mail = prompt('<?php echo $_smarty_tpl->getConfigVariable('Validate_email');?>
', '');
    if (mail) {
        return loadCode('[mail]' + mail + '[/mail] ');
    }
    alert('<?php echo $_smarty_tpl->getConfigVariable('Format_error_no_email');?>
');
    return false;
}
function addVideo() {
    var url = prompt('<?php echo $_smarty_tpl->getConfigVariable('Format_text_enter_youtube');?>
', '');
    if (!url) {
        alert('<?php echo $_smarty_tpl->getConfigVariable('Format_error_no_url');?>
');
        return false;
    }
    var video = prompt('<?php echo $_smarty_tpl->getConfigVariable('Format_text_enter_youtubeName');?>
', '');
    if (url.indexOf('youtu.be') !== -1) {
        var param = url.match(/^http[s]*:\/\/youtu\.be\/([a-z0-9_-]+)/i);
    } else {
        var param = url.match(/^http[s]*:\/\/www\.youtube\.com\/watch\?.*?v=([a-z0-9_-]+)/i);
    }
    if (param && param.length === 2) {
        if (video) {
            return loadCode('[youtube:' + video + ']' + param[1] + '[/youtube] ');
        } else {
            return loadCode('[youtube]' + param[1] + '[/youtube] ');
        }
    }
    alert('<?php echo $_smarty_tpl->getConfigVariable('Error');?>
');
    return false;
}
function addUrl() {
    var error = '';
    var url = prompt('<?php echo $_smarty_tpl->getConfigVariable('Format_enterUrl');?>
', 'http://');
    var name = prompt('<?php echo $_smarty_tpl->getConfigVariable('Format_enterUrlName');?>
', '<?php echo $_smarty_tpl->getConfigVariable('Format_enterUrlName2');?>
');
    if (!url) {
        error += '<?php echo $_smarty_tpl->getConfigVariable('Format_error_no_url');?>
\n';
    }
    if (!name) {
        error += '<?php echo $_smarty_tpl->getConfigVariable('Format_error_no_title');?>
\n';
    }
    if (!error) {
        return loadCode('[url=' + url + ']' + name + '[/url] ');
    }
    alert(error);
    return false;
}
function addHide() {
    var error = '';
    var hide = prompt('<?php echo $_smarty_tpl->getConfigVariable('Format_enterHide');?>
', '<?php echo $_smarty_tpl->getConfigVariable('Format_enterHide2');?>
');
    var text = prompt('<?php echo $_smarty_tpl->getConfigVariable('Format_enterHideName');?>
', '<?php echo $_smarty_tpl->getConfigVariable('Format_enterHideName2');?>
');
    if (!hide) {
        error += '<?php echo $_smarty_tpl->getConfigVariable('Format_error_no_hide');?>
\n';
    }
    if (!text) {
        error += '<?php echo $_smarty_tpl->getConfigVariable('Format_error_no_title_hide');?>
\n';
    }
    if (!error) {
        return loadCode('[hide=' + hide + ']' + text + '[/hide] ');
    }
    alert(error);
    return false;
}
function addImg() {
    var url = prompt('<?php echo $_smarty_tpl->getConfigVariable('Format_text_enter_image');?>
', 'http://');
    if (url) {
        return loadCode('[img]' + url + '[/img] ');
    }
    alert('<?php echo $_smarty_tpl->getConfigVariable('Format_error_no_url');?>
');
    return false;
}
function addList() {
    var type = prompt('<?php echo $_smarty_tpl->getConfigVariable('Format_listprompt');?>
', '');
    switch (type) {
        case 'a':
        case 'i':
        case 'A':
        case 'I':
        case '1':
            var list = '[list=' + type + ']\n';
            break;
        default:
            var list = '[list]\n';
            break;
    }
    var entry = '';
    while (true) {
        entry = prompt('<?php echo $_smarty_tpl->getConfigVariable('Format_listprompt2');?>
', '');
        if (!entry) {
            break;
        }
        list = list + '[*] ' + entry + '\n';
    }
    return loadCode(list + '[/list]\n');
}
function countComments(postmaxchars) {
    var elem = document.getElementById(BBcode.elem);
    var message = '';
    if (postmaxchars > 0) {
        message = '<?php echo $_smarty_tpl->getConfigVariable('Forums_max_Forums_post_length_t');?>
 ' + postmaxchars;
    }
    alert('<?php echo $_smarty_tpl->getConfigVariable('Forums_post_length_t');?>
 ' + elem.value.length + '\n' + message);
}
<?php }} ?>
