<?php /* Smarty version Smarty-3.1.18, created on 2017-02-24 16:12:37
         compiled from "/home/vampireos/www/he6oru.localhost/theme/standard/comments/format.tpl" */ ?>
<?php /*%%SmartyHeaderCode:195339503758b031458ae798-36039518%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1fb9d641ddfba204261326621d6fb9c120163ffb' => 
    array (
      0 => '/home/vampireos/www/he6oru.localhost/theme/standard/comments/format.tpl',
      1 => 1407012488,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '195339503758b031458ae798-36039518',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'lang' => 0,
    'imgpath' => 0,
    'settings' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_58b031458e7206_04496355',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58b031458e7206_04496355')) {function content_58b031458e7206_04496355($_smarty_tpl) {?><img class="format_buttons stip" title="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['tooltip'][0][0]->tooltip($_smarty_tpl->tpl_vars['lang']->value['Format_Tip_Bold']);?>
" onclick="addCode('b');" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/comment/text_bold.png" alt="" />
<img class="format_buttons stip" title="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['tooltip'][0][0]->tooltip($_smarty_tpl->tpl_vars['lang']->value['Format_Tip_Italic']);?>
" onclick="addCode('i');"  src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/comment/text_kursive.png" alt="" />
<img class="format_buttons stip" title="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['tooltip'][0][0]->tooltip($_smarty_tpl->tpl_vars['lang']->value['Format_Tip_Underline']);?>
" onclick="addCode('u');" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/comment/text_underline.png" alt="" />
<img class="format_buttons stip" title="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['tooltip'][0][0]->tooltip($_smarty_tpl->tpl_vars['lang']->value['Format_Tip_Line']);?>
" onclick="addCode('s');" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/comment/text_line.png" alt="" />
<img class="format_buttons stip" title="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['tooltip'][0][0]->tooltip($_smarty_tpl->tpl_vars['lang']->value['Format_Tip_Center']);?>
" onclick="addCode('center');" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/comment/text_center.png" alt="" />
<img class="format_buttons stip" title="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['tooltip'][0][0]->tooltip($_smarty_tpl->tpl_vars['lang']->value['Format_Tip_Right']);?>
" onclick="addCode('right');" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/comment/text_right.png" alt="" />
<img class="format_buttons stip" title="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['tooltip'][0][0]->tooltip($_smarty_tpl->tpl_vars['lang']->value['Format_Tip_Left']);?>
" onclick="addCode('left');" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/comment/text_left.png" alt="" />
<img class="format_buttons stip" title="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['tooltip'][0][0]->tooltip($_smarty_tpl->tpl_vars['lang']->value['Format_Tip_Justify']);?>
" onclick="addCode('justify');" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/comment/text_justify.png" alt="" />
<img class="format_buttons stip" title="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['tooltip'][0][0]->tooltip($_smarty_tpl->tpl_vars['lang']->value['Format_Tip_List']);?>
" onclick="addCode('list');" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/comment/text_list.png" alt="" />
<img class="format_buttons stip" title="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['tooltip'][0][0]->tooltip($_smarty_tpl->tpl_vars['lang']->value['Format_Tip_High']);?>
" onclick="addCode('highlight');" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/comment/text_highlight.png" alt="" />
<?php if ($_smarty_tpl->tpl_vars['settings']->value['SysCode_Links']==1) {?>
  <img class="format_buttons stip" title="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['tooltip'][0][0]->tooltip($_smarty_tpl->tpl_vars['lang']->value['Format_Tip_Url']);?>
" onclick="addCode('url');"  src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/comment/text_link.png" alt="" />
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['settings']->value['SysCode_Email']==1) {?>
  <img class="format_buttons stip" title="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['tooltip'][0][0]->tooltip($_smarty_tpl->tpl_vars['lang']->value['Format_Tip_Email']);?>
" onclick="addCode('mail');"  src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/comment/text_email.png" alt="" />
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['settings']->value['SysCode_Bild']==1) {?>
  <img class="format_buttons stip" title="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['tooltip'][0][0]->tooltip($_smarty_tpl->tpl_vars['lang']->value['Format_Tip_Image']);?>
" onclick="addCode('img');"  src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/comment/text_image.png" alt="" />
<?php }?>
<img class="format_buttons stip" title="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['tooltip'][0][0]->tooltip($_smarty_tpl->tpl_vars['lang']->value['Format_Tip_Quote']);?>
" onclick="addCode('quote');" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/comment/text_quote.png" alt="" />
<img class="format_buttons stip" title="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['tooltip'][0][0]->tooltip($_smarty_tpl->tpl_vars['lang']->value['Format_Tip_Code']);?>
" onclick="addCode('code');" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/comment/text_code.png" alt="" />
<img class="format_buttons stip" title="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['tooltip'][0][0]->tooltip($_smarty_tpl->tpl_vars['lang']->value['Format_Tip_Php']);?>
" onclick="addCode('php');" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/comment/text_php.png" alt="" />
<img class="format_buttons stip" title="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['tooltip'][0][0]->tooltip($_smarty_tpl->tpl_vars['lang']->value['Format_Tip_CloseAll']);?>
" onclick="closeCodes();" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/comment/text_close.png" alt="" />

<?php }} ?>
