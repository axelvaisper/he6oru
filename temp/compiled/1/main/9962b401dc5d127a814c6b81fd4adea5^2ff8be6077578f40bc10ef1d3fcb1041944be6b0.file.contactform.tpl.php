<?php /* Smarty version Smarty-3.1.18, created on 2017-02-25 21:09:56
         compiled from "/home/vampireos/www/he6oru.localhost/theme/standard/contact/contactform.tpl" */ ?>
<?php /*%%SmartyHeaderCode:124394489258b1c8745dd4a1-02978403%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2ff8be6077578f40bc10ef1d3fcb1041944be6b0' => 
    array (
      0 => '/home/vampireos/www/he6oru.localhost/theme/standard/contact/contactform.tpl',
      1 => 1405960001,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '124394489258b1c8745dd4a1-02978403',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'contact_fields' => 0,
    'form_id' => 0,
    'cf' => 0,
    'use_code' => 0,
    'baseurl' => 0,
    'referer' => 0,
    'form_intro' => 0,
    'rw' => 0,
    'form_attachment' => 0,
    'form_id_raw' => 0,
    'contact_button' => 0,
    'lang' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_58b1c8746eb354_45588531',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58b1c8746eb354_45588531')) {function content_58b1c8746eb354_45588531($_smarty_tpl) {?><?php if ($_smarty_tpl->tpl_vars['contact_fields']->value) {?>
<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['script'][0][0]->script(array('file'=>((string)$_smarty_tpl->tpl_vars['jspath']->value)."/jvalidate.js",'position'=>'head'),$_smarty_tpl);?>

<script type="text/javascript">
<!-- //
<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['incpath']->value)."/other/jsvalidate.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

$(document).ready(function() {
    $('#<?php echo $_smarty_tpl->tpl_vars['form_id']->value;?>
').validate( {
        rules: {
            <?php  $_smarty_tpl->tpl_vars['cf'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['cf']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['contact_fields']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['cf']->key => $_smarty_tpl->tpl_vars['cf']->value) {
$_smarty_tpl->tpl_vars['cf']->_loop = true;
?>
            '<?php echo $_smarty_tpl->tpl_vars['cf']->value->Name;?>
': {
                <?php if ($_smarty_tpl->tpl_vars['cf']->value->Email==1) {?>
                email: true<?php if ($_smarty_tpl->tpl_vars['cf']->value->Pflicht==1||$_smarty_tpl->tpl_vars['cf']->value->Zahl==1) {?>,<?php }?>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['cf']->value->Pflicht==1) {?>
                required: true<?php if ($_smarty_tpl->tpl_vars['cf']->value->Zahl==1) {?>,<?php }?>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['cf']->value->Zahl==1) {?>
                number: true
                <?php }?>
            },
            <?php } ?>
            <?php if ($_smarty_tpl->tpl_vars['use_code']->value==1) {?>
            scode: {
                required: true, remote: '<?php echo $_smarty_tpl->tpl_vars['baseurl']->value;?>
/lib/secure.php?action=checkcode'
            },
            <?php }?>
            '<?php echo $_smarty_tpl->getConfigVariable('Contact_myName');?>
': { required: true },
            '<?php echo $_smarty_tpl->getConfigVariable('SendEmail_Email');?>
': { required: true, email: true }
        },
        messages: { scode: { remote: '<?php echo $_smarty_tpl->getConfigVariable('Validate_wrong');?>
' } },
        submitHandler: function(form) {
            $(form).ajaxSubmit({
                target: '#secure_code',
                success: showResponse,
                clearForm: false,
                resetForm: true
            });
        },
        success: function(label) {
            label.html('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;').addClass('checked');
        }
    });
});
function showResponse() {
    showNotice('<h2><?php echo $_smarty_tpl->getConfigVariable('Contact_thankyou');?>
</h2>', 2000);
}
function submitOtherData() {
    document.getElementById('___hmail').value = document.getElementById('ye_<?php echo $_smarty_tpl->tpl_vars['form_id']->value;?>
').value;
    document.getElementById('___hname').value = document.getElementById('yn_<?php echo $_smarty_tpl->tpl_vars['form_id']->value;?>
').value;
}
//-->
</script>

<div style="margin: 20px 0 5px 0">
  <?php if ($_smarty_tpl->tpl_vars['referer']->value) {?>
    <div class="error_box"> <?php echo sanitize($_smarty_tpl->tpl_vars['referer']->value);?>
 </div>
  <?php }?>
  <?php if ($_smarty_tpl->tpl_vars['form_intro']->value) {?>
    <div class="comment_intro"> <?php echo sanitize($_smarty_tpl->tpl_vars['form_intro']->value);?>
 </div>
  <?php }?>
  <form onsubmit="submitOtherData();" id="<?php echo $_smarty_tpl->tpl_vars['form_id']->value;?>
" method="post" enctype="multipart/form-data" action="index.php?p=contact">
    <div class="round">
      <div class="contact_form">
        <input type="hidden" name="__hmail" id="___hmail" />
        <input type="hidden" name="__hname" id="___hname" />
        <p>
          <label>
            <input class="input" id="yn_<?php echo $_smarty_tpl->tpl_vars['form_id']->value;?>
" style="width: 200px" name="<?php echo $_smarty_tpl->getConfigVariable('Contact_myName');?>
" type="text" value="<?php echo $_SESSION['user_name'];?>
" />&nbsp;
            <strong><?php echo $_smarty_tpl->getConfigVariable('Contact_myName');?>
</strong>
          </label>
        </p>
        <p>
          <label>
            <input class="input" id="ye_<?php echo $_smarty_tpl->tpl_vars['form_id']->value;?>
" style="width: 200px" name="<?php echo $_smarty_tpl->getConfigVariable('SendEmail_Email');?>
" type="text" value="<?php echo $_SESSION['login_email'];?>
" />&nbsp;
            <strong><?php echo $_smarty_tpl->getConfigVariable('SendEmail_Email');?>
</strong>
          </label>
        </p>
        <?php  $_smarty_tpl->tpl_vars['cf'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['cf']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['contact_fields']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['cf']->key => $_smarty_tpl->tpl_vars['cf']->value) {
$_smarty_tpl->tpl_vars['cf']->_loop = true;
?>
          <?php if ($_smarty_tpl->tpl_vars['cf']->value->Typ=='textfield') {?>
            <p>
              <label>
                <input name="<?php echo $_smarty_tpl->tpl_vars['cf']->value->Name;?>
" type="text" class="input" id="cf_<?php echo $_smarty_tpl->tpl_vars['cf']->value->Id;?>
" style="width: 200px" value="<?php echo sanitize($_smarty_tpl->tpl_vars['cf']->value->Werte);?>
" maxlength="255" />&nbsp;
                <strong><?php echo sanitize($_smarty_tpl->tpl_vars['cf']->value->Name);?>
</strong>
              </label>
            </p>
          <?php } elseif ($_smarty_tpl->tpl_vars['cf']->value->Typ=='radio') {?>
            <p>
              <strong><?php echo sanitize($_smarty_tpl->tpl_vars['cf']->value->Name);?>
</strong>
              <br />
              <span id="cf_<?php echo $_smarty_tpl->tpl_vars['cf']->value->Id;?>
"></span>
              <?php  $_smarty_tpl->tpl_vars['rw'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['rw']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['cf']->value->OutElemVal; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['rw']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['rw']->key => $_smarty_tpl->tpl_vars['rw']->value) {
$_smarty_tpl->tpl_vars['rw']->_loop = true;
 $_smarty_tpl->tpl_vars['rw']->index++;
 $_smarty_tpl->tpl_vars['rw']->first = $_smarty_tpl->tpl_vars['rw']->index === 0;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['rwn']['first'] = $_smarty_tpl->tpl_vars['rw']->first;
?>
                <label><input type="radio" name="<?php echo $_smarty_tpl->tpl_vars['cf']->value->Name;?>
" value="<?php echo $_smarty_tpl->tpl_vars['rw']->value;?>
" <?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['rwn']['first']) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->tpl_vars['rw']->value;?>
</label>
                <?php } ?>
            </p>
          <?php } elseif ($_smarty_tpl->tpl_vars['cf']->value->Typ=='checkbox') {?>
            <p>
              <strong><?php echo sanitize($_smarty_tpl->tpl_vars['cf']->value->Name);?>
</strong>
              <br />
              <span id="cf_<?php echo $_smarty_tpl->tpl_vars['cf']->value->Id;?>
"></span>
              <?php  $_smarty_tpl->tpl_vars['rw'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['rw']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['cf']->value->OutElemVal; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['rw']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['rw']->key => $_smarty_tpl->tpl_vars['rw']->value) {
$_smarty_tpl->tpl_vars['rw']->_loop = true;
 $_smarty_tpl->tpl_vars['rw']->index++;
 $_smarty_tpl->tpl_vars['rw']->first = $_smarty_tpl->tpl_vars['rw']->index === 0;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['rwn']['first'] = $_smarty_tpl->tpl_vars['rw']->first;
?>
                <label><input type="checkbox" name="<?php echo $_smarty_tpl->tpl_vars['cf']->value->Name;?>
[]" value="<?php echo $_smarty_tpl->tpl_vars['rw']->value;?>
" <?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['rwn']['first']) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->tpl_vars['rw']->value;?>
</label>
                <?php } ?>
            </p>
          <?php } elseif ($_smarty_tpl->tpl_vars['cf']->value->Typ=='dropdown') {?>
            <p>
              <label>
                <select id="cf_<?php echo $_smarty_tpl->tpl_vars['cf']->value->Id;?>
" class="input" style="width: 205px" name="<?php echo $_smarty_tpl->tpl_vars['cf']->value->Name;?>
">
                  <?php  $_smarty_tpl->tpl_vars['rw'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['rw']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['cf']->value->OutElemVal; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['rw']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['rw']->key => $_smarty_tpl->tpl_vars['rw']->value) {
$_smarty_tpl->tpl_vars['rw']->_loop = true;
 $_smarty_tpl->tpl_vars['rw']->index++;
 $_smarty_tpl->tpl_vars['rw']->first = $_smarty_tpl->tpl_vars['rw']->index === 0;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['rwn']['first'] = $_smarty_tpl->tpl_vars['rw']->first;
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['rw']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['rw']->value;?>
</option>
                  <?php } ?>
                </select>&nbsp;
                <strong><?php echo sanitize($_smarty_tpl->tpl_vars['cf']->value->Name);?>
</strong>
              </label>
            </p>
          <?php } elseif ($_smarty_tpl->tpl_vars['cf']->value->Typ=='textarea') {?>
            <br />
            <label>
              <strong><?php echo sanitize($_smarty_tpl->tpl_vars['cf']->value->Name);?>
</strong>
              <br />
              <textarea class="input" id="cf_<?php echo $_smarty_tpl->tpl_vars['cf']->value->Id;?>
" name="<?php echo $_smarty_tpl->tpl_vars['cf']->value->Name;?>
" cols="30" rows="5" style="width: 98%; height: 180px"></textarea>
            </label>
            <br />
          <?php }?>
        <?php } ?>
        <?php if ($_smarty_tpl->tpl_vars['form_attachment']->value) {?>
          <br />
          <fieldset>
            <legend><strong><?php echo $_smarty_tpl->getConfigVariable('Contact_attachment_mes');?>
</strong></legend>
            <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['xx'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['xx']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['xx']['name'] = 'xx';
$_smarty_tpl->tpl_vars['smarty']->value['section']['xx']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['form_attachment']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['xx']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['xx']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['xx']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['xx']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['xx']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['xx']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['xx']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['xx']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['xx']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['xx']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['xx']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['xx']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['xx']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['xx']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['xx']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['xx']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['xx']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['xx']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['xx']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['xx']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['xx']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['xx']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['xx']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['xx']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['xx']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['xx']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['xx']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['xx']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['xx']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['xx']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['xx']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['xx']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['xx']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['xx']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['xx']['total']);
?>
              <input name="files[]" type="file" class="input" size="35" style="width: 255px" />
              <br />
            <?php endfor; endif; ?>
          </fieldset>
          <br />
        <?php }?>
        <?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['incpath']->value)."/other/captcha.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

      </div>
    </div>
    <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['form_id_raw']->value;?>
" />
    <div style="text-align: center">
      <p><label><input type="checkbox" name="mailcopy" value="1" checked="checked" /><strong><?php echo $_smarty_tpl->getConfigVariable('Contact_wish_mailcopy');?>
</strong></label></p>
      <input type="submit" class="button" value="<?php echo (($tmp = @sanitize($_smarty_tpl->tpl_vars['contact_button']->value))===null||$tmp==='' ? $_smarty_tpl->tpl_vars['lang']->value['ButtonSend'] : $tmp);?>
" />
    </div>
  </form>
</div>
<?php }?>
<?php }} ?>
