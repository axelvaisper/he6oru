<?php /* Smarty version Smarty-3.1.18, created on 2017-02-25 21:11:16
         compiled from "/home/vampireos/www/he6oru.localhost/theme/standard/navi/navigation.tpl" */ ?>
<?php /*%%SmartyHeaderCode:149478893158b1c8c49e4089-10374227%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7ed2635c91b5d5a84d33685b16d62be414f74087' => 
    array (
      0 => '/home/vampireos/www/he6oru.localhost/theme/standard/navi/navigation.tpl',
      1 => 1405973460,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '149478893158b1c8c49e4089-10374227',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'SiteNavigation' => 0,
    'navi_title' => 0,
    'lang' => 0,
    'navi' => 0,
    'document' => 0,
    'sub_navi' => 0,
    'sub_sub_navi' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_58b1c8c4a4f024_24495755',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58b1c8c4a4f024_24495755')) {function content_58b1c8c4a4f024_24495755($_smarty_tpl) {?><?php if (!empty($_smarty_tpl->tpl_vars['SiteNavigation']->value)) {?>
  <div class="page_navibox">
    <div class="page_navibox_header"><?php echo (($tmp = @$_smarty_tpl->tpl_vars['navi_title']->value)===null||$tmp==='' ? $_smarty_tpl->tpl_vars['lang']->value['Title_Navi'] : $tmp);?>
</div>
    <ul>
      <?php  $_smarty_tpl->tpl_vars['navi'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['navi']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['SiteNavigation']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['navi']->key => $_smarty_tpl->tpl_vars['navi']->value) {
$_smarty_tpl->tpl_vars['navi']->_loop = true;
?>
        <li><a title="<?php echo sanitize($_smarty_tpl->tpl_vars['navi']->value->AltTitle);?>
" target="<?php echo (($tmp = @$_smarty_tpl->tpl_vars['navi']->value->target)===null||$tmp==='' ? '_self' : $tmp);?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['navi']->value->document, ENT_QUOTES, 'windows-1251', true);?>
" class="<?php if ($_smarty_tpl->tpl_vars['navi']->value->document==$_smarty_tpl->tpl_vars['document']->value||!empty($_smarty_tpl->tpl_vars['navi']->value->active)) {?>navi_first_active<?php } else { ?>navi_first<?php }?>"><?php echo sanitize($_smarty_tpl->tpl_vars['navi']->value->title);?>
</a>
          <?php if (empty($_smarty_tpl->tpl_vars['navi']->value->sub_navi)||!count($_smarty_tpl->tpl_vars['navi']->value->sub_navi)) {?></li><?php }?>
          <?php if (!empty($_smarty_tpl->tpl_vars['navi']->value->sub_navi)&&count($_smarty_tpl->tpl_vars['navi']->value->sub_navi)) {?>
          <ul>
            <?php  $_smarty_tpl->tpl_vars['sub_navi'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['sub_navi']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['navi']->value->sub_navi; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['sub_navi']->key => $_smarty_tpl->tpl_vars['sub_navi']->value) {
$_smarty_tpl->tpl_vars['sub_navi']->_loop = true;
?>
              <li><a title="<?php echo sanitize($_smarty_tpl->tpl_vars['sub_navi']->value->AltTitle);?>
" target="<?php echo (($tmp = @$_smarty_tpl->tpl_vars['sub_navi']->value->target)===null||$tmp==='' ? '_self' : $tmp);?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sub_navi']->value->document, ENT_QUOTES, 'windows-1251', true);?>
" class="<?php if ($_smarty_tpl->tpl_vars['sub_navi']->value->document==$_smarty_tpl->tpl_vars['document']->value||!empty($_smarty_tpl->tpl_vars['sub_navi']->value->active)) {?>navi_second_active<?php } else { ?>navi_second<?php }?>"><?php echo sanitize($_smarty_tpl->tpl_vars['sub_navi']->value->title);?>
</a>
                <?php if (empty($_smarty_tpl->tpl_vars['sub_navi']->value->sub_navi)||!count($_smarty_tpl->tpl_vars['sub_navi']->value->sub_navi)) {?></li><?php }?>
                <?php if (!empty($_smarty_tpl->tpl_vars['sub_navi']->value->sub_navi)&&count($_smarty_tpl->tpl_vars['sub_navi']->value->sub_navi)) {?>
                <ul>
                  <?php  $_smarty_tpl->tpl_vars['sub_sub_navi'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['sub_sub_navi']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['sub_navi']->value->sub_navi; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['sub_sub_navi']->key => $_smarty_tpl->tpl_vars['sub_sub_navi']->value) {
$_smarty_tpl->tpl_vars['sub_sub_navi']->_loop = true;
?>
                    <li><a title="<?php echo sanitize($_smarty_tpl->tpl_vars['sub_sub_navi']->value->AltTitle);?>
" target="<?php echo (($tmp = @$_smarty_tpl->tpl_vars['sub_sub_navi']->value->target)===null||$tmp==='' ? '_self' : $tmp);?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sub_sub_navi']->value->document, ENT_QUOTES, 'windows-1251', true);?>
" class="<?php if ($_smarty_tpl->tpl_vars['sub_sub_navi']->value->document==$_smarty_tpl->tpl_vars['document']->value||!empty($_smarty_tpl->tpl_vars['sub_sub_navi']->value->active)) {?>navi_third_active<?php } else { ?>navi_third<?php }?>"><?php echo sanitize($_smarty_tpl->tpl_vars['sub_sub_navi']->value->title);?>
</a></li>
                    <?php } ?>
                </ul>
                </li>
              <?php }?>
            <?php } ?>
          </ul>
          </li>
        <?php }?>
      <?php } ?>
    </ul>
  </div>
<?php }?>
<?php }} ?>
