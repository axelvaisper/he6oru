<?php /* Smarty version Smarty-3.1.18, created on 2017-02-25 21:11:16
         compiled from "/home/vampireos/www/he6oru.localhost/theme/standard/page/main.tpl" */ ?>
<?php /*%%SmartyHeaderCode:90928105658b1c8c493e577-85537998%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5dd4fc26cf6b46520f57b6ad09aef687e976f854' => 
    array (
      0 => '/home/vampireos/www/he6oru.localhost/theme/standard/page/main.tpl',
      1 => 1468288696,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '90928105658b1c8c493e577-85537998',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'langcode' => 0,
    'quicknavi' => 0,
    'langchooser' => 0,
    'area' => 0,
    'imgpath' => 0,
    'basket_small' => 0,
    'PollOutSmall' => 0,
    'SmallCalendar' => 0,
    'SmallCalendarNewEvents' => 0,
    'PartnerDisplay' => 0,
    'WhoisOnline' => 0,
    'CounterDisplay' => 0,
    'headernav' => 0,
    'content' => 0,
    'user_login' => 0,
    'SearchForm' => 0,
    'Newsletter' => 0,
    'NewShopProductsNavi' => 0,
    'NewUsers' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_58b1c8c4962901_68753304',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58b1c8c4962901_68753304')) {function content_58b1c8c4962901_68753304($_smarty_tpl) {?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $_smarty_tpl->tpl_vars['langcode']->value;?>
" lang="<?php echo $_smarty_tpl->tpl_vars['langcode']->value;?>
" dir="ltr">
<head>
<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['incpath']->value)."/other/header_head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['incpath']->value)."/other/header_scripts.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

</head>
<body>
  <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['result'][0][0]->result(array('type'=>'script','format'=>'file','position'=>'body_start'),$_smarty_tpl);?>
 
  <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['result'][0][0]->result(array('type'=>'script','format'=>'code','position'=>'body_start'),$_smarty_tpl);?>
 
  <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['result'][0][0]->result(array('type'=>'code','format'=>'code','position'=>'body_start'),$_smarty_tpl);?>
   
  <div id="body">
    <div class="body_padding">
      <div id="page_main">
        <div id="startcontentcontents">
          <div class="quicknavicontainer"><?php echo $_smarty_tpl->tpl_vars['quicknavi']->value;?>
</div>
          <div class="langchooser">
            <?php echo $_smarty_tpl->tpl_vars['langchooser']->value;?>

            <?php if (empty($_smarty_tpl->tpl_vars['langchooser']->value)) {?>
              <?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['incpath']->value)."/navi/mini_nav.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            <?php }?>
          </div>
          <div class="menuline">&nbsp;</div>
          <?php if (get_active('shop')) {?>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td id="header_shop"><a href="index.php?area=<?php echo $_smarty_tpl->tpl_vars['area']->value;?>
"><img id="logo_shop" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/page/logo.png" alt="" /></a></td>
                <td id="header_basket" valign="top"><?php echo $_smarty_tpl->tpl_vars['basket_small']->value;?>
</td>
              </tr>
            </table>
          <?php } else { ?>
            <div id="header"> <a href="index.php?area=<?php echo $_smarty_tpl->tpl_vars['area']->value;?>
"><img id="logo" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/page/logo.png" alt="" /></a> </div>
          <?php }?>
          <div id="contents_left">
            <div class="leftright_content">
              <div id="leftnavi">
                <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['navigation'][0][0]->navigation(array('id'=>1,'tpl'=>'navigation.tpl'),$_smarty_tpl);?>

                <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['navigation'][0][0]->navigation(array('id'=>2,'tpl'=>'navigation.tpl'),$_smarty_tpl);?>

                <?php echo $_smarty_tpl->tpl_vars['PollOutSmall']->value;?>

                <?php echo $_smarty_tpl->tpl_vars['SmallCalendar']->value;?>

                <?php echo $_smarty_tpl->tpl_vars['SmallCalendarNewEvents']->value;?>

                <?php echo $_smarty_tpl->tpl_vars['PartnerDisplay']->value;?>

                <?php echo $_smarty_tpl->tpl_vars['WhoisOnline']->value;?>

                <?php echo $_smarty_tpl->tpl_vars['CounterDisplay']->value;?>

                <?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['incpath']->value)."/other/outlinks.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

              </div>
            </div>
          </div>
          <div id="contents_middle">
            <div class="main_content">
              <div class="location">
                <?php echo $_smarty_tpl->tpl_vars['headernav']->value;?>

              </div>
              <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['phrases'][0][0]->phrases(array(),$_smarty_tpl);?>

              <?php echo $_smarty_tpl->tpl_vars['content']->value;?>

            </div>
          </div>
          <div id="contents_right">
            <div class="leftright_content">
              <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['flashtag'][0][0]->flashtag(array(),$_smarty_tpl);?>

              <?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['incpath']->value)."/shop/basket_saved_small.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

              <?php echo $_smarty_tpl->tpl_vars['user_login']->value;?>

              <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['bookmarks'][0][0]->bookmarks(array(),$_smarty_tpl);?>

              <?php echo $_smarty_tpl->tpl_vars['SearchForm']->value;?>

              <?php echo $_smarty_tpl->tpl_vars['Newsletter']->value;?>

              <?php echo $_smarty_tpl->tpl_vars['NewShopProductsNavi']->value;?>

              <?php echo $_smarty_tpl->tpl_vars['NewUsers']->value;?>

            </div>
          </div>
          <div class="clear"></div>
        </div>
        <div class="foot"><a target="_blank" href="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['print_link'][0][0]->print_link(array(),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->getConfigVariable('Print');?>
</a> | <?php echo $_smarty_tpl->getConfigVariable('copyright_text');?>
 | <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['version'][0][0]->version(array(),$_smarty_tpl);?>
 | <a href="index.php?p=imprint"><?php echo $_smarty_tpl->getConfigVariable('Imprint');?>
</a> </div>
      </div>
    </div>
  </div>
  <?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['incpath']->value)."/other/google.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


  <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['result'][0][0]->result(array('type'=>'code','format'=>'code','position'=>'body_end'),$_smarty_tpl);?>
   
  <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['result'][0][0]->result(array('type'=>'script','format'=>'file','position'=>'body_end'),$_smarty_tpl);?>
 
  <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['result'][0][0]->result(array('type'=>'script','format'=>'code','position'=>'body_end'),$_smarty_tpl);?>
 
</body>
</html>
<?php }} ?>
