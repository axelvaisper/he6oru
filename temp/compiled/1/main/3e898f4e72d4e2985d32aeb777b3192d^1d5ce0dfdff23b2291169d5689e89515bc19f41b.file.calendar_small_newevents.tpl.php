<?php /* Smarty version Smarty-3.1.18, created on 2017-02-25 21:11:16
         compiled from "/home/vampireos/www/he6oru.localhost/theme/standard/calendar/calendar_small_newevents.tpl" */ ?>
<?php /*%%SmartyHeaderCode:167823094958b1c8c472ddd7-70781892%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1d5ce0dfdff23b2291169d5689e89515bc19f41b' => 
    array (
      0 => '/home/vampireos/www/he6oru.localhost/theme/standard/calendar/calendar_small_newevents.tpl',
      1 => 1406741182,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '167823094958b1c8c472ddd7-70781892',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'NewCalEvents' => 0,
    'basepath' => 0,
    'nce' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_58b1c8c473f0b3_42763094',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58b1c8c473f0b3_42763094')) {function content_58b1c8c473f0b3_42763094($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_striptags')) include '/home/vampireos/www/he6oru.localhost/lib/smarty/statusplugins/modifier.striptags.php';
if (!is_callable('smarty_modifier_truncate')) include '/home/vampireos/www/he6oru.localhost/lib/smarty/plugins/modifier.truncate.php';
if (!is_callable('smarty_modifier_date_format')) include '/home/vampireos/www/he6oru.localhost/lib/smarty/plugins/modifier.date_format.php';
?><?php if (get_active('calendar')&&$_smarty_tpl->tpl_vars['NewCalEvents']->value) {?>
<script type="text/javascript">
<!-- //
togglePanel('navpanel_newevents', 'togglerboxes', 30, '<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
');
//-->
</script>

<div class="round">
  <div class="opened" id="navpanel_newevents" title="<?php echo $_smarty_tpl->getConfigVariable('CalendarNewEvents');?>
">
    <div class="boxes_body">
      <?php  $_smarty_tpl->tpl_vars['nce'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['nce']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['NewCalEvents']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['nce']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['nce']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['nce']->key => $_smarty_tpl->tpl_vars['nce']->value) {
$_smarty_tpl->tpl_vars['nce']->_loop = true;
 $_smarty_tpl->tpl_vars['nce']->iteration++;
 $_smarty_tpl->tpl_vars['nce']->last = $_smarty_tpl->tpl_vars['nce']->iteration === $_smarty_tpl->tpl_vars['nce']->total;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['nextevents']['last'] = $_smarty_tpl->tpl_vars['nce']->last;
?>
        <a class="stip" title="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['tooltip'][0][0]->tooltip($_smarty_tpl->tpl_vars['nce']->value->Beschreibung,200);?>
" href="<?php echo $_smarty_tpl->tpl_vars['nce']->value->EventLink;?>
"><strong><?php echo sanitize($_smarty_tpl->tpl_vars['nce']->value->Titel);?>
</strong></a>
        <br />
        <?php echo sanitize(smarty_modifier_truncate(smarty_modifier_striptags($_smarty_tpl->tpl_vars['nce']->value->Beschreibung),100));?>

        <br />
        <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['nce']->value->Start,'%d-%m-%Y');?>
&nbsp;<?php echo $_smarty_tpl->getConfigVariable('Arrow');?>
<a href="<?php echo $_smarty_tpl->tpl_vars['nce']->value->EventLink;?>
"><?php echo $_smarty_tpl->getConfigVariable('MoreDetails');?>
</a>
        <?php if (!$_smarty_tpl->getVariable('smarty')->value['foreach']['nextevents']['last']) {?>
          <br />
          <br />
        <?php }?>
      <?php } ?>
    </div>
  </div>
</div>
<?php }?>
<?php }} ?>
