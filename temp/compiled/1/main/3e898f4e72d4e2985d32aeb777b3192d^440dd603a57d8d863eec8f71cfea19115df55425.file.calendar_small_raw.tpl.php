<?php /* Smarty version Smarty-3.1.18, created on 2017-02-25 21:11:16
         compiled from "/home/vampireos/www/he6oru.localhost/theme/standard/calendar/calendar_small_raw.tpl" */ ?>
<?php /*%%SmartyHeaderCode:182282708358b1c8c47485c5-45477338%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '440dd603a57d8d863eec8f71cfea19115df55425' => 
    array (
      0 => '/home/vampireos/www/he6oru.localhost/theme/standard/calendar/calendar_small_raw.tpl',
      1 => 1406959236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '182282708358b1c8c47485c5-45477338',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'prevMonth' => 0,
    'nextMonth' => 0,
    'header' => 0,
    'DayNamesShortArray' => 0,
    'day' => 0,
    'cal_data' => 0,
    'cd' => 0,
    'baseurl' => 0,
    'area' => 0,
    'td' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_58b1c8c475b799_51186809',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58b1c8c475b799_51186809')) {function content_58b1c8c475b799_51186809($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_truncate')) include '/home/vampireos/www/he6oru.localhost/lib/smarty/plugins/modifier.truncate.php';
if (!is_callable('smarty_modifier_date_format')) include '/home/vampireos/www/he6oru.localhost/lib/smarty/plugins/modifier.date_format.php';
?><script type="text/javascript">
<!-- //
$(document).ready(function() {
    var options_prev = {
        target: '#calraw', url: '<?php echo $_smarty_tpl->tpl_vars['prevMonth']->value;?>
', timeout: 3000
    };
    var options_next = {
        target: '#calraw', url: '<?php echo $_smarty_tpl->tpl_vars['nextMonth']->value;?>
', timeout: 3000
    };
    $('#switchcal_prev').on('click', function() {
        $(this).ajaxSubmit(options_prev);
        return false;
    });
    $('#switchcal_next').on('click', function() {
        $(this).ajaxSubmit(options_next);
        return false;
    });
});
//-->
</script>

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="1" class="calendarBackground">
  <tr>
    <td align="left" valign="top"><a id="switchcal_prev" href="javascript: void(0);">&lt;&lt;</a></td>
    <td align="center" valign="top" colspan="6"><strong><?php echo $_smarty_tpl->tpl_vars['header']->value;?>
</strong></td>
    <td align="right" valign="top"><a id="switchcal_next" href="javascript: void(0);">&gt;&gt;</a></td>
  </tr>
  <tr>
    <td class="calendarHeader">&nbsp;</td>
    <?php  $_smarty_tpl->tpl_vars['day'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['day']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['DayNamesShortArray']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['day']->key => $_smarty_tpl->tpl_vars['day']->value) {
$_smarty_tpl->tpl_vars['day']->_loop = true;
?>
      <td title="<?php echo $_smarty_tpl->tpl_vars['day']->value;?>
" class="calendarHeader"> <?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['day']->value,'2',false);?>
. </td>
    <?php } ?>
  </tr>
  <?php  $_smarty_tpl->tpl_vars['cd'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['cd']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['cal_data']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['cd']->key => $_smarty_tpl->tpl_vars['cd']->value) {
$_smarty_tpl->tpl_vars['cd']->_loop = true;
?>
    <tr>
      <td class="calendarBlanc" style="text-align: center"><a title="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['cd']->value->StartWeek,'%d.%m.%Y');?>
 - <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['cd']->value->EndWeek,'%d.%m.%Y');?>
 " style="font-weight: bold" href="<?php echo $_smarty_tpl->tpl_vars['baseurl']->value;?>
/index.php?p=calendar&amp;show=public&amp;action=week&amp;weekstart=<?php echo $_smarty_tpl->tpl_vars['cd']->value->StartWeek;?>
&amp;weekend=<?php echo $_smarty_tpl->tpl_vars['cd']->value->EndWeek;?>
&amp;area=<?php echo $_smarty_tpl->tpl_vars['area']->value;?>
">&gt;</a></td>
      <?php  $_smarty_tpl->tpl_vars['td'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['td']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['cd']->value->CalDataInner; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['td']->key => $_smarty_tpl->tpl_vars['td']->value) {
$_smarty_tpl->tpl_vars['td']->_loop = true;
?>
        <td class="<?php echo $_smarty_tpl->tpl_vars['td']->value->tdclass;?>
" align="right" valign="top"><?php echo $_smarty_tpl->tpl_vars['td']->value->thelink;?>
</td>
      <?php } ?>
    </tr>
  <?php } ?>
</table>
<?php }} ?>
