<?php /* Smarty version Smarty-3.1.18, created on 2017-02-25 14:56:34
         compiled from "/home/vampireos/www/he6oru.localhost/setup/theme/step1.tpl" */ ?>
<?php /*%%SmartyHeaderCode:159767490458ae99d310e199-30491057%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ebfe92ffcdb57418f6e7317aade49fa547ef8e2b' => 
    array (
      0 => '/home/vampireos/www/he6oru.localhost/setup/theme/step1.tpl',
      1 => 1488030990,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '159767490458ae99d310e199-30491057',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_58ae99d323df28_18177641',
  'variables' => 
  array (
    'setupdir' => 0,
    'params' => 0,
    'lang' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58ae99d323df28_18177641')) {function content_58ae99d323df28_18177641($_smarty_tpl) {?><form autocomplete="off" name="Form" id="Form"  method="post" action="<?php echo $_smarty_tpl->tpl_vars['setupdir']->value;?>
/setup.php" cardx style="width:400px">

<h1><?php echo $_smarty_tpl->getConfigVariable('Step1');?>
</h1>

  <?php if ($_smarty_tpl->tpl_vars['params']->value['php']==1) {?>
    <span title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['Test_Ok'];?>
">
      <?php echo $_smarty_tpl->getConfigVariable('icon_check');?>
 <?php echo $_smarty_tpl->getConfigVariable('Load_php');?>

    </span>
  <?php } else { ?>
    <em title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['Test_Fail'];?>
">
      <?php echo $_smarty_tpl->getConfigVariable('icon_alert');?>
 <?php echo $_smarty_tpl->getConfigVariable('Load_php');?>
 <?php echo $_smarty_tpl->getConfigVariable('SetSetup');?>

    </em>
  <?php }?>
  <br>

  <?php if ($_smarty_tpl->tpl_vars['params']->value['mbstring']==1) {?>
    <span title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['Test_Ok'];?>
">
      <?php echo $_smarty_tpl->getConfigVariable('icon_check');?>
 <?php echo $_smarty_tpl->getConfigVariable('Load_Ext');?>
 mbstring
    </span>
  <?php } else { ?>
    <em title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['Test_Fail'];?>
">
      <?php echo $_smarty_tpl->getConfigVariable('icon_alert');?>
 <?php echo $_smarty_tpl->getConfigVariable('Load_Ext');?>
 mbstring <?php echo $_smarty_tpl->getConfigVariable('SetSetup');?>

    </em>
  <?php }?>
  <br>

  <?php if ($_smarty_tpl->tpl_vars['params']->value['spl']==1) {?>
    <span title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['Test_Ok'];?>
">
      <?php echo $_smarty_tpl->getConfigVariable('icon_check');?>
 <?php echo $_smarty_tpl->getConfigVariable('Load_Ext');?>
 spl
    </span>
  <?php } else { ?>
    <em title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['Test_Fail'];?>
">
      <?php echo $_smarty_tpl->getConfigVariable('icon_alert');?>
 <?php echo $_smarty_tpl->getConfigVariable('Load_Ext');?>
 spl <?php echo $_smarty_tpl->getConfigVariable('SetSetup');?>

    </em>
  <?php }?>
  <br>

  <?php if ($_smarty_tpl->tpl_vars['params']->value['mysqli']==1) {?>
    <span title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['Test_Ok'];?>
">
      <?php echo $_smarty_tpl->getConfigVariable('icon_check');?>
 <?php echo $_smarty_tpl->getConfigVariable('Load_Ext');?>
 mysqli
    </span>
  <?php } else { ?>
    <em title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['Test_Fail'];?>
">
      <?php echo $_smarty_tpl->getConfigVariable('icon_alert');?>
 <?php echo $_smarty_tpl->getConfigVariable('Load_Ext');?>
 mysqli <?php echo $_smarty_tpl->getConfigVariable('SetSetup');?>

    </em>
  <?php }?>
  <br>

  <?php if ($_smarty_tpl->tpl_vars['params']->value['iconv']==1) {?>
    <span title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['Test_Ok'];?>
">
      <?php echo $_smarty_tpl->getConfigVariable('icon_check');?>

    </span>
  <?php } else { ?>
    <span title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['Test_Fail'];?>
" class="red">
      <?php echo $_smarty_tpl->getConfigVariable('icon_alert');?>

    </span>
  <?php }?>
  <?php echo $_smarty_tpl->getConfigVariable('Load_Ext');?>
 iconv
  <br>

  <?php if ($_smarty_tpl->tpl_vars['params']->value['gd']==1) {?>
    <span title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['Test_Ok'];?>
">
      <?php echo $_smarty_tpl->getConfigVariable('icon_check');?>

    </span>
  <?php } else { ?>
    <em title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['Test_Fail'];?>
">
      <?php echo $_smarty_tpl->getConfigVariable('icon_alert');?>

    </em>
  <?php }?>
  <?php echo $_smarty_tpl->getConfigVariable('Load_Ext');?>
 gd
  <br>

  <?php if ($_smarty_tpl->tpl_vars['params']->value['zlib']==1) {?>
    <span title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['Test_Ok'];?>
">
      <?php echo $_smarty_tpl->getConfigVariable('icon_check');?>

    </span>
  <?php } else { ?>
     <em title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['Test_Fail'];?>
">
      <?php echo $_smarty_tpl->getConfigVariable('icon_alert');?>

    </em>
  <?php }?>
  <?php echo $_smarty_tpl->getConfigVariable('Load_Ext');?>
 zlib
  <br>

  <?php if ($_smarty_tpl->tpl_vars['params']->value['locale']==1) {?>
    <span title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['Test_Ok'];?>
">
      <?php echo $_smarty_tpl->getConfigVariable('icon_check');?>

    </span>
  <?php } else { ?>
    <em title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['Test_Fail'];?>
">
      <?php echo $_smarty_tpl->getConfigVariable('icon_alert');?>

    </em>
  <?php }?>
  <?php echo $_smarty_tpl->getConfigVariable('Load_Locale');?>

  <br>

  <?php if ($_smarty_tpl->tpl_vars['params']->value['safemode']==0) {?>
    <span title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['Test_Ok'];?>
">
    <?php echo $_smarty_tpl->getConfigVariable('icon_check');?>

    </span>
  <?php } else { ?>
    <em title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['Test_Fail'];?>
">
      <?php echo $_smarty_tpl->getConfigVariable('icon_alert');?>

    </em>
  <?php }?>
  <?php echo $_smarty_tpl->getConfigVariable('Load_Param');?>
 safe_mode
  <br>

  <?php if ($_smarty_tpl->tpl_vars['params']->value['magic_quotes_gpc']==0) {?>
    <span title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['Test_Ok'];?>
">
      <?php echo $_smarty_tpl->getConfigVariable('icon_check');?>

    </span>
  <?php } else { ?>
    <em title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['Test_Fail'];?>
">
      <?php echo $_smarty_tpl->getConfigVariable('icon_alert');?>

    </em>
  <?php }?>
  <?php echo $_smarty_tpl->getConfigVariable('Load_Param');?>
 magic_quotes_gpc
  <br>

  <?php if ($_smarty_tpl->tpl_vars['params']->value['magic_quotes_runtime']==0) {?>
    <span title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['Test_Ok'];?>
">
    <?php echo $_smarty_tpl->getConfigVariable('icon_check');?>

    </span>
  <?php } else { ?>
    <em title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['Test_Fail'];?>
">
    <?php echo $_smarty_tpl->getConfigVariable('icon_alert');?>

    </em>
  <?php }?>
  <?php echo $_smarty_tpl->getConfigVariable('Load_Param');?>
 magic_quotes_runtime
  <br>

  <?php if ($_smarty_tpl->tpl_vars['params']->value['magic_quotes_sybase']==0) {?>
    <span title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['Test_Ok'];?>
">
      <?php echo $_smarty_tpl->getConfigVariable('icon_check');?>

    </span>
  <?php } else { ?>
    <em title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['Test_Fail'];?>
">
      <?php echo $_smarty_tpl->getConfigVariable('icon_alert');?>

    </em>
  <?php }?>
  <?php echo $_smarty_tpl->getConfigVariable('Load_Param');?>
 magic_quotes_sybase
  <br>

  <?php if ($_smarty_tpl->tpl_vars['params']->value['memory_limit']>16) {?>
    <span title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['Test_Ok'];?>
">
      <?php echo $_smarty_tpl->getConfigVariable('icon_check');?>

    </span>
  <?php } else { ?>
    <em title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['Test_Fail'];?>
">
      <?php echo $_smarty_tpl->getConfigVariable('icon_alert');?>

    </em>
  <?php }?>
  <?php echo $_smarty_tpl->getConfigVariable('Mem_limit');?>


<br>

<p>
<?php if ($_smarty_tpl->tpl_vars['params']->value['php']==1&&$_smarty_tpl->tpl_vars['params']->value['mbstring']==1&&$_smarty_tpl->tpl_vars['params']->value['spl']==1&&$_smarty_tpl->tpl_vars['params']->value['mysqli']==1) {?>
  <input type="hidden" name="step" value="2" />
  <input type="submit" value="<?php echo $_smarty_tpl->getConfigVariable('Step_Button');?>
" btnx/>
<?php } else { ?>
  <h2><span><?php echo $_smarty_tpl->getConfigVariable('ErrorSetup');?>
</span></h2>
<?php }?>
</p>

</form>
<?php }} ?>
