<?php $_config_vars = array (
  'sections' => 
  array (
  ),
  'vars' => 
  array (
    'cheaper_subject' => '����� ������ � �������� ���� �������',
    'cheaper_send' => '������������ �������������,
� �������� �� __URL__ ��������� ������ �� ����� � �������� ���� �������.

������ ������:

����:            __DATUM__
�������� ������: __PRODUCT__
���� �� �����:   __PRICE__
������ �� �����: __LINK__

������ ��������� ������������:

��� �� ������ ���� �����: __WHERE__
����� ���������:          __TEXT__

������ �����������

IP �����������:     __IP__
E-mail �����������: __MAIL__
��� �����������:    __USERNAME__
',
    'ShopPriceAlertEmailSubject' => '����������� � ������ �� ����� �� ���� ����������',
    'ShopPriceAlertEmail' => '������������ �������������,
� �������� �� __URL__ ��������� ������ �� ����� �� ���� ������������.

������ �������:

IP ������������:           __IP__
E-mail-����� ������������: __MAIL__
��� ������������:          __USERNAME__
����:                      __DATUM__
ID ������:                 __ID__
�������� ������:           __PRODNAME__
������ �� �����:           __LINK__
��������� ����:            __SUMM__
',
    'AntivirusMailSubj' => '����������� �� ���������� ��������� ������ �� ����� __URL__',
    'AntivirusMail' => '������������ __USER__!
�� ����� �������������� �������� ��������� ������ �� ����� __URL__, ���������� ��������� � ������.
��� ��������� ����� ���� �������� �������������������� ��������� � ���� �������.

���� ��������:    __DATE__
���������� �����: __FILE__
',
    'ForumFriendSendSubj' => '���������� ������������ ������������ � ����� __URL__ ',
    'ForumFriendSend' => '������������!

������������ __USER__ � ����� __URL__ ������� ��������� � ������� ��� ��� �������� ��� ����������.
����� ���������:
__TEXT__
',
    'SystemOrderSendSubj' => '����� �� CMS Status-X � ����� __URL__ ',
    'SystemOrderSend' => '������������!
������������ ����� __URL__ ������� ����� �� CMS Status-X.

����� �������: __ORDER__

����� ����������: __DATA__

������: __SUM__
',
    'SystemErrorSendSubj' => '��������� �� ������ � CMS Status-X � ����� __URL__ ',
    'SystemErrorSend' => '������������!
������������ ����� __URL__ ������� ��������� �� ������ ��������� � CMS Status-X.

����� ������: __ERROR__

������ ��������: __AGENT__

��������� ������������: __MESAGE__
',
    'ErrorEmailSendSubj' => '��������� ����������� �� ����� __URL__ ',
    'ErrorEmailSend' => '������������!

�� ����� ����� __URL__ ���� ������� �������������� � ������� ������ ������ ����������� ����� __MAIL__

� ��������� ����� __MAIL__ �� ��������� � ����� ���� ������. ���� �� ���������������� �� ����� __URL__ , �� �������� ��� ����������� �� ������������ ������ ����� ����������� �����.
',
    'body_to_mods_moderated' => '������������ __MODS__!
�� ������ �� ������� �� ��������� �����������, ������������� __USER__ ������� ����� ����.

���� ��������: __DATUM__
��������: __BETREFF__

��� �������� ���� ��������� �� ��������� ������:
__LINK__',
    'Newsletter_UnsubscribeInf_html' => '��������� ������������,<br />�� �������� ��� ��������, �.�. ����������� �� ��� �� __WEBSEITE__. �� ������ ����������, ������� �� ������: <br /> __LINK__',
    'Newsletter_UnsubscribeInf_html2' => '������������ ��� __BENUTZER__!<br />�� �������� ��� ������, ��� ��� ������������������ �� ����� __WEBSEITE__ � ��������������  ������� <strong>�������� ��������� ��������</strong> � ����� �������. �� ������ ���������� �� ��������, �������� ������� <strong>�������� ��������� ��������</strong> � ����� �������.',
    'NewComplaintSubj' => '�������� ������ �� ��������� ������',
    'NewComplaintSend' => '������������ __USER__!
�� ������ ������������ �� ���������, ������������ � �����������:

Email �����������: __MAIL__
IP ����� �����������: __IP__

����� ������: __TEXT__

����� �����: __MESAGE__

������ �� ����: __URL__
',
    'Body_to_postautor_moderated' => '������������ __USER__,
���� ��������� �� ������ ������������ � ���������.
�� ������ ����������� ��� ��������� �� ��������� ������:
__LINK__
',
    'Activate_Email' => '������������ __USER__!
C������ �� ����������� �� ����� __SITE__

� ������ ������ ��������� ������ ��� ����������� � ����� �������.

E-mail:   __MAIL__
������:   __PASS__

�� ������ ������ ��� ������� ��� �� �������, ��� ������������� ����������� ����������� �� ������:
__LINK__

� ���������, ������������� __SITENAME__
',
    'Reg_Email_Admin' => 'Hello Admin,
an new user has been registered.

Daten:

Username:         __USER__
E-mail-address:   __MAIL__
Password:         ***********
Date:             __TIME__
IP:               __IP__
',
    'NewLinksSendSubj' => '���������� ����� ������',
    'NewLinksSend' => '������������ �������������!
�� ����� ���������� ����� ������, ������������ � �����������:

��������: __NAME__

������: __URL__

��������: __DESCR__

',
    'MailFaqSendSubj' => '����� �� ��� ������ �������� �� __LINK__',
    'MailFaqSend' => '������������!
����������� ����� �� ������ ������� �� ������ __DATUM__

������ ��� �����:
__QUEST__

�����:
__TEXT__

����������� � ������� ����� ����� ������� �� ��������� ������:
__LINK__
',
    'NewFaqSendSubj' => '����� ����� ������ � ������� ������-�����',
    'NewFaqSend' => '������������ �������������!
� ������� ������-����� ����� ����� ������, ������������ � ���c���:

__TEXT__

����� ��� ����� � ������������� �������� ������: __MAIL__
',
    'NewPassMailSend' => '��������� ������������,
�� ������� ����� ��� ������������� ��� �������������� ������ �� __WEBSITE__

��� ����� ������: __PASS__

',
    'Friends_Emailbody' => '������ __USER__,
�� �������� �� __AUTOR__ ����������� ����� ��������.
����������, ������� �� ������ ���� ��� ��������� ������� ������������
__LINK__
��� �� ����������� ��� ��������� �����������, ��������� � ���� ������� �� ��������� ������
__LINK2__
',
    'Birthdays_Mail' => '������������ __USER__!

��������� ��� �� ���� ������������� �����, ���������� � ���� ��������!
� �������� ��� ��������, �������, �������, ������������,
� ����� �������� ������� �� ���������� ������������ � ������������ ��������������.
',
    'Links_E_BrokenText' => '__BENUTZER__ (__MAIL__) submitted a Deadlink:
__LINK__

Error-Type: __GRUND__

Please check this message. Please delete the message after the test in the Admin area, otherwise this link will not be reported again.
',
    'Newsletter_text' => 'You get this e-mail, because you have subscribed a Newsletter on __WEBSEITE__.
Before you receive this newsletter, it must be activated from you for security reasons.

With the activation of this newsletter you confirm that you have this newsletter of us really want to receive.
A link to unsubscribe you is in every newsletter that you receive from us.

__LINK__
',
    'PN_new_pn_emailbody' => 'Hi __USER__,
you received a new private message from __AUTOR__.
To read the message please click the following link .
__LINK__
',
    'Forums_msg_newpost_body' => 'Hello __USER__, __AUTOR__ wrote a new posting at __DATUM__ that you subscribed
---------------------------------------------------------------
__SUBJECT__
__MESSAGE__
---------------------------------------------------------------
You can view this posting by following the link:
 __LINK__
',
    'Forums_msg_newpost_body_adminnotification' => 'Hello,
you receive this e-mail because you were entered as receiver of new postings of this forum by administrator.
-----------------------------------------------------------------------
__USER__ wrote a new posting on __DATUM__ with subject __SUBJECT__:
__MESSAGE__
-----------------------------------------------------------------------
You can view this posting by following the link:
__LINK__
',
    'Forums_msg_newpost_body_adminnotification_toactivate' => 'Hello,
__USER__ wrote a new posting on __DATUM__ with subject __SUBJECT__:
__MESSAGE__
This posting has to be activated.
You can view an if need be activate the posting by following the link:
__LINK__
',
    'f_msg_newtopic_body_adminnotification' => 'Hello,
you receive this e-mail because you were entered as receiver of new topics of this forum by administrator.
-----------------------------------------------------------------------
__USER__ wrote a new posting on __DATUM__ with subject __SUBJECT__:
__MESSAGE__
-----------------------------------------------------------------------
You can view this topic by following the link:
__LINK__
',
    'Forums_body_to_autor_moderated' => 'Hello __USER__,
your topic has been activated.
You can view this topic by following the link:
__LINK__
',
    'Reg_Email_1' => 'Hello __USER__,
thank you for signing in at __SITE__

Within this e-mail you receive your access data, which you need
to sign in into our system.

e-mail-address:   __MAIL__
Password:         __PASS__

Have fun. Your __SITENAME__ team.
',
    'PassLostTextMail' => 'Dear Sir, dear Madam,

you or someone else has just requested a new password.
To activate this password, you have to follow the link:
__LINK__

Please, enter the password and your e-mail-address to certify the new password.
Passwort: __PASS__

---------------------
Important tip!
Didn\'t you request the new password please, delete this e-mail immediately
and do not activate the password.
',
    'emailform_homepage_footer' => '
-----------------------------
This e-mail was sent by user __USER__ by using the e-mail form of the user\'s list at __HOMEPAGEURL__
In no case the operator of this page is to be held responsible for the contents of this e-mail.
If you don\'t want to receive any e-mails from this user in future please enclose this user in your ignore list.
',
    'Shop_Settings_AlertText' => 'Dear Sir or Madam,

we are glad to tell you, that the following item has become cheaper and corresponds to your price ideas now:

__PRODUCT__

Previous price*: __OLD__ __CURRENCY__
New price*: __NEW__ __CURRENCY__

Please follow the link to watch the item:

__LINK__


* Can be from-prices or special offers.
',
    'Shop_convertGuestMail' => 'Dear Sir or Madam,

Your account at __WEBSITE__ was opened.
You can sign in with the following data:

E-Mail:    __MAIL__
Passwort:  __PASS__


Please check your data in \'My Account\'. You can edit or complement all entries.
',
  ),
); ?>