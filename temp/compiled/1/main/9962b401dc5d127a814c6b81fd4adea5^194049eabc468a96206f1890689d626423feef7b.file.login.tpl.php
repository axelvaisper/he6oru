<?php /* Smarty version Smarty-3.1.18, created on 2017-02-25 11:53:03
         compiled from "/home/vampireos/www/he6oru.localhost/theme/standard/user/login.tpl" */ ?>
<?php /*%%SmartyHeaderCode:198691738358b145efceecb8-11344443%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '194049eabc468a96206f1890689d626423feef7b' => 
    array (
      0 => '/home/vampireos/www/he6oru.localhost/theme/standard/user/login.tpl',
      1 => 1406742614,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '198691738358b145efceecb8-11344443',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'basepath' => 0,
    'imgpath_page' => 0,
    'baseurl' => 0,
    'lang' => 0,
    'area' => 0,
    'langcode' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_58b145efdf92e2_84254713',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58b145efdf92e2_84254713')) {function content_58b145efdf92e2_84254713($_smarty_tpl) {?><?php if (!isset($_REQUEST['subaction'])||($_REQUEST['subaction']!='step2'&&$_REQUEST['subaction']!='step3'&&$_REQUEST['subaction']!='step4')) {?>
<script type="text/javascript">
<!-- //
$(document).ready(function() {
    var options = {
        target: '#login_form_users', timeout: 3000
    };
    $('#ajloginform').submit(function() {
        $(this).ajaxSubmit(options);
        document.getElementById('ajl').style.display='none';
        document.getElementById('ajlw').style.display='';
        return false;
    });
});
togglePanel('navpanel_userpanel', 'togglerboxes', 30, '<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
');
//-->
</script>

<div class="round">
  <div class="opened" id="navpanel_userpanel" title="<?php echo $_smarty_tpl->getConfigVariable('Login');?>
">
    <div class="boxes_body user_back_small">
      <div id="login_form_users">
        <?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['incpath']->value)."/user/login_raw.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

      </div>
      <div id="ajlw" style="display: none">
        <div style="text-align: center; padding: 10px"><img src="<?php echo $_smarty_tpl->tpl_vars['imgpath_page']->value;?>
loading.gif" alt="" border="0" /></div>
      </div>
      <div id="ajl">
        <form  method="post" action="<?php echo $_smarty_tpl->tpl_vars['baseurl']->value;?>
/index.php?p=userlogin&amp;action=ajaxlogin" name="login" id="ajloginform">
          <label for="login_email_r"><?php echo $_smarty_tpl->getConfigVariable('LoginMailUname');?>
&nbsp;</label>
          <div>
            <input class="input_fields" type="text" name="login_email" id="login_email_r" style="width: 140px" />
          </div>
          <label for="login_pass_r"><?php echo $_smarty_tpl->getConfigVariable('Pass');?>
&nbsp;</label>
          <div>
            <input class="input_fields" type="password" name="login_pass" id="login_pass_r" style="width: 140px" />
          </div>
          <label>
            <input name="staylogged" type="checkbox" value="1" checked="checked" class="absmiddle" />
            <span class="tooltip stip" title="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['tooltip'][0][0]->tooltip($_smarty_tpl->tpl_vars['lang']->value['PassCookieT']);?>
"><?php echo $_smarty_tpl->getConfigVariable('PassCookieHelp');?>
</span> </label>
          <div>
            <input type="hidden" name="p" value="userlogin" />
            <input type="hidden" name="action" value="ajaxlogin" />
            <input type="hidden" name="area" value="<?php echo $_smarty_tpl->tpl_vars['area']->value;?>
" />
            <input type="hidden" name="backurl" value="<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['page_link'][0][0]->page_link(array(),$_smarty_tpl);?>
<?php echo base64_encode(ob_get_clean())?>" />
            <input type="submit" class="button" value="<?php echo $_smarty_tpl->getConfigVariable('Login_Button');?>
" />
            <br />
            <br />
            <?php if (get_active('Register')) {?>
              <?php echo $_smarty_tpl->getConfigVariable('Arrow');?>
<a href="index.php?p=register&amp;lang=<?php echo $_smarty_tpl->tpl_vars['langcode']->value;?>
&amp;area=<?php echo $_smarty_tpl->tpl_vars['area']->value;?>
"><?php echo $_smarty_tpl->getConfigVariable('RegNew');?>
</a>
              <br />
            <?php }?>
            <?php echo $_smarty_tpl->getConfigVariable('Arrow');?>
<a href="index.php?p=pwlost"><?php echo $_smarty_tpl->getConfigVariable('PassLost');?>
</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<?php }?>
<?php }} ?>
