<?php /* Smarty version Smarty-3.1.18, created on 2017-02-25 21:11:16
         compiled from "/home/vampireos/www/he6oru.localhost/theme/standard/shop/page_extern_products_new_navi.tpl" */ ?>
<?php /*%%SmartyHeaderCode:49069199358b1c8c45c93d4-28844933%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b6326ba113a5fcc02a2d98a4782557a2b741fce6' => 
    array (
      0 => '/home/vampireos/www/he6oru.localhost/theme/standard/shop/page_extern_products_new_navi.tpl',
      1 => 1406873535,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '49069199358b1c8c45c93d4-28844933',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'extern_products_array' => 0,
    'basepath' => 0,
    'p' => 0,
    'shopsettings' => 0,
    'loggedin' => 0,
    'currency_symbol' => 0,
    'no_nettodisplay' => 0,
    'price_onlynetto' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_58b1c8c45f2773_16169686',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58b1c8c45f2773_16169686')) {function content_58b1c8c45f2773_16169686($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_truncate')) include '/home/vampireos/www/he6oru.localhost/lib/smarty/plugins/modifier.truncate.php';
if (!is_callable('smarty_modifier_numformat')) include '/home/vampireos/www/he6oru.localhost/lib/smarty/statusplugins/modifier.numformat.php';
?><?php if ($_smarty_tpl->tpl_vars['extern_products_array']->value) {?>
<script type="text/javascript">
<!-- //
togglePanel('navpanel_shopnew', 'togglerboxes', 30, '<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
');
//-->
</script>

<div class="round">
  <div class="opened" id="navpanel_shopnew" title="<?php echo $_smarty_tpl->getConfigVariable('Shop_newin');?>
">
    <div class="boxes_body">
      <?php  $_smarty_tpl->tpl_vars['p'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['p']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['extern_products_array']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['p']->key => $_smarty_tpl->tpl_vars['p']->value) {
$_smarty_tpl->tpl_vars['p']->_loop = true;
?>
        <div class="shop_extern_newest_boxes">
          <div class="shop_extern_product_text stip" title="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['tooltip'][0][0]->tooltip($_smarty_tpl->tpl_vars['p']->value['Beschreibung']);?>
">
            <div class="shop_extern_image_newstart"><a href="<?php echo $_smarty_tpl->tpl_vars['p']->value['ProdLink'];?>
<?php if (isset($_REQUEST['blanc'])&&$_REQUEST['blanc']==1) {?>&amp;blanc=1<?php }?>"><img src="<?php echo $_smarty_tpl->tpl_vars['p']->value['Bild_Mittel'];?>
" alt="" /></a></div>
            <br />
            <div class="shop_product_title_new"><a href="<?php echo $_smarty_tpl->tpl_vars['p']->value['ProdLink'];?>
<?php if (isset($_REQUEST['blanc'])&&$_REQUEST['blanc']==1) {?>&amp;blanc=1<?php }?>"><?php echo sanitize(smarty_modifier_truncate($_smarty_tpl->tpl_vars['p']->value['Titel'],25));?>
</a></div>
          </div>
          <div class="shop_price_detail_footer">
            <?php if ($_smarty_tpl->tpl_vars['shopsettings']->value->PreiseGaeste==1||$_smarty_tpl->tpl_vars['loggedin']->value) {?>
              <?php if ($_smarty_tpl->tpl_vars['p']->value['Preis_Liste']!=$_smarty_tpl->tpl_vars['p']->value['Preis']) {?>
                <?php echo $_smarty_tpl->getConfigVariable('Shop_instead');?>
&nbsp;&nbsp;
                <br/>
                <span class="shop_price_old"><?php echo smarty_modifier_numformat($_smarty_tpl->tpl_vars['p']->value['Preis_Liste']);?>
 <?php echo $_smarty_tpl->tpl_vars['currency_symbol']->value;?>
</span>
                <br/>
              <?php } else { ?>
                <br />
              <?php }?>
              <?php if (!empty($_smarty_tpl->tpl_vars['p']->value['Vars'])) {?>
                <?php echo $_smarty_tpl->getConfigVariable('Shop_priceFrom');?>

              <?php }?>
              <?php if ($_smarty_tpl->tpl_vars['p']->value['Preis']>0) {?>
                <span class="shop_price"><?php echo smarty_modifier_numformat($_smarty_tpl->tpl_vars['p']->value['Preis']);?>
 <?php echo $_smarty_tpl->tpl_vars['currency_symbol']->value;?>
</span>
                <?php if ($_smarty_tpl->tpl_vars['no_nettodisplay']->value!=1) {?>
                  <?php if ($_smarty_tpl->tpl_vars['price_onlynetto']->value!=1) {?>
                    <br />
                    <div class="shop_subtext">
                      <?php if ($_smarty_tpl->tpl_vars['shopsettings']->value->NettoKlein==1) {?>
                        <?php echo $_smarty_tpl->getConfigVariable('Shop_netto');?>
 <?php echo smarty_modifier_numformat($_smarty_tpl->tpl_vars['p']->value['netto_price']);?>
 <?php echo $_smarty_tpl->tpl_vars['currency_symbol']->value;?>

                        <br />
                      <?php }?>
                    </div>
                  <?php }?>
                  <?php if ($_smarty_tpl->tpl_vars['price_onlynetto']->value==1&&!empty($_smarty_tpl->tpl_vars['p']->value['price_ust_ex'])) {?>
                    <br />
                    <div class="shop_subtext"> <?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['incpath']->value)."/shop/tax_inf_small.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
 </div>
                  <?php }?>
                <?php }?>
              <?php } else { ?>
                <span class="shop_price"><?php echo $_smarty_tpl->getConfigVariable('Zvonite');?>
</span>
              <?php }?>
            <?php } else { ?>
              <strong><?php echo $_smarty_tpl->getConfigVariable('Shop_prices_justforUsers');?>
</strong>
            <?php }?>
          </div>
        </div>
      <?php } ?>
    </div>
  </div>
</div>
<?php }?>
<?php }} ?>
