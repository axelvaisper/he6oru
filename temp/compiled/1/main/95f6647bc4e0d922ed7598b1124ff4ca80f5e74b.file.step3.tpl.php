<?php /* Smarty version Smarty-3.1.18, created on 2017-02-25 15:18:59
         compiled from "/home/vampireos/www/he6oru.localhost/setup/theme/step3.tpl" */ ?>
<?php /*%%SmartyHeaderCode:207023382158b027a89ac678-71856580%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '95f6647bc4e0d922ed7598b1124ff4ca80f5e74b' => 
    array (
      0 => '/home/vampireos/www/he6oru.localhost/setup/theme/step3.tpl',
      1 => 1488032335,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '207023382158b027a89ac678-71856580',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_58b027a8a9b288_61800872',
  'variables' => 
  array (
    'setupdir' => 0,
    'lang' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58b027a8a9b288_61800872')) {function content_58b027a8a9b288_61800872($_smarty_tpl) {?><script>
<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['theme']->value)."/validate.txt", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

$.validator.setDefaults({
  submitHandler: function() {
      document.forms['Form'].submit();
  }
});
$(document).ready(function() {
  $('#Form').validate({
      rules: {
          first: { required: false },
          last: { required: false },
          username: { required: true },
          pass: { required: true, minlength: 3 },
          email: { required: true, email: true },
          street: { required: false },
          zip: { required: false, number: false },
          town: { required: false },
          company: { required: false },
          websitename: { required: true }
      },
      messages: { }
  });
});
</script>


<h1><?php echo $_smarty_tpl->getConfigVariable('Step3');?>
</h1>

<form name="Form" id="Form" method="post" action="<?php echo $_smarty_tpl->tpl_vars['setupdir']->value;?>
/setup.php" cardx style="width:350px">

<label title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['s2_3inf']);?>
">
<small><?php echo $_smarty_tpl->getConfigVariable('s2_3');?>
</small>
<input name="username" type="text" value="<?php echo (($tmp = @sanitize($_POST['username']))===null||$tmp==='' ? '' : $tmp);?>
" />
</label>

<label title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['s2_5inf']);?>
">
<small><?php echo $_smarty_tpl->getConfigVariable('s2_5');?>
</small>
<input name="email" type="email" value="<?php echo (($tmp = @sanitize($_POST['email']))===null||$tmp==='' ? '' : $tmp);?>
" />
</label>

<label title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['s2_4inf']);?>
">
<small><?php echo $_smarty_tpl->getConfigVariable('s2_4');?>
</small>
<input name="pass" type="password" value="<?php echo (($tmp = @sanitize($_POST['pass']))===null||$tmp==='' ? '' : $tmp);?>
" />
</label>

<label><small><?php echo $_smarty_tpl->getConfigVariable('s2_1');?>
</small>
<input name="first" type="text" value="<?php echo (($tmp = @sanitize($_POST['first']))===null||$tmp==='' ? '' : $tmp);?>
" />
</label>

<label for="last"><small><?php echo $_smarty_tpl->getConfigVariable('s2_2');?>
</small>
<input name="last" type="text" value="<?php echo (($tmp = @sanitize($_POST['last']))===null||$tmp==='' ? '' : $tmp);?>
" />
</label>

<label><small><?php echo $_smarty_tpl->getConfigVariable('s2_6');?>
</small>
<input name="street" type="text"  value="<?php echo (($tmp = @sanitize($_POST['street']))===null||$tmp==='' ? '' : $tmp);?>
" />
</label>

<label><small><?php echo $_smarty_tpl->getConfigVariable('s2_7');?>
</small>
<input name="zip" type="text" value="<?php echo (($tmp = @sanitize($_POST['zip']))===null||$tmp==='' ? '' : $tmp);?>
" />
</label>

<label><small><?php echo $_smarty_tpl->getConfigVariable('s2_8');?>
</small>
<input name="town" type="text" value="<?php echo (($tmp = @sanitize($_POST['town']))===null||$tmp==='' ? '' : $tmp);?>
" />
</label>

<label><small><?php echo $_smarty_tpl->getConfigVariable('s2_9');?>
</small>
<input name="company" type="text" value="<?php echo (($tmp = @sanitize($_POST['company']))===null||$tmp==='' ? '' : $tmp);?>
" />
</label>

<label><small><?php echo $_smarty_tpl->getConfigVariable('s2_11');?>
</small>
<input name="websitename" type="text"  value="<?php echo (($tmp = @sanitize($_POST['websitename']))===null||$tmp==='' ? '' : $tmp);?>
" />
</label>

<label><small><?php echo $_smarty_tpl->getConfigVariable('s2_12');?>
</small>
<input name="phone" type="text" value="<?php echo (($tmp = @sanitize($_POST['phone']))===null||$tmp==='' ? '' : $tmp);?>
" />
</label>

<label><small><?php echo $_smarty_tpl->getConfigVariable('s2_13');?>
</small>
<input name="fax" type="text" value="<?php echo (($tmp = @sanitize($_POST['fax']))===null||$tmp==='' ? '' : $tmp);?>
" />
</label>

<p>
<input type="hidden" name="step" value="4" />
<input type="submit" value="<?php echo $_smarty_tpl->getConfigVariable('Step_Button');?>
" btnx/>
</p>

</form>
<?php }} ?>
