<?php /* Smarty version Smarty-3.1.18, created on 2017-02-25 21:09:53
         compiled from "/home/vampireos/www/he6oru.localhost/theme/standard/user/userloginpage.tpl" */ ?>
<?php /*%%SmartyHeaderCode:205559054858b1c87124c593-95425899%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f492ea65f65be13a564759ae67b9f9658a177602' => 
    array (
      0 => '/home/vampireos/www/he6oru.localhost/theme/standard/user/userloginpage.tpl',
      1 => 1406677827,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '205559054858b1c87124c593-95425899',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'loggedin' => 0,
    'welcome' => 0,
    'baseurl' => 0,
    'area' => 0,
    'LoginError' => 0,
    'imgpath_page' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_58b1c8712bcc28_55770791',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58b1c8712bcc28_55770791')) {function content_58b1c8712bcc28_55770791($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home/vampireos/www/he6oru.localhost/lib/smarty/plugins/modifier.date_format.php';
?><?php if ($_smarty_tpl->tpl_vars['loggedin']->value) {?>
  <?php if (isset($_GET['success'])&&$_GET['success']==1) {?>
    <div class="infobox"><strong><?php echo $_smarty_tpl->getConfigVariable('LoginExternSuccess');?>
</strong></div>
      <?php }?>
  <div class="infobox">
    <?php if (get_active('shop')) {?>
      <?php echo $_smarty_tpl->tpl_vars['welcome']->value;?>
, <strong><?php echo $_SESSION['benutzer_vorname'];?>
 <?php echo $_SESSION['benutzer_nachname'];?>
!</strong> <?php echo $_smarty_tpl->getConfigVariable('LoginExternCustomerNr');?>
: <strong><?php echo $_SESSION['benutzer_id'];?>
</strong>
      <br />
    <?php } else { ?>
      <?php echo $_smarty_tpl->tpl_vars['welcome']->value;?>
, <strong><?php echo $_SESSION['user_name'];?>
</strong>!
    <?php }?>
  </div>
  <strong><?php echo $_smarty_tpl->getConfigVariable('LoginExternActions');?>
</strong>
  <div class="infobox user_back_small" style="margin-top: 10px">
    <?php echo $_smarty_tpl->getConfigVariable('Arrow');?>
<a href="<?php echo $_smarty_tpl->tpl_vars['baseurl']->value;?>
/index.php?p=useraction&amp;action=profile"><?php echo $_smarty_tpl->getConfigVariable('LoginExternPc');?>
</a><br />
    <?php echo $_smarty_tpl->getConfigVariable('Arrow');?>
<a href="<?php echo $_smarty_tpl->tpl_vars['baseurl']->value;?>
/index.php?p=user&amp;id=<?php echo $_SESSION['benutzer_id'];?>
&amp;area=<?php echo $_smarty_tpl->tpl_vars['area']->value;?>
"><?php echo $_smarty_tpl->getConfigVariable('LoginExternVp');?>
</a><br />
    <?php echo $_smarty_tpl->getConfigVariable('Arrow');?>
<a href="<?php echo $_smarty_tpl->tpl_vars['baseurl']->value;?>
/index.php?p=useraction&amp;action=changepass"><?php echo $_smarty_tpl->getConfigVariable('LoginExternCp');?>
</a><br />
    <?php if (get_active('shop')) {?>
      <?php echo $_smarty_tpl->getConfigVariable('Arrow');?>
<a href="<?php echo $_smarty_tpl->tpl_vars['baseurl']->value;?>
/index.php?p=shop&amp;action=myorders"><?php echo $_smarty_tpl->getConfigVariable('Shop_go_myorders');?>
</a><br />
      <?php echo $_smarty_tpl->getConfigVariable('Arrow');?>
<a href="<?php echo $_smarty_tpl->tpl_vars['baseurl']->value;?>
/index.php?p=shop&amp;action=mydownloads"><?php echo $_smarty_tpl->getConfigVariable('LoginExternVd');?>
</a><br />
    <?php }?>
    <?php if (get_active('calendar')) {?>
      <?php echo $_smarty_tpl->getConfigVariable('Arrow');?>
<a href="<?php echo $_smarty_tpl->tpl_vars['baseurl']->value;?>
/index.php?p=calendar&amp;month=<?php echo smarty_modifier_date_format(time(),'m');?>
&amp;year=<?php echo smarty_modifier_date_format(time(),'Y');?>
&amp;area=<?php echo $_smarty_tpl->tpl_vars['area']->value;?>
&amp;show=private"><?php echo $_smarty_tpl->getConfigVariable('UserCalendar');?>
</a><br />
    <?php }?>
  </div>
  <div class="infobox">
    <?php if (permission('adminpanel')) {?>
      <?php echo $_smarty_tpl->getConfigVariable('Arrow');?>
<a href="javascript: void(0);" onclick="openWindow('<?php echo $_smarty_tpl->tpl_vars['baseurl']->value;?>
/admin', 'admin', '', '', 1);"><?php echo $_smarty_tpl->getConfigVariable('LoginExternAd');?>
</a> - <a target="_blank" href="<?php echo $_smarty_tpl->tpl_vars['baseurl']->value;?>
/admin"><?php echo $_smarty_tpl->getConfigVariable('LoginExternAd2');?>
</a>
      <br />
    <?php }?>
    <form method="post" name="logout_form_user" action="index.php">
      <input type="hidden" name="p" value="userlogin" />
      <input type="hidden" name="action" value="logout" />
      <input type="hidden" name="area" value="<?php echo $_smarty_tpl->tpl_vars['area']->value;?>
" />
      <input type="hidden" name="backurl" value="<?php echo base64_encode("index.php");?>
" />
      <?php echo $_smarty_tpl->getConfigVariable('Arrow');?>
<a onclick="return confirm('<?php echo $_smarty_tpl->getConfigVariable('Confirm_Logout');?>
');" href="javascript: document.forms['logout_form_user'].submit();"><?php echo $_smarty_tpl->getConfigVariable('Logout');?>
</a>
    </form>
    <br />
    <?php if (permission('deleteaccount')&&$_SESSION['benutzer_id']!=1) {?>
      <br />
      <?php echo $_smarty_tpl->getConfigVariable('Arrow');?>
<a href="index.php?p=useraction&amp;action=deleteaccount"><?php echo $_smarty_tpl->getConfigVariable('AccountDel');?>
</a>
    <?php }?>
  </div>
<?php } else { ?>
  <div id="logh">
    <?php if (isset($_smarty_tpl->tpl_vars['LoginError']->value)) {?>
      <div class="error_box"><strong><?php echo $_smarty_tpl->getConfigVariable('Error');?>
</strong> <?php echo $_smarty_tpl->getConfigVariable('WrongLoginData');?>
 </div>
        <?php }?>
    <div class="infobox user_back_small">
      <strong><?php echo $_smarty_tpl->getConfigVariable('LoginExternHeader');?>
</strong>
      <br />
      <br />
      <form  method="post" action="index.php?p=userlogin" name="login" id="ajlogintrue">
        <label for="login_email_r2"><strong><?php echo $_smarty_tpl->getConfigVariable('LoginMailUname');?>
</strong></label>
        <br />
        <input class="input_fields" type="text" name="login_email" id="login_email_r2" style="width: 180px" />
        <br />
        <label for="login_pass_r2"><strong><?php echo $_smarty_tpl->getConfigVariable('Pass');?>
</strong></label>
        <br />
        <input class="input_fields" type="password" name="login_pass" id="login_pass_r2" style="width: 180px" />
        <br />
        <label><input name="staylogged" type="checkbox" value="1" checked="checked" class="absmiddle" /> <?php echo $_smarty_tpl->getConfigVariable('LoginExternSave');?>
</label>
        <input type="hidden" name="p" value="userlogin" />
        <input type="hidden" name="action" value="newlogin" />
        <input type="hidden" name="area" value="<?php echo $_smarty_tpl->tpl_vars['area']->value;?>
" />
        <input type="hidden" name="backurl" value="<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['page_link'][0][0]->page_link(array(),$_smarty_tpl);?>
<?php echo base64_encode(ob_get_clean())?>" />
        <br />
        <br />
        <input type="submit" class="button" value="<?php echo $_smarty_tpl->getConfigVariable('Login_Button');?>
" onclick="document.getElementById('logh').style.display = 'none';
          document.getElementById('logspinner').style.display = ''" />
      </form>
    </div>
    <div class="box_data">
      <?php if (get_active('Register')) {?>
        <?php echo $_smarty_tpl->getConfigVariable('Arrow');?>
<a href="index.php?p=register&amp;area=<?php echo $_smarty_tpl->tpl_vars['area']->value;?>
"><?php echo $_smarty_tpl->getConfigVariable('LoginExternNew');?>
</a>
        <br />
      <?php }?>
      <?php echo $_smarty_tpl->getConfigVariable('Arrow');?>
<a href="index.php?p=pwlost"><?php echo $_smarty_tpl->getConfigVariable('LoginExternPwLost');?>
</a>
    </div>
  </div>
  <div id="logspinner" style="display: none">
    <div style="padding: 30px; text-align: center"><img src="<?php echo $_smarty_tpl->tpl_vars['imgpath_page']->value;?>
loading.gif" alt="" border="0" /></div>
  </div>
<?php }?>
<?php }} ?>
