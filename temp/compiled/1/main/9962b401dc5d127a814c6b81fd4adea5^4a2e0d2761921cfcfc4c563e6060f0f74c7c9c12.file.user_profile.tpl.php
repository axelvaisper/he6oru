<?php /* Smarty version Smarty-3.1.18, created on 2017-02-24 16:12:37
         compiled from "/home/vampireos/www/he6oru.localhost/theme/standard/user/user_profile.tpl" */ ?>
<?php /*%%SmartyHeaderCode:123464871458b0314567dd73-51960042%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4a2e0d2761921cfcfc4c563e6060f0f74c7c9c12' => 
    array (
      0 => '/home/vampireos/www/he6oru.localhost/theme/standard/user/user_profile.tpl',
      1 => 1405984662,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '123464871458b0314567dd73-51960042',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'user' => 0,
    'autoritet_bar' => 0,
    'autoritet' => 0,
    'lang' => 0,
    'user_thanks' => 0,
    'user_activity' => 0,
    'user_friends' => 0,
    'user_visits' => 0,
    'user_gallery' => 0,
    'user_gallery_profile' => 0,
    'imgpath_forums' => 0,
    'empty_profil' => 0,
    'user_videos' => 0,
    'u' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_58b03145737789_37360950',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58b03145737789_37360950')) {function content_58b03145737789_37360950($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home/vampireos/www/he6oru.localhost/lib/smarty/plugins/modifier.date_format.php';
?><script type="text/javascript">
<!-- //
$(document).ready(function() {
    $('.user_pop').colorbox({ height: "550px", width: "550px", iframe: true });
});
//-->
</script>

<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['incpath']->value)."/forums/user_panel_forums.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<div class="round">
  <div class="box_innerhead_userprofile"><?php echo $_smarty_tpl->getConfigVariable('MyAccount');?>
 <?php echo sanitize($_smarty_tpl->tpl_vars['user']->value['Benutzername']);?>
</div>
</div>
<?php if ($_smarty_tpl->tpl_vars['user']->value['Profil_public']==1||$_SESSION['benutzer_id']==$_REQUEST['id']) {?>
  <table width="100%" cellspacing="0" cellpadding="0">
    <tr>
      <td width="75%" valign="top">
        <div class="box_innerhead"><?php echo $_smarty_tpl->getConfigVariable('Profile_GenInfo');?>
</div>
        <div class="infobox">
          <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
              <td width="35%" align="right" class="row_first"><?php echo $_smarty_tpl->getConfigVariable('Autoritets');?>
: &nbsp;</td>
              <td class="row_second">
                <div style="width: 100%;border: 1px solid #665e58">
                  <div style="background-color: #F00;text-align: center;width: <?php echo $_smarty_tpl->tpl_vars['autoritet_bar']->value;?>
%"><strong><?php echo $_smarty_tpl->tpl_vars['autoritet']->value;?>
%</strong></div>
                </div>
              </td>
            </tr>
            <tr>
              <td width="35%" align="right" class="row_first"><?php echo $_smarty_tpl->getConfigVariable('Profile_Regged');?>
: &nbsp;</td>
              <td class="row_second"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['user']->value['Regdatum'],$_smarty_tpl->tpl_vars['lang']->value['DateFormatExtended']);?>
</td>
            </tr>
            <tr>
              <td width="35%" align="right" class="row_first"><?php echo $_smarty_tpl->getConfigVariable('Profile_CountProfile');?>
: &nbsp;</td>
              <td class="row_second"><?php echo $_smarty_tpl->tpl_vars['user']->value['Profil_Hits'];?>
</td>
            </tr>
            <?php if ($_smarty_tpl->tpl_vars['user']->value['Rang']) {?>
              <tr>
                <td width="35%" align="right" class="row_first"><?php echo $_smarty_tpl->getConfigVariable('Profile_Rank');?>
: &nbsp;</td>
                <td class="row_second">
                  <?php if ($_smarty_tpl->tpl_vars['user']->value['Team']==1) {?>
                    <?php echo sanitize($_smarty_tpl->tpl_vars['user']->value['TeamName']);?>

                  <?php } else { ?>
                    <?php echo sanitize($_smarty_tpl->tpl_vars['user']->value['Rang']);?>

                  <?php }?>
                </td>
              </tr>
            <?php }?>
            <tr>
              <td width="35%" align="right" class="row_first"><?php echo $_smarty_tpl->getConfigVariable('Profile_LastAction');?>
: &nbsp;</td>
              <td class="row_second"><?php if ($_smarty_tpl->tpl_vars['user']->value['Zuletzt_Aktiv']>1) {?><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['user']->value['Zuletzt_Aktiv'],$_smarty_tpl->tpl_vars['lang']->value['DateFormatExtended']);?>
<?php } else { ?>-<?php }?></td>
            </tr>
            <tr>
              <td width="35%" align="right" class="row_first"><?php echo $_smarty_tpl->getConfigVariable('Forums_Postings');?>
: &nbsp;</td>
              <td class="row_second"><?php if ($_smarty_tpl->tpl_vars['user']->value['Beitraege']>=1) {?><a href="index.php?p=forum&amp;action=print&amp;what=posting&amp;id=<?php echo $_smarty_tpl->tpl_vars['user']->value['Id'];?>
"><?php echo $_smarty_tpl->tpl_vars['user']->value['Beitraege'];?>
</a><?php } else { ?>-<?php }?></td>
            </tr>
            <?php if (isset($_smarty_tpl->tpl_vars['user_thanks']->value['num_post'])&&$_smarty_tpl->tpl_vars['user_thanks']->value['num_post']>=1) {?>
              <tr>
                <td width="35%" align="right" class="row_first"><?php echo $_smarty_tpl->getConfigVariable('ThanksMes');?>
: &nbsp;</td>
                <td class="row_second"><?php echo $_smarty_tpl->tpl_vars['user_thanks']->value['num_post'];?>
</td>
              </tr>
              <tr>
                <td width="35%" align="right" class="row_first"><?php echo $_smarty_tpl->getConfigVariable('ThanksAll');?>
: &nbsp;</td>
                <td class="row_second"><?php echo $_smarty_tpl->tpl_vars['user_thanks']->value['num_thanks'];?>
</td>
              </tr>
              <tr>
                <td width="35%" align="right" class="row_first"><?php echo $_smarty_tpl->getConfigVariable('ThanksUser');?>
: &nbsp;</td>
                <td class="row_second"><?php echo $_smarty_tpl->tpl_vars['user_thanks']->value['num_user'];?>
</td>
              </tr>
            <?php }?>
          </table>
        </div>
        <?php if (!empty($_smarty_tpl->tpl_vars['user']->value)) {?>
          <div class="box_innerhead"><?php echo $_smarty_tpl->getConfigVariable('PersonalData');?>
</div>
          <div class="infobox">
            <table width="100%" cellpadding="0" cellspacing="0">
              <?php if (!empty($_smarty_tpl->tpl_vars['user']->value['Status'])) {?>
                <tr>
                  <td align="right" class="row_first"><?php echo $_smarty_tpl->getConfigVariable('Profile_Status');?>
: &nbsp;</td>
                  <td class="row_second"><?php echo sanitize($_smarty_tpl->tpl_vars['user']->value['Status']);?>
</td>
                </tr>
              <?php }?>
              <?php if ($_smarty_tpl->tpl_vars['user']->value['Geburtstag_public']==1&&$_smarty_tpl->tpl_vars['user']->value['Geburtstag']>1) {?>
                <tr>
                  <td width="35%" align="right" class="row_first"><?php echo $_smarty_tpl->getConfigVariable('Birth');?>
: &nbsp;</td>
                  <td class="row_second"><?php echo sanitize($_smarty_tpl->tpl_vars['user']->value['Geburtstag']);?>
</td>
                </tr>
              <?php }?>
              <?php if ($_smarty_tpl->tpl_vars['user']->value['Geschlecht']!='-') {?>
                <tr>
                  <td width="35%" align="right" class="row_first"><?php echo $_smarty_tpl->getConfigVariable('Profile_Gender');?>
: &nbsp;</td>
                  <td class="row_second">
                    <?php if ($_smarty_tpl->tpl_vars['user']->value['Geschlecht']=='m') {?>
                      <?php echo $_smarty_tpl->getConfigVariable('User_Male');?>

                    <?php } elseif ($_smarty_tpl->tpl_vars['user']->value['Geschlecht']=='f') {?>
                      <?php echo $_smarty_tpl->getConfigVariable('User_Female');?>

                    <?php } else { ?>
                      <?php echo $_smarty_tpl->getConfigVariable('User_NoSettings');?>

                    <?php }?>
                  </td>
                </tr>
              <?php }?>
              <?php if (!empty($_smarty_tpl->tpl_vars['user']->value['Ort'])&&$_smarty_tpl->tpl_vars['user']->value['Ort_Public']==1) {?>
                <tr>
                  <td width="35%" align="right" class="row_first"><?php echo $_smarty_tpl->getConfigVariable('Town');?>
: &nbsp;</td>
                  <td class="row_second"> <?php echo sanitize($_smarty_tpl->tpl_vars['user']->value['Ort']);?>
 / <a href="http://maps.google.ru/maps?f=q&hl=<?php echo sanitize($_smarty_tpl->tpl_vars['user']->value['LandCode']);?>
&q=<?php echo sanitize($_smarty_tpl->tpl_vars['user']->value['Postleitzahl']);?>
+<?php echo sanitize(urlencode($_smarty_tpl->tpl_vars['user']->value['Ort']));?>
" target="_blank" rel="nofollow"><?php echo $_smarty_tpl->getConfigVariable('Profile_ShowGoogleMaps');?>
</a></td>
                </tr>
              <?php }?>
              <?php if (!empty($_smarty_tpl->tpl_vars['user']->value['Beruf'])) {?>
                <tr>
                  <td width="35%" align="right" class="row_first"><?php echo $_smarty_tpl->getConfigVariable('Profile_Job');?>
: &nbsp;</td>
                  <td class="row_second"><?php echo nl2br(sanitize($_smarty_tpl->tpl_vars['user']->value['Beruf']));?>
</td>
                </tr>
              <?php }?>
              <?php if (!empty($_smarty_tpl->tpl_vars['user']->value['Interessen'])) {?>
                <tr>
                  <td align="right" class="row_first"><?php echo $_smarty_tpl->getConfigVariable('Profile_Int');?>
: &nbsp;</td>
                  <td class="row_second"><?php echo nl2br(sanitize($_smarty_tpl->tpl_vars['user']->value['Interessen']));?>
</td>
                </tr>
              <?php }?>
              <?php if (!empty($_smarty_tpl->tpl_vars['user']->value['Hobbys'])) {?>
                <tr>
                  <td width="35%" align="right" class="row_first"><?php echo $_smarty_tpl->getConfigVariable('Profile_Hobbys');?>
: &nbsp;</td>
                  <td class="row_second"><?php echo nl2br(sanitize($_smarty_tpl->tpl_vars['user']->value['Hobbys']));?>
</td>
                </tr>
              <?php }?>
              <?php if (!empty($_smarty_tpl->tpl_vars['user']->value['Essen'])) {?>
                <tr>
                  <td width="35%" align="right" class="row_first"><?php echo $_smarty_tpl->getConfigVariable('Profile_Food');?>
: &nbsp;</td>
                  <td class="row_second"><?php echo nl2br(sanitize($_smarty_tpl->tpl_vars['user']->value['Essen']));?>
</td>
                </tr>
              <?php }?>
              <?php if (!empty($_smarty_tpl->tpl_vars['user']->value['Musik'])) {?>
                <tr>
                  <td align="right" class="row_first"><?php echo $_smarty_tpl->getConfigVariable('Profile_Music');?>
: &nbsp;</td>
                  <td class="row_second"><?php echo nl2br(sanitize($_smarty_tpl->tpl_vars['user']->value['Musik']));?>
</td>
                </tr>
              <?php }?>
              <?php if (!empty($_smarty_tpl->tpl_vars['user']->value['Films'])) {?>
                <tr>
                  <td align="right" class="row_first"><?php echo $_smarty_tpl->getConfigVariable('Profile_Films');?>
: &nbsp;</td>
                  <td class="row_second"><?php echo nl2br(sanitize($_smarty_tpl->tpl_vars['user']->value['Films']));?>
</td>
                </tr>
              <?php }?>
              <?php if (!empty($_smarty_tpl->tpl_vars['user']->value['Tele'])) {?>
                <tr>
                  <td align="right" class="row_first"><?php echo $_smarty_tpl->getConfigVariable('Profile_Tele');?>
: &nbsp;</td>
                  <td class="row_second"><?php echo nl2br(sanitize($_smarty_tpl->tpl_vars['user']->value['Tele']));?>
</td>
                </tr>
              <?php }?>
              <?php if (!empty($_smarty_tpl->tpl_vars['user']->value['Book'])) {?>
                <tr>
                  <td align="right" class="row_first"><?php echo $_smarty_tpl->getConfigVariable('Profile_Book');?>
: &nbsp;</td>
                  <td class="row_second"><?php echo nl2br(sanitize($_smarty_tpl->tpl_vars['user']->value['Book']));?>
</td>
                </tr>
              <?php }?>
              <?php if (!empty($_smarty_tpl->tpl_vars['user']->value['Game'])) {?>
                <tr>
                  <td align="right" class="row_first"><?php echo $_smarty_tpl->getConfigVariable('Profile_Game');?>
: &nbsp;</td>
                  <td class="row_second"><?php echo nl2br(sanitize($_smarty_tpl->tpl_vars['user']->value['Game']));?>
</td>
                </tr>
              <?php }?>
              <?php if (!empty($_smarty_tpl->tpl_vars['user']->value['Citat'])) {?>
                <tr>
                  <td align="right" class="row_first"><?php echo $_smarty_tpl->getConfigVariable('Profile_Citat');?>
: &nbsp;</td>
                  <td class="row_second"><?php echo nl2br(sanitize($_smarty_tpl->tpl_vars['user']->value['Citat']));?>
</td>
                </tr>
              <?php }?>
              <?php if (!empty($_smarty_tpl->tpl_vars['user']->value['Other'])) {?>
                <tr>
                  <td align="right" class="row_first"><?php echo $_smarty_tpl->getConfigVariable('Profile_Other');?>
: &nbsp;</td>
                  <td class="row_second"><?php echo nl2br(sanitize($_smarty_tpl->tpl_vars['user']->value['Other']));?>
</td>
                </tr>
              <?php }?>
            </table>
          </div>
        <?php }?>
        <?php echo $_smarty_tpl->tpl_vars['user_activity']->value;?>

        <?php echo $_smarty_tpl->tpl_vars['user_friends']->value;?>

        <?php echo $_smarty_tpl->tpl_vars['user_visits']->value;?>

        <?php echo $_smarty_tpl->tpl_vars['user_gallery']->value;?>

        <?php echo $_smarty_tpl->tpl_vars['user_gallery_profile']->value;?>

        <?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['incpath']->value)."/user/user_profile_guestbook.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

      </td>
      <td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;</td>
      <td width="25%" valign="top">
        <div class="box_innerhead"><?php echo $_smarty_tpl->getConfigVariable('Forums_avatar');?>
</div>
        <div class="infobox">
          <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
              <td align="center" class="row_second"><?php echo $_smarty_tpl->tpl_vars['user']->value['Avatar'];?>
</td>
            </tr>
          </table>
        </div>
        <div class="box_innerhead"><?php echo $_smarty_tpl->getConfigVariable('Imprint');?>
</div>
        <div class="infobox">
          <table width="100%" cellpadding="0" cellspacing="0">
            <?php if (!empty($_smarty_tpl->tpl_vars['user']->value['Webseite'])) {?>
              <?php $_smarty_tpl->tpl_vars['empty_profil'] = new Smarty_variable(1, null, 0);?>
              <tr>
                <td width="60%" align="right" class="row_first"><?php echo $_smarty_tpl->getConfigVariable('Web');?>
</td>
                <td class="row_second"><a rel="nofollow" href="<?php echo sanitize($_smarty_tpl->tpl_vars['user']->value['Webseite']);?>
" target="_blank"> <img src="<?php echo $_smarty_tpl->tpl_vars['imgpath_forums']->value;?>
home.png" border="" alt="<?php echo sanitize($_smarty_tpl->tpl_vars['user']->value['Webseite']);?>
" /></a></td>
              </tr>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['user']->value['Email_User']) {?>
              <?php $_smarty_tpl->tpl_vars['empty_profil'] = new Smarty_variable(1, null, 0);?>
              <tr>
                <td width="60%" align="right" class="row_first"><?php echo $_smarty_tpl->getConfigVariable('Profile_EmailContact');?>
&nbsp;</td>
                <td class="row_second"><?php echo $_smarty_tpl->tpl_vars['user']->value['Email_User'];?>
&nbsp;</td>
              </tr>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['user']->value['Pn_User']) {?>
              <?php $_smarty_tpl->tpl_vars['empty_profil'] = new Smarty_variable(1, null, 0);?>
              <tr>
                <td width="60%" align="right" class="row_first"><?php echo $_smarty_tpl->getConfigVariable('Profile_SendPN');?>
&nbsp;</td>
                <td class="row_second"><?php echo $_smarty_tpl->tpl_vars['user']->value['Pn_User'];?>
</td>
              </tr>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['user']->value['Icq_User']) {?>
              <?php $_smarty_tpl->tpl_vars['empty_profil'] = new Smarty_variable(1, null, 0);?>
              <tr>
                <td width="60%" align="right" class="row_first"><?php echo $_smarty_tpl->getConfigVariable('Profile_ICQ');?>
&nbsp;</td>
                <td class="row_second"><?php echo $_smarty_tpl->tpl_vars['user']->value['Icq_User'];?>
</td>
              </tr>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['user']->value['Skype_User']) {?>
              <?php $_smarty_tpl->tpl_vars['empty_profil'] = new Smarty_variable(1, null, 0);?>
              <tr>
                <td width="60%" align="right" class="row_first"><?php echo $_smarty_tpl->getConfigVariable('Profile_ScypeOpt');?>
&nbsp;</td>
                <td class="row_second"><?php echo $_smarty_tpl->tpl_vars['user']->value['Skype_User'];?>
</td>
              </tr>
            <?php }?>
            <?php if (!empty($_smarty_tpl->tpl_vars['user']->value['msn'])) {?>
              <?php $_smarty_tpl->tpl_vars['empty_profil'] = new Smarty_variable(1, null, 0);?>
              <tr>
                <td width="60%" align="right" class="row_first"><?php echo $_smarty_tpl->getConfigVariable('Profile_MSN');?>
&nbsp;</td>
                <td class="row_second"><?php echo sanitize($_smarty_tpl->tpl_vars['user']->value['msn']);?>
</td>
              </tr>
            <?php }?>
            <?php if (!empty($_smarty_tpl->tpl_vars['user']->value['Vkontakte'])) {?>
              <?php $_smarty_tpl->tpl_vars['empty_profil'] = new Smarty_variable(1, null, 0);?>
              <tr>
                <td width="60%" align="right" class="row_first"><?php echo $_smarty_tpl->getConfigVariable('Vkontakte');?>
</td>
                <td class="row_second"><a rel="nofollow" href="<?php echo sanitize($_smarty_tpl->tpl_vars['user']->value['Vkontakte']);?>
" target="_blank"> <img src="<?php echo $_smarty_tpl->tpl_vars['imgpath_forums']->value;?>
vkontakte.png" border="" alt="<?php echo sanitize($_smarty_tpl->tpl_vars['user']->value['Vkontakte']);?>
" /></a></td>
              </tr>
            <?php }?>
            <?php if (!empty($_smarty_tpl->tpl_vars['user']->value['Odnoklassniki'])) {?>
              <?php $_smarty_tpl->tpl_vars['empty_profil'] = new Smarty_variable(1, null, 0);?>
              <tr>
                <td width="60%" align="right" class="row_first"><?php echo $_smarty_tpl->getConfigVariable('Odnoklassniki');?>
</td>
                <td class="row_second"><a rel="nofollow" href="<?php echo sanitize($_smarty_tpl->tpl_vars['user']->value['Odnoklassniki']);?>
" target="_blank"> <img src="<?php echo $_smarty_tpl->tpl_vars['imgpath_forums']->value;?>
odnoklassniki.png" border="" alt="<?php echo sanitize($_smarty_tpl->tpl_vars['user']->value['Odnoklassniki']);?>
" /></a></td>
              </tr>
            <?php }?>
            <?php if (!empty($_smarty_tpl->tpl_vars['user']->value['Mymail'])) {?>
              <?php $_smarty_tpl->tpl_vars['empty_profil'] = new Smarty_variable(1, null, 0);?>
              <tr>
                <td width="60%" align="right" class="row_first"><?php echo $_smarty_tpl->getConfigVariable('Mymail');?>
</td>
                <td class="row_second"><a rel="nofollow" href="<?php echo sanitize($_smarty_tpl->tpl_vars['user']->value['Mymail']);?>
" target="_blank"> <img src="<?php echo $_smarty_tpl->tpl_vars['imgpath_forums']->value;?>
mymail.png" border="" alt="<?php echo sanitize($_smarty_tpl->tpl_vars['user']->value['Mymail']);?>
" /></a></td>
              </tr>
            <?php }?>
            <?php if (!empty($_smarty_tpl->tpl_vars['user']->value['Google'])) {?>
              <?php $_smarty_tpl->tpl_vars['empty_profil'] = new Smarty_variable(1, null, 0);?>
              <tr>
                <td width="60%" align="right" class="row_first"><?php echo $_smarty_tpl->getConfigVariable('Google');?>
</td>
                <td class="row_second"><a rel="nofollow" href="<?php echo sanitize($_smarty_tpl->tpl_vars['user']->value['Google']);?>
" target="_blank"> <img src="<?php echo $_smarty_tpl->tpl_vars['imgpath_forums']->value;?>
google.png" border="" alt="<?php echo sanitize($_smarty_tpl->tpl_vars['user']->value['Google']);?>
" /></a></td>
              </tr>
            <?php }?>
            <?php if (!empty($_smarty_tpl->tpl_vars['user']->value['Facebook'])) {?>
              <?php $_smarty_tpl->tpl_vars['empty_profil'] = new Smarty_variable(1, null, 0);?>
              <tr>
                <td width="60%" align="right" class="row_first"><?php echo $_smarty_tpl->getConfigVariable('Facebook');?>
</td>
                <td class="row_second"><a rel="nofollow" href="<?php echo sanitize($_smarty_tpl->tpl_vars['user']->value['Facebook']);?>
" target="_blank"> <img src="<?php echo $_smarty_tpl->tpl_vars['imgpath_forums']->value;?>
facebook.png" border="" alt="<?php echo sanitize($_smarty_tpl->tpl_vars['user']->value['Facebook']);?>
" /></a></td>
              </tr>
            <?php }?>
            <?php if (!empty($_smarty_tpl->tpl_vars['user']->value['Twitter'])) {?>
              <?php $_smarty_tpl->tpl_vars['empty_profil'] = new Smarty_variable(1, null, 0);?>
              <tr>
                <td width="60%" align="right" class="row_first"><?php echo $_smarty_tpl->getConfigVariable('Twitter');?>
</td>
                <td class="row_second"><a rel="nofollow" href="<?php echo sanitize($_smarty_tpl->tpl_vars['user']->value['Twitter']);?>
" target="_blank"> <img src="<?php echo $_smarty_tpl->tpl_vars['imgpath_forums']->value;?>
twitter.png" border="" alt="<?php echo sanitize($_smarty_tpl->tpl_vars['user']->value['Twitter']);?>
" /></a></td>
              </tr>
            <?php }?>
            <?php if (empty($_smarty_tpl->tpl_vars['empty_profil']->value)) {?>
              <tr>
                <td colspan="2" class="row_first"><?php echo $_smarty_tpl->getConfigVariable('ProfilContactEmpty');?>
</a></td>
              </tr>
            <?php }?>
          </table>
        </div>
        <?php if (get_active('user_videos')&&$_smarty_tpl->tpl_vars['user_videos']->value) {?> <a name="uservideos"></a>
          <div class="box_innerhead"><?php echo $_smarty_tpl->getConfigVariable('Forums_UserVideos');?>
 <?php echo sanitize($_smarty_tpl->tpl_vars['user']->value['Benutzername']);?>
</div>
          <div class="infobox" style="text-align: center">
            <?php  $_smarty_tpl->tpl_vars['u'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['u']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['user_videos']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['u']->key => $_smarty_tpl->tpl_vars['u']->value) {
$_smarty_tpl->tpl_vars['u']->_loop = true;
?>
              <?php if ($_smarty_tpl->tpl_vars['u']->value->Name) {?>
                <div style="margin-top: 5px">
                  <h3><?php echo $_smarty_tpl->tpl_vars['u']->value->Name;?>
</h3>
                </div>
              <?php }?>
              <?php echo $_smarty_tpl->tpl_vars['u']->value->VideoData;?>

              <br />
              <br />
            <?php } ?>
          </div>
        <?php }?>
      </td>
    </tr>
  </table>
<?php } else { ?>
  <br />
  <h3><?php echo $_smarty_tpl->getConfigVariable('Profile_NotPublicThis');?>
</h3>
  <br />
<?php }?>
<?php }} ?>
