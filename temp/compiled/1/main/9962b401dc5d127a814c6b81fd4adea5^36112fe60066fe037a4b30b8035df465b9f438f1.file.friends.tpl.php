<?php /* Smarty version Smarty-3.1.18, created on 2017-02-24 16:12:37
         compiled from "/home/vampireos/www/he6oru.localhost/theme/standard/user/friends.tpl" */ ?>
<?php /*%%SmartyHeaderCode:206019230958b03145600b40-65465527%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '36112fe60066fe037a4b30b8035df465b9f438f1' => 
    array (
      0 => '/home/vampireos/www/he6oru.localhost/theme/standard/user/friends.tpl',
      1 => 1405722069,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '206019230958b03145600b40-65465527',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'num' => 0,
    'area' => 0,
    'num_all' => 0,
    'isf' => 0,
    'friends' => 0,
    'n' => 0,
    'NLine' => 0,
    'avatar' => 0,
    'f' => 0,
    'newfriends' => 0,
    'nn' => 0,
    'newf' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_58b0314563c257_01769620',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58b0314563c257_01769620')) {function content_58b0314563c257_01769620($_smarty_tpl) {?><a name="friends"></a>
<div class="box_innerhead"><strong><?php echo $_smarty_tpl->getConfigVariable('Profile_Friends');?>
</strong></div>
<div class="infobox">
  <?php echo $_smarty_tpl->getConfigVariable('SeenFriend');?>
 <?php echo $_smarty_tpl->tpl_vars['num']->value;?>
 <?php echo $_smarty_tpl->getConfigVariable('FriendsIs');?>
 <a href="index.php?p=user&amp;id=<?php echo $_REQUEST['id'];?>
&amp;area=<?php echo $_smarty_tpl->tpl_vars['area']->value;?>
&amp;friends=all#friends"><?php echo $_smarty_tpl->tpl_vars['num_all']->value;?>
 <?php echo $_smarty_tpl->getConfigVariable('Friends');?>
</a>&nbsp;&nbsp;&nbsp;&nbsp;
  <?php if ($_REQUEST['id']!=$_SESSION['benutzer_id']&&$_SESSION['benutzer_id']!='0') {?>
    <?php if ($_smarty_tpl->tpl_vars['isf']->value==0) {?>
      <?php echo $_smarty_tpl->getConfigVariable('WaitFriend');?>

    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['isf']->value==1) {?>
      <a href="index.php?p=user&amp;action=friends&amp;do=add&amp;id=<?php echo $_REQUEST['id'];?>
&amp;area=<?php echo $_smarty_tpl->tpl_vars['area']->value;?>
"><?php echo $_smarty_tpl->getConfigVariable('FriendshipCreate');?>
</a>
    <?php }?>
  <?php }?>
  <br />
  <br />
  <table>
    <tr>
      <?php $_smarty_tpl->tpl_vars['n'] = new Smarty_variable(-1, null, 0);?>
      <?php  $_smarty_tpl->tpl_vars['f'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['f']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['friends']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['f']->key => $_smarty_tpl->tpl_vars['f']->value) {
$_smarty_tpl->tpl_vars['f']->_loop = true;
?>
        <?php $_smarty_tpl->tpl_vars['n'] = new Smarty_variable($_smarty_tpl->tpl_vars['n']->value+1, null, 0);?>
        <?php if ($_smarty_tpl->tpl_vars['n']->value==$_smarty_tpl->tpl_vars['NLine']->value) {?>
        </tr>
        <tr><td>&nbsp;</td></tr>
        <tr>
          <?php $_smarty_tpl->tpl_vars['n'] = new Smarty_variable(0, null, 0);?>
        <?php }?>
        <td width="<?php echo $_smarty_tpl->tpl_vars['avatar']->value+14;?>
" height="80" valign="bottom" style="text-align: center">
          <a title="<?php echo $_smarty_tpl->tpl_vars['f']->value['Freundname'];?>
" href="index.php?p=user&amp;id=<?php echo $_smarty_tpl->tpl_vars['f']->value['FreundId'];?>
&amp;area=<?php echo $_smarty_tpl->tpl_vars['area']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['f']->value['Avatar'];?>
</a>
          <br />
          <a href="index.php?p=user&amp;id=<?php echo $_smarty_tpl->tpl_vars['f']->value['FreundId'];?>
&amp;area=<?php echo $_smarty_tpl->tpl_vars['area']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['f']->value['Freundname'];?>
</a>
          <br />
          <?php if ($_REQUEST['id']==$_SESSION['benutzer_id']) {?>
            <small><a onclick="return confirm('<?php echo $_smarty_tpl->getConfigVariable('FriendshipDel_C');?>
');" href="index.php?p=user&amp;action=friends&amp;do=del&amp;id=<?php echo $_smarty_tpl->tpl_vars['f']->value['FreundId'];?>
&amp;area=<?php echo $_smarty_tpl->tpl_vars['area']->value;?>
"><?php echo $_smarty_tpl->getConfigVariable('Delete');?>
</a></small>
            <?php }?>
        </td>
      <?php } ?>
    </tr>
  </table>
  <?php if (!$_smarty_tpl->tpl_vars['friends']->value) {?>
    <small><?php echo $_smarty_tpl->getConfigVariable('NoFriends');?>
</small>
  <?php }?>
  <?php if ($_REQUEST['id']==$_SESSION['benutzer_id']) {?>
    <?php if ($_smarty_tpl->tpl_vars['newfriends']->value) {?>
      <br />
      <br />
      <strong><?php echo $_smarty_tpl->getConfigVariable('AddUsersFriend');?>
</strong>
      <br />
      <br />
      <table>
        <tr>
          <?php $_smarty_tpl->tpl_vars['nn'] = new Smarty_variable(-1, null, 0);?>
          <?php  $_smarty_tpl->tpl_vars['newf'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['newf']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['newfriends']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['newf']->key => $_smarty_tpl->tpl_vars['newf']->value) {
$_smarty_tpl->tpl_vars['newf']->_loop = true;
?>
            <?php $_smarty_tpl->tpl_vars['nn'] = new Smarty_variable($_smarty_tpl->tpl_vars['nn']->value+1, null, 0);?>
            <?php if ($_smarty_tpl->tpl_vars['nn']->value==$_smarty_tpl->tpl_vars['NLine']->value) {?>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr>
              <?php $_smarty_tpl->tpl_vars['nn'] = new Smarty_variable(0, null, 0);?>
            <?php }?>
            <td width="<?php echo $_smarty_tpl->tpl_vars['avatar']->value+14;?>
" height="80" valign="bottom" style="text-align: center">
              <a title="<?php echo $_smarty_tpl->tpl_vars['newf']->value['Freundname'];?>
" href="index.php?p=user&amp;id=<?php echo $_smarty_tpl->tpl_vars['newf']->value['BenutzerId'];?>
"><?php echo $_smarty_tpl->tpl_vars['newf']->value['Avatar'];?>
</a>
              <br />
              <a href="index.php?p=user&amp;id=<?php echo $_smarty_tpl->tpl_vars['newf']->value['BenutzerId'];?>
"><?php echo $_smarty_tpl->tpl_vars['newf']->value['Freundname'];?>
</a>
              <br />
              <small><a href="index.php?p=user&amp;action=friends&amp;do=confirm&amp;id=<?php echo $_smarty_tpl->tpl_vars['newf']->value['Id'];?>
&amp;area=<?php echo $_smarty_tpl->tpl_vars['area']->value;?>
"><?php echo $_smarty_tpl->getConfigVariable('Gl_Ok');?>
</a></small>
              <br />
              <small><a href="index.php?p=user&amp;action=friends&amp;do=del&amp;id=<?php echo $_smarty_tpl->tpl_vars['newf']->value['BenutzerId'];?>
&amp;area=<?php echo $_smarty_tpl->tpl_vars['area']->value;?>
"><?php echo $_smarty_tpl->getConfigVariable('Gl_No');?>
</a></small>
            </td>
          <?php } ?>
        </tr>
      </table>
    <?php }?>
  <?php }?>
</div>
<?php }} ?>
