<?php /* Smarty version Smarty-3.1.18, created on 2017-02-25 21:11:30
         compiled from "/home/vampireos/www/he6oru.localhost/theme/standard/shop/shop_start_products_new.tpl" */ ?>
<?php /*%%SmartyHeaderCode:121704803558b1c8d2b73423-08439651%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '004cb517f827e01a4630087a5276de880c3aa899' => 
    array (
      0 => '/home/vampireos/www/he6oru.localhost/theme/standard/shop/shop_start_products_new.tpl',
      1 => 1406959098,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '121704803558b1c8d2b73423-08439651',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'products_array' => 0,
    'basepath' => 0,
    'count2' => 0,
    'newest_colums' => 0,
    'nc' => 0,
    'count_split' => 0,
    'div_split' => 0,
    'count' => 0,
    'colums_width' => 0,
    'p' => 0,
    'shopsettings' => 0,
    'loggedin' => 0,
    'currency_symbol' => 0,
    'no_nettodisplay' => 0,
    'price_onlynetto' => 0,
    'fsk_user' => 0,
    'imgpath' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_58b1c8d2bf1595_57650134',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58b1c8d2bf1595_57650134')) {function content_58b1c8d2bf1595_57650134($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_truncate')) include '/home/vampireos/www/he6oru.localhost/lib/smarty/plugins/modifier.truncate.php';
if (!is_callable('smarty_modifier_numformat')) include '/home/vampireos/www/he6oru.localhost/lib/smarty/statusplugins/modifier.numformat.php';
?><?php if ($_smarty_tpl->tpl_vars['products_array']->value) {?>
<script type="text/javascript">
<!-- //
togglePanel('navpanel_shopinnew', 'toggler', 30, '<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
');

$(document).ready(function() {
    $('#container-options ul.rounded').tabs();

    var options = { target: '#ajaxbasket', timeout: 3000 };
    $('.new_products').submit(function() {
        showNotice($('#new_prodmessage'), 10000);
        $(this).ajaxSubmit(options);
        return false;
    });
    $('#new_yes').on('click', function() {
        document.location = 'index.php?action=showbasket&p=shop';
        $.unblockUI();
        return false;
    });
    $('#new_no').on('click', function() {
        $.unblockUI();
        return false;
    });
});
//-->
</script>

<div id="new_prodmessage" style="display: none">
  <br />
  <p class="h3"><?php echo $_smarty_tpl->getConfigVariable('Shop_ProdAddedToBasket');?>
</p>
  <p><?php echo $_smarty_tpl->getConfigVariable('LoginExternActions');?>
</p>
  <input class="shop_buttons_big" type="button" id="new_yes" value="<?php echo $_smarty_tpl->getConfigVariable('Shop_go_basket');?>
" />
  <input class="shop_buttons_big_second" type="button" id="new_no" value="<?php echo $_smarty_tpl->getConfigVariable('WinClose');?>
" />
  <br />
  <br />
</div>
<div class="opener">
  <div class="opened" id="navpanel_shopinnew" title="&lt;h3&gt;<?php echo $_smarty_tpl->getConfigVariable('Shop_NewProducts');?>
&lt;/h3&gt;">
    <div id="container-options">
      <ul class="rounded">
        <?php $_smarty_tpl->tpl_vars['count2'] = new Smarty_variable(0, null, 0);?>
        <?php $_smarty_tpl->tpl_vars['nc'] = new Smarty_variable(0, null, 0);?>
        <?php  $_smarty_tpl->tpl_vars['p'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['p']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['products_array']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['p']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['p']->iteration=0;
 $_smarty_tpl->tpl_vars['p']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['p']->key => $_smarty_tpl->tpl_vars['p']->value) {
$_smarty_tpl->tpl_vars['p']->_loop = true;
 $_smarty_tpl->tpl_vars['p']->iteration++;
 $_smarty_tpl->tpl_vars['p']->index++;
 $_smarty_tpl->tpl_vars['p']->first = $_smarty_tpl->tpl_vars['p']->index === 0;
 $_smarty_tpl->tpl_vars['p']->last = $_smarty_tpl->tpl_vars['p']->iteration === $_smarty_tpl->tpl_vars['p']->total;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['pro']['first'] = $_smarty_tpl->tpl_vars['p']->first;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['pro']['last'] = $_smarty_tpl->tpl_vars['p']->last;
?>
          <?php $_smarty_tpl->tpl_vars['count2'] = new Smarty_variable($_smarty_tpl->tpl_vars['count2']->value+1, null, 0);?>
          <?php if ($_smarty_tpl->tpl_vars['count2']->value%$_smarty_tpl->tpl_vars['newest_colums']->value==0&&!$_smarty_tpl->getVariable('smarty')->value['foreach']['pro']['last']||($_smarty_tpl->getVariable('smarty')->value['foreach']['pro']['first'])) {?>
            <?php $_smarty_tpl->tpl_vars['nc'] = new Smarty_variable($_smarty_tpl->tpl_vars['nc']->value+1, null, 0);?>
            <li><a href="#opt-<?php echo $_smarty_tpl->tpl_vars['nc']->value;?>
"><span><?php echo $_smarty_tpl->getConfigVariable('PageNavi_Page');?>
 <?php echo $_smarty_tpl->tpl_vars['nc']->value;?>
</span></a></li>
            <?php }?>
          <?php } ?>
      </ul>
      <div class="clear"></div>
      <div id="opt-1" class="ui-tabs-panel-content">
        <table width="100%" cellpadding="0" cellspacing="0">
          <tr>
            <?php $_smarty_tpl->tpl_vars['count_split'] = new Smarty_variable(0, null, 0);?>
            <?php $_smarty_tpl->tpl_vars['div_split'] = new Smarty_variable(0, null, 0);?>
            <?php $_smarty_tpl->tpl_vars['count'] = new Smarty_variable(0, null, 0);?>
            <?php  $_smarty_tpl->tpl_vars['p'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['p']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['products_array']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['p']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['p']->iteration=0;
 $_smarty_tpl->tpl_vars['p']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['p']->key => $_smarty_tpl->tpl_vars['p']->value) {
$_smarty_tpl->tpl_vars['p']->_loop = true;
 $_smarty_tpl->tpl_vars['p']->iteration++;
 $_smarty_tpl->tpl_vars['p']->index++;
 $_smarty_tpl->tpl_vars['p']->first = $_smarty_tpl->tpl_vars['p']->index === 0;
 $_smarty_tpl->tpl_vars['p']->last = $_smarty_tpl->tpl_vars['p']->iteration === $_smarty_tpl->tpl_vars['p']->total;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['pro']['first'] = $_smarty_tpl->tpl_vars['p']->first;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['pro']['last'] = $_smarty_tpl->tpl_vars['p']->last;
?>
              <?php $_smarty_tpl->tpl_vars['count_split'] = new Smarty_variable($_smarty_tpl->tpl_vars['count_split']->value+1, null, 0);?>
              <?php if ($_smarty_tpl->tpl_vars['count_split']->value%$_smarty_tpl->tpl_vars['newest_colums']->value==0&&!$_smarty_tpl->getVariable('smarty')->value['foreach']['pro']['last']||($_smarty_tpl->getVariable('smarty')->value['foreach']['pro']['first'])) {?>
                <?php $_smarty_tpl->tpl_vars['div_split'] = new Smarty_variable($_smarty_tpl->tpl_vars['div_split']->value+1, null, 0);?>
              <?php }?>
              <?php $_smarty_tpl->tpl_vars['count'] = new Smarty_variable($_smarty_tpl->tpl_vars['count']->value+1, null, 0);?>
              <td style="width: <?php echo $_smarty_tpl->tpl_vars['colums_width']->value;?>
%">
                <div class="shop_newest_boxes">
                  <div class="<?php if ($_smarty_tpl->tpl_vars['count']->value%$_smarty_tpl->tpl_vars['newest_colums']->value==0) {?>shop_newest_first<?php } else { ?>shop_newest_second<?php }?>" <?php if ($_smarty_tpl->tpl_vars['count']->value>$_smarty_tpl->tpl_vars['newest_colums']->value) {?>style="border-top: 0px"<?php }?>>
                    <form class="new_products" method="post" action="<?php if (empty($_smarty_tpl->tpl_vars['p']->value['Vars'])&&$_smarty_tpl->tpl_vars['p']->value['Lagerbestand']>0) {?>index.php?p=shop<?php } else { ?>index.php?p=shop&amp;action=showproduct&amp;id=<?php echo $_smarty_tpl->tpl_vars['p']->value['Id'];?>
<?php }?>">
                      <div class="shop_product_text">
                        <div class="shop_image_newstart"> <a class="stip" title="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['tooltip'][0][0]->tooltip($_smarty_tpl->tpl_vars['p']->value['Beschreibung'],200);?>
" href="<?php echo $_smarty_tpl->tpl_vars['p']->value['ProdLink'];?>
<?php if (isset($_REQUEST['blanc'])&&$_REQUEST['blanc']==1) {?>&amp;blanc=1<?php }?>"><img src="<?php echo $_smarty_tpl->tpl_vars['p']->value['Bild_Mittel'];?>
" alt="<?php echo sanitize($_smarty_tpl->tpl_vars['p']->value['Titel']);?>
" border="0" /></a> </div>
                        <div class="shop_product_title_new">
                          <h2><a  class="stip" title="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['tooltip'][0][0]->tooltip($_smarty_tpl->tpl_vars['p']->value['Beschreibung'],200);?>
" href="<?php echo $_smarty_tpl->tpl_vars['p']->value['ProdLink'];?>
<?php if (isset($_REQUEST['blanc'])&&$_REQUEST['blanc']==1) {?>&amp;blanc=1<?php }?>"><?php echo sanitize(smarty_modifier_truncate($_smarty_tpl->tpl_vars['p']->value['Titel'],25));?>
</a></h2>
                        </div>
                      </div>
                      <div class="shop_price_detail_footer">
                        <?php if ($_smarty_tpl->tpl_vars['shopsettings']->value->PreiseGaeste==1||$_smarty_tpl->tpl_vars['loggedin']->value) {?>
                          <?php if ($_smarty_tpl->tpl_vars['p']->value['Preis']>0) {?>
                            <?php if ($_smarty_tpl->tpl_vars['p']->value['Preis_Liste']!=$_smarty_tpl->tpl_vars['p']->value['Preis']) {?>
                              <?php echo $_smarty_tpl->getConfigVariable('Shop_instead');?>
&nbsp;&nbsp;<span class="shop_price_old"><?php echo smarty_modifier_numformat($_smarty_tpl->tpl_vars['p']->value['Preis_Liste']);?>
 <?php echo $_smarty_tpl->tpl_vars['currency_symbol']->value;?>
</span>
                              <br/>
                            <?php } else { ?>
                              <br />
                            <?php }?>
                            <?php if (!empty($_smarty_tpl->tpl_vars['p']->value['Vars'])) {?>
                              <?php echo $_smarty_tpl->getConfigVariable('Shop_priceFrom');?>

                            <?php }?> <span class="shop_price_start"><?php echo smarty_modifier_numformat($_smarty_tpl->tpl_vars['p']->value['Preis']);?>
 <?php echo $_smarty_tpl->tpl_vars['currency_symbol']->value;?>
</span> <?php if ($_smarty_tpl->tpl_vars['no_nettodisplay']->value!=1) {?>
                              <?php if ($_smarty_tpl->tpl_vars['price_onlynetto']->value!=1) {?> <br />
                                <div class="shop_subtext">
                                  <?php if ($_smarty_tpl->tpl_vars['shopsettings']->value->NettoKlein==1) {?>
                                    <?php echo $_smarty_tpl->getConfigVariable('Shop_netto');?>
 <?php echo smarty_modifier_numformat($_smarty_tpl->tpl_vars['p']->value['netto_price']);?>
 <?php echo $_smarty_tpl->tpl_vars['currency_symbol']->value;?>

                                    <br />
                                  <?php }?>
                                </div>
                              <?php }?>
                              <?php if ($_smarty_tpl->tpl_vars['price_onlynetto']->value==1&&!empty($_smarty_tpl->tpl_vars['p']->value['price_ust_ex'])) {?>
                                <br />
                                <div class="shop_subtext">
                                  <?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['incpath']->value)."/shop/tax_inf_small.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

                                </div>
                              <?php }?>
                            <?php }?>
                          <?php } else { ?>
                            <br />
                            <span class="shop_price_start"><?php echo $_smarty_tpl->getConfigVariable('Zvonite');?>
</span>
                            <br />
                            <br />
                          <?php }?>
                          <?php if ($_smarty_tpl->tpl_vars['p']->value['Fsk18']==1&&$_smarty_tpl->tpl_vars['fsk_user']->value!=1) {?>
                            <br />
                            <button class="shop_buttons_big_second" type="button" onclick="location.href = '<?php echo $_smarty_tpl->tpl_vars['p']->value['ProdLink'];?>
';"><img src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/shop/basket_simple.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('buttonDetails');?>
</button>
                            <?php } else { ?>
                              <?php if (empty($_smarty_tpl->tpl_vars['p']->value['Vars'])&&$_smarty_tpl->tpl_vars['p']->value['Lagerbestand']>0&&$_smarty_tpl->tpl_vars['p']->value['Preis']>0&&empty($_smarty_tpl->tpl_vars['p']->value['Frei_1'])&&empty($_smarty_tpl->tpl_vars['p']->value['Frei_2'])&&empty($_smarty_tpl->tpl_vars['p']->value['Frei_3'])) {?>
                              <input type="hidden" name="amount" value="1" />
                              <input type="hidden" name="action" value="to_cart" />
                              <input type="hidden" name="redir" value="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['page_link'][0][0]->page_link(array(),$_smarty_tpl);?>
" />
                              <input type="hidden" name="product_id" value="<?php echo $_smarty_tpl->tpl_vars['p']->value['Id'];?>
" />
                              <input type="hidden" name="ajax" value="1" />
                              <noscript>
                              <input type="hidden" name="ajax" value="0" />
                              </noscript>
                              <br />
                              <button id="modal_<?php echo $_smarty_tpl->tpl_vars['p']->value['Id'];?>
" class="shop_buttons_big" type="submit"><img src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/shop/basket_simple.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Shop_toBasket');?>
</button>
                              <?php } else { ?>
                              <input type="hidden" name="cid" value="<?php echo (($tmp = @$_smarty_tpl->tpl_vars['p']->value['Kategorie'])===null||$tmp==='' ? '' : $tmp);?>
" />
                              <input type="hidden" name="parent" value="<?php echo (($tmp = @$_smarty_tpl->tpl_vars['p']->value['Parent'])===null||$tmp==='' ? '' : $tmp);?>
" />
                              <input type="hidden" name="navop" value="<?php echo (($tmp = @$_smarty_tpl->tpl_vars['p']->value['Navop'])===null||$tmp==='' ? '' : $tmp);?>
" />
                              <br />
                              <button class="shop_buttons_big_second" type="button" onclick="location.href = '<?php echo $_smarty_tpl->tpl_vars['p']->value['ProdLink'];?>
';"><img src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/shop/basket_simple.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('buttonDetails');?>
</button>
                              <?php }?>
                            <?php }?>
                          <?php } else { ?>
                          <strong><?php echo $_smarty_tpl->getConfigVariable('Shop_prices_justforUsers');?>
</strong>
                        <?php }?>
                      </div>
                    </form>
                  </div>
                </div>
              </td>
              <?php if ($_smarty_tpl->tpl_vars['count']->value%$_smarty_tpl->tpl_vars['newest_colums']->value==0&&!$_smarty_tpl->getVariable('smarty')->value['foreach']['pro']['last']) {?>
              </tr>
            </table>
          </div>
          <div id="opt-<?php echo $_smarty_tpl->tpl_vars['div_split']->value;?>
" class="ui-tabs-panel-content">
            <table width="100%" cellpadding="0" cellspacing="0">
              <tr>
              <?php } else { ?>
              <?php }?>
            <?php } ?>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>
<?php }?>
<?php }} ?>
