<?php /* Smarty version Smarty-3.1.18, created on 2017-02-25 11:47:59
         compiled from "/home/vampireos/www/he6oru.localhost/admin/theme/standard/navielements/shop.tpl" */ ?>
<?php /*%%SmartyHeaderCode:7974636958b144bfd70901-66703053%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '26c5dab4c1b7692e05a6424e38a1edfd99a81767' => 
    array (
      0 => '/home/vampireos/www/he6oru.localhost/admin/theme/standard/navielements/shop.tpl',
      1 => 1406938240,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7974636958b144bfd70901-66703053',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'imgpath' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_58b144bfde47c8_44052810',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58b144bfde47c8_44052810')) {function content_58b144bfde47c8_44052810($_smarty_tpl) {?><?php if (perm('shop')&&admin_active('shop')) {?>
  <a class="menuitem submenuheader" href="#"><img class="absmiddle" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/shoppingcart.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Global_Shop');?>
</a>
  <div class="submenu">
    <ul>
      <?php if (perm('shop_settings')) {?>
        <li><a class="<?php if (isset($_REQUEST['sub'])&&$_REQUEST['sub']=='settings') {?>nav_subs_active<?php } else { ?>nav_subs<?php }?>" href="index.php?do=shop&amp;sub=settings"><?php echo $_smarty_tpl->getConfigVariable('Global_Settings');?>
</a></li>
        <li><a class="<?php if (isset($_REQUEST['sub'])&&$_REQUEST['sub']=='regions') {?>nav_subs_active<?php } else { ?>nav_subs<?php }?>" href="index.php?do=shop&amp;sub=regions"><?php echo $_smarty_tpl->getConfigVariable('Settings_countries_title');?>
</a></li>
        <li><a class="<?php if (isset($_REQUEST['sub'])&&$_REQUEST['sub']=='infomsg') {?>nav_subs_active<?php } else { ?>nav_subs<?php }?>" href="index.php?do=shop&amp;sub=infomsg"><?php echo $_smarty_tpl->getConfigVariable('ShopInfoM');?>
</a></li>
        <li><a class="<?php if (isset($_REQUEST['sub'])&&$_REQUEST['sub']=='shopinfos') {?>nav_subs_active<?php } else { ?>nav_subs<?php }?>" href="index.php?do=shop&amp;sub=shopinfos"><?php echo $_smarty_tpl->getConfigVariable('Shop_Infos');?>
</a></li>
        <?php }?>
        <?php if (perm('edit_order')) {?>
        <li><a title="" class="<?php if (isset($_REQUEST['sub'])&&$_REQUEST['sub']=='orders') {?>nav_subs_active<?php } else { ?>nav_subs<?php }?>" href="index.php?do=shop&amp;sub=orders"><?php echo $_smarty_tpl->getConfigVariable('Shop_ordersS');?>
</a></li>
        <?php }?>
        <?php if (perm('shop_settings')) {?>
        <li><a title="<?php echo $_smarty_tpl->getConfigVariable('Shop_showmoney_title');?>
" class="colorbox <?php if (isset($_REQUEST['sub'])&&$_REQUEST['sub']=='showmoney') {?>nav_subs_active<?php } else { ?>nav_subs<?php }?>" href="index.php?do=shop&amp;sub=showmoney&amp;noframes=1"><?php echo $_smarty_tpl->getConfigVariable('Shop_showmoney_title');?>
</a></li>
        <?php }?>
        <?php if (perm('shop_articleedit')) {?>
        <li><a class="<?php if (isset($_REQUEST['sub'])&&$_REQUEST['sub']=='articles') {?>nav_subs_active<?php } else { ?>nav_subs<?php }?>" href="index.php?do=shop&amp;sub=articles"><?php echo $_smarty_tpl->getConfigVariable('Shop_articlesS');?>
</a></li>
        <?php }?>
        <?php if (perm('shop_articlenew')) {?>
        <li><a title="<?php echo $_smarty_tpl->getConfigVariable('Shop_articles_addnew');?>
" class="colorbox <?php if (isset($_REQUEST['sub'])&&$_REQUEST['sub']=='new') {?>nav_subs_active<?php } else { ?>nav_subs<?php }?>" href="index.php?do=shop&amp;sub=new&amp;noframes=1"><?php echo $_smarty_tpl->getConfigVariable('Shop_articles_addnew');?>
</a></li>
        <?php }?>
        <?php if (perm('shop_catdeletenew')) {?>
        <li><a class="<?php if (isset($_REQUEST['sub'])&&$_REQUEST['sub']=='categories') {?>nav_subs_active<?php } else { ?>nav_subs<?php }?>" href="index.php?do=shop&amp;sub=categories"><?php echo $_smarty_tpl->getConfigVariable('Global_Categories');?>
</a></li>
        <?php }?>
        <?php if (perm('shop_addons')) {?>
        <li><a class="<?php if (isset($_REQUEST['sub'])&&$_REQUEST['sub']=='categories_addons') {?>nav_subs_active<?php } else { ?>nav_subs<?php }?>" href="index.php?do=shop&amp;sub=categories_addons"><?php echo $_smarty_tpl->getConfigVariable('Shop_CategoriesAddons');?>
</a></li>
        <?php }?>
        <?php if (perm('shop_paymentmethods')) {?>
        <li><a class="<?php if (isset($_REQUEST['sub'])&&$_REQUEST['sub']=='paymentmethods') {?>nav_subs_active<?php } else { ?>nav_subs<?php }?>" href="index.php?do=shop&amp;sub=paymentmethods"><?php echo $_smarty_tpl->getConfigVariable('Shop_payment_title');?>
</a></li>
        <?php }?>
        <?php if (perm('shop_shipper')) {?>
        <li><a class="<?php if (isset($_REQUEST['sub'])&&$_REQUEST['sub']=='shipper') {?>nav_subs_active<?php } else { ?>nav_subs<?php }?>" href="index.php?do=shop&amp;sub=shipper"><?php echo $_smarty_tpl->getConfigVariable('Shop_shipper_title');?>
</a></li>
        <?php }?>
        <?php if (perm('shop_settings')) {?>
        <li><a class="<?php if (isset($_REQUEST['sub'])&&$_REQUEST['sub']=='tracking') {?>nav_subs_active<?php } else { ?>nav_subs<?php }?>" href="index.php?do=shop&amp;sub=tracking"><?php echo $_smarty_tpl->getConfigVariable('Shop_Tracking');?>
</a></li>
        <?php }?>
        <?php if (perm('shop_taxes')) {?>
        <li><a class="<?php if (isset($_REQUEST['sub'])&&$_REQUEST['sub']=='taxes') {?>nav_subs_active<?php } else { ?>nav_subs<?php }?>" href="index.php?do=shop&amp;sub=taxes"><?php echo $_smarty_tpl->getConfigVariable('Shop_taxes_title');?>
</a></li>
        <?php }?>
        <?php if (perm('shop_shippingready')) {?>
        <li><a class="<?php if (isset($_REQUEST['sub'])&&$_REQUEST['sub']=='shippingready') {?>nav_subs_active<?php } else { ?>nav_subs<?php }?>" href="index.php?do=shop&amp;sub=shippingready"><?php echo $_smarty_tpl->getConfigVariable('Shop_shippingready_title');?>
</a></li>
        <?php }?>
        <?php if (perm('shop_availability')) {?>
        <li><a class="<?php if (isset($_REQUEST['sub'])&&$_REQUEST['sub']=='availabilities') {?>nav_subs_active<?php } else { ?>nav_subs<?php }?>" href="index.php?do=shop&amp;sub=availabilities"><?php echo $_smarty_tpl->getConfigVariable('Shop_availabilities_title');?>
</a></li>
        <?php }?>
        <?php if (perm('shop_couponcodes')) {?>
        <li><a class="<?php if (isset($_REQUEST['sub'])&&$_REQUEST['sub']=='couponcodes') {?>nav_subs_active<?php } else { ?>nav_subs<?php }?>" href="index.php?do=shop&amp;sub=couponcodes"><?php echo $_smarty_tpl->getConfigVariable('Shop_couponcodes_title');?>
</a></li>
        <?php }?>
        <?php if (perm('shop_units')) {?>
        <li><a class="<?php if (isset($_REQUEST['sub'])&&$_REQUEST['sub']=='units') {?>nav_subs_active<?php } else { ?>nav_subs<?php }?>" href="index.php?do=shop&amp;sub=units"><?php echo $_smarty_tpl->getConfigVariable('Shop_units_title');?>
</a></li>
        <?php }?>
        <?php if (perm('shop_groupsettings')) {?>
        <li><a class="<?php if (isset($_REQUEST['sub'])&&$_REQUEST['sub']=='groupsettings') {?>nav_subs_active<?php } else { ?>nav_subs<?php }?>" href="index.php?do=shop&amp;sub=groupsettings"><?php echo $_smarty_tpl->getConfigVariable('Shop_groupsettings');?>
</a></li>
        <?php }?>
        <?php if (perm('shop_settings')) {?>
        <li><a class="<?php if (isset($_REQUEST['do'])&&$_REQUEST['do']=='shopimport') {?>nav_subs_active<?php } else { ?>nav_subs<?php }?>" href="index.php?do=shopimport"><?php echo $_smarty_tpl->getConfigVariable('Import_art');?>
</a></li>
        <li><a class="<?php if (isset($_REQUEST['sub'])&&$_REQUEST['sub']=='yamarket') {?>nav_subs_active<?php } else { ?>nav_subs<?php }?>" href="index.php?do=shop&amp;sub=yamarket"><?php echo $_smarty_tpl->getConfigVariable('YaMarket');?>
</a></li>
        <?php }?>
    </ul>
  </div>
<?php }?>
<?php }} ?>
