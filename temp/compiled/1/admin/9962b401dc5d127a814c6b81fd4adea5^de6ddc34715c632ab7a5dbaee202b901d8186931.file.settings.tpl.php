<?php /* Smarty version Smarty-3.1.18, created on 2017-02-25 11:51:20
         compiled from "/home/vampireos/www/he6oru.localhost/admin/theme/standard/shop/settings.tpl" */ ?>
<?php /*%%SmartyHeaderCode:178623197358b14588de58c0-23341140%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'de6ddc34715c632ab7a5dbaee202b901d8186931' => 
    array (
      0 => '/home/vampireos/www/he6oru.localhost/admin/theme/standard/shop/settings.tpl',
      1 => 1407308140,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '178623197358b14588de58c0-23341140',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'jspath' => 0,
    'admin_settings' => 0,
    'helpquery' => 0,
    'imgpath' => 0,
    'valut_out1' => 0,
    'valut_out2' => 0,
    'row' => 0,
    'lang' => 0,
    'valut_out3' => 0,
    'time' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_58b145890ec8f6_17136592',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58b145890ec8f6_17136592')) {function content_58b145890ec8f6_17136592($_smarty_tpl) {?><script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['jspath']->value;?>
/jupload.js"></script>
<script type="text/javascript">
<!-- //
<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['incpath']->value)."/other/jsvalidate.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

$.validator.setDefaults({
    submitHandler: function() {
        document.forms['shopsettings'].submit();
    }
});

$(document).ready(function() {
    $('#commentForm').validate( {
	rules: {
            ShopLand: { required: true, minlength: 2 },
            Email_Abs: { required: true, email: true },
            Name_Abs: { required: true },
            Email_Bestellung: { required: true },
            Subjekt_Bestellung: { required: true },
            Subjekt_Best_Kopie: { required: true },
            RechnungsLogo: { url: true },
            thumb_width_small: { required: true, range: [20, 60] },
            thumb_width_norm: { required: true, range: [80, 250] },
            thumb_width_middle: { required: true, range: [60, 250] },
            thumb_width_big: { required: true, range: [300, 950] },
            BestMin: { required: true, number: true },
            BestMax: { required: true, number: true },
            Start_Limit: { required: true, range: [3, 20] },
            Spalten_Neueste: { required: true, range: [2, 4] },
            Topseller_Limit: { required: true, range: [3, 20] },
            Spalten_Topseller: { required: true, range: [2, 4] },
            Angebote_Limit: { required: true, range: [3, 20] },
            Spalten_Angebote: { required: true, range: [2, 4] },
            Topseller_Navi_Limit: { required: true, range: [2, 25] },
            Zubehoer_Limit: { required: true, range: [2, 35] },
            LimitExternNeu: { required: true, range: [2, 15] },
            Lager_Gering: { required: true, range: [1, 20] },
            Tab_Limit: { required: true, range: [2, 8] },
            Prodtext_Laenge: { required: true, range: [100, 450] },
            WasserzeichenKomp: { required: true, range: [10, 100] }
        },
        messages: {
            ShopLand: {
                required: '<?php echo $_smarty_tpl->getConfigVariable('SettingsCountry');?>
',
                minlength: '<?php echo $_smarty_tpl->getConfigVariable('SettingsCountryLength');?>
'
            }
        }
    });
});

function fileUpload(sub, divid) {
    $(document).ajaxStart(function() {
        $('#loading_' + divid).show();
        $('#buttonUpload_' + divid).val('<?php echo $_smarty_tpl->getConfigVariable('Global_Wait');?>
').prop('disabled', true);
    }).ajaxComplete(function() {
        $('#loading_' + divid).hide();
        $('#buttonUpload_' + divid).val('<?php echo $_smarty_tpl->getConfigVariable('UploadButton');?>
').prop('disabled', false);
    });
    var resize = document.getElementById('resizeUpload_' + divid).value;
    $.ajaxFileUpload({
	url: 'index.php?do=shop&sub=' + sub + '&divid=' + divid + '&resize=' + resize,
	secureuri: false,
	fileElementId: 'fileToUpload_' + divid,
	dataType: 'json',
	success: function (data) {
	    if(typeof(data.result) !== 'undefined') {
                document.getElementById('UpInf_' + divid).innerHTML = data.result;
                if(data.filename !== '') {
                    document.getElementById('newFile_' + divid).value = data.filename;
                }
	    }
	},
	error: function (data, status, e) {
	    document.getElementById('UpInf_' + divid).innerHTML = e;
	}
    });
    return false;
}
//-->
</script>

<div class="header"><?php echo $_smarty_tpl->getConfigVariable('Global_Shop');?>
 - <?php echo $_smarty_tpl->getConfigVariable('Global_Settings');?>
</div>
<div class="subheaders">
  <?php if ($_smarty_tpl->tpl_vars['admin_settings']->value['Ahelp']==1) {?>
    <a class="colorbox" href="index.php?do=help&amp;sub=<?php echo $_smarty_tpl->tpl_vars['helpquery']->value;?>
&amp;noframes=1"><img class="absmiddle" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/s_help.png" alt="" border="0" /> <?php echo $_smarty_tpl->getConfigVariable('GlobalHelp');?>
</a>&nbsp;&nbsp;&nbsp;
    <?php }?>
  <a class="colorbox" href="index.php?do=support&amp;sub=send_order&amp;noframes=1"><img class="absmiddle" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/send.png" alt="" border="0" /> <?php echo $_smarty_tpl->getConfigVariable('SendOrder');?>
</a>
</div>
<form id="commentForm" name="shopsettings" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="3">
    <tr>
      <td width="450" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_curr1');?>
</td>
      <td class="row_right">
        <select name="Waehrung_1" id="Waehrung_1" class="input">
          <?php echo $_smarty_tpl->tpl_vars['valut_out1']->value;?>

        </select>
        <a class="colorbox" href="http://www.cbr.ru/"><?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_currWeb');?>
</a>
      </td>
    </tr>
    <tr>
      <td width="450" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_curr2');?>
</td>
      <td class="row_right">
        <select name="Waehrung_2" class="input">
          <option value=""></option>
          <?php echo $_smarty_tpl->tpl_vars['valut_out2']->value;?>

        </select>
        <input name="Multiplikator_2" type="text" class="input" id="Multiplikator_2" value="<?php if (!empty($_smarty_tpl->tpl_vars['row']->value['Waehrung_2'])) {?><?php echo $_smarty_tpl->tpl_vars['row']->value['Multiplikator_2'];?>
<?php }?>" size="16" maxlength="16" />
        <img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Shop_Settings_MultiInf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" />
      </td>
    </tr>
    <tr>
      <td width="450" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_curr3');?>
</td>
      <td class="row_right">
        <select name="Waehrung_3" class="input">
          <option value=""></option>
          <?php echo $_smarty_tpl->tpl_vars['valut_out3']->value;?>

        </select>
        <input name="Multiplikator_3" type="text" class="input" id="Multiplikator_3" value="<?php if (!empty($_smarty_tpl->tpl_vars['row']->value['Waehrung_3'])) {?><?php echo $_smarty_tpl->tpl_vars['row']->value['Multiplikator_3'];?>
<?php }?>" size="16" maxlength="16" />
        <img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Shop_Settings_MultiInf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" />
      </td>
    </tr>
    <tr>
      <td width="450" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Shop_Settings_CountryInf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_shopCountry');?>
</td>
      <td class="row_right"><label><input class="input { required: true }" name="ShopLand" type="text" id="ShopLand" size="4" maxlength="2" value="<?php echo mb_strtoupper($_smarty_tpl->tpl_vars['row']->value['ShopLand'], 'windows-1251');?>
" /></label></td>
    </tr>
    <tr>
      <td width="450" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Setting_shopstart_inf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Settings_shopasstartpage');?>
 </td>
      <td class="row_right">
        <label><input type="radio" name="shop_is_startpage" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['shop_is_startpage']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
        <label><input type="radio" name="shop_is_startpage" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['shop_is_startpage']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
      </td>
    </tr>
     <tr>
      <td width="450" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['ShopStartInf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('ShopStartT');?>
 </td>
      <td class="row_right">
        <select class="input" name="StartSeite" id="StartSeite">
          <option value="artikel" <?php if ($_smarty_tpl->tpl_vars['row']->value['StartSeite']=='artikel') {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->getConfigVariable('ShopStartAll');?>
</option>
          <option value="shopstart" <?php if ($_smarty_tpl->tpl_vars['row']->value['StartSeite']=='shopstart') {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->getConfigVariable('ShopStartNewOff');?>
</option>
          <option value="startartikel" <?php if ($_smarty_tpl->tpl_vars['row']->value['StartSeite']=='startartikel') {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->getConfigVariable('ShopStartArtikel');?>
</option>
        </select>
      </td>
    </tr>
    <tr>
      <td width="450" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Shop_settings_ancategsInf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Shop_settings_ancategs');?>
</td>
      <td class="row_right">
        <label><input type="radio" name="ArtikelBeiKateg" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['ArtikelBeiKateg']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
        <label><input type="radio" name="ArtikelBeiKateg" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['ArtikelBeiKateg']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
      </td>
    </tr>
    <tr>
    <tr>
      <td width="450" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Shop_Settings_PriceInf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_priceDisplay');?>
</td>
      <td class="row_right">
        <label><input type="radio" name="NettoPreise" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['NettoPreise']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_priceB');?>
</label>
        <label><input type="radio" name="NettoPreise" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['NettoPreise']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_priceN');?>
</label>
      </td>
    </tr>
    <tr>
      <td width="450" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Shop_Settings_NettoKleinInf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_NettoKlein');?>
</td>
      <td class="row_right">
        <label><input type="radio" name="NettoKlein" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['NettoKlein']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
        <label><input type="radio" name="NettoKlein" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['NettoKlein']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
      </td>
    </tr>
    <tr>
      <td width="450" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Shop_Settings_GuestInf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_guestorder');?>
</td>
      <td class="row_right">
        <label><input type="radio" name="Gastbestellung" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['Gastbestellung']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
        <label><input type="radio" name="Gastbestellung" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['Gastbestellung']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
      </td>
    </tr>
    <tr>
      <td width="450" class="row_left"><img class="absmiddle stip" title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['Shop_settings_nguestsInf'];?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Shop_settings_nguests');?>
</td>
      <td class="row_right">
        <label><input type="radio" name="PreiseGaeste" id="radio" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['PreiseGaeste']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
        <label><input type="radio" name="PreiseGaeste" id="radio" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['PreiseGaeste']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
      </td>
    </tr>
    <tr>
      <td width="450" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Shop_phone_MustInf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Shop_phone_Must');?>
</td>
      <td class="row_right">
        <label><input type="radio" name="Telefon_Pflicht" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['Telefon_Pflicht']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
        <label><input type="radio" name="Telefon_Pflicht" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['Telefon_Pflicht']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
      </td>
    </tr>

    <tr>
      <td width="450" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['ShopCheaperInf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('ShopCheaper');?>
</td>
      <td class="row_right">
        <label><input type="radio" name="cheaper" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['cheaper']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
        <label><input type="radio" name="cheaper" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['cheaper']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
      </td>
    </tr>

    <tr>
      <td width="450" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['TopNavTabsInf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('TopNavTabs');?>
</td>
      <td class="row_right">
        <label><input type="radio" name="TopNewOffers" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['TopNewOffers']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
        <label><input type="radio" name="TopNewOffers" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['TopNewOffers']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
      </td>
    </tr>
    <tr>
      <td width="450" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['TopNavTabsPosInf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('TopNavTabsPos');?>
</td>
      <td class="row_right">
        <label><input type="radio" name="TopNewOffersPos" value="top" <?php if ($_smarty_tpl->tpl_vars['row']->value['TopNewOffersPos']=='top') {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('TopNavTabsTop');?>
</label>
        <label><input type="radio" name="TopNewOffersPos" value="bottom" <?php if ($_smarty_tpl->tpl_vars['row']->value['TopNewOffersPos']=='bottom') {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('TopNavTabsBottom');?>
</label>
      </td>
    </tr>
    <tr>
      <td width="450" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Shop_Settings_TopTabsInf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_CountInTabs');?>
 </td>
      <td class="row_right"><input name="Tab_Limit" type="text" class="input" id="Tab_Limit" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['Tab_Limit'];?>
" size="4" /></td>
    </tr>
    <tr>
      <td width="450" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Shop_Settings_CouponsInf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_Coupons');?>
</td>
      <td class="row_right">
        <label><input name="Gutscheine" type="radio" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['Gutscheine']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
        <label><input name="Gutscheine" type="radio" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['Gutscheine']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
      </td>
    </tr>
    <tr>
      <td width="450" class="row_left"> <img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Shop_product_requestainf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Shop_product_requesta');?>
</td>
      <td class="row_right">
        <label><input name="AnfrageForm" type="radio" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['AnfrageForm']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
        <label><input name="AnfrageForm" type="radio" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['AnfrageForm']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
      </td>
    </tr>
    <tr>
      <td width="450" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Shop_Settings_OMinInf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_Ordermin');?>
</td>
      <td class="row_right"><input name="BestMin" type="text" class="input" id="BestMin" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['BestMin'];?>
" size="10" /></td>
    </tr>
    <tr>
      <td width="450" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Shop_Settings_LimMaxInf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_Ordermax');?>
</td>
      <td class="row_right"><input name="BestMax" type="text" class="input" id="BestMax" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['BestMax'];?>
" size="10" /></td>
    </tr>
    <tr>
      <td width="450" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Shop_Settings_ShippFreeInf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_ShippingFree');?>
</td>
      <td class="row_right"> <?php echo $_smarty_tpl->getConfigVariable('Settings_countries_text');?>
 - <a href="index.php?do=shop&amp;sub=regions"><?php echo $_smarty_tpl->getConfigVariable('Settings_countries_title');?>
</a></td>
    </tr>
    <tr>
      <td width="450" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Shop_Settings_ReductInf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_Storereduct');?>
</td>
      <td class="row_right">
        <label><input type="radio" name="Bestand_Zaehlen" id="radio3" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['Bestand_Zaehlen']==1) {?>checked="checked"<?php }?>/><?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_Storereduct1');?>
</label>
        <label><input type="radio" name="Bestand_Zaehlen" id="radio4" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['Bestand_Zaehlen']==0) {?>checked="checked"<?php }?>/><?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_Storereduct2');?>
</label>
      </td>
    </tr>
    <tr>
      <td width="450" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Shop_Settings_StartNewCountInf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_CountNew');?>
</td>
      <td class="row_right"><input name="Start_Limit" type="text" class="input" id="Start_Limit" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['Start_Limit'];?>
" size="4" /></td>
    </tr>
    <tr>
      <td width="450" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Shop_Settings_StartNewCountRowInf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_CountNewRow');?>
</td>
      <td class="row_right"><input name="Spalten_Neueste" type="text" class="input" id="Spalten_Neueste" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['Spalten_Neueste'];?>
" size="4" /></td>
    </tr>
    <tr>
      <td width="450" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Shop_Settings_StartTsCountInf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_CountTopseller');?>
</td>
      <td class="row_right"><input name="Topseller_Limit" type="text" class="input" id="Topseller_Limit" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['Topseller_Limit'];?>
" size="4" /></td>
    </tr>
    <tr>
      <td width="450" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Shop_Settings_StartNewCountRowInf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_CountTopsellerRow');?>
</td>
      <td class="row_right"><input name="Spalten_Topseller" type="text" class="input" id="Spalten_Topseller" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['Spalten_Topseller'];?>
" size="4" /></td>
    </tr>
    <tr>
      <td width="450" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Shop_Settings_StartOffersCountInf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_CountOffers');?>
</td>
      <td class="row_right"><input name="Angebote_Limit" type="text" class="input" id="Angebote_Limit" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['Angebote_Limit'];?>
" size="4" /></td>
    </tr>
    <tr>
      <td width="450" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Shop_Settings_StartNewCountRowInf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_CountOffersRow');?>
</td>
      <td class="row_right"><input name="Spalten_Angebote" type="text" class="input" id="Spalten_Angebote" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['Spalten_Angebote'];?>
" size="4" /></td>
    </tr>
    <tr>
      <td width="450" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Shop_Settings_CountExternInf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_CountExtern');?>
</td>
      <td class="row_right"><input name="LimitExternNeu" type="text" class="input" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['LimitExternNeu'];?>
" size="4" /></td>
    </tr>
    <tr>
      <td width="450" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Shop_Settings_LowAmountInf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_LowAmmountWarn');?>
</td>
      <td class="row_right"><input name="Lager_Gering" type="text" class="input" id="Lager_Gering" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['Lager_Gering'];?>
" size="4" /></td>
    </tr>
    <tr>
      <td width="450" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Shop_Settings_ProdTextInf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_ProdTextLength');?>
</td>
      <td class="row_right"><input name="Prodtext_Laenge" type="text" class="input" id="Prodtext_Laenge" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['Prodtext_Laenge'];?>
" size="4" /></td>
    </tr>
    <tr>
      <td width="450" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_CountProductsPage');?>
</td>
      <td class="row_right">
        <select class="input" name="Produkt_Limit_Seite" id="Produkt_Limit_Seite">
          <option value="5" <?php if ($_smarty_tpl->tpl_vars['row']->value['Produkt_Limit_Seite']==5) {?>selected="selected"<?php }?>>5</option>
          <option value="10" <?php if ($_smarty_tpl->tpl_vars['row']->value['Produkt_Limit_Seite']=='10') {?>selected="selected"<?php }?>>10</option>
          <option value="20" <?php if ($_smarty_tpl->tpl_vars['row']->value['Produkt_Limit_Seite']=='20') {?>selected="selected"<?php }?>>20</option>
          <option value="50" <?php if ($_smarty_tpl->tpl_vars['row']->value['Produkt_Limit_Seite']=='50') {?>selected="selected"<?php }?>>50</option>
          <option value="100" <?php if ($_smarty_tpl->tpl_vars['row']->value['Produkt_Limit_Seite']=='100') {?>selected="selected"<?php }?>>100</option>
        </select>
      </td>
    </tr>
    <tr>
      <td width="450" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_CountTopSNavi');?>
</td>
      <td class="row_right"><input name="Topseller_Navi_Limit" type="text" class="input" id="Topseller_Navi_Limit" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['Topseller_Navi_Limit'];?>
" size="4" /></td>
    </tr>
    <tr>
      <td width="450" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_TplProducts');?>
</td>
      <td class="row_right">
        <select style="width: 250px" name="Template_Produkte" id="Template_Produkte" class="input">
          <option value="products" <?php if ($_smarty_tpl->tpl_vars['row']->value['Template_Produkte']=='products') {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_TplProducts1');?>
 (products.tpl)</option>
          <option value="products_2colums" <?php if ($_smarty_tpl->tpl_vars['row']->value['Template_Produkte']=='products_2colums') {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_TplProducts_2');?>
 (products_2colums.tpl)</option>
          <option value="products_3colums" <?php if ($_smarty_tpl->tpl_vars['row']->value['Template_Produkte']=='products_3colums') {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_TplProducts_3');?>
 (products_3colums.tpl)</option>
          <option value="products_table" <?php if ($_smarty_tpl->tpl_vars['row']->value['Template_Produkte']=='products_table') {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_TplTable');?>
 (products_table.tpl)</option>
        </select>
      </td>
    </tr>
    <tr>
      <td width="450" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('ShopSortable');?>
</td>
      <td class="row_right">
        <select name="Typesort" id="Typesort" class="input">
          <option value="date" <?php if ($_smarty_tpl->tpl_vars['row']->value['Typesort']=='date') {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->getConfigVariable('ShopSortableTDate');?>
</option>
          <option value="title" <?php if ($_smarty_tpl->tpl_vars['row']->value['Typesort']=='title') {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->getConfigVariable('ShopSortableTitle');?>
</option>
          <option value="price" <?php if ($_smarty_tpl->tpl_vars['row']->value['Typesort']=='price') {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->getConfigVariable('ShopSortablePrice');?>
</option>
          <option value="klick" <?php if ($_smarty_tpl->tpl_vars['row']->value['Typesort']=='klick') {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->getConfigVariable('ShopSortableKlicks');?>
</option>
          <option value="art" <?php if ($_smarty_tpl->tpl_vars['row']->value['Typesort']=='art') {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->getConfigVariable('ShopSortableArtikel');?>
</option>
        </select>
        <select style="width: 140px" name="Sortable" id="Sortable" class="input">
          <option value="asc" <?php if ($_smarty_tpl->tpl_vars['row']->value['Sortable']=='asc') {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->getConfigVariable('asc_t');?>
</option>
          <option value="desc" <?php if ($_smarty_tpl->tpl_vars['row']->value['Sortable']=='desc') {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->getConfigVariable('desc_t');?>
</option>
        </select>
      </td>
    </tr>
    <tr>
      <td width="450" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('ShopSimilarProduct');?>
</td>
      <td class="row_right">
        <label><input name="similar_product" type="radio" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['similar_product']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
        <label><input name="similar_product" type="radio" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['similar_product']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
      </td>
    </tr>
    <tr>
      <td width="450" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_CountZuProducts');?>
</td>
      <td class="row_right"><input name="Zubehoer_Limit" type="text" class="input" id="Zubehoer_Limit" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['Zubehoer_Limit'];?>
" size="4" /></td>
    </tr>
    <tr>
      <td width="450" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('ShopZeige_Text');?>
</td>
      <td class="row_right">
        <label><input name="Zeige_Text" type="radio" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['Zeige_Text']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
        <label><input name="Zeige_Text" type="radio" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['Zeige_Text']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
      </td>
    </tr>
    <tr>
      <td width="450" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('ShopZeige_Verfuegbarkeit');?>
</td>
      <td class="row_right">
        <label><input name="Zeige_Verfuegbarkeit" type="radio" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['Zeige_Verfuegbarkeit']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
        <label><input name="Zeige_Verfuegbarkeit" type="radio" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['Zeige_Verfuegbarkeit']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
      </td>
    </tr>
    <tr>
      <td width="450" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('ShopZeige_Lagerbestand');?>
</td>
      <td class="row_right">
        <label><input name="Zeige_Lagerbestand" type="radio" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['Zeige_Lagerbestand']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
        <label><input name="Zeige_Lagerbestand" type="radio" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['Zeige_Lagerbestand']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
      </td>
    </tr>
    <tr>
      <td width="450" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('ShopZeige_Lieferzeit');?>
</td>
      <td class="row_right">
        <label><input name="Zeige_Lieferzeit" type="radio" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['Zeige_Lieferzeit']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
        <label><input name="Zeige_Lieferzeit" type="radio" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['Zeige_Lieferzeit']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
      </td>
    </tr>
    <tr>
      <td width="450" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('ShopZeige_ArtNr');?>
</td>
      <td class="row_right">
        <label><input name="Zeige_ArtNr" type="radio" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['Zeige_ArtNr']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
        <label><input name="Zeige_ArtNr" type="radio" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['Zeige_ArtNr']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
      </td>
    </tr>
    <tr>
      <td width="450" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('ShopZeige_Hersteller');?>
</td>
      <td class="row_right">
        <label><input name="Zeige_Hersteller" type="radio" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['Zeige_Hersteller']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
        <label><input name="Zeige_Hersteller" type="radio" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['Zeige_Hersteller']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
      </td>
    </tr>
    <tr>
      <td width="450" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('ShopZeige_ErschienAm');?>
</td>
      <td class="row_right">
        <label><input name="Zeige_ErschienAm" type="radio" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['Zeige_ErschienAm']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
        <label><input name="Zeige_ErschienAm" type="radio" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['Zeige_ErschienAm']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
      </td>
    </tr>
    <tr>
      <td width="450" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('ShopMenuLowAmount');?>
</td>
      <td class="row_right">
        <label><input name="menu_low_amount" type="radio" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['menu_low_amount']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
        <label><input name="menu_low_amount" type="radio" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['menu_low_amount']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
      </td>
    </tr>
    <tr>
      <td width="450" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('ShopSeenCat');?>
</td>
      <td class="row_right">
        <label><input name="seen_cat" type="radio" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['seen_cat']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
        <label><input name="seen_cat" type="radio" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['seen_cat']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
      </td>
    </tr>
    <tr>
      <td width="450" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('ShopVatInfoCat');?>
</td>
      <td class="row_right">
        <label><input name="vat_info_cat" type="radio" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['vat_info_cat']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
        <label><input name="vat_info_cat" type="radio" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['vat_info_cat']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
      </td>
    </tr>
    <tr>
      <td width="450" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('ShopShippingInfo');?>
</td>
      <td class="row_right">
        <label><input name="shipping_info" type="radio" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['shipping_info']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
        <label><input name="shipping_info" type="radio" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['shipping_info']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
      </td>
    </tr>
    <tr>
      <td width="450" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('PriceGroup');?>
</td>
      <td class="row_right">
        <label><input name="PriceGroup" type="radio" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['PriceGroup']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
        <label><input name="PriceGroup" type="radio" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['PriceGroup']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
      </td>
    </tr>
    <tr>
      <td width="450" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('ShopVatInfoProduct');?>
</td>
      <td class="row_right">
        <label><input name="vat_info_product" type="radio" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['vat_info_product']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
        <label><input name="vat_info_product" type="radio" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['vat_info_product']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
      </td>
    </tr>
    <tr>
      <td width="450" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('ShopPopupProduct');?>
</td>
      <td class="row_right">
        <label><input name="popup_product" type="radio" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['popup_product']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
        <label><input name="popup_product" type="radio" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['popup_product']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
      </td>
    </tr>
    <tr>
      <td width="450" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('AvailType');?>
</td>
      <td class="row_right">
        <label><input name="AvailType" type="radio" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['AvailType']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('AvailTypeJust');?>
</label>
        <label><input name="AvailType" type="radio" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['AvailType']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('AvailTypeDiff');?>
</label>
      </td>
    </tr>
    <tr>
      <td width="450" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('OnlyFhrase');?>
</td>
      <td class="row_right">
        <label><input name="OnlyFhrase" type="radio" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['OnlyFhrase']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('OnlyFhraseWord');?>
</label>
        <label><input name="OnlyFhrase" type="radio" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['OnlyFhrase']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('OnlyFhraseNotWord');?>
</label>
      </td>
    </tr>
  </table>
  <div class="subheaders"><?php echo $_smarty_tpl->getConfigVariable('Settings_mailsettings');?>
</div>
  <table width="100%" border="0" cellspacing="0" cellpadding="3">
    <tr>
      <td width="450" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Shop_Settings_EmailStdInf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_EmailStandard');?>
</td>
      <td class="row_right"><label><input name="Email_Abs" type="text" class="input" id="Email_Abs" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['Email_Abs'];?>
" size="30" /></label></td>
    </tr>
    <tr>
      <td width="450" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Shop_Settings_EmailStdFInf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_EmailStandardFrom');?>
</td>
      <td class="row_right"><input name="Name_Abs" type="text" id="Name_Abs" class="input" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['Name_Abs'];?>
" size="30" /></td>
    </tr>
    <tr>
      <td width="450" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Shop_Settings_EmailCopyInf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_EmailOrders');?>
</td>
      <td class="row_right"><input name="Email_Bestellung" type="text" class="input" id="Email_Bestellung" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['Email_Bestellung'];?>
" size="30" /></td>
    </tr>
    <tr>
      <td width="450" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_SubjectOrder');?>
</td>
      <td class="row_right"><input name="Subjekt_Bestellung" id="Subjekt_Bestellung" type="text" class="input { required: true }" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['Subjekt_Bestellung'];?>
" size="30" /></td>
    </tr>
    <tr>
      <td width="450" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_SubjectOrder2');?>
</td>
      <td class="row_right"><input name="Subjekt_Best_Kopie" id="Subjekt_Best_Kopie" type="text" class="input { required: true }" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['Subjekt_Best_Kopie'];?>
" size="30" /></td>
    </tr>
    <tr>
      <td width="450" class="row_left"> <?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_OrderLogo');?>
</td>
      <td class="row_right"><input name="RechnungsLogo" id="RechnungsLogo" type="text" class="input { url: true }" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['RechnungsLogo'];?>
" size="30" /></td>
    </tr>
    <tr>
      <td width="450" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Shop_settings_referedoptions']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Shop_settings_refered');?>
 </td>
      <td class="row_right"><textarea cols="" rows="" name="GefundenOptionen" class="input" style="width: 400px; height: 120px"><?php echo $_smarty_tpl->tpl_vars['row']->value['GefundenOptionen'];?>
</textarea></td>
    </tr>
  </table>
  <div class="subheaders">
    <?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_Images');?>

    <br />
    <?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_ImagesInf');?>

  </div>
  <table width="100%" border="0" cellspacing="0" cellpadding="3">
    <tr>
      <td width="450" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Shop_Settings_ThumbSmall']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_ImagesSmall');?>
</td>
      <td class="row_right"><input onchange="document.getElementById('ThumbRenew_Yes').checked='true';" name="thumb_width_small" type="text" class="input" id="thumb_width_small" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['thumb_width_small'];?>
" size="10" maxlength="3" /></td>
    </tr>
    <tr>
      <td width="450" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Shop_Settings_ThumbNorm']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_ImagesNormal');?>
 </td>
      <td class="row_right"><input onchange="document.getElementById('ThumbRenew_Yes').checked='true';" name="thumb_width_norm" type="text" class="input" id="thumb_width_norm" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['thumb_width_norm'];?>
" size="10" maxlength="3" /></td>
    </tr>
    <tr>
      <td width="450" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Shop_Settings_ThumbMiddle']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_ImagesMiddle');?>
 </td>
      <td class="row_right"><input onchange="document.getElementById('ThumbRenew_Yes').checked = 'true';" name="thumb_width_middle" type="text" class="input" id="thumb_width_middle" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['thumb_width_middle'];?>
" size="10" maxlength="3" /></td>
    </tr>
    <tr>
      <td width="450" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Shop_Settings_ThumbBig']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_ImagesBig');?>
</td>
      <td class="row_right"><input onchange="document.getElementById('ThumbRenew_Yes').checked = 'true';" name="thumb_width_big" type="text" class="input" id="thumb_width_big" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['thumb_width_big'];?>
" size="10" maxlength="3" /></td>
    </tr>
    <tr>
      <td width="450" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_ImagesQuality');?>
</td>
      <td class="row_right">
        <select style="width: 250px" onchange="document.getElementById('ThumbRenew_Yes').checked = 'true';" name="thumb_quality" id="thumb_quality">
          <option value="99" <?php if ($_smarty_tpl->tpl_vars['row']->value['thumb_quality']=='99') {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_Q1');?>
</option>
          <option value="90" <?php if ($_smarty_tpl->tpl_vars['row']->value['thumb_quality']=='90') {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_Q2');?>
</option>
          <option value="60" <?php if ($_smarty_tpl->tpl_vars['row']->value['thumb_quality']=='60') {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_Q3');?>
</option>
          <option value="35" <?php if ($_smarty_tpl->tpl_vars['row']->value['thumb_quality']=='35') {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_Q4');?>
</option>
        </select>
      </td>
    </tr>
    <tr>
      <td width="450" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_Watermark');?>
</td>
      <td class="row_right">
        <label><input onchange="document.getElementById('ThumbRenew_Yes').checked = 'true';" type="radio" name="Wasserzeichen" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['Wasserzeichen']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
        <label><input onchange="document.getElementById('ThumbRenew_Yes').checked = 'true';" type="radio" name="Wasserzeichen" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['Wasserzeichen']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
      </td>
    </tr>
    <?php if (!empty($_smarty_tpl->tpl_vars['row']->value['Wasserzeichen_Bild'])) {?>
      <tr>
        <td width="450" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_WatermarkCurrent');?>
</td>
        <td class="row_right">
          <img src="../uploads/watermarks/<?php echo $_smarty_tpl->tpl_vars['row']->value['Wasserzeichen_Bild'];?>
?<?php echo $_smarty_tpl->tpl_vars['time']->value;?>
" alt="" border="" />
          <input type="hidden" name="watermark_old" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['Wasserzeichen_Bild'];?>
" />
        </td>
      </tr>
    <?php }?>
    <tr>
      <td width="450" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Shop_Settings_WatermarkInf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_WatermarkUp');?>
</td>
      <td class="row_right"><div id="UpInf_1"></div>
        <div id="loading_1" style="display: none;"><img src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/ajaxbar.gif" alt="" /></div>
        <input id="resizeUpload_1" type="text" size="3" name="resizeUpload_1" class="input" value="300" /> px. &nbsp;&nbsp;&nbsp;
        <input id="fileToUpload_1" type="file" size="30" name="fileToUpload_1" class="input" />
        <input type="button" class="button" id="buttonUpload_1" onclick="fileUpload('watermark', 1);" value="<?php echo $_smarty_tpl->getConfigVariable('UploadButton');?>
" />
        <?php if (perm('mediapool')) {?>
          <input type="button" class="button" onclick="uploadBrowser('image', 'watermarks', 1);" value="<?php echo $_smarty_tpl->getConfigVariable('Global_ImgSel');?>
" />
        <?php }?>
        <input type="hidden" name="newImg_1" id="newFile_1" />
      </td>
    </tr>
    <tr>
      <td width="450" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('WatermarkPosistion');?>
</td>
      <td class="row_right">
        <select style="width: 250px" class="input" onchange="document.getElementById('ThumbRenew_Yes').checked = 'true';" name="Wasserzeichen_Position" id="Wasserzeichen_Position">
          <option value="bottom_right" <?php if ($_smarty_tpl->tpl_vars['row']->value['Wasserzeichen_Position']=='bottom_right') {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->getConfigVariable('BottomRight');?>
</option>
          <option value="bottom_left" <?php if ($_smarty_tpl->tpl_vars['row']->value['Wasserzeichen_Position']=='bottom_left') {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->getConfigVariable('BottomLeft');?>
</option>
          <option value="bottom_center" <?php if ($_smarty_tpl->tpl_vars['row']->value['Wasserzeichen_Position']=='bottom_center') {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->getConfigVariable('BottomCenter');?>
</option>
          <option value="top_right" <?php if ($_smarty_tpl->tpl_vars['row']->value['Wasserzeichen_Position']=='top_right') {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->getConfigVariable('TopRight');?>
</option>
          <option value="top_left" <?php if ($_smarty_tpl->tpl_vars['row']->value['Wasserzeichen_Position']=='top_left') {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->getConfigVariable('TopLeft');?>
</option>
          <option value="top_center" <?php if ($_smarty_tpl->tpl_vars['row']->value['Wasserzeichen_Position']=='top_center') {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->getConfigVariable('TopCenter');?>
</option>
          <option value="center_right" <?php if ($_smarty_tpl->tpl_vars['row']->value['Wasserzeichen_Position']=='center_right') {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->getConfigVariable('CenterRight');?>
</option>
          <option value="center_left" <?php if ($_smarty_tpl->tpl_vars['row']->value['Wasserzeichen_Position']=='center_left') {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->getConfigVariable('CenterLeft');?>
</option>
          <option value="center" <?php if ($_smarty_tpl->tpl_vars['row']->value['Wasserzeichen_Position']=='center') {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->getConfigVariable('Center');?>
</option>
        </select>
      </td>
    </tr>
    <tr>
      <td width="450" class="row_left"> <?php echo $_smarty_tpl->getConfigVariable('Settings_WmTrans');?>
 </td>
      <td class="row_right"><input onchange="document.getElementById('ThumbRenew_Yes').checked = 'true';" name="WasserzeichenKomp" type="text" class="input" id="WasserzeichenKomp" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['WasserzeichenKomp'];?>
" size="10" maxlength="3" /> % </td>
    </tr>
    <tr>
      <td width="450" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_RenewImages');?>
</td>
      <td class="row_right">
        <label><input type="radio" name="ThumbRenew" id="ThumbRenew_Yes" value="1" /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
        <label><input name="ThumbRenew" type="radio" id="radio2" value="0" checked="checked" /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
      </td>
    </tr>
  </table>
  <input name="save" type="hidden" id="save" value="1" />
  <input type="submit" class="button" value="<?php echo $_smarty_tpl->getConfigVariable('Global_savesettings');?>
" />
</form>
<?php }} ?>
