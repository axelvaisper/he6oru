<?php /* Smarty version Smarty-3.1.18, created on 2017-02-25 11:47:59
         compiled from "/home/vampireos/www/he6oru.localhost/admin/theme/standard/shop/start_orders.tpl" */ ?>
<?php /*%%SmartyHeaderCode:153879526558b144bfad3df4-55604708%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1b5ab6d8528b739be682bd465cde2372a00a7faf' => 
    array (
      0 => '/home/vampireos/www/he6oru.localhost/admin/theme/standard/shop/start_orders.tpl',
      1 => 1407622608,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '153879526558b144bfad3df4-55604708',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'count' => 0,
    'basepath' => 0,
    'imgpath' => 0,
    'orders' => 0,
    'o' => 0,
    'lang' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_58b144bfb00012_88815607',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58b144bfb00012_88815607')) {function content_58b144bfb00012_88815607($_smarty_tpl) {?><?php if (!is_callable('smarty_function_cycle')) include '/home/vampireos/www/he6oru.localhost/lib/smarty/plugins/function.cycle.php';
if (!is_callable('smarty_modifier_numformat')) include '/home/vampireos/www/he6oru.localhost/lib/smarty/statusplugins/modifier.numformat.php';
if (!is_callable('smarty_modifier_date_format')) include '/home/vampireos/www/he6oru.localhost/lib/smarty/plugins/modifier.date_format.php';
if (!is_callable('smarty_modifier_truncate')) include '/home/vampireos/www/he6oru.localhost/lib/smarty/plugins/modifier.truncate.php';
?><?php if (perm('shop')&&admin_active('shop')&&$_smarty_tpl->tpl_vars['count']->value>0) {?>
<script type="text/javascript">
<!-- //
$(document).ready(function() {
    toggleCookie('shop_navi', 'shop_open', 30, '<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
');
});
//-->
</script>

<div class="header">
  <div id="shop_navi" class="navi_toggle"><img class="absmiddle" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/toggle.png" alt="" /></div>
  <img class="absmiddle" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/shoppingcart.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('StartNewOrders');?>
 - <a href="index.php?do=shop&amp;sub=orders"><?php echo $_smarty_tpl->getConfigVariable('Global_ShowAll');?>
 (<?php echo $_smarty_tpl->tpl_vars['count']->value;?>
)</a>
</div>
<div id="shop_open" class="sysinfos">
  <div class="maintable">
    <table width="100%" border="0" cellspacing="0" cellpadding="3">
      <?php  $_smarty_tpl->tpl_vars['o'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['o']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['orders']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['o']->key => $_smarty_tpl->tpl_vars['o']->value) {
$_smarty_tpl->tpl_vars['o']->_loop = true;
?>
        <tr class="<?php echo smarty_function_cycle(array('values'=>'second,first'),$_smarty_tpl);?>
">
          <td width="100"><?php echo smarty_modifier_numformat($_smarty_tpl->tpl_vars['o']->value->Betrag);?>
</td>
          <td width="60" align="center" class="stip" title="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['o']->value->Datum,"%d/%m/%Y - %H:%M");?>
"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['o']->value->Datum,"%d/%m/%y");?>
</td>
          <td>
            <?php if ($_smarty_tpl->tpl_vars['o']->value->Benutzer!='0') {?>
              <a title="<?php echo $_smarty_tpl->getConfigVariable('User_edit');?>
" class="colorbox stip" href="index.php?do=user&amp;sub=edituser&amp;user=<?php echo $_smarty_tpl->tpl_vars['o']->value->Benutzer;?>
&amp;noframes=1"><?php echo sanitize($_smarty_tpl->tpl_vars['o']->value->Rng_Nachname);?>
 <?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['o']->value->Rng_Vorname,2,".");?>
</a>
            <?php } else { ?>
              <?php echo sanitize($_smarty_tpl->tpl_vars['o']->value->Rng_Nachname);?>
 <?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['o']->value->Rng_Vorname,2,".");?>
 (g)
            <?php }?>
          </td>
          <td width="60" nowrap="nowrap">
            <a class="colorbox stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Shop_edit_order']);?>
" href="?do=shop&amp;sub=edit_order&amp;id=<?php echo $_smarty_tpl->tpl_vars['o']->value->Id;?>
&amp;noframes=1&amp;status=<?php echo $_smarty_tpl->tpl_vars['o']->value->Status;?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/edit.png" alt="" border="0" /></a>
              <?php if ($_smarty_tpl->tpl_vars['o']->value->Benutzer>=1) {?>
              <a class="colorbox stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Shop_edit_orderDownloads']);?>
" href="?do=shop&amp;sub=user_downloads&amp;user=<?php echo $_smarty_tpl->tpl_vars['o']->value->Benutzer;?>
&amp;name=<?php echo sanitize($_smarty_tpl->tpl_vars['o']->value->Rng_Nachname);?>
&amp;noframes=1"><img src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/download<?php if (!$_smarty_tpl->tpl_vars['o']->value->downloads) {?>_none<?php }?>.png" alt="" border="0" /></a>
              <?php }?>
              <?php if ($_smarty_tpl->tpl_vars['o']->value->Benutzer<1) {?>
              <a class="colorbox stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Shop_convertGuestToUser']);?>
" href="index.php?do=user&amp;sub=convertguesttouser&amp;order=<?php echo $_smarty_tpl->tpl_vars['o']->value->Id;?>
&amp;noframes=1"><img src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/convert_touser.png" alt="" border="0" /></a>
              <?php } else { ?>
              <img class="stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Shop_convertGuestToUserN']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/convert_touser_no.png" alt="" border="0" />
              <?php }?>
          </td>
        </tr>
      <?php } ?>
    </table>
  </div>
</div>
<?php }?>
<?php }} ?>
