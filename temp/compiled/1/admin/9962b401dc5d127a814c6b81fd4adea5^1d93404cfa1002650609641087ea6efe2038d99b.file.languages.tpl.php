<?php /* Smarty version Smarty-3.1.18, created on 2017-02-25 21:10:55
         compiled from "/home/vampireos/www/he6oru.localhost/admin/theme/standard/settings/languages.tpl" */ ?>
<?php /*%%SmartyHeaderCode:73953428058b1c8afaa6e10-75828525%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1d93404cfa1002650609641087ea6efe2038d99b' => 
    array (
      0 => '/home/vampireos/www/he6oru.localhost/admin/theme/standard/settings/languages.tpl',
      1 => 1405549835,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '73953428058b1c8afaa6e10-75828525',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'admin_settings' => 0,
    'helpquery' => 0,
    'imgpath' => 0,
    'languages' => 0,
    'c' => 0,
    'folders' => 0,
    'f' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_58b1c8afb7eb53_27832600',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58b1c8afb7eb53_27832600')) {function content_58b1c8afb7eb53_27832600($_smarty_tpl) {?><?php if (!is_callable('smarty_function_cycle')) include '/home/vampireos/www/he6oru.localhost/lib/smarty/plugins/function.cycle.php';
?><script type="text/javascript">
<!-- //
function check_languages() {
    var lang_1 = document.getElementById('Sprache_1').value;
    var lang_2 = document.getElementById('Sprache_2').value;
    var lang_3 = document.getElementById('Sprache_3').value;
    if(lang_1 == lang_2 || lang_1 == lang_3 || lang_2 == lang_3) {
        alert('<?php echo $_smarty_tpl->getConfigVariable('Settings_language_jse');?>
');
        return false;
    }
}
//-->
</script>

<form action="" method="post" onsubmit="return check_languages();">
  <div class="header"><?php echo $_smarty_tpl->getConfigVariable('Settings_languages');?>
</div>
  <div class="subheaders">
    <?php if ($_smarty_tpl->tpl_vars['admin_settings']->value['Ahelp']==1) {?>
      <a class="colorbox" href="index.php?do=help&amp;sub=<?php echo $_smarty_tpl->tpl_vars['helpquery']->value;?>
&amp;noframes=1"><img class="absmiddle" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/s_help.png" alt="" border="0" /> <?php echo $_smarty_tpl->getConfigVariable('GlobalHelp');?>
</a>&nbsp;&nbsp;&nbsp;
      <?php }?>
    <a class="colorbox" href="index.php?do=support&amp;sub=send_order&amp;noframes=1"><img class="absmiddle" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/send.png" alt="" border="0" /> <?php echo $_smarty_tpl->getConfigVariable('SendOrder');?>
</a>
  </div>
  <div class="maintable">
    <table width="100%" border="0" cellpadding="3" cellspacing="0" class="tableborder">
      <tr class="firstrow">
        <td width="100" class="headers"><?php echo $_smarty_tpl->getConfigVariable('LoginLang');?>
</td>
        <td width="100" class="headers"><?php echo $_smarty_tpl->getConfigVariable('Global_Name');?>
</td>
        <td width="140" class="headers"><?php echo $_smarty_tpl->getConfigVariable('Global_Active');?>
</td>
        <td class="headers"><?php echo $_smarty_tpl->getConfigVariable('Global_Position');?>
</td>
      </tr>
      <?php  $_smarty_tpl->tpl_vars['c'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['c']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['c']->key => $_smarty_tpl->tpl_vars['c']->value) {
$_smarty_tpl->tpl_vars['c']->_loop = true;
?>
        <tr class="<?php echo smarty_function_cycle(array('values'=>'first,second'),$_smarty_tpl);?>
">
          <td width="100">
            <select style="width: 100px" onchange="document.getElementById('Bez_<?php echo $_smarty_tpl->tpl_vars['c']->value->Id;?>
').value = this.options[this.selectedIndex].id;" class="input" name="Sprachcode[<?php echo $_smarty_tpl->tpl_vars['c']->value->Id;?>
]" id="Sprache_<?php echo $_smarty_tpl->tpl_vars['c']->value->Id;?>
">
              <?php  $_smarty_tpl->tpl_vars['f'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['f']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['folders']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['f']->key => $_smarty_tpl->tpl_vars['f']->value) {
$_smarty_tpl->tpl_vars['f']->_loop = true;
?>
                <?php if ($_smarty_tpl->tpl_vars['f']->value->Exists==1) {?>
                  <option id="<?php echo $_smarty_tpl->tpl_vars['f']->value->Long;?>
" value="<?php echo $_smarty_tpl->tpl_vars['f']->value->Name;?>
" <?php if ($_smarty_tpl->tpl_vars['f']->value->Name==$_smarty_tpl->tpl_vars['c']->value->Sprachcode) {?>selected="selected" <?php }?>><?php echo $_smarty_tpl->tpl_vars['f']->value->Long;?>
</option>
                <?php }?>
              <?php } ?>
            </select>
          </td>
          <td width="100"><input id="Bez_<?php echo $_smarty_tpl->tpl_vars['c']->value->Id;?>
" class="input" name="Sprache[<?php echo $_smarty_tpl->tpl_vars['c']->value->Id;?>
]" type="text" value="<?php echo $_smarty_tpl->tpl_vars['c']->value->Sprache;?>
" size="25" /></td>
          <td>
            <?php if ($_smarty_tpl->tpl_vars['c']->value->Id==1) {?>
              <label><input disabled="disabled" type="radio" name="Aktiv[<?php echo $_smarty_tpl->tpl_vars['c']->value->Id;?>
]" value="1" <?php if ($_smarty_tpl->tpl_vars['c']->value->Aktiv==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
              <label><input disabled="disabled" type="radio" name="Aktiv[<?php echo $_smarty_tpl->tpl_vars['c']->value->Id;?>
]" value="2" <?php if ($_smarty_tpl->tpl_vars['c']->value->Aktiv!=1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
              <input type="hidden" name="Aktiv[<?php echo $_smarty_tpl->tpl_vars['c']->value->Id;?>
]" value="1" />
            <?php } else { ?>
              <?php if ($_smarty_tpl->tpl_vars['c']->value->Exists==1) {?>
                <label><input type="radio" name="Aktiv[<?php echo $_smarty_tpl->tpl_vars['c']->value->Id;?>
]" value="1" <?php if ($_smarty_tpl->tpl_vars['c']->value->Aktiv==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
                <label><input type="radio" name="Aktiv[<?php echo $_smarty_tpl->tpl_vars['c']->value->Id;?>
]" value="2" <?php if ($_smarty_tpl->tpl_vars['c']->value->Aktiv!=1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
                <?php } else { ?>
                <input type="hidden" name="Aktiv[<?php echo $_smarty_tpl->tpl_vars['c']->value->Id;?>
]" value="0" />
              <?php }?>
            <?php }?>
          </td>
          <td><input name="Posi[<?php echo $_smarty_tpl->tpl_vars['c']->value->Id;?>
]" type="text" class="input" id="Posi[<?php echo $_smarty_tpl->tpl_vars['c']->value->Id;?>
]" style="width: 40px" value="<?php echo $_smarty_tpl->tpl_vars['c']->value->Posi;?>
" maxlength="2" /></td>
        </tr>
      <?php } ?>
    </table>
  </div>
  <input name="save" type="hidden" id="save" value="1" />
  <input type="submit" value="<?php echo $_smarty_tpl->getConfigVariable('Save');?>
" class="button" />
</form>
<?php }} ?>
