<?php /* Smarty version Smarty-3.1.18, created on 2017-02-25 11:47:59
         compiled from "/home/vampireos/www/he6oru.localhost/admin/theme/standard/other/error_links.tpl" */ ?>
<?php /*%%SmartyHeaderCode:213149288458b144bfa2b913-53012913%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '524f19c4fb183b9f0ffc9719f0e94d29cf8ce253' => 
    array (
      0 => '/home/vampireos/www/he6oru.localhost/admin/theme/standard/other/error_links.tpl',
      1 => 1407622608,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '213149288458b144bfa2b913-53012913',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'downloads' => 0,
    'cheats' => 0,
    'links' => 0,
    'basepath' => 0,
    'imgpath' => 0,
    'down' => 0,
    'lang' => 0,
    'che' => 0,
    'lin' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_58b144bfa7e8f4_63078451',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58b144bfa7e8f4_63078451')) {function content_58b144bfa7e8f4_63078451($_smarty_tpl) {?><?php if (!is_callable('smarty_function_cycle')) include '/home/vampireos/www/he6oru.localhost/lib/smarty/plugins/function.cycle.php';
if (!is_callable('smarty_modifier_date_format')) include '/home/vampireos/www/he6oru.localhost/lib/smarty/plugins/modifier.date_format.php';
?><?php if ($_smarty_tpl->tpl_vars['downloads']->value||$_smarty_tpl->tpl_vars['cheats']->value||$_smarty_tpl->tpl_vars['links']->value) {?>
<script type="text/javascript">
<!-- //
$(document).ready(function() {
    toggleCookie('links_navi', 'links_open', 30, '<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
');
});
//-->
</script>

<div class="header">
  <div id="links_navi" class="navi_toggle"><img class="absmiddle" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/toggle.png" alt="" /></div>
  <img class="absmiddle" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/warning.gif" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Links_broken');?>

</div>
<div id="links_open" class="sysinfos">
  <div class="maintable">
    <table width="100%" border="0" cellspacing="0" cellpadding="3">
      <?php  $_smarty_tpl->tpl_vars['down'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['down']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['downloads']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['down']->key => $_smarty_tpl->tpl_vars['down']->value) {
$_smarty_tpl->tpl_vars['down']->_loop = true;
?>
        <tr class="<?php echo smarty_function_cycle(array('values'=>'second,first'),$_smarty_tpl);?>
">
          <td width="10%"><a href="index.php?do=downloads&sub=overview"><?php echo $_smarty_tpl->getConfigVariable('Downloads');?>
</a></td>
          <td width="9%" class="stip" title="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['down']->value->DDatum,"%d/%m/%Y - %H:%M");?>
"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['down']->value->DDatum,"%d/%m/%y");?>
</td>
          <td width="40%" class="stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Global_Author']);?>
"><?php echo $_smarty_tpl->tpl_vars['down']->value->DName;?>
 (<a href="mailto: <?php echo $_smarty_tpl->tpl_vars['down']->value->DEmail;?>
"><?php echo $_smarty_tpl->tpl_vars['down']->value->DEmail;?>
</a>)</td>
          <td width="40%" class="stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Global_Name']);?>
"><?php echo sanitize($_smarty_tpl->tpl_vars['down']->value->Name);?>
</td>
          <td width="1%" nowrap="nowrap"><a class="colorbox stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Edit']);?>
" href="index.php?do=downloads&amp;sub=edit&amp;id=<?php echo $_smarty_tpl->tpl_vars['down']->value->Id;?>
&amp;noframes=1&amp;langcode=1"><img src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/edit.png" alt="" border="0" /></a></td>
        </tr>
      <?php } ?>
      <?php  $_smarty_tpl->tpl_vars['che'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['che']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['cheats']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['che']->key => $_smarty_tpl->tpl_vars['che']->value) {
$_smarty_tpl->tpl_vars['che']->_loop = true;
?>
        <tr class="<?php echo smarty_function_cycle(array('values'=>'second,first'),$_smarty_tpl);?>
">
          <td width="10%"><a href="index.php?do=cheats&sub=show"><?php echo $_smarty_tpl->getConfigVariable('Gaming_cheats');?>
</a></td>
          <td width="9%" class="stip" title="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['che']->value->DDatum,"%d/%m/%Y - %H:%M");?>
"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['che']->value->DDatum,"%d/%m/%y");?>
</td>
          <td width="40%" class="stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Global_Author']);?>
"><?php echo $_smarty_tpl->tpl_vars['che']->value->DName;?>
 (<a href="mailto: <?php echo $_smarty_tpl->tpl_vars['che']->value->DEmail;?>
"><?php echo $_smarty_tpl->tpl_vars['che']->value->DEmail;?>
</a>)</td>
          <td width="40%" class="stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Global_Name']);?>
"><?php echo sanitize($_smarty_tpl->tpl_vars['che']->value->Name);?>
</td>
          <td width="1%" nowrap="nowrap"><a class="colorbox stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Edit']);?>
" href="index.php?do=cheats&amp;sub=edit&amp;id=<?php echo $_smarty_tpl->tpl_vars['che']->value->Id;?>
&amp;noframes=1&amp;langcode=1"><img src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/edit.png" alt="" border="0" /></a></td>
        </tr>
      <?php } ?>
      <?php  $_smarty_tpl->tpl_vars['lin'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['lin']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['links']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['lin']->key => $_smarty_tpl->tpl_vars['lin']->value) {
$_smarty_tpl->tpl_vars['lin']->_loop = true;
?>
        <tr class="<?php echo smarty_function_cycle(array('values'=>'second,first'),$_smarty_tpl);?>
">
          <td width="10%"><a href="index.php?do=links&sub=overview"><?php echo $_smarty_tpl->getConfigVariable('Links');?>
</a></td>
          <td width="9%" class="stip" title="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['lin']->value->DDatum,"%d/%m/%Y - %H:%M");?>
"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['lin']->value->DDatum,"%d/%m/%y");?>
</td>
          <td width="40%" class="stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Global_Author']);?>
"><?php echo $_smarty_tpl->tpl_vars['lin']->value->DName;?>
 (<a href="mailto: <?php echo $_smarty_tpl->tpl_vars['lin']->value->DEmail;?>
"><?php echo $_smarty_tpl->tpl_vars['lin']->value->DEmail;?>
</a>)</td>
          <td width="40%" class="stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Global_Name']);?>
"><?php echo sanitize($_smarty_tpl->tpl_vars['lin']->value->Name);?>
</td>
          <td width="1%" nowrap="nowrap"><a class="colorbox stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Edit']);?>
" href="index.php?do=links&amp;sub=edit&amp;id=<?php echo $_smarty_tpl->tpl_vars['lin']->value->Id;?>
&amp;noframes=1&amp;langcode=1"><img src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/edit.png" alt="" border="0" /></a></td>
        </tr>
      <?php } ?>
    </table>
  </div>
</div>
<?php }?>
<?php }} ?>
