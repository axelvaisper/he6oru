<?php /* Smarty version Smarty-3.1.18, created on 2017-02-25 11:56:18
         compiled from "/home/vampireos/www/he6oru.localhost/admin/theme/standard/user/userform_newedit.tpl" */ ?>
<?php /*%%SmartyHeaderCode:4923818958b146b27dd3f1-40349584%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8f1ec0206f76418d2d29b01b68b6845da02b9d07' => 
    array (
      0 => '/home/vampireos/www/he6oru.localhost/admin/theme/standard/user/userform_newedit.tpl',
      1 => 1406660913,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4923818958b146b27dd3f1-40349584',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'settings' => 0,
    'res' => 0,
    'countries' => 0,
    'c' => 0,
    'lang' => 0,
    'imgpath' => 0,
    'groups' => 0,
    'g' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_58b146b289e960_51127478',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58b146b289e960_51127478')) {function content_58b146b289e960_51127478($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_replace')) include '/home/vampireos/www/he6oru.localhost/lib/smarty/plugins/modifier.replace.php';
?><script type="text/javascript">
<!-- //
<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['incpath']->value)."/other/jsvalidate.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

$(document).ready(function() {
    $('#Geburtstag').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd.mm.yy',
        dayNamesMin: [<?php echo $_smarty_tpl->getConfigVariable('Calendar_daysmin');?>
],
        monthNamesShort: [<?php echo $_smarty_tpl->getConfigVariable('Calendar_monthNamesShort');?>
],
        firstDay: 1
    });

    $('#convert').validate( {
	rules: {
            <?php if ($_smarty_tpl->tpl_vars['settings']->value['Reg_DataPflichtFill']==1) {?>
            Vorname: { required: true, minlength: 3 },
            Nachname: { required: true, minlength: 3 },
            <?php }?>
            Benutzername: { required: true, remote: "index.php?do=user&sub=checkuserdata&ext=<?php echo sanitize($_smarty_tpl->tpl_vars['res']->value->Benutzername);?>
" },
            <?php if ($_smarty_tpl->tpl_vars['settings']->value['Reg_AddressFill']==1) {?>
            Strasse_Nr: { required: true },
            Postleitzahl: { required: true },
            Ort: { required: true },
           <?php }?>
            Email: { required: true, email: true, remote: "index.php?do=user&sub=checkuserdata&ext=<?php echo $_smarty_tpl->tpl_vars['res']->value->Email;?>
" }
	},
	messages: {
	    Email: { remote: $.validator.format("<?php echo $_smarty_tpl->getConfigVariable('Validate_usedMail');?>
") },
	    Benutzername: { remote: $.validator.format("<?php echo $_smarty_tpl->getConfigVariable('Validate_usedUName');?>
") }
	},
	submitHandler: function() {
	    document.forms['convert'].submit();
	},
	success: function(label) {
	    label.html("&nbsp;").addClass("checked");
	}
    });
});
//-->
</script>


<form method="post" action="" name="convert" id="convert">
  <table width="100%" border="0" cellspacing="0" cellpadding="2">
    <tr>
      <td width="200" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('User_last');?>
</td>
      <td width="200" class="row_right"><input class="input" type="text" name="Nachname" value="<?php echo sanitize($_smarty_tpl->tpl_vars['res']->value->Nachname);?>
" /></td>
      <td width="200" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('User_street');?>
</td>
      <td class="row_right"><input class="input" type="text" name="Strasse_Nr" value="<?php echo sanitize($_smarty_tpl->tpl_vars['res']->value->Strasse_Nr);?>
" /></td>
    </tr>
    <tr>
      <td width="200" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('User_first');?>
</td>
      <td width="200" class="row_right"><input class="input" type="text" name="Vorname" value="<?php echo sanitize($_smarty_tpl->tpl_vars['res']->value->Vorname);?>
" /></td>
      <td width="200" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('User_zip');?>
</td>
      <td class="row_right"><input class="input" type="text" name="Postleitzahl" value="<?php echo sanitize($_smarty_tpl->tpl_vars['res']->value->Postleitzahl);?>
" /></td>
    </tr>
    <tr>
      <td width="200" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('User_MiddleName');?>
</td>
      <td width="200" class="row_right"><input class="input" type="text" name="MiddleName" value="<?php echo sanitize($_smarty_tpl->tpl_vars['res']->value->MiddleName);?>
" /></td>
      <td width="200" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('User_town');?>
</td>
      <td class="row_right"><input class="input" type="text" name="Ort" value="<?php echo sanitize($_smarty_tpl->tpl_vars['res']->value->Ort);?>
" /></td>
    </tr>
    <tr>
      <td width="200" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('User_company');?>
</td>
      <td width="200" class="row_right"><input class="input" type="text" name="Firma" value="<?php echo sanitize($_smarty_tpl->tpl_vars['res']->value->Firma);?>
" /></td>
      <td width="200" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('User_country');?>
</td>
      <td class="row_right">
        <select class="input" style="width: 130px" name="Land">
          <?php  $_smarty_tpl->tpl_vars['c'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['c']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['countries']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['c']->key => $_smarty_tpl->tpl_vars['c']->value) {
$_smarty_tpl->tpl_vars['c']->_loop = true;
?>
            <option value="<?php echo mb_strtolower($_smarty_tpl->tpl_vars['c']->value['Code'], 'windows-1251');?>
" <?php if (mb_strtolower($_smarty_tpl->tpl_vars['c']->value['Code'], 'windows-1251')==mb_strtolower($_smarty_tpl->tpl_vars['res']->value->LandCode, 'windows-1251')) {?>selected="selected"<?php }?>><?php echo sanitize($_smarty_tpl->tpl_vars['c']->value['Name']);?>
</option>
          <?php } ?>
        </select>
      </td>
    </tr>
    <tr>
      <td width="200" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('User_ustid');?>
</td>
      <td width="200" class="row_right"><input class="input" type="text" name="UStId" value="<?php echo sanitize($_smarty_tpl->tpl_vars['res']->value->UStId);?>
" /></td>
      <td width="200" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Comments_web');?>
</td>
      <td class="row_right"><input class="input" type="text" name="Webseite" value="<?php echo sanitize($_smarty_tpl->tpl_vars['res']->value->Webseite);?>
" /></td>
    </tr>
    <tr>
      <td width="200" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('User_Bank');?>
</td>
      <td colspan="3" class="row_right"><textarea cols="" rows="" name="BankName" id="BankName" class="input" style="width: 350px; height: 70px"><?php echo sanitize($_smarty_tpl->tpl_vars['res']->value->BankName);?>
</textarea></td>
    </tr>
    <tr>
      <td width="200" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Global_Email');?>
</td>
      <td width="200" class="row_right"><input class="input" type="text" name="Email" value="<?php echo $_smarty_tpl->tpl_vars['res']->value->Email;?>
" /></td>
      <td width="200" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('User_phone');?>
</td>
      <td class="row_right"><input class="input" type="text" name="Telefon" value="<?php echo sanitize($_smarty_tpl->tpl_vars['res']->value->Telefon);?>
" /></td>
    </tr>
    <tr>
      <td width="200" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('User_username');?>
</td>
      <td width="200" class="row_right"><input class="input" type="text" name="Benutzername" value="<?php echo sanitize($_smarty_tpl->tpl_vars['res']->value->Benutzername);?>
" /></td>
      <td width="200" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('User_fax');?>
</td>
      <td class="row_right"><input class="input" type="text" name="Telefax" value="<?php echo sanitize($_smarty_tpl->tpl_vars['res']->value->Telefax);?>
" /></td>
    </tr>
    <tr>
      <td width="200" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Geburtstag');?>
 - ??.??.????</td>
      <td width="200" class="row_right"><input class="input" type="text" name="Geburtstag" id="Geburtstag" value="<?php echo $_smarty_tpl->tpl_vars['res']->value->Geburtstag;?>
" /></td>
      <td width="200" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['FSK18_Userinf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('FSK18_User');?>
</td>
      <td class="row_right">
        <label><input name="Fsk18" type="radio" value="1" <?php if ($_smarty_tpl->tpl_vars['res']->value->Fsk18==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
        <label><input name="Fsk18" type="radio" value="0" <?php if ($_smarty_tpl->tpl_vars['res']->value->Fsk18==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
      </td>
    </tr>
    <tr>
      <td width="200" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Groups_Name');?>
</td>
      <td width="200" class="row_right">
        <?php if ($_SESSION['benutzer_id']!=$_smarty_tpl->tpl_vars['res']->value->Id&&$_smarty_tpl->tpl_vars['res']->value->Id!=1) {?>
          <select class="input" style="width: 130px" name="Gruppe">
            <?php  $_smarty_tpl->tpl_vars['g'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['g']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['groups']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['g']->key => $_smarty_tpl->tpl_vars['g']->value) {
$_smarty_tpl->tpl_vars['g']->_loop = true;
?>
              <?php if ($_smarty_tpl->tpl_vars['g']->value->Id!=2) {?>
                <option value="<?php echo $_smarty_tpl->tpl_vars['g']->value->Id;?>
" <?php if ($_smarty_tpl->tpl_vars['g']->value->Id==$_smarty_tpl->tpl_vars['res']->value->Gruppe) {?>selected="selected"<?php }?>><?php echo sanitize($_smarty_tpl->tpl_vars['g']->value->Name_Intern);?>
</option>
              <?php }?>
            <?php } ?>
          </select>
        <?php } else { ?>
          <input type="hidden" name="Gruppe" value="<?php echo $_smarty_tpl->tpl_vars['res']->value->Gruppe;?>
" />
          <em><?php echo $_smarty_tpl->getConfigVariable('User_noChange');?>
</em>
        <?php }?>
      </td>
      <td width="200" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['User_markteamI']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('User_team');?>
</td>
      <td class="row_right">
        <label><input type="radio" name="Team" value="1" <?php if ($_smarty_tpl->tpl_vars['res']->value->Team==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
        <label><input type="radio" name="Team" value="0" <?php if ($_smarty_tpl->tpl_vars['res']->value->Team==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
      </td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="2">
    <tr>
      <td width="200" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('User_nopassmail');?>
</td>
      <td class="row_right">
        <label><input type="radio" name="Geloescht" value="1" <?php if ($_smarty_tpl->tpl_vars['res']->value->Geloescht==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
        <label><input type="radio" name="Geloescht" value="0" <?php if ($_smarty_tpl->tpl_vars['res']->value->Geloescht==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
      </td>
    </tr>
    <?php if ($_smarty_tpl->tpl_vars['res']->value->Id!=1) {?>
      <tr>
        <td width="200" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('User_sstatus');?>
</td>
        <td class="row_right">
          <label><input type="radio" name="Aktiv" value="1" <?php if ($_smarty_tpl->tpl_vars['res']->value->Aktiv==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Global_Active');?>
</label>
          <label><input type="radio" name="Aktiv" value="0" <?php if ($_smarty_tpl->tpl_vars['res']->value->Aktiv==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Global_Inactive');?>
</label>
        </td>
      </tr>
    <?php }?>
    <tr>
      <td width="200" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('User_newPass');?>
</td>
      <td class="row_right"><input class="input" type="text" name="Kennwort" /></td>
    </tr>
    <tr>
      <td width="200" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('User_semail');?>
</td>
      <td class="row_right">
        <label><input name="send_mail" type="radio" id="send_mail" value="1" /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
        <label><input name="send_mail" type="radio" value="0" checked="checked" /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
      </td>
    </tr>
    <tr>
      <td width="200" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('User_emailtext');?>
</td>
      <td class="row_right"><textarea cols="" rows="" name="mail_text" id="mail_text" class="input" style="width: 500px; height: 120px"><?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['lang']->value['User_profileChangeText'],"__N__","\n");?>
</textarea></td>
    </tr>
  </table>
  <input class="button" type="submit" name="button" value="<?php echo $_smarty_tpl->getConfigVariable('Save');?>
" />
  <input class="button" type="button" name="button" onclick="closeWindow(true);" value="<?php echo $_smarty_tpl->getConfigVariable('Close');?>
" />
  <input name="save" type="hidden" id="save" value="1" />
</form>
<?php }} ?>
