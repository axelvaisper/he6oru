<?php /* Smarty version Smarty-3.1.18, created on 2017-02-25 11:57:00
         compiled from "/home/vampireos/www/he6oru.localhost/admin/theme/standard/settings/general.tpl" */ ?>
<?php /*%%SmartyHeaderCode:150488450958b146dc05c0a3-42551601%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e142ecfa383e5d70fe89822d21c8d5d147736df1' => 
    array (
      0 => '/home/vampireos/www/he6oru.localhost/admin/theme/standard/settings/general.tpl',
      1 => 1429051018,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '150488450958b146dc05c0a3-42551601',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'admin_settings' => 0,
    'helpquery' => 0,
    'imgpath' => 0,
    'iframe_index' => 0,
    'shop_aktiv' => 0,
    'lang' => 0,
    'rewrite_error1' => 0,
    'rewrite_error2' => 0,
    'row' => 0,
    'timezone' => 0,
    'key' => 0,
    'tz' => 0,
    'Impressum' => 0,
    'Reg_Agb' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_58b146dc29d038_40482310',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58b146dc29d038_40482310')) {function content_58b146dc29d038_40482310($_smarty_tpl) {?><script type="text/javascript">
<!-- //
<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['incpath']->value)."/other/jsvalidate.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

$.validator.setDefaults({
    submitHandler: function() {
        document.forms['editforms'].submit();
    }
});
$(document).ready(function() {
    if ($('#js_off').prop('checked')) {
       $('.combjs').hide();
    }
    if ($('#css_off').prop('checked')) {
       $('.combcss').hide();
    }
    $('#js_off').on('click', function() {
        $('.combjs').hide();
    });
    $('#js_on').on('click', function() {
        $('.combjs').show();
    });
    $('#css_off').on('click', function() {
        $('.combcss').hide();
    });
    $('#css_on').on('click', function() {
        $('.combcss').show();
    });

    $('#editform').validate({
        rules: {
            Land: { required: true, minlength: 2 },
            Seitenname: { required: true, minlength: 2 },
            Seitenbetreiber: { required: true, minlength: 2 },
            Strasse: { required: true, minlength: 2 },
            Stadt: { required: true, minlength: 2 },
            Mail_Absender: { required: true, email: true },
            Mail_Name: { required: true, minlength: 5 },
            Mail_Header: { required: true },
            Mail_Fuss_HTML: { required: true },
            Mail_Fuss: { required: true },
            Spamwoerter: { required: true },
            SpamRegEx: { required: true },
            Kommentare_IconBreite: { required: true, range: [40, 120] },
            Kommentar_Laenge: { required: true, range: [100, 5000] },
            Kommentare_Seite: { required: true, range: [5, 35] },
            Loesch_Gruende: { required: true }
        },
        messages: {
            Land: {
                required: '<?php echo $_smarty_tpl->getConfigVariable('SettingsCountry');?>
',
                minlength: '<?php echo $_smarty_tpl->getConfigVariable('SettingsCountryLength');?>
'
            }
        },
        success: function(label) {
            label.html("&nbsp;").addClass("checked");
        }
    });

    $('#a_sett').accordion({
	autoHeight: false,
	icons: {
    	    header: 'ui-icon-circle-arrow-e',
   	    headerSelected: 'ui-icon-circle-arrow-s'
	}
    });
});

function set_method() {
    document.getElementById('Mail_Port').style.display = 'none';
    document.getElementById('Mail_Host').style.display = 'none';
    document.getElementById('Mail_Type_Auth').style.display = 'none';
    document.getElementById('Mail_Username').style.display = 'none';
    document.getElementById('Mail_Passwort').style.display = 'none';
    document.getElementById('Mail_Sendmailpfad').style.display = 'none';
    document.getElementById('Auth').style.display = 'none';
    if(document.getElementById('smtp').selected == true) {
        document.getElementById('Mail_Port').style.display = '';
        document.getElementById('Mail_Host').style.display = '';
        document.getElementById('Mail_Type_Auth').style.display = '';
        document.getElementById('Mail_Username').style.display = '';
        document.getElementById('Mail_Passwort').style.display = '';
        document.getElementById('Mail_Sendmailpfad').style.display = 'none';
        document.getElementById('Auth').style.display = '';
    }
    if(document.getElementById('sendmail').selected == true) {
        document.getElementById('Mail_Port').style.display = 'none';
        document.getElementById('Mail_Host').style.display = 'none';
        document.getElementById('Mail_Type_Auth').style.display = 'none';
        document.getElementById('Mail_Username').style.display = 'none';
        document.getElementById('Mail_Passwort').style.display = 'none';
        document.getElementById('Mail_Sendmailpfad').style.display = '';
        document.getElementById('Auth').style.display = 'none';
    }
}
function emailcheck() {
    var mailserver = document.getElementById('mah').value;
    var username = document.getElementById('mau').value;
    var password = document.getElementById('map').value;
    var method = document.getElementById('Mail_Typ').value;
    var mailabs = document.getElementById('mabs').value;
    var smpath = document.getElementById('smpath').value;
    var smport = document.getElementById('mailp').value;
    var auth = document.getElementById('auth').checked == true && document.getElementById('Mail_Typ').value == 'smtp' ? 1 : 0;
    window.open('index.php?do=settings&sub=emailcheck&noframes=1&f=' + mailabs + '&auth=' + auth + '&method=' + method + '&mailserver=' + mailserver + '&username=' + username + '&password=' + password + '&smpath=' + smpath + '&smport=' + smport + '', 'emc', 'top=0,left=0,width=700,height=400');
}
//-->
</script>

<div class="header"><?php echo $_smarty_tpl->getConfigVariable('Settings_general');?>
</div>
<div class="subheaders">
  <?php if ($_smarty_tpl->tpl_vars['admin_settings']->value['Ahelp']==1) {?>
    <a class="colorbox" href="index.php?do=help&amp;sub=<?php echo $_smarty_tpl->tpl_vars['helpquery']->value;?>
&amp;noframes=1"><img class="absmiddle" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/s_help.png" alt="" border="0" /> <?php echo $_smarty_tpl->getConfigVariable('GlobalHelp');?>
</a>&nbsp;&nbsp;&nbsp;
    <?php }?>
  <a class="colorbox" href="index.php?do=support&amp;sub=send_order&amp;noframes=1"><img class="absmiddle" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/send.png" alt="" border="0" /> <?php echo $_smarty_tpl->getConfigVariable('SendOrder');?>
</a>
</div>
<?php echo (($tmp = @$_smarty_tpl->tpl_vars['iframe_index']->value)===null||$tmp==='' ? '' : $tmp);?>

<form class="adminform" method="post" action="" autocomplete="off" name="editforms" id="editform">
  <?php if ($_smarty_tpl->tpl_vars['shop_aktiv']->value!=1) {?>
    <input type="hidden" name="ShopStart" value="0" />
  <?php }?>
  <div id="a_sett">
    <div><a href="#"><?php echo $_smarty_tpl->getConfigVariable('Settings_websitesettings');?>
</a></div>
    <div>
      <table width="100%" border="0" cellpadding="4" cellspacing="0">
        <tr>
          <td width="350" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['RwEHtacces_Inf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Global_Settings_seos');?>
</td>
          <td class="row_right">
            <?php if (!empty($_smarty_tpl->tpl_vars['rewrite_error1']->value)||!empty($_smarty_tpl->tpl_vars['rewrite_error2']->value)) {?>
              <span style="color: red"><?php echo $_smarty_tpl->tpl_vars['rewrite_error1']->value;?>
<?php echo $_smarty_tpl->tpl_vars['rewrite_error2']->value;?>
</span>
              <input type="hidden" name="use_seo" value="0" />
            <?php } else { ?>
              <label><input type="radio" name="use_seo" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['use_seo']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
              <label><input type="radio" name="use_seo" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['use_seo']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('RwEHtacces_dym');?>
</label>
              <?php }?>
          </td>
        </tr>
        <tr>
          <td width="350" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['LogsTInf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('LogsT');?>
</td>
          <td class="row_right">
            <label><input type="radio" name="Logging" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['Logging']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
            <label><input type="radio" name="Logging" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['Logging']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
          </td>
        </tr>
        <tr>
          <td width="350" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Editor_text']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Admin_Editor');?>
</td>
          <td class="row_right">
            <label><input type="radio" name="SiteEditor" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['SiteEditor']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('EditorCKE');?>
</label>
            <label><input type="radio" name="SiteEditor" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['SiteEditor']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('EditorText');?>
</label>
          </td>
        </tr>
        <tr>
          <td width="350" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['TimeZone_Inf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('TimeZone');?>
</td>
          <td class="row_right">
            <select class="input" name="timezone">
              <?php  $_smarty_tpl->tpl_vars['tz'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['tz']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['timezone']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['tz']->key => $_smarty_tpl->tpl_vars['tz']->value) {
$_smarty_tpl->tpl_vars['tz']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['tz']->key;
?>
                <option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['row']->value['timezone']==$_smarty_tpl->tpl_vars['key']->value) {?>selected="selected"<?php }?>> <?php echo sanitize($_smarty_tpl->tpl_vars['tz']->value);?>
 </option>
              <?php } ?>
            </select>
          </td>
        </tr>
        <tr>
          <td width="350" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Shop_Settings_CountryInf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Shop_Settings_shopCountry');?>
</td>
          <td class="row_right"><input class="input" name="Land" type="text" style="width: 50px" value="<?php echo mb_strtoupper($_smarty_tpl->tpl_vars['row']->value['Land'], 'windows-1251');?>
" /></td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('AktivDomains');?>
</td>
          <td class="row_right">
            <label><input type="radio" name="Domains" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['Domains']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
            <label><input type="radio" name="Domains" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['Domains']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
          </td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('AktivYandex');?>
</td>
          <td class="row_right">
            <label><input type="radio" name="meta_yandex" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['meta_yandex']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
            <label><input type="radio" name="meta_yandex" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['meta_yandex']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
          </td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('CodeYandex');?>
</td>
          <td class="row_right"><input class="input" name="code_yandex" type="text" style="width: 160px" value="<?php echo sanitize($_smarty_tpl->tpl_vars['row']->value['code_yandex']);?>
" /></td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('AktivGoogle');?>
</td>
          <td class="row_right">
            <label><input type="radio" name="meta_google" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['meta_google']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
            <label><input type="radio" name="meta_google" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['meta_google']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
          </td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('CodeGoogle');?>
</td>
          <td class="row_right"><input class="input" name="code_google" type="text" style="width: 160px" value="<?php echo sanitize($_smarty_tpl->tpl_vars['row']->value['code_google']);?>
" /></td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('AktivAnalytics');?>
</td>
          <td class="row_right">
            <label><input type="radio" name="analytics" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['analytics']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
            <label><input type="radio" name="analytics" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['analytics']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
          </td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('CodeAnalytics');?>
</td>
          <td class="row_right"><input class="input" name="analytics_code" type="text" style="width: 160px" value="<?php echo sanitize($_smarty_tpl->tpl_vars['row']->value['analytics_code']);?>
" /></td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('AutoMailBirthdays');?>
</td>
          <td class="row_right">
            <label><input type="radio" name="birthdays_mail" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['birthdays_mail']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
            <label><input type="radio" name="birthdays_mail" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['birthdays_mail']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
          </td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('SendErrorEmail');?>
</td>
          <td class="row_right">
            <label><input type="radio" name="Error_Email" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['Error_Email']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
            <label><input type="radio" name="Error_Email" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['Error_Email']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
          </td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('AllowedHtml');?>
</td>
          <td class="row_right"><input class="input" name="allowed" style="width: 160px" type="text" style="width: 160px" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['allowed'];?>
" /></td>
        </tr>
      </table>
    </div>
    <div><a href="#"><?php echo $_smarty_tpl->getConfigVariable('Settings_Rekvizit');?>
</a></div>
    <div>
      <table width="100%" border="0" cellpadding="4" cellspacing="0">
        <tr>
          <td  width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_websitename');?>
</td>
          <td class="row_right"><input class="input" name="Seitenname" id="sname" type="text" style="width: 160px" value="<?php echo sanitize($_smarty_tpl->tpl_vars['row']->value['Seitenname']);?>
" /></td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_company');?>
</td>
          <td class="row_right"><input class="input" name="Firma" type="text" style="width: 160px" value="<?php echo sanitize($_smarty_tpl->tpl_vars['row']->value['Firma']);?>
" /></td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_Inn');?>
</td>
          <td class="row_right"><input class="input" name="Inn" type="text" style="width: 160px" value="<?php echo sanitize($_smarty_tpl->tpl_vars['row']->value['Inn']);?>
" /></td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_Kpp');?>
</td>
          <td class="row_right"><input class="input" name="Kpp" type="text" style="width: 160px" value="<?php echo sanitize($_smarty_tpl->tpl_vars['row']->value['Kpp']);?>
" /></td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_Bik');?>
</td>
          <td class="row_right"><input class="input" name="Bik" type="text" style="width: 160px" value="<?php echo sanitize($_smarty_tpl->tpl_vars['row']->value['Bik']);?>
" /></td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_Bank');?>
</td>
          <td class="row_right"><input class="input" name="Bank" type="text" style="width: 160px" value="<?php echo sanitize($_smarty_tpl->tpl_vars['row']->value['Bank']);?>
" /></td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_Kschet');?>
</td>
          <td class="row_right"><input class="input" name="Kschet" type="text" style="width: 160px" value="<?php echo sanitize($_smarty_tpl->tpl_vars['row']->value['Kschet']);?>
" /></td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_Rschet');?>
</td>
          <td class="row_right"><input class="input" name="Rschet" type="text" style="width: 160px" value="<?php echo sanitize($_smarty_tpl->tpl_vars['row']->value['Rschet']);?>
" /></td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_Zip');?>
</td>
          <td class="row_right"><input class="input" name="Zip" type="text" style="width: 160px" value="<?php echo sanitize($_smarty_tpl->tpl_vars['row']->value['Zip']);?>
" /></td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('User_town');?>
</td>
          <td class="row_right"><input class="input" name="Stadt" type="text" style="width: 160px" value="<?php echo sanitize($_smarty_tpl->tpl_vars['row']->value['Stadt']);?>
" /></td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_streetnumber');?>
</td>
          <td class="row_right"><input class="input" name="Strasse" type="text" style="width: 160px" value="<?php echo sanitize($_smarty_tpl->tpl_vars['row']->value['Strasse']);?>
" /></td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_Telefon');?>
</td>
          <td class="row_right"><input class="input" name="Telefon" type="text" style="width: 160px" value="<?php echo sanitize($_smarty_tpl->tpl_vars['row']->value['Telefon']);?>
" /></td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_Fax');?>
</td>
          <td class="row_right"><input class="input" name="Fax" type="text" style="width: 160px" value="<?php echo sanitize($_smarty_tpl->tpl_vars['row']->value['Fax']);?>
" /></td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_yname');?>
</td>
          <td class="row_right"><input class="input" name="Seitenbetreiber" type="text" style="width: 160px" value="<?php echo sanitize($_smarty_tpl->tpl_vars['row']->value['Seitenbetreiber']);?>
" /></td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_Buh');?>
</td>
          <td class="row_right"><input class="input" name="Buh" type="text" style="width: 160px" value="<?php echo sanitize($_smarty_tpl->tpl_vars['row']->value['Buh']);?>
" /></td>
        </tr>
      </table>
    </div>
    <div><a href="#"><?php echo $_smarty_tpl->getConfigVariable('SystemOptimize');?>
</a></div>
    <div>
      <table width="100%" border="0" cellpadding="4" cellspacing="0">
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('MaxCount');?>
 &lt;title&gt;</td>
          <td class="row_right"><input class="input" name="CountTitle" type="text" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['CountTitle'];?>
" size="4" maxlength="3" /></td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('MaxCount');?>
 &lt;keywords&gt</td>
          <td class="row_right"><input class="input" name="CountKeywords" type="text" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['CountKeywords'];?>
" size="4" maxlength="3" /></td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('MaxCount');?>
 &lt;description&gt;</td>
          <td class="row_right"><input class="input" name="CountDescription" type="text" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['CountDescription'];?>
" size="4" maxlength="3" /></td>
        </tr>
        <tr>
          <td width="350" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['RecomendNo']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('AutoCheckFile');?>
</td>
          <td class="row_right">
            <label><input type="radio" name="cleanup" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['cleanup']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
            <label><input type="radio" name="cleanup" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['cleanup']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
          </td>
        </tr>
        <tr>
          <td width="350" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['RecomendYes']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('PageOptimize');?>
</td>
          <td class="row_right">
            <label><input type="radio" name="min_page" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['min_page']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
            <label><input type="radio" name="min_page" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['min_page']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
          </td>
        </tr>
        <tr>
          <td width="350" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['RecomendYes']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('PageGzip');?>
</td>
          <td class="row_right">
            <label><input type="radio" name="gzip_page" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['gzip_page']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
            <label><input type="radio" name="gzip_page" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['gzip_page']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
          </td>
        </tr>
        <tr>
          <td width="350" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['RecomendYes']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('JsCombine');?>
</td>
          <td class="row_right">
            <label><input id="js_on" type="radio" name="comb_js" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['comb_js']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
            <label><input id="js_off" type="radio" name="comb_js" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['comb_js']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
          </td>
        </tr>
        <tr class="combjs">
          <td width="350" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['RecomendYes']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('JsOptimize');?>
</td>
          <td class="row_right">
            <label><input type="radio" name="min_js" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['min_js']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
            <label><input type="radio" name="min_js" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['min_js']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
          </td>
        </tr>
        <tr class="combjs">
          <td width="350" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['RecomendYes']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('JsGzip');?>
</td>
          <td class="row_right">
            <label><input type="radio" name="gzip_js" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['gzip_js']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
            <label><input type="radio" name="gzip_js" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['gzip_js']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
          </td>
        </tr>
        <tr class="combjs">
          <td width="350" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['RecomendYes']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('JsCache');?>
</td>
          <td class="row_right">
            <label><input type="radio" name="expires_js" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['expires_js']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
            <label><input type="radio" name="expires_js" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['expires_js']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
          </td>
        </tr>
        <tr>
          <td width="350" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['RecomendYes']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('CssCombine');?>
</td>
          <td class="row_right">
            <label><input id="css_on" type="radio" name="comb_css" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['comb_css']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
            <label><input id="css_off" type="radio" name="comb_css" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['comb_css']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
          </td>
        </tr>
        <tr class="combcss">
          <td width="350" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['RecomendYes']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('CssOptimize');?>
</td>
          <td class="row_right">
            <label><input type="radio" name="min_css" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['min_css']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
            <label><input type="radio" name="min_css" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['min_css']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
          </td>
        </tr>
        <tr class="combcss">
          <td width="350" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['RecomendYes']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('CssGzip');?>
</td>
          <td class="row_right">
            <label><input type="radio" name="gzip_css" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['gzip_css']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
            <label><input type="radio" name="gzip_css" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['gzip_css']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
          </td>
        </tr>
        <tr class="combcss">
          <td width="350" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['RecomendYes']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('CssCache');?>
</td>
          <td class="row_right">
            <label><input type="radio" name="expires_css" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['expires_css']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
            <label><input type="radio" name="expires_css" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['expires_css']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
          </td>
        </tr>
        <tr>
          <td width="350" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['NoFileOptimizeInf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('NoFileOptimize');?>
 </td>
          <td class="row_right"><textarea cols="" rows="" class="input" style="width: 400px; height: 50px" onclick="focusArea(this, 120);" name="ignore_list"><?php echo sanitize($_smarty_tpl->tpl_vars['row']->value['ignore_list']);?>
</textarea></td>
        </tr>
      </table>
    </div>
    <div><a href="#"><?php echo $_smarty_tpl->getConfigVariable('Settings_companyImp');?>
&nbsp;&nbsp;<img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Settings_companyImpInf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /></a></div>
    <div> <?php echo $_smarty_tpl->tpl_vars['Impressum']->value;?>
 </div>
    <div><a href="#"><?php echo $_smarty_tpl->getConfigVariable('Settings_agb');?>
&nbsp;&nbsp;<img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Settings_agbInf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /></a></div>
    <div> <?php echo $_smarty_tpl->tpl_vars['Reg_Agb']->value;?>
 </div>
    <?php if ($_smarty_tpl->tpl_vars['shop_aktiv']->value==1) {?>
      <div><a href="#"><?php echo $_smarty_tpl->getConfigVariable('Settings_startsettings');?>
</a></div>
      <div>
        <table width="100%" border="0" cellpadding="4" cellspacing="0">
          <tr>
            <td width="350" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Setting_shopstart_inf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Settings_shopasstartpage');?>
 </td>
            <td class="row_right">
              <label><input type="radio" name="shop_is_startpage" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['shop_is_startpage']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
              <label><input type="radio" name="shop_is_startpage" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['shop_is_startpage']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
            </td>
          </tr>
        </table>
      </div>
    <?php }?>
    <div><a href="#"><?php echo $_smarty_tpl->getConfigVariable('Settings_regsettings');?>
</a></div>
    <div>
      <table width="100%" border="0" cellpadding="4" cellspacing="0">
        <tr>
          <td width="350" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Settings_userreg_inf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Settings_userregdone');?>
</td>
          <td class="row_right">
            <label><input type="radio" name="Reg_Typ" value="norm" <?php if ($_smarty_tpl->tpl_vars['row']->value['Reg_Typ']=='norm') {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('ConfirmRegNoMail');?>
</label>
            <label><input type="radio" name="Reg_Typ" value="email" <?php if ($_smarty_tpl->tpl_vars['row']->value['Reg_Typ']=='email') {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('ConfirmRegMail');?>
</label>
          </td>
        </tr>
        <tr>
          <td width="350" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Settings_Reg_Pass_Inf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Settings_Reg_Pass');?>
</td>
          <td class="row_right">
            <label><input type="radio" name="Reg_Pass" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['Reg_Pass']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
            <label><input type="radio" name="Reg_Pass" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['Reg_Pass']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
          </td>
        </tr>
      </table>
    </div>
    <div><a href="#"><?php echo $_smarty_tpl->getConfigVariable('Settings_mailserversettings');?>
</a></div>
    <div>
      <table width="100%" border="0" cellpadding="4" cellspacing="0">
        <tr>
          <td width="350" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Settings_emailm_inf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Settings_mailmethod');?>
</td>
          <td class="row_right">
            <select class="input" onchange="set_method();" id="Mail_Typ" name="Mail_Typ">
              <option value="mail" <?php if ($_smarty_tpl->tpl_vars['row']->value['Mail_Typ']=='mail') {?>selected="selected"<?php }?>>MAIL</option>
              <option id="smtp" value="smtp" <?php if ($_smarty_tpl->tpl_vars['row']->value['Mail_Typ']=='smtp') {?>selected="selected"<?php }?>>SMTP</option>
              <option id="sendmail" value="sendmail" <?php if ($_smarty_tpl->tpl_vars['row']->value['Mail_Typ']=='sendmail') {?>selected="selected"<?php }?>>SENDMAIL</option>
            </select>
          </td>
        </tr>
        <tr id="Mail_Port">
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_smtp_port');?>
</td>
          <td class="row_right"><input class="input" name="Mail_Port" id="mailp" type="text" style="width: 45px" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['Mail_Port'];?>
" size="5" maxlength="5" /></td>
        </tr>
        <tr id="Auth">
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_smtp_autor');?>
</td>
          <td class="row_right"><input type="checkbox" id="auth" name="Mail_Auth" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['Mail_Auth']==1) {?>checked="checked"<?php }?> /></td>
        </tr>
        <tr id="Mail_Type_Auth">
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_smtp_type_auth');?>
</td>
          <td class="row_right">
            <select class="input" name="Mail_Type_Auth">
              <option value="not" <?php if ($_smarty_tpl->tpl_vars['row']->value['Mail_Type_Auth']=='not') {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->getConfigVariable('No');?>
</option>
              <option value="ssl" <?php if ($_smarty_tpl->tpl_vars['row']->value['Mail_Type_Auth']=='ssl') {?>selected="selected"<?php }?>>SSL</option>
              <option value="tls" <?php if ($_smarty_tpl->tpl_vars['row']->value['Mail_Type_Auth']=='tls') {?>selected="selected"<?php }?>>TLS</option>
            </select>
          </td>
        </tr>
        <tr id="Mail_Host">
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_smtp_server');?>
</td>
          <td class="row_right"><input class="input" id="mah" name="Mail_Host" type="text" style="width: 160px" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['Mail_Host'];?>
" /></td>
        </tr>
        <tr id="Mail_Username">
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_sm_user');?>
</td>
          <td class="row_right"><input id="mau" class="input" name="Mail_Username" type="text" style="width: 160px" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['Mail_Username'];?>
" /></td>
        </tr>
        <tr id="Mail_Passwort">
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_sm_pass');?>
</td>
          <td class="row_right"><input id="map" class="input" name="Mail_Passwort"type="text" style="width: 160px" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['Mail_Passwort'];?>
" /></td>
        </tr>
        <tr id="Mail_Sendmailpfad">
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_sm_path');?>
</td>
          <td class="row_right"><input class="input" id="smpath" name="Mail_Sendmailpfad" type="text" style="width: 160px" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['Mail_Sendmailpfad'];?>
" /></td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Test_email');?>
</td>
          <td class="row_right"><input class="button" type="button" value="<?php echo $_smarty_tpl->getConfigVariable('Test');?>
" onclick="emailcheck();" /></td>
        </tr>
      </table>
    </div>
    <div><a href="#"><?php echo $_smarty_tpl->getConfigVariable('Settings_mailsettings');?>
</a></div>
    <div>
      <table width="100%" border="0" cellpadding="4" cellspacing="0">
        <tr>
          <td width="350" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Settings_emaila_inf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Settings_mailemail');?>
</td>
          <td class="row_right"><input class="input" id="mabs" name="Mail_Absender" type="text" style="width: 160px" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['Mail_Absender'];?>
" /></td>
        </tr>
        <tr>
          <td width="350" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Settings_emailab_inf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Settings_mailsender');?>
</td>
          <td class="row_right"><input class="input" name="Mail_Name" type="text" style="width: 160px" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['Mail_Name'];?>
" /></td>
        </tr>
        <tr>
          <td width="350" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Settings_htmlh_inf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Settings_mailheader');?>
 </td>
          <td class="row_right"><textarea cols="" rows="" class="input" style="width: 98%; height: 120px" onclick="focusArea(this, 200);" name="Mail_Header"><?php echo sanitize($_smarty_tpl->tpl_vars['row']->value['Mail_Header']);?>
</textarea></td>
        </tr>
        <tr>
          <td width="350" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Settings_htmlf_inf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Settings_mailfooter_html');?>

            <div class="leftinf">
              <strong><?php echo $_smarty_tpl->getConfigVariable('Settings_rp');?>
</strong> <br />
              %%COMPANY%% = <?php echo $_smarty_tpl->getConfigVariable('User_company');?>
 <br />
              %%TOWN%% = <?php echo $_smarty_tpl->getConfigVariable('User_town');?>
 <br />
              %%ZIP%% = <?php echo $_smarty_tpl->getConfigVariable('User_zip');?>
 <br />
              %%STREET%% = <?php echo $_smarty_tpl->getConfigVariable('User_street');?>
 <br />
              %%ADRESS%% = <?php echo $_smarty_tpl->getConfigVariable('User_street');?>
 <br />
              %%MAIL%% = <?php echo $_smarty_tpl->getConfigVariable('Global_Email');?>
 <br />
              %%TELEFON%% = <?php echo $_smarty_tpl->getConfigVariable('User_phone');?>
 <br />
              %%FAX%% = <?php echo $_smarty_tpl->getConfigVariable('User_fax');?>
 <br />
              %%HTTP%% = <?php echo $_smarty_tpl->getConfigVariable('Comments_web');?>
 <br />
              %%INN%% = <?php echo $_smarty_tpl->getConfigVariable('Settings_Inn');?>
 <br />
              %%KPP%% = <?php echo $_smarty_tpl->getConfigVariable('Settings_Kpp');?>
 <br />
              %%BIK%% = <?php echo $_smarty_tpl->getConfigVariable('Settings_Bik');?>
 <br />
              %%BANK%% = <?php echo $_smarty_tpl->getConfigVariable('Settings_Bank');?>
 <br />
              %%KSCHET%% = <?php echo $_smarty_tpl->getConfigVariable('Settings_Kschet');?>
 <br />
              %%RSCHET%% = <?php echo $_smarty_tpl->getConfigVariable('Settings_Rschet');?>
 <br />
              %%OWNER%% = <?php echo $_smarty_tpl->getConfigVariable('User_first');?>
 <br />
              %%DIREKTOR%% = <?php echo $_smarty_tpl->getConfigVariable('Settings_yname');?>
 <br />
              %%BUH%% = <?php echo $_smarty_tpl->getConfigVariable('Settings_Buh');?>
 <br />
            </div>
          </td>
          <td class="row_right"><textarea cols="" rows="" id="xi" class="input" style="width: 98%; height: 350px" name="Mail_Fuss_HTML"><?php echo sanitize($_smarty_tpl->tpl_vars['row']->value['Mail_Fuss_HTML']);?>
</textarea></td>
        </tr>
        <tr>
          <td width="350" class="row_left">
            <img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Settings_textf_inf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Settings_mailfooter_text');?>

            <div class="leftinf">
              <strong><?php echo $_smarty_tpl->getConfigVariable('Settings_rp');?>
</strong> <br />
              %%COMPANY%% = <?php echo $_smarty_tpl->getConfigVariable('User_company');?>
 <br />
              %%TOWN%% = <?php echo $_smarty_tpl->getConfigVariable('User_town');?>
 <br />
              %%ZIP%% = <?php echo $_smarty_tpl->getConfigVariable('User_zip');?>
 <br />
              %%STREET%% = <?php echo $_smarty_tpl->getConfigVariable('User_street');?>
 <br />
              %%ADRESS%% = <?php echo $_smarty_tpl->getConfigVariable('User_street');?>
 <br />
              %%MAIL%% = <?php echo $_smarty_tpl->getConfigVariable('Global_Email');?>
 <br />
              %%TELEFON%% = <?php echo $_smarty_tpl->getConfigVariable('User_phone');?>
 <br />
              %%FAX%% = <?php echo $_smarty_tpl->getConfigVariable('User_fax');?>
 <br />
              %%HTTP%% = <?php echo $_smarty_tpl->getConfigVariable('Comments_web');?>
 <br />
              %%INN%% = <?php echo $_smarty_tpl->getConfigVariable('Settings_Inn');?>
 <br />
              %%KPP%% = <?php echo $_smarty_tpl->getConfigVariable('Settings_Kpp');?>
 <br />
              %%BIK%% = <?php echo $_smarty_tpl->getConfigVariable('Settings_Bik');?>
 <br />
              %%BANK%% = <?php echo $_smarty_tpl->getConfigVariable('Settings_Bank');?>
 <br />
              %%KSCHET%% = <?php echo $_smarty_tpl->getConfigVariable('Settings_Kschet');?>
 <br />
              %%RSCHET%% = <?php echo $_smarty_tpl->getConfigVariable('Settings_Rschet');?>
 <br />
              %%OWNER%% = <?php echo $_smarty_tpl->getConfigVariable('User_first');?>
 <br />
              %%DIREKTOR%% = <?php echo $_smarty_tpl->getConfigVariable('Settings_yname');?>
 <br />
              %%BUH%% = <?php echo $_smarty_tpl->getConfigVariable('Settings_Buh');?>
 <br />
            </div>
          </td>
          <td class="row_right"><textarea cols="" rows="" class="input" style="width: 98%; height: 350px" name="Mail_Fuss"><?php echo sanitize($_smarty_tpl->tpl_vars['row']->value['Mail_Fuss']);?>
</textarea></td>
        </tr>
      </table>
    </div>
    <div><a href="#"><?php echo $_smarty_tpl->getConfigVariable('Settings_forbidden');?>
</a></div>
    <div>
      <table width="100%" border="0" cellpadding="4" cellspacing="0">
        <tr>
          <td width="350" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Settings_delReasons_inf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Settings_delReasons');?>
</td>
          <td class="row_right"><textarea cols="" rows="" class="input" style="width: 400px; height: 70px" name="Loesch_Gruende"><?php echo sanitize($_smarty_tpl->tpl_vars['row']->value['Loesch_Gruende']);?>
</textarea></td>
        </tr>
        <tr>
          <td width="350" class="row_left"><img class="absmiddle stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['Settings_spam_inf']);?>
" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/help.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Settings_fwords');?>
 </td>
          <td class="row_right"><textarea cols="" rows="" class="input" style="width: 400px; height: 70px" onclick="focusArea(this, 140);" name="Spamwoerter"><?php echo sanitize($_smarty_tpl->tpl_vars['row']->value['Spamwoerter']);?>
</textarea></td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_spamrp');?>
</td>
          <td class="row_right"><input class="input" name="SpamRegEx" type="text" style="width: 160px" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['SpamRegEx'];?>
" /></td>
        </tr>
      </table>
    </div>
    <div><a href="#"><?php echo $_smarty_tpl->getConfigVariable('Settings_kcoptions');?>
</a></div>
    <div>
      <table width="100%" border="0" cellpadding="4" cellspacing="0">
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_kcactive');?>
</td>
          <td class="row_right">
            <label><input type="radio" name="SysCode_Aktiv" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['SysCode_Aktiv']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
            <label><input type="radio" name="SysCode_Aktiv" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['SysCode_Aktiv']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
          </td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_kcformat');?>
</td>
          <td class="row_right">
            <label><input type="radio" name="KommentarFormat" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['KommentarFormat']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
            <label><input type="radio" name="KommentarFormat" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['KommentarFormat']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
          </td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_kcsmilies');?>
</td>
          <td class="row_right">
            <label><input type="radio" name="SysCode_Smilies" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['SysCode_Smilies']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
            <label><input type="radio" name="SysCode_Smilies" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['SysCode_Smilies']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
          </td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_kcimages');?>
</td>
          <td class="row_right">
            <label><input type="radio" name="SysCode_Bild" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['SysCode_Bild']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
            <label><input type="radio" name="SysCode_Bild" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['SysCode_Bild']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
          </td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_kclinkcode');?>
</td>
          <td class="row_right">
            <label><input type="radio" name="SysCode_Links" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['SysCode_Links']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
            <label><input type="radio" name="SysCode_Links" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['SysCode_Links']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
          </td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_kcemailcode');?>
</td>
          <td class="row_right">
            <label><input type="radio" name="SysCode_Email" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['SysCode_Email']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
            <label><input type="radio" name="SysCode_Email" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['SysCode_Email']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
          </td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_commentsmoderated');?>
</td>
          <td class="row_right">
            <label><input type="radio" name="Kommentar_Moderiert" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['Kommentar_Moderiert']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
            <label><input type="radio" name="Kommentar_Moderiert" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['Kommentar_Moderiert']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
          </td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_commentsiconsuse');?>
</td>
          <td class="row_right">
            <label><input type="radio" name="Kommentare_Icon" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['Kommentare_Icon']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
            <label><input type="radio" name="Kommentare_Icon" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['Kommentare_Icon']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
          </td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_commenticonsw');?>
</td>
          <td class="row_right"><input class="input" name="Kommentare_IconBreite" type="text" style="width: 50px" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['Kommentare_IconBreite'];?>
" size="5" maxlength="3" /></td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_commentslength');?>
</td>
          <td class="row_right"><input class="input" name="Kommentar_Laenge" type="text" style="width: 50px" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['Kommentar_Laenge'];?>
" size="5" maxlength="4" /></td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_commentspage');?>
</td>
          <td class="row_right"><input class="input" name="Kommentare_Seite" type="text" style="width: 50px" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['Kommentare_Seite'];?>
" size="5" maxlength="3" /></td>
        </tr>
      </table>
    </div>
    <div><a href="#"><?php echo $_smarty_tpl->getConfigVariable('Settings_fieldsettings');?>
</a></div>
    <div>
      <table width="100%" border="0" cellpadding="4" cellspacing="0">
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_NSO');?>
</td>
          <td width="100" class="row_right">
            <label><input type="radio" name="Reg_DataPflicht" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['Reg_DataPflicht']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
            <label><input type="radio" name="Reg_DataPflicht" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['Reg_DataPflicht']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
          </td>
          <td class="row_left"><label><input type="checkbox" class="absmiddle" name="Reg_DataPflichtFill" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['Reg_DataPflichtFill']==1) {?>checked="checked"<?php }?>/><?php echo $_smarty_tpl->getConfigVariable('Settings_InputFill');?>
</label></td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_Address');?>
</td>
          <td width="100" class="row_right">
            <label><input type="radio" name="Reg_Address" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['Reg_Address']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
            <label><input type="radio" name="Reg_Address" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['Reg_Address']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
          </td>
          <td class="row_left"><label><input type="checkbox" class="absmiddle" name="Reg_AddressFill" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['Reg_AddressFill']==1) {?>checked="checked"<?php }?>/><?php echo $_smarty_tpl->getConfigVariable('Settings_InputFill');?>
</label></td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_agbInf2');?>
</td>
          <td colspan="3" class="row_right">
            <label><input type="radio" name="Reg_AgbPflicht" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['Reg_AgbPflicht']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
            <label><input type="radio" name="Reg_AgbPflicht" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['Reg_AgbPflicht']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
          </td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_f_company');?>
</td>
          <td colspan="3" class="row_right">
            <label><input type="radio" name="Reg_Firma" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['Reg_Firma']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
            <label><input type="radio" name="Reg_Firma" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['Reg_Firma']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
          </td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_f_ustid');?>
</td>
          <td colspan="3" class="row_right">
            <label><input type="radio" name="Reg_Ust" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['Reg_Ust']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
            <label><input type="radio" name="Reg_Ust" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['Reg_Ust']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
          </td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_bank');?>
</td>
          <td colspan="3" class="row_right">
            <label><input type="radio" name="Reg_Bank" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['Reg_Bank']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
            <label><input type="radio" name="Reg_Bank" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['Reg_Bank']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
          </td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_f_fon');?>
</td>
          <td colspan="3" class="row_right">
            <label><input type="radio" name="Reg_Fon" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['Reg_Fon']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
            <label><input type="radio" name="Reg_Fon" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['Reg_Fon']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
          </td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_f_fax');?>
</td>
          <td colspan="3" class="row_right">
            <label><input type="radio" name="Reg_Fax" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['Reg_Fax']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
            <label><input type="radio" name="Reg_Fax" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['Reg_Fax']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
          </td>
        </tr>
        <tr>
          <td width="350" class="row_left"><?php echo $_smarty_tpl->getConfigVariable('Settings_f_birth');?>
</td>
          <td colspan="3" class="row_right">
            <label><input type="radio" name="Reg_Birth" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['Reg_Birth']==1) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('Yes');?>
</label>
            <label><input type="radio" name="Reg_Birth" value="0" <?php if ($_smarty_tpl->tpl_vars['row']->value['Reg_Birth']==0) {?>checked="checked"<?php }?> /><?php echo $_smarty_tpl->getConfigVariable('No');?>
</label>
          </td>
        </tr>
      </table>
    </div>
  </div>
  <input name="save" type="hidden" id="save" value="1" />
  <input type="submit" value="<?php echo $_smarty_tpl->getConfigVariable('Save');?>
" class="button" />
</form>
<script type="text/javascript">
<!-- //
 set_method();
//-->
</script>
<?php }} ?>
