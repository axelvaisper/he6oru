<?php /* Smarty version Smarty-3.1.18, created on 2017-02-25 11:47:59
         compiled from "/home/vampireos/www/he6oru.localhost/admin/theme/standard/other/sysinfo.tpl" */ ?>
<?php /*%%SmartyHeaderCode:53165176458b144bf98f0f7-20394758%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2bc292708563543db76a05ab265750c62da1c4c7' => 
    array (
      0 => '/home/vampireos/www/he6oru.localhost/admin/theme/standard/other/sysinfo.tpl',
      1 => 1406215921,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '53165176458b144bf98f0f7-20394758',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'basepath' => 0,
    'imgpath' => 0,
    'version' => 0,
    'dbsize' => 0,
    'mysqlversion' => 0,
    'gdinfo' => 0,
    'lang' => 0,
    'phpversion' => 0,
    'maxupload' => 0,
    'maxmemory' => 0,
    'safemode' => 0,
    'magicquotes' => 0,
    'runtime' => 0,
    'sybase' => 0,
    'maxtime' => 0,
    'disabled' => 0,
    'apache' => 0,
    'am' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_58b144bf9ad1d8_91423355',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58b144bf9ad1d8_91423355')) {function content_58b144bf9ad1d8_91423355($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_replace')) include '/home/vampireos/www/he6oru.localhost/lib/smarty/plugins/modifier.replace.php';
?><?php if (perm('settings')) {?>
<script type="text/javascript">
<!-- //
$(document).ready(function() {
    toggleCookie('sx_navi', 'sx_open', 30, '<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
');
});
//-->
</script>

<div class="header">
  <div id="sx_navi" class="navi_toggle"><img class="absmiddle" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/toggle.png" alt="" /></div>
  <img class="absmiddle" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/sysinfo.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Start_SysInfos');?>

</div>
<div  id="sx_open" class="sysinfos" style="font-weight: normal">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><strong><?php echo $_smarty_tpl->getConfigVariable('Start_Version');?>
: </strong></td>
      <td align="right"><?php echo $_smarty_tpl->tpl_vars['version']->value;?>
</td>
    </tr>
    <tr>
      <td width="180"><strong><?php echo $_smarty_tpl->getConfigVariable('Start_dbsize');?>
: </strong></td>
      <td align="right"><?php echo $_smarty_tpl->tpl_vars['dbsize']->value;?>
</td>
    </tr>
    <tr>
      <td width="180"><strong><?php echo $_smarty_tpl->getConfigVariable('Start_mysqlV');?>
: </strong></td>
      <td align="right"><?php echo $_smarty_tpl->tpl_vars['mysqlversion']->value;?>
</td>
    </tr>
    <tr>
      <td width="180"><strong><?php echo $_smarty_tpl->getConfigVariable('Start_GD');?>
: </strong></td>
      <td align="right"><?php echo $_smarty_tpl->tpl_vars['gdinfo']->value;?>
</td>
    </tr>
    <tr>
      <td width="180"><a class="colorbox stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['PhpInfo']);?>
" href="index.php?do=main&sub=phpinfo&amp;noframes=1"><strong><?php echo $_smarty_tpl->getConfigVariable('Start_phpV');?>
: </strong></a></td>
      <td align="right"><?php echo $_smarty_tpl->tpl_vars['phpversion']->value;?>
</td>
    </tr>
    <tr>
      <td width="180"><strong><?php echo $_smarty_tpl->getConfigVariable('Start_maxUpload');?>
: </strong></td>
      <td align="right"><?php echo $_smarty_tpl->tpl_vars['maxupload']->value;?>
</td>
    </tr>
    <tr>
      <td width="180"><strong><?php echo $_smarty_tpl->getConfigVariable('Start_maxRam');?>
: </strong></td>
      <td align="right"><?php echo $_smarty_tpl->tpl_vars['maxmemory']->value;?>
</td>
    </tr>
    <tr>
      <td width="180"><strong>SAFE_MODE: </strong></td>
      <td align="right"><?php echo $_smarty_tpl->tpl_vars['safemode']->value;?>
</td>
    </tr>
    <tr>
      <td width="180"><strong>MAGIC_QUOTES_GPC: </strong></td>
      <td align="right"><?php echo $_smarty_tpl->tpl_vars['magicquotes']->value;?>
</td>
    </tr>
    <tr>
      <td width="180"><strong>MAGIC_QUOTES_RUNTIME: </strong></td>
      <td align="right"><?php echo $_smarty_tpl->tpl_vars['runtime']->value;?>
</td>
    </tr>
    <tr>
      <td width="180"><strong>MAGIC_QUOTES_SYBASE: </strong></td>
      <td align="right"><?php echo $_smarty_tpl->tpl_vars['sybase']->value;?>
</td>
    </tr>
    <tr>
      <td width="180"><strong><?php echo $_smarty_tpl->getConfigVariable('Start_exeT');?>
: </strong></td>
      <td align="right"><?php echo $_smarty_tpl->tpl_vars['maxtime']->value;?>
</td>
    </tr>
  </table>
  <strong><?php echo $_smarty_tpl->getConfigVariable('Start_deacFuncs');?>
: </strong><br />
  <em><?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['disabled']->value,',',', ');?>
</em>
  <br />
  <br />
  <?php if (perm('settings')&&$_smarty_tpl->tpl_vars['apache']->value) {?>
    <br />
    <strong><?php echo $_smarty_tpl->getConfigVariable('Start_apacheMods');?>
</strong>
    <br />
    <?php  $_smarty_tpl->tpl_vars['am'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['am']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['apache']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['am']->key => $_smarty_tpl->tpl_vars['am']->value) {
$_smarty_tpl->tpl_vars['am']->_loop = true;
?>
      <?php echo $_smarty_tpl->tpl_vars['am']->value;?>
,
    <?php } ?>
  <?php }?>
</div>
<?php }?>
<?php }} ?>
