<?php /* Smarty version Smarty-3.1.18, created on 2017-02-25 11:47:59
         compiled from "/home/vampireos/www/he6oru.localhost/admin/theme/standard/navielements/settings.tpl" */ ?>
<?php /*%%SmartyHeaderCode:88863363958b144bfcd4632-10372838%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '525b55ca8b74b6fe68357b3cd71ec0e5ca7bd4a7' => 
    array (
      0 => '/home/vampireos/www/he6oru.localhost/admin/theme/standard/navielements/settings.tpl',
      1 => 1407107207,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '88863363958b144bfcd4632-10372838',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'imgpath' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_58b144bfd18309_30982129',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58b144bfd18309_30982129')) {function content_58b144bfd18309_30982129($_smarty_tpl) {?><?php if (perm('settings')) {?>
  <a class="menuitem submenuheader" href="#"><img class="absmiddle" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/settings.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Global_Settings');?>
</a>
  <div class="submenu">
    <ul>
      <li><a class="<?php if (isset($_REQUEST['sub'])&&$_REQUEST['sub']=='admin_global') {?>nav_subs_active<?php } else { ?>nav_subs<?php }?>" href="index.php?do=settings&amp;sub=admin_global"><?php echo $_smarty_tpl->getConfigVariable('Admin_Global');?>
</a></li>
      <li><a class="<?php if (isset($_REQUEST['sub'])&&$_REQUEST['sub']=='global') {?>nav_subs_active<?php } else { ?>nav_subs<?php }?>" href="index.php?do=settings&amp;sub=global"><?php echo $_smarty_tpl->getConfigVariable('Settings_general');?>
</a></li>
      <li><a class="<?php if (isset($_REQUEST['sub'])&&$_REQUEST['sub']=='money') {?>nav_subs_active<?php } else { ?>nav_subs<?php }?>" href="index.php?do=settings&amp;sub=money"><?php echo $_smarty_tpl->getConfigVariable('MoneySite');?>
</a></li>
      <li><a class="<?php if (isset($_REQUEST['sub'])&&$_REQUEST['sub']=='sectionsettings') {?>nav_subs_active<?php } else { ?>nav_subs<?php }?>" href="index.php?do=settings&amp;sub=sectionsettings"><?php echo $_smarty_tpl->getConfigVariable('Global_SettingsSections');?>
</a></li>
      <li><a class="<?php if (isset($_REQUEST['sub'])&&$_REQUEST['sub']=='widgets') {?>nav_subs_active<?php } else { ?>nav_subs<?php }?>" href="index.php?do=settings&amp;sub=widgets"><?php echo $_smarty_tpl->getConfigVariable('Global_Widgets');?>
</a></li>
      <li><a class="<?php if (isset($_REQUEST['sub'])&&$_REQUEST['sub']=='languages') {?>nav_subs_active<?php } else { ?>nav_subs<?php }?>" href="index.php?do=settings&amp;sub=languages"><?php echo $_smarty_tpl->getConfigVariable('Settings_languages');?>
</a></li>
      <li><a class="<?php if (isset($_REQUEST['sub'])&&$_REQUEST['sub']=='adminlanguages') {?>nav_subs_active<?php } else { ?>nav_subs<?php }?>" href="index.php?do=settings&amp;sub=adminlanguages"><?php echo $_smarty_tpl->getConfigVariable('Settings_languages_a');?>
</a></li>
        <?php if (perm('lang_edit')) {?>
        <li><a class="<?php if (isset($_REQUEST['sub'])&&$_REQUEST['sub']=='lang_edit') {?>nav_subs_active<?php } else { ?>nav_subs<?php }?>" href="index.php?do=settings&amp;sub=lang_edit"><?php echo $_smarty_tpl->getConfigVariable('SettingsLangEdit');?>
</a></li>
        <?php }?>
      <li><a class="<?php if (isset($_REQUEST['sub'])&&$_REQUEST['sub']=='sectionsdisplay') {?>nav_subs_active<?php } else { ?>nav_subs<?php }?>" href="index.php?do=settings&amp;sub=sectionsdisplay"><?php echo $_smarty_tpl->getConfigVariable('Sections');?>
</a></li>
        <?php if ($_SESSION['benutzer_id']==1) {?>
        <li><a class="<?php if (isset($_REQUEST['sub'])&&$_REQUEST['sub']=='phpedit') {?>nav_subs_active<?php } else { ?>nav_subs<?php }?>" href="index.php?do=settings&amp;sub=phpedit"><?php echo $_smarty_tpl->getConfigVariable('ConfPhp');?>
</a></li>
        <li><a class="<?php if (isset($_REQUEST['sub'])&&$_REQUEST['sub']=='htaccess') {?>nav_subs_active<?php } else { ?>nav_subs<?php }?>" href="index.php?do=settings&amp;sub=htaccess"><?php echo $_smarty_tpl->getConfigVariable('HtaccessSettings');?>
</a></li>
        <?php }?>
      <li><a class="<?php if (isset($_REQUEST['sub'])&&$_REQUEST['sub']=='secure') {?>nav_subs_active<?php } else { ?>nav_subs<?php }?>" href="index.php?do=settings&amp;sub=secure"><?php echo $_smarty_tpl->getConfigVariable('SecureSettings');?>
</a></li>
      <li><a class="<?php if (isset($_REQUEST['sub'])&&$_REQUEST['sub']=='antivirus') {?>nav_subs_active<?php } else { ?>nav_subs<?php }?>" href="index.php?do=settings&amp;sub=antivirus"><?php echo $_smarty_tpl->getConfigVariable('AntivirusModule');?>
</a></li>
      <li><a class="<?php if (isset($_REQUEST['sub'])&&$_REQUEST['sub']=='cron') {?>nav_subs_active<?php } else { ?>nav_subs<?php }?>" href="index.php?do=settings&amp;sub=cron"><?php echo $_smarty_tpl->getConfigVariable('Scheduler');?>
</a></li>
      <li><a class="<?php if (isset($_REQUEST['do'])&&$_REQUEST['do']=='expimp') {?>nav_subs_active<?php } else { ?>nav_subs<?php }?>" href="index.php?do=expimp"><?php echo $_smarty_tpl->getConfigVariable('Admin_ExpImp');?>
</a></li>
      <li><a class="<?php if (isset($_REQUEST['sub'])&&$_REQUEST['sub']=='logs') {?>nav_subs_active<?php } else { ?>nav_subs<?php }?>" href="?do=settings&amp;sub=logs"><?php echo $_smarty_tpl->getConfigVariable('Admin_Logs');?>
</a></li>
    </ul>
  </div>
<?php }?>
<?php }} ?>
