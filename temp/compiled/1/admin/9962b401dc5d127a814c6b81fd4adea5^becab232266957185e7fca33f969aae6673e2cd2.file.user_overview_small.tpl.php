<?php /* Smarty version Smarty-3.1.18, created on 2017-02-25 11:47:59
         compiled from "/home/vampireos/www/he6oru.localhost/admin/theme/standard/user/user_overview_small.tpl" */ ?>
<?php /*%%SmartyHeaderCode:195586276758b144bfbf7307-27447713%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'becab232266957185e7fca33f969aae6673e2cd2' => 
    array (
      0 => '/home/vampireos/www/he6oru.localhost/admin/theme/standard/user/user_overview_small.tpl',
      1 => 1406215966,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '195586276758b144bfbf7307-27447713',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'basepath' => 0,
    'imgpath' => 0,
    'all_user' => 0,
    'user' => 0,
    'lang' => 0,
    'u' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_58b144bfc0db11_32348971',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58b144bfc0db11_32348971')) {function content_58b144bfc0db11_32348971($_smarty_tpl) {?><?php if (!is_callable('smarty_function_cycle')) include '/home/vampireos/www/he6oru.localhost/lib/smarty/plugins/function.cycle.php';
if (!is_callable('smarty_modifier_truncate')) include '/home/vampireos/www/he6oru.localhost/lib/smarty/plugins/modifier.truncate.php';
if (!is_callable('smarty_modifier_date_format')) include '/home/vampireos/www/he6oru.localhost/lib/smarty/plugins/modifier.date_format.php';
?><?php if (perm('users_overview')) {?>
<script type="text/javascript">
<!-- //
$(document).ready(function() {
    toggleCookie('user_navi', 'user_open', 30, '<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
');
});
//-->
</script>

<div class="header">
  <div id="user_navi" class="navi_toggle"><img class="absmiddle" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/toggle.png" alt="" /></div>
  <img class="absmiddle" src="<?php echo $_smarty_tpl->tpl_vars['imgpath']->value;?>
/user.png" alt="" /> <?php echo $_smarty_tpl->getConfigVariable('Start_NUser');?>
 - <a href="?do=user&amp;sub=showusers"><?php echo $_smarty_tpl->getConfigVariable('Global_ShowAll');?>
 (<?php echo $_smarty_tpl->tpl_vars['all_user']->value;?>
)</a>
</div>
<div id="user_open" class="sysinfos">
  <table width="100%" border="0" cellspacing="0" cellpadding="2">
    <?php  $_smarty_tpl->tpl_vars['u'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['u']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['user']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['u']->key => $_smarty_tpl->tpl_vars['u']->value) {
$_smarty_tpl->tpl_vars['u']->_loop = true;
?>
      <tr class="<?php echo smarty_function_cycle(array('values'=>'second,first'),$_smarty_tpl);?>
">
        <td>
          <a class="colorbox stip" title="<?php echo sanitize($_smarty_tpl->tpl_vars['lang']->value['User_edit']);?>
" href="index.php?do=user&amp;sub=edituser&amp;user=<?php echo $_smarty_tpl->tpl_vars['u']->value->Id;?>
&amp;noframes=1"><strong><?php if (empty($_smarty_tpl->tpl_vars['u']->value->Vorname)) {?><em><?php echo $_smarty_tpl->getConfigVariable('Edit');?>
</em><?php } else { ?><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['u']->value->Vorname,'15','...');?>
<?php }?> <?php echo sanitize($_smarty_tpl->tpl_vars['u']->value->Nachname);?>
</strong></a>
          <input type="hidden" name="deldetails[<?php echo $_smarty_tpl->tpl_vars['u']->value->Id;?>
]" value="<?php echo sanitize($_smarty_tpl->tpl_vars['u']->value->Benutzername);?>
 (<?php echo $_smarty_tpl->tpl_vars['u']->value->Email;?>
)" /></td>
        <td><?php echo sanitize($_smarty_tpl->tpl_vars['u']->value->Benutzername);?>
</td>
        <td align="right"> <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['u']->value->Regdatum,"%d.%m.%Y");?>
 </td>
      </tr>
    <?php } ?>
  </table>
</div>
<?php }?><?php }} ?>
