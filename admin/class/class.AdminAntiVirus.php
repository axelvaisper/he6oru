<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (!defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}

class AdminAntiVirus extends Magic {

    protected $_dir      = STATUS_DIR;
    protected $_size     = 500;     // ������������ ������ ����� � ���������� ����������� �����������
    protected $_count    = 20;      // ���������� ����������� ������ ������ ������
    protected $_limit    = 100;     // ���������� ����� ��� ������ ������������ ��������� ����������
    protected $base      = array(); // ������ � ���������� ����
    protected $temp      = array(); // ������ � ��������� ����������� � �����
    protected $error     = array(); // ������ �� ������� ���������� ������ (������ ���������)
    protected $segments  = array(); // ������ � ������� ������� ������������
    protected $analized  = array(); // ������ �� ������� ���� ������ � ������ �����������
    protected $infected  = array(); // ������ �� ������� ���������� ������ (���������)
    protected $serialize = array('Value', 'Scan', 'Infected', 'SnapError', 'VirusBase', 'Folders', 'ExtScan', 'ExtVir', 'ExtAll', 'Ignore');

    /* ����� ��������� ������������� ������ */
    public function __construct() {
        ignore_user_abort(true);
        set_time_limit(600);
        clearstatcache();
        $this->base = $this->loadBase();
    }

    /* ����� ������� �������� �� ���������� */
    public function cron($url) {
        $files = $this->loadFiles();
        $this->analizeArray($this->base['Value'], $files);
        if (!empty($this->error)) {
            $status = 'error';
            $this->mail($url);
        } else {
            $status = 'ok';
        }
        SX::setLog('��������� �������������� �������� ������ �� ���������. ������ ��������: ' . $status, '0', $_SESSION['benutzer_id']);
        $this->saveBase(array('SnapError' => $this->error, 'DatumCheck' => time(), 'StatusCheck' => $status));
    }

    /* ����� ������ */
    public function get() {
        switch (Arr::getRequest('snap')) {
            default:
            case 'settings':
                $this->settings();
                break;

            case 'new':
                $this->newSnap();
                break;

            case 'check':
                $this->checkSnap();
                break;

            case 'error':
                $this->checkError();
                break;

            case 'base':
                $this->loadSignatures();
                break;

            case 'scan':
                $this->virusScan();
                break;

            case 'save':
                $this->ignoreSave();
                break;

            case 'ignore':
                $this->showVirus();
                break;
        }
    }

    /* ����� ������ �������� �������� � ���������� �������� */
    protected function settings() {
        if (Arr::getPost('save') == 1) {
            $array = array(
                'ExtScan' => Arr::getPost('ExtScan'),
                'ExtVir'  => Arr::getPost('ExtVir'),
                'Folders' => Arr::getPost('Folders'));
            $this->saveBase($array);
            $this->base = $this->loadBase();
            $this->__object('AdminCore')->script('save', 5000);
        }
        $folders = array();
        $this->loadFolders($this->_dir, $folders);
        sort($folders);
        $tpl_array = array(
            'folders' => $folders,
            'base'    => $this->base,
            'title'   => $this->_lang['Global_Settings'] . ' - ' . $this->_lang['AntivirusModule']);
        $this->_view->assign($tpl_array);
        $this->_view->content('/settings/av_settings.tpl');
    }

    /* ����� ������� ��������� ������ ������ */
    protected function newSnap() {
        $files = $this->loadFiles();
        $this->saveBase(array('Value' => $files, 'Datum' => time()));
        $this->__object('AdminCore')->script('save', 5000);
        $tpl_array = array(
            'files' => $files,
            'snap'  => 'new',
            'title' => $this->_lang['AntivirusNewSnap']);
        $this->_view->assign($tpl_array);
        $this->_view->content('/settings/av_files.tpl');
    }

    /* ����� ��������� ��������� ������ � �������� */
    protected function checkSnap() {
        if (!empty($this->base['Value'])) {
            $files = $this->loadFiles();
            $this->analizeArray($this->base['Value'], $files);
            if (!empty($this->error)) {
                $this->__object('AdminCore')->script('message', 5000, $this->_lang['AntivirusSnapError'] . ' ' . $this->_lang['AntivirusFilesError']);
                $status = 'error';
            } else {
                $this->__object('AdminCore')->script('message', 5000, $this->_lang['AntivirusSnapOk']);
                $status = 'ok';
            }
            SX::setLog('��������� �������� ������ �� ���������. ������ ��������: ' . $status, '0', $_SESSION['benutzer_id']);
            $this->saveBase(array('SnapError' => $this->error, 'DatumCheck' => time(), 'StatusCheck' => $status));
            $tpl_array = array(
                'files' => $this->analized,
                'snap'  => 'check',
                'title' => $this->_lang['AntivirusSnapCheck']);
            $this->_view->assign($tpl_array);
            $this->_view->content('/settings/av_files.tpl');
        } else {
            $this->settings();
            $this->__object('AdminCore')->script('message', 5000, $this->_lang['AntivirusErrorSnap']);
        }
    }

    /* ����� ������ ������ ���������� ������ */
    protected function checkError() {
        $tpl_array = array(
            'files' => $this->base['SnapError'],
            'snap'  => 'error',
            'title' => $this->_lang['AntivirusFilesError']);
        $this->_view->assign($tpl_array);
        $this->_view->content('/settings/av_files.tpl');
    }

    /* ����� ��������� ����������� �� ������������ ������ */
    protected function excludedInfected() {
        $result = $this->base['Infected'];
        if (!empty($this->base['Ignore']) && !empty($this->base['Infected'])) {
            foreach (array_keys($this->base['Ignore']) as $hash) {
                if (isset($result[$hash])) {
                    $result[$hash]['ignore'] = 1;
                }
            }
        }
        return $result;
    }

    /* ����� ������ ��������� �������� ���������� */
    protected function showVirus() {
        $array = $this->excludedInfected();
        $sort = Arr::getRequest('sort', 0);
        if ($sort == 1) {
            sort($array);
        }
        if (Arr::getRequest('seen') != 'all') {
            $num = count($array);
            $seiten = ceil($num / $this->_limit);
            $a = Tool::getLimit($this->_limit, $seiten);
            $array = array_slice($array, $a, $this->_limit);
            if ($num > $this->_limit) {
                $this->_view->assign('Navi', $this->__object('AdminCore')->pagination($seiten, '<a class="page_navigation" href="index.php?do=settings&amp;sub=antivirus&amp;snap=save&amp;sort=' . $sort . '&amp;page={s}">{t}</a>'));
            }
        }
        $tpl_array = array(
            'sort'     => $sort,
            'revers'   => ($sort == 1 ? 0 : 1),
            'infected' => $array,
            'snap'     => 'ignore',
            'title'    => $this->_lang['AntivirusScanBase']);
        $this->_view->assign($tpl_array);
        $this->_view->content('/settings/av_files.tpl');
    }

    /* ����� �������� ������� ���������� ��� ���������� */
    protected function ignoreArray($array) {
        $ignore = array();
        if (!empty($array)) {
            $time = time();
            $ignore = $this->base['Ignore'];
            foreach ($array as $key => $value) {
                $key = trim($key);
                if (!empty($key)) {
                    if ($value == 1) {
                        $ignore[$key] = $time;
                    } elseif ($value == 0) {
                        unset($ignore[$key]);
                    }
                }
            }
        }
        return $ignore;
    }

    /* ����� �������� ������� ���� ���������� ��� ���������� */
    protected function ignoreAllArray($array) {
        $ignore = array();
        if (!empty($array)) {
            $time = time();
            foreach (array_keys($array) as $key) {
                $key = trim($key);
                if (!empty($key)) {
                    $ignore[$key] = $time;
                }
            }
        }
        return $ignore;
    }

    /* ����� ���������� ������� ���������� ������������ ���������� */
    protected function ignoreSave() {
        if (Arr::getRequest('ignore_send') == 1) {
            if (Arr::getRequest('save') == 'all') {
                $ignore = $this->ignoreAllArray($this->base['Infected']);
            } else {
                $ignore = $this->ignoreArray($_POST['hash']);
            }
            $this->saveBase(array('Ignore' => $ignore));
            $this->base = $this->loadBase();
            $this->__object('AdminCore')->script('save');
        }
        $this->showVirus();
    }

    /* ����� ������������ �������� ������ �� ����������� ��� */
    protected function virusScan() {
        if (!empty($this->base['VirusBase'])) {
            if (Arr::getPost('scan_send') == 1) {
                $gesamt = Arr::getPost('gesamt', 0);
                $count = Arr::getPost('count', 0) + $this->_count;
                $count = $count > $gesamt ? $gesamt : $count;
            } else {
                $array = $this->loadFiles();
                $this->saveScan($array, $this->_count);
                $gesamt = count($array);
                $count = 0;
            }
            $nexts = $this->scanArray() ? 'setTimeout(\'nexts();\', 3000);' : 'parent.location.href = \'index.php?do=settings&sub=antivirus&snap=ignore\';';
            $bar = $count / ($gesamt / 100);
            $tpl_array = array(
                'segments' => $this->segments,
                'bar'      => round($bar),
                'count'    => $count,
                'gesamt'   => $gesamt,
                'nexts'    => $nexts,
                'snap'     => 'scan',
                'title'    => $this->_lang['AntivirusScanBase']);
            $this->_view->assign($tpl_array);
            $this->_view->content('/settings/av_files.tpl');
        } else {
            $this->settings();
            $this->__object('AdminCore')->script('message', 5000, $this->_lang['AntivirusErrorScan']);
        }
    }

    /* ����� �������� ���� �������� */
    protected function loadSignatures() {
        $xml = simplexml_load_file('http://www.status-x.ru/supports/signatures/');
        if ($xml !== false) {
            if (!empty($xml->version) && $this->versionBase($xml->version)) {
                $base = $this->setArray($xml->signature);
                $message = $this->virusBase($xml->version, $base) ? 'GlobalUpdateOk' : 'GlobalUpdateFail';
            } else {
                $message = 'GlobalUpdateNot';
            }
        } else {
            $message = 'GlobalUpdateFail';
        }
        $this->settings();
        $this->__object('AdminCore')->script('message', 5000, $this->_lang[$message]);
    }

    /* ����� �������� ������� */
    protected function setArray($input) {
        $array = array();
        foreach ($input as $value) {
            $array[] = base64_decode(trim((string) $value));
        }
        return $array;
    }

    /* ����� ��������� ������ ���� �������� */
    protected function versionBase($version) {
        return $version > $this->base['VersionBase'] || empty($this->base['VirusBase']) ? true : false;
    }

    /* ����� ���������� ���� �������� */
    protected function virusBase($version, $base) {
        if (serialize($base) !== false) {
            $this->saveBase(array('VersionBase' => $version, 'VirusBase' => $base));
            $this->base = $this->loadBase();
            return true;
        }
        return false;
    }

    /* ����� ��������� ���� �����, ������� ��������� */
    public function loadFolders($dir, &$array) {
        $files = glob($dir . '/*', GLOB_ONLYDIR);
        foreach ($files as $file) {
            $this->loadFolders($file, $array);
            $array[] = $this->nameFolder($file);
        }
    }

    /* ����� ���������� � ����, ��������� ������������� ������ */
    public function saveBase($value = array()) {
        $set = array();
        foreach ($value as $key => $val) {
            if (in_array($key, $this->serialize)) {
                $val = serialize($val);
            }
            $set[] = '`' . $key . '` = \'' . $this->_db->escape($val) . '\'';
        }
        if (!empty($set)) {
            $this->_db->query("UPDATE `" . PREFIX . "_antivirus` SET " . implode(',', $set));
        }
    }

    /* ����� ��������� �� ���� ������ */
    public function loadBase() {
        $array = $this->_db->fetch_assoc("SELECT * FROM `" . PREFIX . "_antivirus`");
        if (is_array($array)) {
            foreach ($array as $key => $val) {
                if (in_array($key, $this->serialize)) {
                    $array[$key] = unserialize($val);
                }
            }
            return $array;
        }
        return array();
    }

    /* ����� ������������� ��������� ���� ������ ����� */
    public function loadFiles() {
        $array = array();
        $this->files($this->_dir, $array);
        return $array;
    }

    /* ����� �������� ���������� ��������� �����, �� ����� ���������� ������� � ������� */
    protected function files($dir, &$array) {
        $handle = opendir($dir);
        while (false !== ($file = readdir($handle))) {
            if ($file != '.' && $file != '..') {
                $file = $dir . '/' . $file;
                $name = $this->nameFolder($file);
                if (!in_array($name, $this->base['Folders'])) {
                    if (is_dir($file)) {
                        $this->files($file, $array);
                    } else {
                        $ext = pathinfo($file, PATHINFO_EXTENSION);
                        if (!in_array($ext, $this->base['ExtScan'])) {
                            $array[$name] = $this->fileInfo($file);
                        }
                    }
                }
            }
        }
        closedir($handle);
    }

    /* ����� ���������� �������� ���� � ������ */
    protected function nameFolder($file) {
        return str_replace($this->_dir, '', $file);
    }

    /* ����� ������������� ������ ���������� ��� �������� */
    protected function fileInfo($file) {
        $array = array(
            'file_md5'  => md5_file($file),
            'file_size' => filesize($file),
            'file_time' => filemtime($file),
            'file_perm' => $this->filePerms($file));
        return $array;
    }

    /* ����� �������� ������� ����� ����� ���� 777 */
    protected function filePerms($file) {
        return substr(decoct(fileperms($file)), -3);
    }

    /* ����� ������������� ������� ��������� � ����������� ������ */
    public function analizeArray($base, $file) {
        if (!empty($base)) {
            foreach ($base as $key => $arr) {
                if (isset($file[$key])) {
                    $this->analized[$key] = $this->compare($arr, $file[$key], 'old');
                    unset($file[$key]);
                } else {
                    $this->analized[$key] = $this->compare($arr, array(), 'del');
                }
                $this->errorArray($this->analized[$key], $key);
            }
        }
        foreach ($file as $key => $arr) {
            $this->analized[$key] = $this->compare($arr, array(), 'new');
            $this->errorArray($this->analized[$key], $key);
        }
    }

    /* ����� ��������� ������ ������� �� �������� error */
    protected function errorArray($array, $key) {
        if (array_search('error', $array, true) !== false) {
            $this->error[$key] = $array;
        }
    }

    /* ����� �������� �������� ��������� � ������������ */
    protected function compare($arr, $file, $type) {
        $arr['file_type'] = $type;
        $arr['status_md5'] = $this->check($arr, $file, 'file_md5');
        $arr['status_size'] = $this->check($arr, $file, 'file_size');
        $arr['status_time'] = $this->check($arr, $file, 'file_time');
        $arr['status_perm'] = $this->check($arr, $file, 'file_perm');
        return $arr;
    }

    /* ����� ������ � ��������� */
    protected function check($arr, $file, $type) {
        return isset($arr[$type], $file[$type]) && $file[$type] == $arr[$type] ? 'ok' : 'error';
    }

    /* ����� ���������� � ���� ����������������� ������� */
    protected function saveScan($array, $count) {
        $array = array_chunk($array, $count, true);
        $this->saveBase(array('Scan' => $array, 'Infected' => array()));
        $this->base = $this->loadBase();
    }

    /* ����� ������������ ������ ����� ������� */
    protected function scanArray() {
        $this->infected = $this->base['Infected'];
        if (!empty($this->base['Scan'])) {
            $this->segments = array_shift($this->base['Scan']);
            $this->scanFiles();
            $this->saveBase(array('Scan' => $this->base['Scan'], 'Infected' => $this->infected));
            return true;
        }
        return false;
    }

    /* ����� ������������ ������� ������ */
    protected function scanFiles() {
        foreach (array_keys($this->segments) as $file) {
            $this->scanFile($file);
        }
    }

    /* ����� ������������ ����a */
    protected function scanFile($file) {
        if (filesize($this->_dir . $file) <= $this->_size * 1024) {
            $data = $this->loadFile($file);
            if ($data !== false) {
                foreach ($this->base['VirusBase'] as $virus) {
                    $this->searchFile($data, $virus, $file);
                }
            }
        }
    }

    /* ����� ������ ���� ��������� ���������� */
    protected function searchFile($data, $virus, $file, $pos = 0) {
        $search = stripos($data, $virus, $pos);
        if ($search !== false) {
            $this->controlFile($file, $search, $virus);
            $this->searchFile($data, $virus, $file, $search + 1);
        }
    }

    /* ����� ���������� ������� ��������� */
    protected function controlFile($file, $search, $virus) {
        $key = $posi = '';
        if (isset($this->temp[$file])) {
            $count = 0;
            foreach ($this->temp[$file] as $key => $val) {
                $cache = $count;
                $count += $val;
                if ($count > $search) {
                    break;
                }
            }
            $posi = $search - $cache;
        }
        $this->saveInfected($file, $virus, $key, $posi);
    }

    /* ����� ���������� ���������� � ���������� ��������� */
    protected function saveInfected($file, $virus, $key, $posi) {
        $hash = md5($file . '_' . $key . '_' . $posi . '_' . $virus);
        $this->infected[$hash] = array(
            'file'     => $file,
            'virus'    => $virus,
            'line'     => $key,
            'position' => $posi);
    }

    /* ����� ��������� ����������� ����� */
    protected function loadFile($file) {
        $data = file($this->_dir . $file);
        if ($data !== false) {
            $data = array_map('rtrim', $data);
            $this->countFile($file, $data);
            $data = implode(PE, $data);
        }
        return $data;
    }

    /* ����� �������� ����� � �������� */
    protected function countFile($file, $data) {
        $i = 1;
        foreach ($data as $val) {
            $this->temp[$file][$i++] = strlen($val . PE);
        }
    }

    /* ����� ���������� �������������� �� ���������� ������ �������� �� ����� ������������������ �������� */
    protected function mail($url) {
        $array = array();
        foreach (array_keys($this->error) as $key) {
            $array[] = $key;
        }
        $name = SX::get('system.Mail_Name');
        $mail = SX::get('system.Mail_Absender');
        $mail_array = array(
            '__URL__'  => $url,
            '__USER__' => $name,
            '__DATE__' => date('Y-m-d H:i:s'),
            '__FILE__' => implode(PE, $array));
        $message = Tool::replace($this->_lang['AntivirusMail'], $mail_array);
        $subject = Tool::replace($this->_lang['AntivirusMailSubj'], array('__URL__' => $url));
        SX::object('Mail')->send(1, $mail, $name, $message, $subject, $mail, $name, 'text', '', '', 1);
    }

}
