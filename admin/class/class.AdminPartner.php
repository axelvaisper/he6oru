<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (!defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}

class AdminPartner extends Magic {

    public function show() {
        if (Arr::getPost('save') == 1 && isset($_POST['PartnerUrl'])) {
            foreach ($_POST['PartnerUrl'] as $pid => $em) {
                if (!empty($_POST['PartnerUrl'][$pid]) && !empty($_POST['PartnerName'][$pid])) {
                    $_POST['PartnerUrl'][$pid] = Tool::prefix($_POST['PartnerUrl'][$pid], 'http://');

                    $array = array(
                        'PartnerUrl'  => $_POST['PartnerUrl'][$pid],
                        'PartnerName' => $_POST['PartnerName'][$pid],
                        'Hits'        => $_POST['Hits'][$pid],
                        'Position'    => $_POST['Position'][$pid],
                        'Nofollow'    => $_POST['Nofollow'][$pid],
                        'Aktiv'       => $_POST['Aktiv'][$pid],
                    );
                    $this->_db->update_query('partner', $array, "Id='" . intval($pid) . "'");
                }
            }
            SX::setLog('������������ ' . $_SESSION['user_name'] . ' ������� �������� (' . $_POST['PartnerName'][$pid] . ')', '0', $_SESSION['benutzer_id']);
            $this->__object('AdminCore')->script('save');
        }

        if (Arr::getPost('new') == 1) {
            $insert_array = array(
                'PartnerUrl'  => Arr::getPost('PartnerUrl'),
                'Position'    => intval(Arr::getPost('Position')),
                'Nofollow'    => intval(Arr::getPost('Nofollow')),
                'Hits'        => 0,
                'PartnerName' => Arr::getPost('PartnerName'),
                'Bild'        => Arr::getPost('newImg_1'),
                'Sektion'     => AREA,
                'Aktiv'       => 1);
            $this->_db->insert_query('partner', $insert_array);
            SX::setLog('������������ ' . $_SESSION['user_name'] . ' ������ ������ �������� (' . $_POST['PartnerName'] . ')', '0', $_SESSION['benutzer_id']);
            $this->__object('AdminCore')->script('save');
        }
        $partners = array();
        $sql = $this->_db->query("SELECT * FROM " . PREFIX . "_partner WHERE Sektion='" . AREA . "' ORDER BY Position ASC");
        while ($row = $sql->fetch_object()) {
            $row->Bild = (is_file(UPLOADS_DIR . '/partner/' . $row->Bild)) ? 'uploads/partner/' . $row->Bild : '';
            $partners[] = $row;
        }
        $sql->close();
        $this->_view->assign('partners', $partners);
        $this->_view->assign('title', $this->_lang['Partners']);
        $this->_view->content('/partner/partner.tpl');
    }

    public function edit($id) {
        $id = intval($id);
        if (Arr::getPost('save') == 1) {
            $this->_db->query("UPDATE " . PREFIX . "_partner SET Bild='" . $this->_db->escape(Arr::getPost('newImg_1')) . "' WHERE Id='" . $id . "'");
            $row = $this->_db->cache_fetch_object("SELECT PartnerName FROM " . PREFIX . "_partner WHERE Id='" . $id . "' LIMIT 1");
            SX::setLog('������������ ' . $_SESSION['user_name'] . ' ������� ������� �������� (' . $row->PartnerName . ')', '0', $_SESSION['benutzer_id']);
            $this->__object('AdminCore')->script('save');
        }
        $row = $this->_db->cache_fetch_object("SELECT * FROM " . PREFIX . "_partner WHERE Id='" . $id . "' LIMIT 1");
        $row->Bild = (is_file(UPLOADS_DIR . '/partner/' . $row->Bild)) ? 'uploads/partner/' . $row->Bild : '';
        $this->_view->assign('res', $row);
        $this->_view->assign('title', $this->_lang['Partners_imgEdit']);
        $this->_view->content('/partner/img_edit.tpl');
    }

    public function delete($id) {
        $id = intval($id);
        $res = $this->_db->cache_fetch_object("SELECT Bild, PartnerName FROM " . PREFIX . "_partner WHERE Id='" . $id . "' LIMIT 1");
        File::delete(UPLOADS_DIR . '/partner/' . $res->Bild);
        $this->_db->query("DELETE FROM " . PREFIX . "_partner WHERE Id='" . $id . "'");
        SX::setLog('������������ ' . $_SESSION['user_name'] . ' ������ �������� (' . $res->PartnerName . ')', '0', $_SESSION['benutzer_id']);
        $this->__object('AdminCore')->backurl();
    }

}
