<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (!defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}

class AdminDataImport extends Magic {

    public function check($pf) {
        $res = $this->_db->fetch_object("SELECT COUNT(ugroup) AS count FROM " . Tool::cleanString($pf) . "_usergroup");
        $out = is_object($res) ? 'true' : 'false';
        SX::output($out, true);
    }

    protected function replace($string) {
        $text = array(array('yes', 'no'), array('1', '0'));
        $string = str_replace($text[0], $text[1], $string);
        return (empty($string)) ? '0' : $string;
    }

    public function get() {
        set_time_limit(600);
        $pf = (!empty($_REQUEST['pf'])) ? Tool::cleanAllow(Arr::getRequest('pf')) : '';
        $_REQUEST['action'] = $action = (isset($_REQUEST['action']) && !empty($pf)) ? Arr::getRequest('action') : '';

        switch ($action) {
            default:
                $this->_view->assign('noAction', '1');
                break;

            case 'users':
                $this->user($pf);
                break;

            case 'news':
                $this->news($pf);
                break;

            case 'gallery':
                $this->gallery($pf);
                break;

            case 'shop':
                $this->shop($pf);
                break;

            case 'forums':
                $this->forums($pf);
                break;

            case 'articles':
                $this->articles($pf);
                break;

            case 'cheats':
                $this->cheats($pf);
                break;

            case 'products':
                $this->products($pf);
                break;

            case 'downloads':
                $this->downloads($pf);
                break;

            case 'links':
                $this->links($pf);
                break;

            case 'calendar':
                $this->calendar($pf);
                break;

            case 'static':
                $this->page($pf);
                break;

            case 'navi':
                $this->navi($pf);
                break;

            case 'manuf':
                $this->manufacturer($pf);
                break;
        }
        $this->_view->content('/exportimport/dataimport.tpl');
    }

    protected function page($pf) {
        Tool::cleanTable('content_kategorien');
        $sql_categ = $this->_db->query("SELECT * FROM " . $pf . "_static_categ");
        $categ = '';
        while ($row_categ = $sql_categ->fetch_object()) {
            $insert_array = array(
                'Id'        => $row_categ->id,
                'Name'      => $row_categ->name,
                'Sektion'   => 1,
                'Tpl_Extra' => '');
            $this->_db->insert_query('content_kategorien', $insert_array);
            $categ++;
        }
        $sql_categ->close();

        $this->_view->assign('res_categ', $categ);
        Tool::cleanTable('content');
        $sql = $this->_db->query("SELECT * FROM " . $pf . "_static");
        $s = '';
        while ($row = $sql->fetch_object()) {
            $row->content = str_replace('{$sname}', '', $row->content);
            $insert_array = array(
                'Sektion'           => $row->area,
                'Datum'             => $row->ctime,
                'Kennwort'          => '',
                'Autor'             => $row->uid,
                'Aktiv'             => 1,
                'Kategorie'         => $row->categ,
                'Titel1'            => $row->title,
                'Titel2'            => $row->title,
                'Titel3'            => $row->title,
                'Topcontent_Bild_1' => '',
                'Topcontent_Bild_2' => '',
                'Topcontent_Bild_3' => '',
                'Inhalt1'           => $row->content,
                'Inhalt2'           => $row->content,
                'Inhalt3'           => $row->content,
                'Bild1'             => '',
                'Bild2'             => '',
                'Bild3'             => '',
                'Textbilder1'       => '',
                'BildAusrichtung'   => '',
                'Bewertung'         => 1,
                'Kommentare'        => 1,
                'Tags'              => '',
                'Galerien'          => '',
                'Topcontent'        => 0,
                'Suche'             => 1,
                'Gruppen'           => '');
            $this->_db->insert_query('content', $insert_array);
            $s++;
        }
        $sql->close();
        $this->_view->assign('res_static', $s);
    }

    protected function navi($pf) {
        Tool::cleanTable('navi');
        $sql = $this->_db->query("SELECT * FROM " . $pf . "_navi");
        $navi = '';
        while ($row = $sql->fetch_object()) {
            $insert_array = array(
                'Id'           => $row->id,
                'NaviCat'      => $row->navi_cat,
                'ParentId'     => $row->parent_id,
                'Titel_1'      => $row->title,
                'Titel_2'      => $row->title,
                'Titel_3'      => $row->title,
                'Dokument'     => $row->document,
                'DokumentRub'  => '',
                'Sektion'      => $row->area,
                'openonclick'  => $row->openonclick,
                'group_id'     => $row->group_id,
                'Position'     => $row->posi,
                'Ziel'         => $row->target,
                'Link_Titel_1' => '',
                'Link_Titel_2' => '',
                'Link_Titel_3' => '');
            $this->_db->insert_query('navi', $insert_array);
            $navi++;
        }
        $this->_view->assign('res_navi', $navi);
        Tool::cleanTable('navi_cat');
        $sql = $this->_db->query("SELECT * FROM " . $pf . "_navi_cat");
        $navi_cat = '';
        while ($row_cat = $sql->fetch_object()) {
            $insert_array = array(
                'Id'       => $row_cat->id,
                'Name_1'   => $row_cat->name,
                'Name_2'   => $row_cat->name,
                'Name_3'   => $row_cat->name,
                'Sektion'  => $row_cat->area,
                'Position' => $row_cat->position,
                'Aktiv'    => 1);
            $this->_db->insert_query('navi_cat', $insert_array);
            $navi_cat++;
        }
        $sql->close();
        $this->_view->assign('res_navi_cat', $navi_cat);
    }

    protected function calendar($pf) {
        Tool::cleanTable('kalender');
        $sql = $this->_db->query("SELECT * FROM " . $pf . "_calendar");
        $calendar = '';
        while ($row = $sql->fetch_object()) {
            $insert_array = array(
                'Id'           => $row->id,
                'Benutzer'     => $row->uid,
                'Datum'        => $row->datum,
                'Titel'        => $row->name,
                'Beschreibung' => $row->descr,
                'Start'        => $row->start,
                'Ende'         => $row->ende,
                'Typ'          => $row->type,
                'Gewicht'      => $row->weight,
                'wd'           => $row->wd,
                'tdays'        => $row->tdays,
                'Erledigt'     => $row->done);
            $this->_db->insert_query('kalender', $insert_array);
            $calendar++;
        }
        $sql->close();
        $this->_view->assign('res_calendar', $calendar);
    }

    protected function links($pf) {
        $this->manufacturer($pf);
        Tool::cleanTable('links_kategorie');
        $sql = $this->_db->query("SELECT * FROM " . $pf . "_linkcat");
        while ($row = $sql->fetch_object()) {
            $insert_array = array(
                'Id'             => $row->catid,
                'Parent_Id'      => $row->parent_id,
                'Sektion'        => $row->area,
                'Name_1'         => $row->catname,
                'Name_2'         => $row->catname,
                'Name_3'         => $row->catname,
                'Beschreibung_1' => $row->catdesc,
                'Beschreibung_2' => $row->catdesc,
                'Beschreibung_3' => $row->catdesc);
            $this->_db->insert_query('links_kategorie', $insert_array);
        }

        Tool::cleanTable('links');
        $sql = $this->_db->query("SELECT * FROM " . $pf . "_links");
        $links = 0;
        while ($res = $sql->fetch_object()) {
            $insert_array = array(
                'Id'             => $res->id,
                'Kategorie'      => $res->catid,
                'Datum'          => $res->ctime,
                'Bild'           => '',
                'Name_1'         => $res->title,
                'Name_2'         => $res->title,
                'Name_3'         => $res->title,
                'Beschreibung_1' => $res->text,
                'Beschreibung_2' => $res->text,
                'Beschreibung_3' => $res->text,
                'Url'            => $res->url,
                'Hits'           => $res->hits,
                'Sprache'        => 'ru');
            $this->_db->insert_query('links', $insert_array);
            $links++;
        }
        $sql->close();
        $this->_view->assign('res_links', $links);
    }

    protected function downloads($pf) {
        $this->manufacturer($pf);
        Tool::cleanTable('downloads_kategorie');
        $sql = $this->_db->query("SELECT * FROM " . $pf . "_downloadcat");
        while ($row = $sql->fetch_object()) {
            $insert_array = array(
                'Id'             => $row->catid,
                'Parent_Id'      => $row->parent_id,
                'Sektion'        => $row->area,
                'Name_1'         => $row->catname,
                'Name_2'         => $row->catname,
                'Name_3'         => $row->catname,
                'Beschreibung_1' => $row->catdesc,
                'Beschreibung_2' => $row->catdesc,
                'Beschreibung_3' => $row->catdesc);
            $this->_db->insert_query('downloads_kategorie', $insert_array);
        }

        Tool::cleanTable('downloads');
        $sql = $this->_db->query("SELECT * FROM " . $pf . "_downloads");
        $downloads = 0;
        while ($res = $sql->fetch_object()) {
            $insert_array = array(
                'Id'             => $res->id,
                'Kategorie'      => $res->catid,
                'Datum'          => $res->ctime,
                'Bild'           => '',
                'Name_1'         => $res->title,
                'Name_2'         => $res->title,
                'Name_3'         => $res->title,
                'Beschreibung_1' => $res->text,
                'Beschreibung_2' => $res->text,
                'Beschreibung_3' => $res->text,
                'Url'            => $res->dlurl_local,
                'Url_Direct'     => $res->dlurl,
                'Size_Direct'    => $res->size,
                'Mirrors'        => '',
                'Hits'           => $res->hits,
                'Sektion'        => $res->area,
                'Autor'          => $_SESSION['benutzer_id'],
                'Aktiv'          => 1,
                'Sponsor'        => 0,
                'BetriebsOs'     => '',
                'SoftwareTyp'    => '',
                'Version'        => '');
            $this->_db->insert_query('downloads', $insert_array);
            $downloads++;
        }
        $sql->close();
        $this->_view->assign('res_downloads', $downloads);
    }

    protected function products($pf) {
        $this->manufacturer($pf);
        $this->plattforms($pf);
        Tool::cleanTable('produkte');
        $sql = $this->_db->query("SELECT * FROM " . $pf . "_products");
        $prodcuts = 0;
        while ($row = $sql->fetch_object()) {
            $row->links = str_replace(';', "\r\n", $row->links);
            $row->links = str_replace(',', ';', $row->links);
            $insert_array = array(
                'Id'                     => $row->id,
                'Benutzer'               => $row->uid,
                'Datum'                  => $row->ctime,
                'Datum_Veroffentlichung' => $row->time_release,
                'Name1'                  => $row->title,
                'Name2'                  => $row->title,
                'Name3'                  => $row->title,
                'Beschreibung1'          => $row->descr,
                'Beschreibung2'          => $row->descr,
                'Beschreibung3'          => $row->descr,
                'Textbilder1'            => '',
                'Textbilder2'            => '',
                'Textbilder3'            => '',
                'Genre'                  => $row->genre,
                'Vertrieb'               => $row->publisher,
                'Hersteller'             => $row->manufacturer,
                'Wertung'                => $row->voting,
                'Asin'                   => $row->asin,
                'Plattform'              => $row->platform,
                'Preis'                  => $row->price,
                'Shopurl'                => $row->shopurl,
                'Shop'                   => $row->shop,
                'Bild'                   => $row->boxshot,
                'Links'                  => $row->links,
                'Galerien'               => $row->gallery,
                'Hits'                   => $row->hits,
                'Sektion'                => $row->area,
                'TopProduct'             => '',
                'Aktiv'                  => 1);
            $this->_db->insert_query('produkte', $insert_array);
            $prodcuts++;
        }
        $sql->close();
        $this->_view->assign('res_prodcuts', $prodcuts);
    }

    protected function cheats($pf) {
        $this->manufacturer($pf);
        $this->plattforms($pf);
        Tool::cleanTable('cheats');
        $sql = $this->_db->query("SELECT * FROM " . $pf . "_cheats");
        $cheats = 0;
        while ($row = $sql->fetch_object()) {
            $row->CheatLinks = str_replace(';', "\r\n", $row->CheatLinks);
            $row->CheatLinks = str_replace(',', ';', $row->CheatLinks);
            $insert_array = array(
                'Id'             => $row->CheatId,
                'Plattform'      => $row->CheatPlattformId,
                'Sprache'        => $row->CheatLanguage,
                'Typ'            => $row->CheatType,
                'Benutzer'       => $row->CheatAuthor,
                'Hits'           => $row->CheatHits,
                'DatumUpdate'    => $row->CheatTimeUpdated,
                'Name_1'         => $row->CheatTitle,
                'Name_2'         => $row->CheatTitle,
                'Name_3'         => $row->CheatTitle,
                'Beschreibung_1' => $row->CheatText,
                'Beschreibung_2' => $row->CheatText,
                'Beschreibung_3' => $row->CheatText,
                'Bild'           => '',
                'Download'       => $row->CheatFile,
                'DownloadHits'   => $row->CheatFileHits,
                'DownloadLink'   => $row->CheatOrderLink,
                'Hersteller'     => $row->CheatManufacturer,
                'Webseite'       => $row->CheatWebsite,
                'Galerien'       => $row->CheatGallery,
                'CheatLinks'     => $row->CheatLinks,
                'CheatProdukt'   => $row->CheatProduct,
                'Sektion'        => $row->area,
                'Aktiv'          => 1);
            $this->_db->insert_query('cheats', $insert_array);
            $cheats++;
        }
        $sql->close();
        $this->_view->assign('res_cheats', $cheats);
    }

    protected function manufacturer($pf) {
        Tool::cleanTable('hersteller');
        $sql = $this->_db->query("SELECT * FROM " . $pf . "_manufacturer");
        $anlagen = 0;
        while ($row = $sql->fetch_object()) {
            $insert_array = array(
                'Id'             => $row->id,
                'Datum'          => $row->ctime,
                'Benutzer'       => $row->uid,
                'Name'           => $row->name,
                'NameLang'       => $row->full_name,
                'Beschreibung_1' => $row->descr,
                'Beschreibung_2' => $row->descr,
                'Beschreibung_3' => $row->descr,
                'Bild'           => $row->icon,
                'Gruendung'      => $row->s_year,
                'GruendungLand'  => $row->s_country,
                'Personen'       => $row->people,
                'Homepage'       => $row->url,
                'Adresse'        => $row->adress,
                'Telefonkontakt' => '',
                'Hits'           => $row->hits);
            $this->_db->insert_query('hersteller', $insert_array);
            $anlagen++;
        }
        $sql->close();
        $this->_view->assign('res_manuf', $anlagen);
    }

    protected function plattforms($pf) {
        Tool::cleanTable('plattformen');
        $sql = $this->_db->query("SELECT * FROM " . $pf . "_platform");
        $anlagen = 0;
        while ($row = $sql->fetch_object()) {
            $insert_array = array(
                'Id'      => $row->id,
                'Name'    => $row->platform,
                'Icon'    => '',
                'Sektion' => $row->area);
            $this->_db->insert_query('plattformen', $insert_array);
            $anlagen++;
        }
        $sql->close();
        $this->_view->assign('res_plattforms', $anlagen);
    }

    protected function articles($pf) {
        $this->manufacturer($pf);
        $this->plattforms($pf);
        Tool::cleanTable('artikel_kategorie');
        $sql = $this->_db->query("SELECT * FROM " . $pf . "_articlecat");
        $anlagen = 0;
        while ($row = $sql->fetch_object()) {
            $insert_array = array(
                'Id'        => $row->catid,
                'Parent_Id' => $row->parent_id,
                'Name_1'    => $row->catname,
                'Name_2'    => $row->catname,
                'Name_3'    => $row->catname,
                'Sektion'   => $row->area,
                'Posi'      => $row->posi);
            $this->_db->insert_query('artikel_kategorie', $insert_array);
            $anlagen++;
        }
        $this->_view->assign('res_articlecat', $anlagen);
        $this->_view->assign('res_plattforms', $anlagen);
        Tool::cleanTable('artikel');
        $sql = $this->_db->query("SELECT * FROM " . $pf . "_articles");
        $articles = 0;
        while ($row = $sql->fetch_object()) {
            if (empty($row->artvotes)) {
                $wertung = '';
            } else {
                $w_raw = explode(';', $row->artvotes);
                foreach ($w_raw as $wertung => $w) {
                    $val = explode(',', $w);
                    foreach ($val as $value_check) {
                        switch ($value_check) {
                            case ($value_check >= '80'):
                                $v = '5';
                                break;
                            case ($value_check >= '60' && $value_check < '80'):
                                $v = '4';
                                break;
                            case ($value_check >= '40' && $value_check < '60'):
                                $v = '3';
                                break;
                            case ($value_check >= '20' && $value_check < '40'):
                                $v = '2';
                                break;
                            case ($value_check >= '0' && $value_check < '20'):
                                $v = '1';
                                break;
                        }
                    }
                    $wr = explode(',', $w);
                    $werte[] = $wr[0] . ';' . $v;
                }
                $wertung = implode("\r\n", $werte);
            }

            $insert_array = array(
                'Id'                => $row->id,
                'Kategorie'         => $row->articlecat,
                'Titel_1'           => $row->title,
                'Titel_2'           => $row->title,
                'Titel_3'           => $row->title,
                'Untertitel_1'      => $row->subtitle,
                'Untertitel_2'      => $row->subtitle,
                'Untertitel_3'      => $row->subtitle,
                'Inhalt_1'          => $row->content,
                'Inhalt_2'          => $row->content,
                'Inhalt_3'          => $row->content,
                'Textbilder_1'      => '',
                'Textbilder_2'      => '',
                'Textbilder_3'      => '',
                'WertungsDaten'     => $wertung,
                'Genre'             => $row->genre,
                'Hersteller'        => $row->publisher,
                'Vertrieb'          => $row->manufacturer,
                'Wertung'           => $row->canvote,
                'Kommentare'        => $row->cancomment,
                'Plattform'         => '',
                'Veroeffentlichung' => $row->time_release,
                'Preis'             => $row->price,
                'Shop'              => $row->shopurl,
                'Bild_1'            => $row->boxshot,
                'Bild_2'            => $row->boxshot,
                'Bild_3'            => $row->boxshot,
                'Links'             => str_replace(array(';', ','), array("\r\n", ';'), $row->links),
                'Galerien'          => $row->gallery,
                'Autor'             => $row->uid,
                'Zeit'              => $row->ctime,
                'Hits'              => $row->hits,
                'Typ'               => $row->type,
                'Sektion'           => $row->area,
                'Druck'             => $row->printed,
                'Aktiv'             => $row->active,
                'Top'               => $row->top,
                'Flop'              => $row->flop,
                'Minimum'           => $row->minimum,
                'Optimum'           => $row->optimum,
                'ZeitStart'         => $row->time_start,
                'ZeitEnde'          => $row->time_end,
                'Suche'             => $this->replace($row->cansearch),
                'Topartikel'        => $this->replace($row->isontop),
                'TopartikelBild_1'  => $row->image_top,
                'TopartikelBild_2'  => $row->image_top,
                'TopartikelBild_3'  => $row->image_top,
                'AlleSektionen'     => $this->replace($row->allareas),
                'Bildausrichtung'   => $row->image_align,
                'Tags'              => $row->matchword,
                'ShopArtikel'       => $row->product_shop,
                'Kennwort'          => $row->password);
            $this->_db->insert_query('artikel', $insert_array);
            $articles++;
        }
        $sql->close();
        $this->_view->assign('res_articles', $articles);
    }

    protected function forums($pf) {
        $this->user($pf);
        Tool::cleanTable('f_attachment');
        $sql = $this->_db->query("SELECT * FROM " . $pf . "_f_attachment");
        $anlagen = 0;
        while ($row = $sql->fetch_object()) {
            $insert_array = array(
                'id'        => $row->id,
                'orig_name' => $row->orig_name,
                'filename'  => $row->filename,
                'hits'      => $row->hits);
            $this->_db->insert_query('f_attachment', $insert_array);
            $anlagen++;
        }

        Tool::cleanTable('f_category');
        $sql = $this->_db->query("SELECT * FROM " . $pf . "_f_category");
        while ($row = $sql->fetch_object()) {
            $insert_array = array(
                'id'        => $row->id,
                'title'     => $row->title,
                'position'  => $row->position,
                'parent_id' => $row->parent_id,
                'comment'   => $row->comment,
                'group_id'  => $row->group_id);
            $this->_db->insert_query('f_category', $insert_array);
        }
        $sql->close();

        Tool::cleanTable('f_post');
        $post = $this->_db->query("SELECT * FROM " . $pf . "_f_post");
        while ($row = $post->fetch_object()) {
            $insert_array = array(
                'id'          => $row->id,
                'title'       => $row->title,
                'topic_id'    => $row->topic_id,
                'datum'       => $row->datum,
                'uid'         => $row->uid,
                'use_bbcode'  => $row->use_bbcode,
                'use_smilies' => $row->use_smilies,
                'use_sig'     => $row->use_sig,
                'message'     => $row->message,
                'attachment'  => $row->attachment,
                'opened'      => $row->opened,
                'thanks'      => '');
            $this->_db->insert_query('f_post', $insert_array);
        }
        $post->close();

        Tool::cleanTable('f_topic');
        $topic = $this->_db->query("SELECT * FROM " . $pf . "_f_topic");
        while ($row = $topic->fetch_object()) {
            $res = $this->_db->fetch_object("SELECT id FROM " . PREFIX . "_f_post WHERE topic_id = '" . $row->id . "' ORDER BY id ASC LIMIT 1");
            $insert_array = array(
                'id'             => $row->id,
                'title'          => $row->title,
                'status'         => $row->status,
                'views'          => $row->views,
                'rating'         => $row->rating,
                'forum_id'       => $row->forum_id,
                'icon'           => $row->icon,
                'posticon'       => $row->posticon,
                'datum'          => $row->datum,
                'replies'        => $row->replies,
                'uid'            => $row->uid,
                'notification'   => $row->notification,
                'type'           => $row->type,
                'last_post'      => $row->last_post,
                'last_post_id'   => $row->last_post_id,
                'opened'         => $row->opened,
                'last_post_int'  => $row->last_post_int,
                'top_first_post' => 0,
                'first_post_id'  => $res->id);
            $this->_db->insert_query('f_topic', $insert_array);
        }
        $topic->close();

        $this->insert($pf, 'f_forum');
        $this->insert($pf, 'f_mods');
        $this->insert($pf, 'f_permissions');
        $this->insert($pf, 'f_rating');
        $this->insert($pf, 'f_topic_read');
        $this->_view->assign('res_forums', '1');
    }

    protected function insert($pf, $table) {
        Tool::cleanTable($table);
        $query = $this->_db->query("SELECT * FROM " . $pf . "_" . $table);
        while ($insert_array = $query->fetch_assoc()) {
            $this->_db->insert_query($table, $insert_array);
        }
        $query->close();
    }

    protected function shop($pf) {
        $this->user($pf);
        $this->manufacturer($pf);
        Tool::cleanTable('shop_bestellungen');
        $sql = $this->_db->query("SELECT * FROM " . $pf . "_shop_orders");
        $c = 0;
        while ($row = $sql->fetch_object()) {
            $insert_array = array(
                'Id'                 => $row->orderid,
                'Datum'              => $row->ordertime,
                'Ip'                 => $row->ip,
                'Benutzer'           => $row->uid,
                'Email'              => '',
                'Betrag'             => $row->ovall,
                'Status'             => $row->status_o,
                'Artikel'            => serialize(explode(',', $row->articles)),
                'TransaktionsNummer' => $row->orderid,
                'USt'                => $row->ust,
                'ZahlungsId'         => $row->payment_id,
                'VersandId'          => 1,
                'Rng_Firma'          => $row->rng_company,
                'Rng_Vorname'        => $row->rng_firstname,
                'Rng_Nachname'       => $row->rng_lastname,
                'Rng_Strasse'        => $row->rng_street,
                'Rng_Plz'            => $row->rng_zip,
                'Rng_Ort'            => $row->rng_town,
                'Rng_Land'           => $row->rng_country,
                'Rng_Fon'            => '',
                'Rng_Fax'            => '',
                'Rng_Email'          => '',
                'Lief_Vorname'       => $row->shipping_firstname,
                'Lief_Nachname'      => $row->shipping_lastname,
                'Lief_Strasse'       => $row->shipping_street,
                'Lief_Plz'           => $row->shipping_zip,
                'Lief_Ort'           => $row->shipping_town,
                'Lief_Fon'           => '',
                'Lief_Fax'           => '',
                'Lief_Land'          => $row->shipping_country,
                'Lief_Firma'         => $row->shipping_company,
                'Bestellung'         => base64_encode($row->calculation),
                'Gewicht'            => $row->weight,
                'GutscheinWert'      => '',
                'GutscheinId'        => '',
                'Geloescht'          => 0,
                'KundenNachricht'    => $row->Bemerkung,
                'RechnungGesendet'   => $row->RechnungGesendet,
                'WareGesendet'       => $row->WareGesendet,
                'Bemerkung'          => '');
            $this->_db->insert_query('shop_bestellungen', $insert_array);
            $c++;
        }

        Tool::cleanTable('shop_varianten_kategorien');
        $sql = $this->_db->query("SELECT * FROM " . $pf . "_shop_artoptions_cat");
        $c = 0;
        while ($row = $sql->fetch_object()) {
            $varcat = $this->_db->cache_fetch_object("SELECT articlecat FROM " . $pf . "_shop_articles WHERE artnumber='" . $this->_db->escape($row->artnumber) . "' LIMIT 1");
            $insert_array = array(
                'Id'             => $row->id,
                'KatId'          => $varcat->articlecat,
                'Name_1'         => $row->name,
                'Name_2'         => $row->name,
                'Name_3'         => $row->name,
                'Beschreibung_1' => $row->artoptions_descr,
                'Beschreibung_2' => $row->artoptions_descr,
                'Beschreibung_3' => $row->artoptions_descr,
                'Aktiv'          => 1,
                'Position'       => 1);
            $this->_db->insert_query('shop_varianten_kategorien', $insert_array);
            $c++;
        }

        Tool::cleanTable('shop_varianten');
        $sql2 = $this->_db->query("SELECT id,artnumber FROM " . $pf . "_shop_articles");
        while ($row2 = $sql2->fetch_object()) {
            $sql = $this->_db->query("SELECT * FROM " . $pf . "_shop_artoptions WHERE artnumber='" . $this->_db->escape($row2->artnumber) . "'");
            $c = 0;
            while ($row = $sql->fetch_object()) {
                switch ($row->math_operant) {
                    default:
                    case '+':
                        $op = 'plus';
                        break;

                    case '-':
                        $op = 'minus';
                        break;
                }
                $artid = $this->_db->cache_fetch_object("SELECT id FROM " . $pf . "_shop_articles WHERE artnumber='" . $this->_db->escape($row->artnumber) . "' LIMIT 1");
                $varcat = $this->_db->cache_fetch_object("SELECT id FROM " . $pf . "_shop_artoptions_cat WHERE artnumber='" . $this->_db->escape($row->artnumber) . "' LIMIT 1");
                $insert_array = array(
                    'Id'            => $row->id,
                    'KatId'         => $row->option_cat,
                    'ArtId'         => $artid->id,
                    'Name_1'        => $row->option_desc,
                    'Name_2'        => $row->option_desc,
                    'Name_3'        => $row->option_desc,
                    'Wert'          => $row->math_value,
                    'Operant'       => $op,
                    'Position'      => 1,
                    'Vorselektiert' => $row->pre_selected,
                    'Gewicht'       => $row->extra_weight);
                $this->_db->insert_query('shop_varianten', $insert_array);
                $c++;
            }
        }

        Tool::cleanTable('shop_kategorie');
        $sql = $this->_db->query("SELECT * FROM " . $pf . "_shop_cat");
        $c = 0;
        while ($row = $sql->fetch_object()) {
            $insert_array = array(
                'Id'             => $row->catid,
                'Parent_Id'      => $row->parent_id,
                'Name_1'         => $row->catname,
                'Name_2'         => $row->catname,
                'Name_3'         => $row->catname,
                'posi'           => $row->posi,
                'icon'           => $row->icon,
                'UstId'          => 1,
                'Bild_Navi'      => '',
                'Bild_Kategorie' => $row->icon);
            $this->_db->insert_query('shop_kategorie', $insert_array);
            $c++;
        }
        $this->_view->assign('res_shopcategs', $c);
        Tool::cleanTable('shop_produkte');
        $sql = $this->_db->query("SELECT * FROM " . $pf . "_shop_articles");
        $c = 0;
        while ($row = $sql->fetch_object()) {
            $insert_array = array(
                'Id'                  => $row->id,
                'Kategorie'           => $row->articlecat,
                'Kategorie_Multi'     => $row->articlecat_2,
                'Schlagwoerter'       => $row->title,
                'Zub_a'               => '',
                'Zub_b'               => '',
                'Zub_c'               => '',
                'Artikelnummer'       => $row->artnumber,
                'Preis'               => $row->price_list,
                'Preis_Liste'         => $row->price,
                'Preis_Liste_Gueltig' => '',
                'Titel_1'             => $row->title,
                'Titel_2'             => $row->title,
                'Titel_3'             => $row->title,
                'Beschreibung_1'      => $row->descr_short,
                'Beschreibung_2'      => $row->descr_short,
                'Beschreibung_3'      => $row->descr_short,
                'Beschreibung_lang_1' => $row->descr . '<br /><br />' . $row->detail_sys,
                'Beschreibung_lang_2' => $row->descr . '<br /><br />' . $row->detail_sys,
                'Beschreibung_lang_3' => $row->descr . '<br /><br />' . $row->detail_sys,
                'Hat_ESD'             => $row->esd,
                'Aktiv'               => ($row->active == 2 ? 1 : $row->active),
                'Erstellt'            => $row->ctime,
                'Klicks'              => '',
                'Bild'                => $row->icon,
                'Bilder'              => '',
                'Gewicht'             => $row->weight,
                'Abmessungen'         => '',
                'Hersteller'          => $row->manufacturer,
                'EinheitCount'        => $row->unit_count,
                'EinheitId'           => $row->EinheitId,
                'Startseite'          => $row->show_startpage,
                'Lagerbestand'        => $row->amount_store,
                'Bestellt'            => '',
                'Verfuegbar'          => 1,
                'EinzelBestellung'    => 0,
                'Verkauft'            => '',
                'MaxBestellung'       => $row->amount_max,
                'MinBestellung'       => $row->amount_min,
                'Lieferzeit'          => 1,
                'Gruppen'             => '');
            $this->_db->insert_query('shop_produkte', $insert_array);
            $c++;
        }
        $sql->close();
        $this->_view->assign('res_shoparticles', $c);
    }

    protected function gallery($pf) {
        Tool::cleanTable('galerie_kategorien');
        $insert_array = array(
            'Id'      => 1,
            'Sektion' => 1,
            'Name_1'  => '���������',
            'Name_2'  => '���������',
            'Name_3'  => '���������',
            'Text_1'  => '��������',
            'Text_2'  => '��������',
            'Text_3'  => '��������',
            'Bild'    => '',
            'Tags'    => '',
            'Autor'   => 1,
            'Datum'   => time(),
            'Aktiv'   => 1);
        $this->_db->insert_query('galerie_kategorien', $insert_array);

        Tool::cleanTable('galerie');
        $sql = $this->_db->query("SELECT * FROM " . $pf . "_gallery");
        $c = 0;
        while ($row = $sql->fetch_object()) {
            $insert_array = array(
                'Id'             => $row->id,
                'Kategorie'      => 1,
                'Parent_Id'      => $row->parent_id,
                'Sektion'        => $row->area,
                'Name_1'         => $row->title,
                'Name_2'         => $row->title,
                'Name_3'         => $row->title,
                'Aktiv'          => $row->active,
                'Beschreibung_1' => $row->descr,
                'Beschreibung_2' => $row->descr,
                'Beschreibung_3' => $row->descr,
                'Datum'          => $row->ctime,
                'Autor'          => $row->uid,
                'Tags'           => $row->matchword,
                'Bilder'         => '');
            $this->_db->insert_query('galerie', $insert_array);
            $c++;
        }
        $this->_view->assign('res_gallerycategs', $c);
        Tool::cleanTable('galerie_bilder');
        $sql = $this->_db->query("SELECT * FROM " . $pf . "_gallery_items");
        $c = 0;
        while ($row = $sql->fetch_object()) {
            $insert_array = array(
                'Id'             => $row->id,
                'Galerie_Id'     => $row->gal_id,
                'Bildname'       => $row->path,
                'Name_1'         => $row->name,
                'Name_2'         => $row->name,
                'Name_3'         => $row->name,
                'Beschreibung_1' => $row->descr,
                'Beschreibung_2' => $row->descr,
                'Beschreibung_3' => $row->descr,
                'Voting'         => '',
                'Datum'          => $row->ctime,
                'Klicks'         => $row->hits,
                'Autor'          => $row->author);
            $this->_db->insert_query('galerie_bilder', $insert_array);
            $c++;
        }
        $sql->close();
        $this->_view->assign('res_gallery', $c);
    }

    protected function news($pf) {
        $this->manufacturer($pf);
        Tool::cleanTable('news_kategorie');
        $sql = $this->_db->query("SELECT * FROM " . $pf . "_newscat");
        $c = 0;
        while ($row = $sql->fetch_object()) {
            $insert_array = array(
                'Id'        => $row->catid,
                'Parent_Id' => $row->parent_id,
                'Name_1'    => $row->catname,
                'Name_2'    => $row->catname,
                'Name_3'    => $row->catname,
                'Sektion'   => $row->area,
                'Posi'      => $row->posi);
            $this->_db->insert_query('news_kategorie', $insert_array);
            $c++;
        }
        $this->_view->assign('res_newscategs', $c);
        Tool::cleanTable('news');
        $sql = $this->_db->query("SELECT * FROM " . $pf . "_news");
        $c = 0;
        while ($row = $sql->fetch_object()) {
            $insert_array = array(
                'Id'              => $row->newsid,
                'Kategorie'       => $row->newscat,
                'Autor'           => $row->uid,
                'Sektion'         => $row->area_id,
                'Zeit'            => $row->ntime,
                'ZeitStart'       => $row->time_start,
                'ZeitEnde'        => 0,
                'Titel1'          => $row->title,
                'Titel2'          => $row->title,
                'Titel3'          => $row->title,
                'Intro1'          => $row->text,
                'Intro2'          => $row->text,
                'Intro3'          => $row->text,
                'News1'           => $row->textmore,
                'News2'           => $row->textmore,
                'News3'           => $row->textmore,
                'Topnews_Bild_1'  => $row->image_top,
                'Topnews_Bild_2'  => $row->image_top,
                'Topnews_Bild_3'  => $row->image_top,
                'Hits'            => $row->hits,
                'Bild1'           => $row->image,
                'Bild2'           => $row->image,
                'Bild3'           => $row->image,
                'BildAusrichtung' => 'right',
                'Textbilder1'     => '',
                'Textbilder2'     => '',
                'Textbilder3'     => '',
                'AlleSektionen'   => $this->replace($row->allareas),
                'Aktiv'           => $row->ispublic,
                'Suche'           => $this->replace($row->cansearch),
                'Bewertung'       => $this->replace($row->canvote),
                'Kommentare'      => $this->replace($row->cancomment),
                'Tags'            => $row->matchword,
                'Galerien'        => '',
                'Topnews'         => $this->replace($row->isontop));
            $this->_db->insert_query('news', $insert_array);
            $c++;
        }
        $sql->close();
        $this->_view->assign('res_news', $c);
    }

    protected function user($pf) {
        $this->_db->query("DELETE FROM " . PREFIX . "_benutzer_gruppen WHERE Id!='1'");
        $this->_db->query("ALTER TABLE `" . PREFIX . "_benutzer_gruppen` AUTO_INCREMENT=1");
        $sql = $this->_db->query("SELECT * FROM " . $pf . "_usergroup WHERE ugroup!='1'");
        $c = 0;
        while ($row = $sql->fetch_object()) {
            $insert_array = array(
                'Id'               => $row->ugroup,
                'Name_Intern'      => $row->groupname_single,
                'Name'             => $row->groupname,
                'VatByCountry'     => 1,
                'Rabatt'           => $row->deductions,
                'ShopAnzeige'      => 'b2c',
                'Avatar_Default'   => $row->set_default_avatar,
                'Avatar'           => $row->default_avatar,
                'Avatar_B'         => $row->avatar_width,
                'Avatar_H'         => $row->avatar_height,
                'MaxPn'            => $row->groupmaxpn,
                'MaxPn_Zeichen'    => $row->maxpnlength,
                'MaxAnlagen'       => $row->max_attachments,
                'MaxZeichenPost'   => $row->maxlength_post,
                'SysCode_Signatur' => 0,
                'Signatur_Laenge'  => $row->groupmaxsig,
                'Signatur_Erlaubt' => 1);
            $this->_db->insert_query('benutzer_gruppen', $insert_array);
            $c++;
        }
        $this->_view->assign('res_usergroups', $c);
        $this->_db->query("DELETE FROM " . PREFIX . "_benutzer WHERE Id!='1'");
        $this->_db->query("ALTER TABLE `" . PREFIX . "_benutzer` AUTO_INCREMENT=1");
        $sql = $this->_db->query("SELECT * FROM " . $pf . "_user WHERE ugroup!='1' AND ugroup!='2'");
        $c = 0;
        while ($row = $sql->fetch_object()) {
            switch ($row->country) {
                case 'DE':
                    $row->land = '��������';
                    break;
                case 'FR':
                    $row->land = '�������';
                    break;
                case 'RU':
                default:
                    $row->land = '������';
                    break;
            }

            $insert_array = array(
                'Id'            => $row->uid,
                'Gruppe'        => $row->ugroup,
                'Team'          => 0,
                'Regdatum'      => $row->user_regdate,
                'Email'         => $row->email,
                'Kennwort'      => $row->pass,
                'Benutzername'  => $row->uname,
                'Vorname'       => $row->name,
                'Nachname'      => $row->lastname,
                'Strasse_Nr'    => $row->street,
                'Postleitzahl'  => $row->zip,
                'Ort'           => $row->user_from,
                'Firma'         => $row->company,
                'UStId'         => $row->ustid,
                'Telefon'       => $row->phone,
                'Telefax'       => $row->fax,
                'Land'          => $row->land,
                'LandCode'      => strtolower($row->country),
                'Aktiv'         => $row->status,
                'Profil_public' => 1,
                'Profil_Alle'   => 1,
                'Pnempfang'     => $row->user_canpn,
                'Gaestebuch'    => 0,
                'Zuletzt_Aktiv' => $row->user_lastonline,
                'icq'           => $row->user_icq,
                'aim'           => $row->user_aim,
                'msn'           => '',
                'skype'         => $row->user_skype,
                'Newsletter'    => $row->recieve_newsletter,
                'Emailempfang'  => $row->user_viewemail,
                'PnPopup'       => 1,
                'Geburtstag'    => $row->user_birthday,
                'Webseite'      => $row->url,
                'Beitraege'     => $row->user_posts,
                'Signatur'      => $row->user_sig);
            $this->_db->insert_query('benutzer', $insert_array);
            $c++;
        }
        $sql->close();
        $this->_view->assign('res_users', $c);
    }

}
