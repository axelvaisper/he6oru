<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (!defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}

SX::setDefine('SMARTY_DIR', STATUS_DIR . '/lib/smarty/');        // ������������� ���� � smarty
SX::setDefine('SMARTY_RESOURCE_CHAR_SET', $_SESSION['Charset']); // ������������� ��������� smarty
SX::setDefine('SMARTY_RESOURCE_DATE_FORMAT', '%d.%m.%Y, %H:%M'); // ������������� ������ ������ ���� � smarty �� ���������
include_once SMARTY_DIR . 'Smarty.class.php';

class View extends Smarty {

    /* �������� �� ���� ���������� � ����� ����������� ������ */
    public static function get() {
        static $object = NULL;
        if ($object === NULL) {
            $object = new self;
        }
        return $object;
    }

    public function __construct() {
        parent::__construct();
        $theme = SX::get('options.theme');
        $langcode = Arr::getSession('admin_lang', 'ru');
        SX::setDefine('AREA', intval($_SESSION['a_area']));

        $this->compile_dir  = TEMP_DIR . '/compiled/' . AREA . '/admin';
        $this->cache_dir    = TEMP_DIR . '/compiled/' . AREA . '/admin';
        $this->config_dir   = LANG_DIR . '/' . $langcode;
        $this->template_dir = THEME;
        $this->compile_id   = md5($langcode . $theme);
        $this->plugins_dir  = array(SMARTY_DIR . 'plugins', SMARTY_DIR . 'statusplugins');
        $this->compile_check           = true;  // ��������� ������ �� ���������
        $this->compile_locking         = true;  // ����������� ������ � ������ �� ����� ����������
        $this->force_compile           = false; // ����� ��������� ����������
        $this->use_sub_dirs            = false; // ������������ �������� ��� ���������������� ��� ������������ ������
        $this->caching                 = false; // ��������� �����������
        $this->merge_compiled_includes = false; // ������������� ������� � ����� ����� � ���������
        $this->cache_lifetime          = 86400; // ����� ����� ���� // 3600
        $this->force_cache             = false; // ����� ���������� �������� ����
        $this->cache_modified_check    = false; // ���������� ��������� If-Modified-Since
        $this->direct_access_security  = true;  // ����� ������� ������� � ���������������� ������ // if(!defined('SMARTY_DIR')) exit('no direct access allowed');

        $this->registerPlugin('modifier', 'translit', 'translit');
        $this->registerPlugin('modifier', 'sanitize', 'sanitize');
        $this->registerPlugin('modifier', 'base64encode', 'base64_encode');
        $this->registerPlugin('modifier', 'utf8', array('Tool', 'win1251'));

        $this->registerPlugin('function', 'perm', 'perm');
        $this->registerPlugin('function', 'admin_active', 'admin_active');
        $this->registerPlugin('function', 'page_link', array(SX::object('Redir'), 'link'));
        if (SX::get('configs.tplcleanid') == '0') {
            $this->registerFilter('output', array($this, 'marker'));
        }
        $tpl_array = array(
            'settings' => SX::get('system'), // �������� � ������ ���������� ��������� �������
            'configs'  => SX::get('configs'),
            'basepath' => BASE_PATH,
            'baseurl'  => BASE_URL,
            'browser'  => Tool::browser(),
            'langcode' => $langcode,
            'source'   => 'theme/' . $theme,
            'theme'    => $theme,
            'area'     => AREA,
            'csspath'  => 'theme/' . $theme . '/css',
            'jspath'   => '../js',
            'imgpath'  => 'theme/' . $theme . '/images',
            'incpath'  => STATUS_DIR . '/admin/theme/' . $theme,
            'time'     => time());
        $this->assign($tpl_array);
    }

    /* ����� ���������� �������������� � ������ � ������ ������� */
    public function marker($source, $view) {
        if (isset($view->template_resource) && stripos($source, '<!DOCTYPE') === false) {
            $id = str_replace(STATUS_DIR, '', $view->template_resource);
            $source = PE . '<!-- Start - ' . $id . ' -->' . PE . $source . PE . '<!-- End - ' . $id . ' -->' . PE;
        }
        return $source;
    }

    /* ����� � ��������� ��� */
    public function content($tpl, $tag = 'content') {
        return $this->assign($tag, $this->fetch(THEME . $tpl));
    }

}
