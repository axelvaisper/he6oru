<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
error_reporting(0);                                                        // ����� ������ php: 0 - ���������, E_ALL - ��������
define('STATUS_DIR', realpath(dirname(dirname(__FILE__))));                // ������������� ������ ���� �� �������
require_once STATUS_DIR . '/class/class.SX.php';                           // ���������� �������� ����� �������
SX::initStatus('admin');                                                   // �������������� �������
$ACore = SX::object('AdminCore');
$ACore->setSection();                                                      // ������������� ������
$ACore->checkLogin();                                                      // ��������� �� ����������� � �����
$ACore->settings();
$ACore->sessionLang();
$ACore->sessionLangcode();
SX::getLocale($_SESSION['admin_lang']);                                    // ������������� ������ PHP
$ACore->theme();
$CS = View::get();
SX::loadLang(LANG_DIR . '/' . $_SESSION['admin_lang'] . '/admin.txt');     // ��������� �������� ����������
SX::loadLang(LANG_DIR . '/' . $_SESSION['admin_lang'] . '/mail.txt');      // ��������� �������� ���������� �����
$_SESSION['Charset'] = SX::$lang['Charset'];                               // ������������� ������ � ������� ����������
header('Content-type: text/html; charset=' . $_SESSION['Charset']);        // ������������� ��������� � ������� ����������
$CS->assign('shop_aktiv', SX::get('admin_active.shop'));                   // ������������� � ������� ������� �� �������
$CS->assign('section_switch', $ACore->switchSection());                    // �������� � ������ ������������� ������
$CS->assign('theme_switch', $ACore->switchTheme());                        // �������� � ������ ������������� ��������
$CS->assign('admin_settings', SX::get('admin'));                           // �������� � ������ ���������� ��������� ����� ������
$CS->assign('area', $_SESSION['a_area']);                                  // �������� � ������ ����� ������
$CS->assign('backurl', base64_encode(SX::object('Redir')->link()));        // �������� � ������ ����� ������� ��������
$CS->assign('helpquery', $ACore->helpQuery());                             // �������� � ������ ��� ������ ��� �������
register_shutdown_function(array('SX', 'sendMail'));                       // �������� ����� �� �������
$ACore->access();                                                          // ��������� �������� �� ������ �������� ����
$ACore->naviModules();                                                     // ���������� ���� � ����� �������� ������� �������
$ACore->languages();                                                       // ������ ��������� �����
$ACore->logout();                                                          // ������� �� ���� ������
$ACore->checkIp();                                                         // ��������� �������� �� ������ � ����� ������ � ������������� IP
$ACore->permisson();                                                       // ������������� ����� �������
$ACore->extensions();                                                      // �������� ���� � ����������� ������
$load = 'login.tpl';
if (Arr::getSession('loggedin') == 1) {
    $load = Arr::getRequest('noframes') == 1 ? 'noframes.tpl' : 'main.tpl';
    $load = !perm('adminpanel') || (!isset($_SESSION['benutzer_id']) || $_SESSION['benutzer_id'] == 0) ? 'login.tpl' : $load;
}
$out = Arr::getRequest('noout') == 1 ? NULL : $CS->fetch(THEME . '/' . $load);
SX::output($out, true);
