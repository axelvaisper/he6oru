<form method="post" action="index.php?do=login">

<figure text_center>
<img src="{$imgpath}/login_logo.png" alt="" />
</figure>

{if !isset($no_ip)}


<label>
<small>{#Global_Email#}</small>
<input name="login_email_a" type="email" id="login_email_a" value="{if isset($smarty.request.login_email_a)}{$smarty.request.login_email_a|sanitize}{/if}" required/>
</label>

<label>
<small>{#LoginPass#}</small>
<input id="login_pass_a" name="login_pass_a" type="password" value="{if isset($smarty.request.login_pass_a)}{$smarty.request.login_pass_a|sanitize}{/if}" required/>
</label>

<label selectx>
<small>{#LoginSection#}</small>
<select name="area" id="area">
{foreach from=$sections item=s}
<option value="{$s->Id}">{$s->Name|sanitize}</option>
{/foreach}
</select>
</label>

<label selectx>
<small>{#Sections_theme#}</small>
<select name="theme">
{foreach from=$themes item=t}
<option value="{$t}" {if isset($smarty.cookies.pre_admin_theme) && $smarty.cookies.pre_admin_theme == $t}selected="selected"{/if}>{$t|sanitize}</option>
{/foreach}
</select>
</label>

<label selectx>
<small>{#LoginLang#}</small>
<select name="lang">
{foreach from=$languages item=l}
<option value="{$l->Sprachcode}" {if isset($smarty.cookies.pre_admin_lang) && $smarty.cookies.pre_admin_lang == $l->Sprachcode}selected="selected"{/if}>{$l->Sprache|sanitize}</option>
{/foreach}
</select>
</label>

<br>
<br>

<div checkboxx float_left>
<input name="save_logindata" type="checkbox" id="save_logindata" value="1" />
<label for="save_logindata">{#LoginRemember#}</label>
</div>

<span text_right>
<input name="action" type="hidden" id="action" value="login" />
<input type="submit" name="Submit" value="{#LoginName#}" btnx/>
</span>

{/if}

{if isset($message)}
<br />
<br />
<strong>{$message}</strong>
{/if}


</form>
