<script type="text/javascript">
<!--
function nexts() {
    document.nextform.submit();
}
{$nexts}

$(document).ready(function() {
    cid = 'no';
    $('#check').on('click', function() {
        cid = cid == 'ok' ? cid = 'no' : cid = 'ok';
        $('.check_' + cid).attr('checked', true);
    });
});
//-->
</script>

<div class="header">{$title}</div>
{if empty($smarty.request.noframes)}
  <div class="subheaders">
    <a href="index.php?do=settings&amp;sub=antivirus"><img class="absmiddle" src="{$imgpath}/arrow_left.png" alt="" border="0" /> {#Global_Published#}</a>&nbsp;&nbsp;&nbsp;
    <a href="index.php?do=settings&amp;sub=antivirus&amp;snap=error"><img class="absmiddle" src="{$imgpath}/warning.gif" alt="" border="0" /> {#AntivirusFilesError#}</a>&nbsp;&nbsp;&nbsp;
    <a href="index.php?do=settings&amp;sub=antivirus&amp;snap=ignore"><img class="absmiddle" src="{$imgpath}/virus.png" alt="" border="0" /> {#AntivirusWarnFiles#}</a>&nbsp;&nbsp;&nbsp;
      {if $admin_settings.Ahelp == 1}
      <a class="colorbox" href="index.php?do=help&amp;sub={$helpquery}&amp;noframes=1"><img class="absmiddle" src="{$imgpath}/s_help.png" alt="" border="0" /> {#GlobalHelp#}</a>&nbsp;&nbsp;&nbsp;
      {/if}
    <a class="colorbox" href="index.php?do=support&amp;sub=send_order&amp;noframes=1"><img class="absmiddle" src="{$imgpath}/send.png" alt="" border="0" /> {#SendOrder#}</a>
  </div>
{/if}
{if !empty($snap)}
  {if $snap == 'new'}
    <table width="100%" border="0" cellspacing="0" cellpadding="3">
      <tr class="headers">
        <td class="headers">{#Global_Target#}</td>
        <td width="90" align="center" nowrap="nowrap" class="headers">{#GlobalSize#}</td>
        <td width="120" align="center" nowrap="nowrap" class="headers">{#Global_Date#}</td>
        <td width="60" align="center" nowrap="nowrap" class="headers">{#GlobalPerm#}</td>
        <td width="200" align="center" class="headers">{#GlobalHash#}</td>
      </tr>
      {assign var=count value=0}
      {foreach from=$files key=key item=file}
        {assign var=count value=$count+1}
        <tr class="{cycle values='second, first'}">
          <td><strong>{$count}. </strong>{$key|sanitize}</td>
          <td width="90" align="center">{$file.file_size}</td>
          <td width="120" align="center" nowrap="nowrap">{$file.file_time|date_format: '%d-%m-%Y, %H:%M:%S'}</td>
          <td width="60" align="center" nowrap="nowrap">{$file.file_perm}</td>
          <td width="200" align="center" nowrap="nowrap">{$file.file_md5}</td>
        </tr>
      {/foreach}
    </table>
  {elseif $snap == 'check' || $snap == 'error'}
    <fieldset>
      <legend>{#Info#}</legend>
      <div style="background-color: green;float: left;width: 33%;text-align: center;font-size: 12px;font-weight: bold">{#AntivirusFileNew#}</div>
      <div style="background-color: yellow;float: left;width: 33%;text-align: center;font-size: 12px;font-weight: bold">{#AntivirusFileDel#}</div>
      <div style="background-color: red;float: left;width: 33%;text-align: center;font-size: 12px;font-weight: bold">{#AntivirusFileError#}</div>
    </fieldset>
    <br/>
    <table width="100%" border="0" cellspacing="0" cellpadding="3">
      <tr class="headers">
        <td class="headers">{#Global_Target#}</td>
        <td width="90" align="center" nowrap="nowrap" class="headers">{#GlobalSize#}</td>
        <td width="120" align="center" nowrap="nowrap" class="headers">{#Global_Date#}</td>
        <td width="60" align="center" nowrap="nowrap" class="headers">{#GlobalPerm#}</td>
        <td width="200" align="center" class="headers">{#GlobalHash#}</td>
      </tr>
      {assign var=count value=0}
      {foreach from=$files key=key item=file}
        {assign var=count value=$count+1}
        <tr class="{cycle values='second, first'}">
          {if $file.file_type == 'del'}
            {assign var=style value=' style="background-color: yellow"'}
            {assign var=red value=' style="background-color: yellow"'}
          {elseif $file.file_type == 'new'}
            {assign var=style value=' style="background-color: green"'}
            {assign var=red value=' style="background-color: green"'}
          {else}
            {if $file.status_size == 'error' || $file.status_time == 'error' || $file.status_perm == 'error' || $file.status_md5 == 'error'}
              {assign var=style value=' style="background-color: red"'}
            {else}
              {assign var=style value=''}
            {/if}
            {assign var=red value=' style="background-color: red"'}
          {/if}
          <td{$style}><strong>{$count}. </strong>{$key|sanitize}</td>
          <td{if $file.status_size == 'error'}{$red}{/if} width="90" align="center">{$file.file_size}</td>
          <td{if $file.status_time == 'error'}{$red}{/if} width="120" align="center" nowrap="nowrap">{$file.file_time|date_format: '%d-%m-%Y, %H:%M:%S'}</td>
          <td{if $file.status_perm == 'error'}{$red}{/if} width="60" align="center" nowrap="nowrap">{$file.file_perm}</td>
          <td{if $file.status_md5 == 'error'}{$red}{/if} width="200" align="center" nowrap="nowrap">{$file.file_md5}</td>
        </tr>
      {/foreach}
    </table>
  {elseif $snap == 'scan'}
    <form action="index.php?do=settings&amp;sub=antivirus&amp;snap=scan&amp;noframes=1" method="post" name="nextform" id="nextform">
      <input type="hidden" name="scan_send" value="1" />
      <input type="hidden" name="count" value="{$count}" />
      <input type="hidden" name="gesamt" value="{$gesamt}" />
    </form>
    <table width="100%" border="0" cellpadding="4" cellspacing="1" class="tableborder">
      <tr>
        <td colspan="2" style="padding: 10px">
          <div class="nl_statusbar1">
            <div class="nl_statusbar2" style="width: {$bar}%"><strong>{$bar}%</strong></div>
          </div>
        </td>
      </tr>
      <tr>
        <td valign="top" width="40%">
          <table width="100%" border="0" cellspacing="1" cellpadding="2">
            <tr>
              <td class="headers">{#AntivirusScanFiles#}</td>
            </tr>
            {foreach from=$segments key=key item=file}
              <tr class="{cycle values='second, first'}">
                <td>{$key|sanitize}</td>
              </tr>
            {/foreach}
          </table>
        </td>
      </tr>
    </table>
  {elseif $snap == 'ignore'}
    {assign var=page value=$smarty.request.page|default:'1'}
    <form action="index.php?do=settings&amp;sub=antivirus&amp;snap=save" method="post" name="kform">
      <table width="100%" border="0" cellpadding="4" cellspacing="1" class="tableborder">
        <tr>
          <td valign="top">
            <table width="100%" border="0" cellspacing="1" cellpadding="2">
              <tr>
                <td class="headers"><a href="index.php?do=settings&amp;sub=antivirus&amp;snap=save{if !empty($smarty.request.seen)}&amp;seen=all{/if}&amp;sort={$revers}&amp;page={$page}">{#AntivirusWarnFiles#}</a></td>
                <td align="center" class="headers">{#AntivirusSignature#}</td>
                <td width="60" align="center" class="headers">{#GlobalString#}</td>
                <td width="60" align="center" class="headers">{#Global_Position#}</td>
                <td width="100" align="center" class="headers stip" title="{#Global_SelAll#}" id="check" style="cursor: pointer;">{#AntivirusInfoCheck#}</td>
              </tr>
              {foreach from=$infected key=key item=f}
                <tr class="{cycle values='second, first'}">
                  <td style="font-weight: bold;color: {if isset($f.ignore) && $f.ignore == 1}green{else}red{/if}">{$f.file|sanitize}</td>
                  <td width="230" align="center" style="font-weight: bold">{$f.virus|sanitize}</td>
                  <td width="60" align="center" style="font-weight: bold">{$f.line}</td>
                  <td width="60" align="center" style="font-weight: bold">{$f.position}</td>
                  <td width="100" align="center" nowrap="nowrap">
                    <label><input type="radio" class="check_ok" name="hash[{$key}]" value="1" {if $f.ignore == 1}checked="checked"{/if} />{#Yes#}</label>
                    <label><input type="radio" class="check_no" name="hash[{$key}]" value="0" {if empty($f.ignore) || $f.ignore == 0}checked="checked"{/if} />{#No#}</label>
                  </td>
                </tr>
              {/foreach}
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            {if !empty($Navi)}
              <div class="navi_div"> {$Navi} </div>
            {/if}
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <br />
            <input type="hidden" name="page" value="{$page}" />
            <input type="hidden" name="sort" value="{$sort}" />
            <input type="hidden" name="ignore_send" value="1" />
            <input type="submit" class="button" value="{#Save#}" />
            <input type="button" class="button" onclick="location.href = 'index.php?do=settings&amp;sub=antivirus&amp;snap=save&amp;ignore_send=1&amp;save=all&amp;sort={$sort}&amp;page={$page}';" value="{#AntivirusCheck#}" />
            <input type="button" class="button" onclick="location.href = 'index.php?do=settings&amp;sub=antivirus&amp;snap=save&amp;ignore_send=1&amp;sort={$sort}&amp;page={$page}';" value="{#AntivirusNoCheck#}" />
            {if empty($smarty.request.seen)}
              <input type="button" class="button" onclick="location.href = 'index.php?do=settings&amp;sub=antivirus&amp;snap=ignore&amp;seen=all&amp;sort={$sort}&amp;page={$page}';" value="{#Global_ShowAll#}" />
            {else}
              <input type="button" class="button" onclick="location.href = 'index.php?do=settings&amp;sub=antivirus&amp;snap=ignore&amp;sort={$sort}&amp;page={$page}';" value="{#AntivirusPageNav#}" />
            {/if}
          </td>
        </tr>
      </table>
    </form>
  {/if}
{/if}
