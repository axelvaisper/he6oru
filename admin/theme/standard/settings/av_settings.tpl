<div class="header">{#AntivirusModule#}</div>
<div class="subheaders">
  <a href="index.php?do=settings&amp;sub=antivirus"><img class="absmiddle" src="{$imgpath}/arrow_left.png" alt="" border="0" /> {#Global_Published#}</a>&nbsp;&nbsp;&nbsp;
  <a href="index.php?do=settings&amp;sub=antivirus&amp;snap=error"><img class="absmiddle" src="{$imgpath}/warning.gif" alt="" border="0" /> {#AntivirusFilesError#}</a>&nbsp;&nbsp;&nbsp;
  <a href="index.php?do=settings&amp;sub=antivirus&amp;snap=ignore"><img class="absmiddle" src="{$imgpath}/virus.png" alt="" border="0" /> {#AntivirusWarnFiles#}</a>&nbsp;&nbsp;&nbsp;
    {if $admin_settings.Ahelp == 1}
    <a class="colorbox" href="index.php?do=help&amp;sub={$helpquery}&amp;noframes=1"><img class="absmiddle" src="{$imgpath}/s_help.png" alt="" border="0" /> {#GlobalHelp#}</a>&nbsp;&nbsp;&nbsp;
    {/if}
  <a class="colorbox" href="index.php?do=support&amp;sub=send_order&amp;noframes=1"><img class="absmiddle" src="{$imgpath}/send.png" alt="" border="0" /> {#SendOrder#}</a>
</div>

<fieldset>
  <legend>{#Global_Actions#}</legend>
  <br />
  {assign var='style' value='float: left;width: 25%;text-align: center;font-size: 14px'}
  <div style="{$style}"><a href="index.php?do=settings&amp;sub=antivirus&amp;snap=base">1. {#AntivirusNewBase#}</a></div>
  <div style="{$style}">
    {if !empty($base.VirusBase)}
      <a class="colorbox" href="index.php?do=settings&amp;sub=antivirus&amp;snap=scan&amp;noframes=1">2. {#AntivirusScanBase#}</a>
    {else}
      <a href="index.php?do=settings&amp;sub=antivirus&amp;snap=start">2. {#AntivirusScanBase#}</a>
    {/if}
  </div>
  <div style="{$style}"><a href="index.php?do=settings&amp;sub=antivirus&amp;snap=new">3. {#AntivirusNewSnap#}</a></div>
  <div style="{$style}"><a href="index.php?do=settings&amp;sub=antivirus&amp;snap=check">4. {#AntivirusSnapCheck#}</a></div>
  <br />
  <br />
</fieldset>
<fieldset>
  <legend>{#Info#}</legend>
  <table width="100%" border="0" cellspacing="3" cellpadding="0">
    <tr>
      <td width="300"><strong>{#AntivirusDatumSnap#}</strong></td>
      <td><strong>{$base.Datum|date_format: '%d-%m-%Y, %H:%M:%S'}</strong></td>
    </tr>
    <tr>
      <td width="300"><strong>{#AntivirusDatumScan#}</strong></td>
      <td><strong>{$base.DatumCheck|date_format: '%d-%m-%Y, %H:%M:%S'}</strong></td>
    </tr>
    <tr>
      <td width="300"><strong>{#AntivirusStatus#}</strong></td>
      <td>
        {if $base.StatusCheck == 'ok'}
          <strong>{#Ok#}</strong>
        {else}
          <strong>{#Error#}</strong>
        {/if}
      </td>
    </tr>
    <tr>
      <td width="300"><strong>{#AntivirusVersion#}</strong></td>
      <td><strong>{$base.VersionBase}</strong></td>
    </tr>
  </table>
</fieldset>
<form action="" method="post">
  <fieldset>
    <legend>{#Exceptions#}</legend>
    <table width="100%" border="0" cellspacing="4" cellpadding="0">
      <tr>
        <td align="center">
          <strong>{#AntivirusScanBase#}</strong>
          <br />
          <select style="width: 200px;" name="ExtVir[]" size="15" multiple="multiple" id="select">
            {foreach from=$base.ExtAll item=p}
              <option value="{$p}" {if !empty($base.ExtVir) && in_array($p, $base.ExtVir)}selected="selected" {/if}> .{$p} </option>
            {/foreach}
          </select>
        </td>
        <td align="center">
          <strong>{#AntivirusNewSnap#}</strong>
          <br />
          <select style="width: 200px;" name="ExtScan[]" size="15" multiple="multiple" id="select">
            {foreach from=$base.ExtAll item=p}
              <option value="{$p}" {if !empty($base.ExtScan) && in_array($p, $base.ExtScan)}selected="selected" {/if}> .{$p} </option>
            {/foreach}
          </select>
        </td>
        <td align="center">
          <strong>{#Folders#}</strong>
          <br />
          <select style="width: 350px;" name="Folders[]" size="15" multiple="multiple" id="select">
            {foreach from=$folders item=p}
              <option value="{$p}" {if in_array($p, $base.Folders)}selected="selected" {/if}> {$p} </option>
            {/foreach}
          </select>
        </td>
      </tr>
    </table>
  </fieldset>
  <br />
  <input class="button" type="submit" value="{#Save#}" />
  <input name="save" type="hidden" id="save" value="1" />
</form>
