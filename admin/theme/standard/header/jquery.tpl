{if $browser == 'ie8' || $browser == 'ie7' || $browser == 'ie6'}
<script type="text/javascript" src="{$jspath}/jquery-1.11.1.js"></script>
{else}
<script type="text/javascript" src="{$jspath}/jquery-2.1.1.js"></script>
{/if}
<script type="text/javascript" src="{$jspath}/jpatch.js"></script>
<script type="text/javascript" src="{$jspath}/jquery.ui.js"></script>
<script type="text/javascript" src="{$jspath}/jtooltip.js"></script>
<script type="text/javascript" src="{$jspath}/jcolorbox.js"></script>
<script type="text/javascript" src="{$jspath}/functions.js"></script>