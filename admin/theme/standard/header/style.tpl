<meta http-equiv="Content-Type" content="text/html; charset={#Charset#}" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="imagetoolbar" content="no" />
<meta name="robots" content="none" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link type="text/css" rel="stylesheet" href="{$csspath}/ui.css" />
<link type="text/css" rel="stylesheet" href="{$csspath}/colorbox.css" />
<link type="text/css" rel="stylesheet" href="{$csspath}/style.css" />
{if $browser == 'ie8' || $browser == 'ie7' || $browser == 'ie6'}
<link type="text/css" rel="stylesheet" href="{$csspath}/ie.css" />
{/if}
