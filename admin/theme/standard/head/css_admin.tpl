<link rel="stylesheet" href="{$csspath}/grid_blueprint.css{#version_file#}" />
<link rel="stylesheet" href="{$csspath}/ui.css{#version_file#}" />
<link rel="stylesheet" href="{$csspath}/colorbox.css{#version_file#}" />
<link rel="stylesheet" href="{$csspath}/admin.css{#version_file#}" />
{if $browser == 'ie7' || $browser == 'ie8' || $browser == 'ie9'}
<link rel="stylesheet" href="{$csspath}/ie.css{#version_file#}" />
{/if}