{if $browser == 'ie7' || $browser == 'ie8' || $browser == 'ie9'}
<script src="{$jspath}/jquery-1.11.1.js{#version_file#}"></script>
{else}
<script src="{$jspath}/jquery-2.1.1.js{#version_file#}"></script>
{/if}
<script src="{$jspath}/jpatch.js{#version_file#}"></script>
<script src="{$jspath}/jquery.ui.js{#version_file#}"></script>
<script src="{$jspath}/jtooltip.js{#version_file#}"></script>
<script src="{$jspath}/jcolorbox.js{#version_file#}"></script>
<script src="{$jspath}/functions.js{#version_file#}"></script>

<script src="{$jspath}/jcookie.js"></script>
<script src="{$jspath}/jtoggle.js"></script>
<script src="{$jspath}/jvalidate.js"></script>
<script src="{$jspath}/jsuggest.js"></script>
<script src="{$jspath}/jblock.js"></script>
<script src="{$jspath}/jform.js"></script>
<script src="{$jspath}/ddaccordion.js"></script>
<script>
<!-- //
$(document).ready(function() {
    $('.colorbox').colorbox({ height: '97%', width: '90%', iframe: true, fastIframe: false });
    $('.colorbox_small').colorbox({ height: '70%', width: '60%', iframe: true, fastIframe: false });
    $('.stip').tooltip();
    $('#com').show();
    setTimeout(function() {
        $('#com_loader').hide();
    }, 500);
});
//-->
</script>