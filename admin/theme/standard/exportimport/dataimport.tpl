<script type="text/javascript">
<!-- //
{include file="$incpath/other/jsvalidate.tpl"}
$(document).ready(function() {
    $('#uimport').validate({
	rules: {
            pf: {
                required: true,
                minlength: 2,
                remote: 'index.php?do=dataimport&sub=checkprefix'
            }
        },
	messages: {
	    pf: {
                remote: $.validator.format("��� ������ � ����� ���������!")
            }
	},
        submitHandler: function() {
	    document.forms['uimport'].submit();
	},
        success: function(label) {
	    label.html("&nbsp;").addClass("checked");
	}
    });
});
// -->
</script>

<div class="header">�������������� ������</div>
<div class="subheaders">������ ������ ��������� ���������� ������ ���� ������ �� ������ Koobi 5-6 � ������ ���� ������ Status-X. ������ ���������, ������� ���������� ������, ������ ��������� � ����� ���� � ������� ������� Status-X � ����������� ���������� ������.
  <br />
  ��������� �������������� ������� �� ��������, ����� ���������� ���������� ����� �������� �������, � �������� ��� �������� �������� ����� � �������������!
  ���� �� ������ ������������ ������ � ����� <strong>&quot;/uploads/&quot;</strong>, �� ��� ���������� ����� ����������� ����� �������� ����� ��������� � ����� <strong>&quot;/uploads/&quot;</strong> Status-X
</div>
<form name="uimport" id="uimport" action="#end" method="post">
  {if $noAction == 1 && isset($smarty.post.save) && $smarty.post.save == 1}
    <div style="padding: 10px"> <h3>����������, �������� ��������...</h3> </div>
  {/if}
  <fieldset>
    <legend><label><input type="radio" name="action" value="users" /> ������ ������������� </label></legend>
    <div style="padding: 6px">
      <strong>������������� ��������� ������: </strong>������ �������������, ������������
      <br />
      <strong>{#Info#}: </strong>
      ��� ������� � ������� ������������ Status-X, ����� ������� ��� ������������ ������������, ����� ������������ � Id �1, ����� ����� ������� ��� ������ �������������, ����� ������ ���������������. ����� ������� ���������� ����������� ������ ����� ������� ��� �����!
    </div>
  </fieldset>
  <fieldset>
    <legend><label><input type="radio" name="action" value="news" /> ������ �������� </label></legend>
    <div style="padding: 6px">
      <strong>������������� ��������� ������: </strong>��������� ��������, �������
      <br />
      <strong>{#Info#}: </strong>
      ��� ���������� ����������� � ��������, ��� ���������� ����������� ����������� �� �������� <strong>&quot;/uploads/&quot;</strong> ������ ������ � ������� <strong>&quot;/uploads/&quot;</strong> CMS Status-X
    </div>
  </fieldset>
  <fieldset>
    <legend><label><input type="radio" name="action" value="gallery" /> ������ ������� </label></legend>
    <div style="padding: 6px">
      <strong>������������� ��������� ������: </strong>��������� �������, �������
      <br />
      <strong>{#Info#}: </strong>
      ��� ���������� ����������� �������, ��� ���������� ����������� ����������� �� �������� <strong>&quot;/uploads/galerie/&quot;</strong> ������ ������ � ������� <strong>&quot;/uploads/galerie/&quot;</strong> CMS Status-X
    </div>
  </fieldset>
  <fieldset>
    <legend><label><input type="radio" name="action" value="shop" /> ������ �������� </label></legend>
    <div style="padding: 6px">
      <strong>������������� ��������� ������: </strong>������������, ��������� �������, ������, �������� �������, ������, �������������
      <br />
      <strong>{#Info#}: </strong>
      ��� ���������� ����������� ������� ��������, ��� ���������� ����������� ����������� �� �������� <strong>&quot;/uploads/shop/icons/&quot;</strong> ������ ������ � ������� <strong>&quot;/uploads/shop/icons/&quot;</strong> CMS Status-X
    </div>
  </fieldset>
  <fieldset>
    <legend><label><input type="radio" name="action" value="manuf" /> ������ �������������� </label></legend>
    <div style="padding: 6px">
      <strong>������������� ��������� ������: </strong>�������������
      <br />
      <strong>{#Info#}: </strong>
      ��� ���������� ��������� ��������������, ��� ���������� ����������� ����������� �� �������� <strong>&quot;/uploads/manufacturer/&quot;</strong> ������ ������ � ������� <strong>&quot;/uploads/manufacturer/&quot;</strong> CMS Status-X
    </div>
  </fieldset>
  <fieldset>
    <legend><label><input type="radio" name="action" value="forums" /> ������ ������ </label></legend>
    <div style="padding: 6px">
      <strong>������������� ��������� ������: </strong>��� ������ ������
      <br />
      <strong>{#Info#}: </strong>
      ��� ���������� ������ ��������� � ��������� �� ������, ��� ���������� ����������� ����� �� �������� <strong>&quot;/uploads/attachment/&quot;</strong> ������ ������ � ������� <strong>&quot;/uploads/attachment/&quot;</strong> CMS Status-X
    </div>
  </fieldset>
  <fieldset>
    <legend><label><input type="radio" name="action" value="static" /> ������ ����������� ������� </label></legend>
    <div style="padding: 6px">
      <strong>������������� ��������� ������: </strong>��������� ����������� �������, ����������� ��������
    </div>
  </fieldset>
  <fieldset>
    <legend><label><input type="radio" name="action" value="articles" /> ������ ������ </label></legend>
    <div style="padding: 6px">
      <strong>������������� ��������� ������: </strong>��������� ������, ������, �������������, ���������
    </div>
  </fieldset>
  <fieldset>
    <legend><label><input type="radio" name="action" value="cheats" /> ������ �������� </label></legend>
    <div style="padding: 6px">
      <strong>������������� ��������� ������: </strong>��������� ��������, ���������, �������������, ���������
      <br />
      <strong>{#Info#}: </strong>
      �� ����������� ��������, ����������� � ������� ����������� �� ����� ���� �������������.
    </div>
  </fieldset>
  <fieldset>
    <legend><label><input type="radio" name="action" value="products" /> ������ ������� (�� �������) </label></legend>
    <div style="padding: 6px">
      <strong>������������� ��������� ������: </strong>������������, ������, �������������, ���������
      <br />
      <strong>{#Info#}: </strong>
      �� ����������� ��������, ����������� � ������� ����������� �� ����� ���� �������������.
    </div>
  </fieldset>
  <fieldset>
    <legend><label><input type="radio" name="action" value="downloads" /> ������ ������� </label></legend>
    <div style="padding: 6px">
      <strong>������������� ��������� ������: </strong>��������� �������, �������
      <br />
      <strong>{#Info#}: </strong>
      ��� ���������� ������ �������, ��� ���������� ����������� ����� �� �������� <strong>&quot;/uploads/files/&quot;</strong> ������ ������ � ������� <strong>&quot;/uploads/downloads_files/&quot;</strong> CMS Status-X
    </div>
  </fieldset>
  <fieldset>
    <legend><label><input type="radio" name="action" value="links" /> ������ ������ </label></legend>
    <div style="padding: 6px">
      <strong>������������� ��������� ������: </strong>��������� ������, ������
      <br />
      <strong>{#Info#}: </strong>
      �� ����������� ��������, ����������� � ������� ����������� �� ����� ���� �������������.
    </div>
  </fieldset>
  <fieldset>
    <legend><label><input type="radio" name="action" value="calendar" /> ������ ��������� </label></legend>
    <div style="padding: 6px">
      <strong>������������� ��������� ������: </strong>��� ������� � ���������
    </div>
  </fieldset>
  <fieldset>
    <legend><label><input type="radio" name="action" value="navi" /> ������ ��������� ����� </label></legend>
    <div style="padding: 6px">
      <strong>������������� ��������� ������: </strong>��������� ���������, ������� ���������
      <br />
      <strong>{#Info#}: </strong>
      � ������� ��������� ������������� ������ �������� � ������������, ������ ������� ���������� ����� �������� ������� ����� �������!
    </div>
  </fieldset>
  <a name="end"></a>
  <fieldset>
    <legend>������� ������</legend>
    <input id="pf" name="pf" type="text" class="input" value="{$smarty.request.pf|sanitize}" /> (������� ������� ������, �� ������� ����� �������� ������)
  </fieldset>
  {if !empty($res_articles)}
    <div class="subheaders" style="font-weight: normal"> �������� ������ <strong>{$res_articles|default:'0'}</strong> ������</div>
  {/if}
  {if !empty($res_prodcuts)}
    <div class="subheaders" style="font-weight: normal"> �������� ������ <strong>{$res_prodcuts|default:'0'}</strong> ������� (�� �������)</div>
  {/if}
  {if !empty($res_gallerycategs)}
    <div class="subheaders" style="font-weight: normal"> �������� ������ <strong>{$res_gallerycategs|default:'0'}</strong> ��������� ������� � <strong>{$res_gallery|default:'0'}</strong> ������� </div>
  {/if}
  {if !empty($res_usergroups)}
    <div class="subheaders" style="font-weight: normal"> �������� ������ <strong>{$res_usergroups|default:'0'}</strong> ����� ������������� � <strong>{$res_users|default:'0'}</strong> �������������</div>
  {/if}
  {if !empty($res_shopcategs)}
    <div class="subheaders" style="font-weight: normal"> �������� ������ <strong>{$res_shopcategs|default:'0'}</strong> ��������� �������� � <strong>{$res_shoparticles|default:'0'}</strong> ������� ��������</div>
  {/if}
  {if !empty($res_forums)}
    <div class="subheaders" style="font-weight: normal"> ����� ������������ </div>
  {/if}
  {if !empty($res_cheats)}
    <div class="subheaders" style="font-weight: normal"> �������� ������ <strong>{$res_cheats|default:'0'}</strong> ��������</div>
  {/if}
  {if !empty($res_newscategs)}
    <div class="subheaders" style="font-weight: normal"> �������� ������ <strong>{$res_newscategs|default:'0'}</strong> ��������� �������� � <strong>{$res_news|default:'0'}</strong> �������� </div>
  {/if}
  {if !empty($res_downloads)}
    <div class="subheaders" style="font-weight: normal"> �������� ������ <strong>{$res_downloads|default:'0'}</strong> ������� </div>
  {/if}
  {if !empty($res_links)}
    <div class="subheaders" style="font-weight: normal"> �������� ������ <strong>{$res_links|default:'0'}</strong> ������ </div>
  {/if}
  {if !empty($res_calendar)}
    <div class="subheaders" style="font-weight: normal"> �������� ������ <strong>{$res_calendar|default:'0'}</strong> ������� ��������� </div>
  {/if}
  {if !empty($res_static)}
    <div class="subheaders" style="font-weight: normal"> �������� ������ <strong>{$res_categ|default:'0'}</strong> ��������� ����������� ������� � <strong>{$res_static|default:'0'}</strong> ����������� ������� </div>
  {/if}
  {if !empty($res_navi_cat)}
    <div class="subheaders" style="font-weight: normal"> �������� ������ <strong>{$res_navi_cat|default:'0'}</strong> ��������� ��������� � <strong>{$res_navi|default:'0'}</strong> ������� ��������� </div>
  {/if}
  {if !empty($res_manuf)}
    <div class="subheaders" style="font-weight: normal"> �������� ������ <strong>{$res_manuf|default:'0'}</strong> �������������� </div>
  {/if}
  {if !empty($res_plattforms)}
    <div class="subheaders" style="font-weight: normal"> �������� ������ <strong>{$res_plattforms|default:'0'}</strong> �������� </div>
  {/if}
  <input name="save" type="hidden" id="save" value="1" />
  <input type="submit" class="button" value="���������" />
</form>
