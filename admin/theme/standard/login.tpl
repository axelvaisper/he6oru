<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>{#LoginName#}</title>
{include file="$incpath/head/meta_admin.tpl"}
{include file="$incpath/head/css_admin.tpl"}
{include file="$incpath/head/js_admin.tpl"}
</head>
<body gridx="row middle center">

<div gridx="col 4@m 3@b" cardx>
{$content}
</div>

{include file="$incpath/other/icon_admin.tpl"}

</body>
</html>
