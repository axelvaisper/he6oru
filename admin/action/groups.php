<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (!defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}

if (!perm('user_groups')) {
    SX::object('AdminCore')->noAccess();
}

switch (Arr::getRequest('sub')) {
    default:
    case 'useroverview':
        SX::object('AdminGroups')->show();
        break;

    case 'groupedit':
        SX::object('AdminGroups')->edit(Arr::getRequest('id'));
        break;

    case 'delgroup':
        SX::object('AdminGroups')->delete(Arr::getRequest('id'));
        break;

    case 'permissions':
        SX::object('AdminGroups')->showGroup();
        break;

    case 'editpermissions':
        SX::object('AdminGroups')->editGroup(Arr::getRequest('id'), Arr::getRequest('area'));
        break;

    case 'iconupload':
        $options = array(
            'type'   => 'image',
            'result' => 'ajax',
            'upload' => '/uploads/avatars/',
            'input'  => 'fileToUpload_' . Arr::getRequest('divid'),
            'resize' => Arr::getRequest('resize'),
        );
        SX::object('Upload')->load($options);
        break;
}
