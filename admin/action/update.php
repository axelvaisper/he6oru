<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (!defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}

define('NOLOGGED', true);
ignore_user_abort(true);
set_time_limit(600);

$DB = DB::get();

$DB->query("DELETE FROM `" . PREFIX . "_settings` WHERE Modul = 'admin' AND Name = 'Admin_Corners'");
$DB->query("DELETE FROM `" . PREFIX . "_settings` WHERE Modul = 'system' AND Name = 'RoundeCorners'");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('shop_cheaper', 'shop', 'cheaper', 'int', '1')");

$DB->query("RENAME TABLE `" . PREFIX . "_codeblock` TO `" . PREFIX . "_codewidget`");
$DB->query("ALTER IGNORE TABLE `" . PREFIX . "_bereiche` CHANGE `Type` `Type` ENUM('modul','extmodul','widget') NOT NULL DEFAULT 'modul'");
$DB->query("ALTER IGNORE TABLE `" . PREFIX . "_bereiche` CHANGE `Settings` `Settings` text");
$DB->query("ALTER IGNORE TABLE `" . PREFIX . "_bereiche` ADD `Result` varchar(100) NOT NULL DEFAULT ''");

$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('system_update', 'system', 'Update', 'int', '0')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('galerie_watermark_position', 'galerie', 'Watermark_Position', 'string', 'bottom_right')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('galerie_watermark_file', 'galerie', 'Watermark_File', 'string', 'watermark.png')");

$DB->query("ALTER IGNORE TABLE `" . PREFIX . "_benutzer` DROP COLUMN `GruppeMisc`");

$DB->query("UPDATE " . PREFIX . "_shop SET shop_wasserzeichen_position = 'bottom_right'");
$DB->query("ALTER IGNORE TABLE `" . PREFIX . "_bereiche` DROP INDEX `Name`");
$DB->query("ALTER IGNORE TABLE `" . PREFIX . "_bereiche` ADD UNIQUE `Name` (`Name`)");
$DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_kategorie` ADD `Gruppen` varchar(255) NOT NULL DEFAULT ''");
$DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_bestellungen` ADD `Payment` tinyint(1) DEFAULT '0'");
$DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_gutscheine` ADD `Hersteller` text");

$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('secure_active', 'secure', 'active', 'int', '1')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('secure_gd', 'secure', 'gd', 'int', '1')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('secure_ttf_font', 'secure', 'ttf_font', 'int', '1')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('secure_max_calc1', 'secure', 'max_calc1', 'int', '9')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('secure_max_calc2', 'secure', 'max_calc2', 'int', '9')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('secure_min_text', 'secure', 'min_text', 'int', '3')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('secure_max_text', 'secure', 'max_text', 'int', '4')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('secure_type', 'secure', 'type', 'string', 'auto')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('secure_text', 'secure', 'text', 'string', '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('forum_nofollow', 'forum', 'nofollow', 'int', '0')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('forum_compres', 'forum', 'compres', 'int', '80')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('forum_size', 'forum', 'size', 'int', '80')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('news_compres', 'news', 'compres', 'int', '80')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('news_size', 'news', 'size', 'int', '80')");

$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('users_usergallery', 'users', 'UserGallery', 'int', '1')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('users_limitalbom', 'users', 'LimitAlbom', 'int', '10')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('users_limitfotos', 'users', 'LimitFotos', 'int', '20')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('users_limitfotosstr', 'users', 'LimitFotosStr', 'int', '8')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('users_widthfotos', 'users', 'WidthFotos', 'int', '600')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('users_userfriends', 'users', 'UserFriends', 'int', '1')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('users_limitfriends', 'users', 'LimitFriends', 'int', '8')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('users_limitfriendsstr', 'users', 'LimitFriendsStr', 'int', '8')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('users_avatarfriends', 'users', 'AvatarFriends', 'int', '60')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('users_imagecompres', 'users', 'ImageCompres', 'int', '80')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('users_useractions', 'users', 'UserActions', 'int', '1')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('users_limitactions', 'users', 'LimitActions', 'int', '8')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('users_uservisits', 'users', 'UserVisits', 'int', '1')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('users_limitvisits', 'users', 'LimitVisits', 'int', '8')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('users_avatarwidth', 'users', 'AvatarWidth', 'int', '100')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('users_avatarcompres', 'users', 'AvatarCompres', 'int', '80')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('rss_all', 'rss', 'all', 'int', '10')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('rss_all_typ', 'rss', 'all_typ', 'int', '0')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('rss_news', 'rss', 'news', 'int', '15')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('rss_news_typ', 'rss', 'news_typ', 'int', '1')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('rss_articles', 'rss', 'articles', 'int', '15')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('rss_articles_typ', 'rss', 'articles_typ', 'int', '1')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('rss_forum', 'rss', 'forum', 'int', '15')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('rss_forum_typ', 'rss', 'forum_typ', 'int', '1')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('htaccess_auto', 'htaccess', 'auto', 'int', '1')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('htaccess_www', 'htaccess', 'www', 'int', '0')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('htaccess_lich', 'htaccess', 'lich', 'int', '0')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('htaccess_exts', 'htaccess', 'exts', 'string', 'gif|jpg|jpeg|bmp|png|swf|flv|mp3')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('htaccess_expires', 'htaccess', 'expires', 'int', '0')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('htaccess_headers', 'htaccess', 'headers', 'int', '0')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('htaccess_rewrite', 'htaccess', 'rewrite', 'int', '1')");

$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('system_allowed', 'system', 'allowed', 'string', '<br><br />')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('system_�omb_js', 'system', '�omb_js', 'int', '1')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('system_�omb_css', 'system', '�omb_css', 'int', '1')");
$DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('shop_shipping_info', 'shop', 'shipping_info', 'int', '1')");
$DB->query("ALTER IGNORE TABLE `" . PREFIX . "_laender` DROP COLUMN `KeineUstFirma`");
$DB->query("ALTER IGNORE TABLE `" . PREFIX . "_laender` ADD UNIQUE `Code` (`Code`)");

$string = "'system_upd_sys', 'system_htaccess_auto', 'system_usergallery', 'system_limitalbom', 'system_limitfotos', 'system_limitfotosstr', 'system_widthfotos', 'system_userfriends', 'system_limitfriends', 'system_limitfriendsstr', 'system_avatarfriends', 'system_useractions', 'system_uservisits', 'system_limitvisits', 'system_sicherheitscode', 'system_limitactions'";
$DB->query("DELETE FROM `" . PREFIX . "_settings` WHERE Id IN($string)");

switch (Arr::getRequest('repair')) {
    case 'birthdays': // ����������� ���� �������� �������������
        $sql = $DB->query("SELECT `Id`, `Geburtstag` FROM `" . PREFIX . "_benutzer`");
        while ($row = $sql->fetch_assoc()) {
            $DB->query("UPDATE `" . PREFIX . "_benutzer` SET `Geburtstag` = '" . Tool::formatDate($row['Geburtstag']) . "' WHERE `Id` = '" . $row['Id'] . "'");
        }
        break;

    case 'payment': // ��������������� ������ ������
        $DB->query("DROP TABLE IF EXISTS `" . PREFIX . "_shop_zahlungsmethoden`");
        $DB->query("CREATE TABLE IF NOT EXISTS `" . PREFIX . "_shop_zahlungsmethoden` (
        `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `Aktiv` tinyint(1) unsigned DEFAULT '1',
        `Name_1` varchar(100) DEFAULT NULL,
        `Name_2` varchar(100) NOT NULL,
        `Name_3` varchar(100) NOT NULL,
        `Beschreibung_1` text,
        `Beschreibung_2` text,
        `Beschreibung_3` text,
        `BeschreibungLang_1` text,
        `BeschreibungLang_2` text,
        `BeschreibungLang_3` text,
        `disabled_noweight` tinyint(1) unsigned DEFAULT '0',
        `Betreff` varchar(255) DEFAULT NULL,
        `Testmodus` varchar(45) DEFAULT '0',
        `Install_Id` varchar(255) DEFAULT NULL,
        `KostenOperant` enum('-','+') NOT NULL DEFAULT '-',
        `Kosten` decimal(8,2) unsigned DEFAULT '0.00',
        `KostenTyp` enum('wert','pro') DEFAULT 'wert',
        `Gruppen` varchar(255) NOT NULL DEFAULT '1,2,3,4,5,6',
        `Laender` text,
        `Versandarten` varchar(255) NOT NULL DEFAULT '1,2,3,4,5,6',
        `Position` smallint(2) unsigned NOT NULL DEFAULT '1',
        `DetailInfo` text,
        `Icon` varchar(200) DEFAULT NULL,
        `ZTyp` enum('Sys','Eigen') NOT NULL DEFAULT 'Sys',
        `MaxWert` decimal(8,2) NOT NULL DEFAULT '0.00',
        PRIMARY KEY (`Id`)
        ) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=cp1251");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_shop_zahlungsmethoden` VALUES (1,1,'����������� ������ ��� ����������� ���','','','����������� ������ ��� ����������� ���','','','<p>������ ������� ��������� - ������������ ������ �������� ������ �� ������������ �������. ����� ���������� ������ ����� ������������� ����������� ���� �� ������, ������� �� ������ ����������� � ��������.</p>\r\n<p>��� ����������� ��� ����������� ��������� (�������� ����� �� ������, ����-�������, ���������) �������� ������ � ������� ��� ���������.<br />\r\n&nbsp;</p>\r\n<h4>��������!</h4>\r\n<p>� ����� � ������������ ���������������� ������ �� ����� ���� �������, ����:<br />\r\n&nbsp;</p>\r\n<ol>\r\n    <li>����� �������� ��� ������������ ����������� ����, � ���������� ����������� ����.</li>\r\n    <li>����� �������� ��� ������������ ������������ ���� &quot;�&quot;, � ������ ���������� ����������� ���� &quot;�&quot;.</li>\r\n    <li>����� �������� ��� ������������ ������������ ����, � ������ ���������� ���������� ����.</li>\r\n</ol>\r\n<p>� ��������� ������� ����� �� ����� ������ � ���������.</p>','','',0,'','','','+',0,'pro','7,1,2,4,5,3,6','AZ,BE,KZ,KG,LV,LT,MD,RU,TJ,TM,UZ,UA,EE','8,3,6,1,7,2,4,5,9,10,11,12,13',2,'<strong>����� ���� ���������� ���������</strong><br />\r\n&nbsp;__ORDER__','beznal.jpg','Sys',0)");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_shop_zahlungsmethoden` VALUES (2,1,'������ ���������','','','<p>������ ���������</p>','','','<div style=\"text-align: justify\">������ ������ ��������� ��� ��� ���������. ������ � ������� �� ��������� �������� ���, ���������� ��� �������� ������� ������ ������.</div>','','',1,'','','','+',0,'wert','7,1,2,4,5,3,6','AZ,BY,KZ,KG,LV,LT,MD,RU,TJ,TM,UZ,UA,EE','8,3,6,1,7,2,4,5,9,10,11,12,13',1,'<br />','nal.jpg','Sys',0)");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_shop_zahlungsmethoden` VALUES (3,0,'������ 1','','','','','','','','',1,'','','','-',0,'pro','7,1,2,4,5,3,6','AZ,BY,KZ,KG,LV,LT,MD,RU,TJ,TM,UZ,UA,EE','8,3,6,1,7,2,4,5,9,10,11,12,13',3,'<br />','','Eigen',0)");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_shop_zahlungsmethoden` VALUES (4,0,'������ 2','','','','','','','','',0,'','','','-',0,'pro','7,1,2,4,5,3,6','AZ,BY,KZ,KG,LV,LT,MD,RU,TJ,TM,UZ,UA,EE','8,3,6,1,7,2,4,5,9,10,11,12,13',4,'<br />','','Eigen',0)");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_shop_zahlungsmethoden` VALUES (5,0,'������ 3','','','','','','','','',0,'','','','-',0,'pro','7,1,2,4,5,3,6','AZ,BY,KZ,KG,LV,LT,MD,RU,TJ,TM,UZ,UA,EE','8,3,6,1,7,2,4,5,9,10,11,12,13',5,'<br />','','Eigen',0)");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_shop_zahlungsmethoden` VALUES (6,0,'������ 4','','','','','','','','',0,'','','','-',0,'pro','7,1,2,4,5,3,6','AZ,BY,KZ,KG,LV,LT,MD,RU,TJ,TM,UZ,UA,EE','8,3,6,1,7,2,4,5,9,10,11,12,13',6,'<br />','','Eigen',0)");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_shop_zahlungsmethoden` VALUES (7,1,'������ ����� ��������, ����� ��-4','','','������ ����� <strong>��������</strong>, ����� ��-4<br />','','','<p>����� ��������� - ��������� �� ����� ��-4 ������������ ��� ������������� �������� ��������������� ������, ������� � ����� ��� ����������� ������ ������� � �����.</p>\r\n<p>������� � ������� ������������� ������������ ��������������� ��������� ������ ��������� �������� ������� (��������) �� ��������� ���������� ��� ��� �������� ���������� ������ � ������ ���������� ���������</p>\r\n<p>�������������� ����������� �������� ���������� ���������, ����������� ������� &laquo;� ������ � ���������� ������������&raquo;, ������� ��������� ������ � ����������� ��������� �� ������������� ���������� �������� � 1481, �������� 03.10.2002�. ����������� ������ ���������� ���������, ����������� ������������� ��������� ������ ������������ ����� �������� ��������-���������� ��� ��������� �������� � ������ ���������� ��������� ��� ������������ �� ����� ����������� ��� � ��������� ������� � �� ��������� ��������:</p>\r\n<ol>\r\n    <li>����� �������� �������������� ��� ������� ������������ ���������-����������� ������ ��������� ���������� � ������������ �����������, ������������ ��� ������������ �������� �� ����������. ��������� ��������� ����������� � ����������� ������� ����������, ����������-�������������� ����� ��� �� ���� ������ � ������ ��� ��������� �������, ������ ��� ����������� �����.</li>\r\n    <li>����� ������ �������� ��������� �������� �������������� ������ ��� ������������ ���������, ��������������� ��������, �� ����������� ��������� ��������, ���� �� ����� �� ��������� 30 000 ������:\r\n    <ul>\r\n        <li>��������� � ��������� � ��������� ���� ������� ��������� ������� ���������� ��������� (������� ��������������� ����������������� ���������� ��������� � ������� � ������ �����������, ������������ � ������� ������ � �����, � ����� ���� � ������);</li>\r\n        <li>��������� � ������� �����, ����������� ���������� ������������, ������������ � ������� ����������� ������� �������������� ������, ������� �������������� ������ ��������� ���������� ��������� � ������� �������� ��������������;</li>\r\n        <li>��������� � �������������� ����� �� ����� ���������, ������������ ������, � ������� ����� �� ������ ������� � ��������� �������� ������������, � ����� � �������������� �������� �� ������ �����;</li>\r\n        <li>��������� � ������� ������� ������� �������������, ��������������, ������ �������������� ����������� �������, �������-������������ ������������, ������� ����� ������� ������������� �������;</li>\r\n        <li>��������� � ������� ���������.</li>\r\n    </ul>\r\n    </li>\r\n    <li>� ������������� ������ ������� ��������-���������� ����� �������� ��������� ��������� ����������.</li>\r\n    <li>������������ �������� �� ��������-���������� ��� ���� �������� ����������� ����� ������������ � �����, ������������� ����������, ������������ � ������������ ������, ���� ����������������� ���������� ���������.</li>\r\n    <li>����� �������� ��������-���������� ��� � ������ ����������� ���, � �������� ��������� �������� �� ����� ��������, �������������� � ������������ � ��������� ��������� � ���������� ���������. ��� ���������� ���������- � ������������ � ���������� ��������� � ��������� �����, ������������� ��������� ������� �� ������, ��������������� ���������� ������, �� ���� �������� ������.</li>\r\n    <li>����� ��������, ��� ���������� � ��������� ���������� ����������, ����������� ��� ������������ �������� �� ����������, ���� � ������ ���������� � ��������-���������� ��� �������� ���������� � �����, ��������� � ��������� ����������, �� ������������.</li>\r\n    <li>�� ������� ��������-���������� ��� � ������� ���� ��� � ���� ������ �������� �������� ������� � ������������� �������� � ����� �� ������������ � ����� ����������� ��� �� ��������� ������������� ���������-����������� ������ ��������� ���������� �� ������. ������ ������ ����������� ���������� ����� �� ��������, ��������������� ��������� ������� �� ������, ��������������� ���������� ������, �� ���� �������� ������.</li>\r\n</ol>\r\n<br />\r\n<p>������� � ������� ������������� ������������ ��������������� ��������� ������ ��������� �������� ������� (��������) �� ��������� ��������-���������� ��� ��� �������� ���������� ������ � ������ ���������� ��������� ��������� ��������� ���������-����������� ������ ��� ���������� ��� ��������� ���������� �� ������������ �������� �������.</p>','','',0,'','','','-',0,'pro','7,1,2,4,5,3,6','AZ,BY,KZ,KG,LV,LT,MD,RU,TJ,TM,UZ,UA,EE','8,3,6,1,7,2,4,5,9,10,11,12,13',7,'<br />','sberbank.jpg','Eigen',0)");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_shop_zahlungsmethoden` VALUES (8,1,'������ ����� ������� INTERKASSA','','','������ ����� ������� <strong>INTERKASSA<br />\r\n</strong>','','','������� ������ �������� <strong><a href=\"http://www.interkassa.com/\">INTERKASSA</a></strong> ������������ ����� ������������� ���������-����������� ��������, ����������� ��������, �������������� ��������� �������� �� �������� ��������� ��������-������.','','',0,'','','','-',0,'pro','7,1,2,4,5,3,6','AZ,BY,KZ,KG,LV,LT,MD,RU,TJ,TM,UZ,UA,EE','8,3,6,1,7,2,4,5,9,10,11,12,13',8,'<br />','interkassa.jpg','Sys',0)");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_shop_zahlungsmethoden` VALUES (9,1,'������ ����� ������� WebMoney','','','<div style=\"text-align: justify\">������ ����� ������� <strong>WebMoney</strong></div>','','','<div style=\"text-align: justify\">������� ������� <strong><a href=\"http://www.webmoney.ru/\">WebMoney</a> </strong>������������ ���������� �������� � �������� ������� ����������� ������� ������ &mdash; ��������� ������ WebMoney (WM-units). ������� �������� �� ����������. ���������� ��������� ��������� ������ �������������� �������������� � ������� ���������� ��������� WM Keeper ��� � ������� ���-���������� (WM Keeper Light).</div>','','',0,'','3','','-',0,'pro','7,1,2,4,5,3,6','AZ,BY,KZ,KG,LV,LT,MD,RU,TJ,TM,UZ,UA,EE','8,3,6,1,7,2,4,5,9,10,11,12,13',9,'<br />','webmoney.jpg','Sys',0)");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_shop_zahlungsmethoden` VALUES (10,1,'������ ����� ������� PayPal','','','<div style=\"text-align: justify\">������ ����� ������� <strong>PayPal<br />\r\n</strong>&nbsp;</div>','','','<p style=\"text-align: justify\"><strong><a href=\"https://www.paypal.com/\">PayPal</a></strong> &mdash; ���������� � ���� ��������� ����������� �������� �������. � ��������� ����� PayPal �������� � 190 ������� � ����� ����� 164 ��������� ������������������ �������������. PayPal �������� � 18 ������������� ��������. C ������� 2002 ���� �������� �������������� �������� eBay.<br />\r\n<br />\r\n������� �������������� ����� ���������� ���������� ����� �������� e-mail � ������, ��������� ����� ������������� ��������. � ������� ������� ������ �����, �� �������� ����� ������������ �������. ������������ PayPal ����� ���������� ������ ���� �����.</p>\r\n<p style=\"text-align: justify\">������������� �������� �������� � ���� ��������� ������ ����� � ����� ������������ � ��������� ����, ������� ���������� �������� PayPal, ��� ������������ ������������ ��������� �����, �������� ������ � ������� ��������, ��������, �������� ������ � ��������� ������ � ������� Paypal.</p>\r\n<p style=\"text-align: justify\">� 2007 ���� ��� ������ ������ � ��� ������� � ��� ������� ����� ��� (��� ����� ����� ��������). �� 2005 ���� ��� ����������� ����� ��������� ����� ������ ��������������. ������� ����� ��������� � ������� ���, � ������� �� ����������� ������ �����, ���. � ������ 2008 ���� � PayPal �������� ������������� ���������.</p>\r\n<p style=\"text-align: justify\">������������� ������� PayPal �������������� �� ���������� ������: ����������� � ������� ���������, �� ����������� �������� ������� �������� � ������������ �� ���������, �� ����������� ����������������� �������� (Premier � Business). �������� ��������� � ���������� �������, ������ �������� ������� �� �������������� ������ ������������ � �������.</p>','','',0,'','','','-',0,'wert','7,1,2,4,5,3,6','AZ,BY,KZ,KG,LV,LT,MD,RU,TJ,TM,UZ,UA,EE','8,3,6,1,7,2,4,5,9,10,11,12,13',10,'<br />','paypal.jpg','Sys',0)");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_shop_zahlungsmethoden` VALUES (11,1,'������ ����� ������� Moneybookers','','','<div style=\"text-align: justify\">������ ����� ������� <strong>Moneybookers</strong></div>','','','<div style=\"text-align: justify\"><strong><a href=\"http://www.moneybookers.com/\">Moneybookers</a></strong> &mdash; �������� �������� ���������, ������� ��������� �������� � �������� ������ ����� ����������� �����. �������� 18 ���� 2001 ���� � �������.<br />\r\n<br />\r\n������������ ����� ������� ������ � ��������� ��� ��������� ����� � ���������� ������ �� ����� � ����� � ����������� ����� &mdash; ������ ����������� �������������� �������������� � ��������. Moneybookers ����� ��������� ��� ���������� � ������� ������� (������� ����� ����������� ����� �������� 0,5 ����). ������� ����� �������� ��� ����������������� ������������ � ����������� ������� ���������� ���������. ������ ����� �� ����� ����� ���� ����������� ���������� �����, �� �������� ��� ��������� ����� VISA ��� ������������� SWIFT-��������� �� ������ ������.</div>\r\n<p style=\"text-align: justify\">��� ���� ������������ Moneybookers �� ��������� ������������ �������� �� ������ ������������� ������� �������� ��� ����. � ������� �� ������ ������������� ������ ����� (�������� ������� ������), Moneybookers ������� �������� �������� ����� �������������� �� ������������; ��� ������������ ������������� � ������������� ��������� �����. �������������� ���� �������� ��������� ������������ ������������ ���������� ����� � ����� ��������� ���������� ����� ���������� ����� 35 000 USD (��� �� �����������) � �������� 90-�������� �������. � �������� �������� �������� ������������ �������� � ��������� ���������� ��������, �������� ����� ��������, �������� ����� � ��������. Moneybookers ������ �� ��������� � ���������� ������, � ������������� ���������� �� ��������� ����� ���� ����������.</p>\r\n<p style=\"text-align: justify\">Moneybookers &mdash; ���� �� ���������� ������ ������������ ������, ������� ����� ���������� �������� �� eBay �� �������� �������� �������� eBay. ����� ����, ��������� ������� Moneybookers �������� ���������� ������� � ����� ��������, ��������� � ������-������, ������������� ���������� � ������� ��������� ��������� �������� ��� � ���������.<br />\r\n&nbsp;</p>','','',0,'','','','-',0,'wert','7,1,2,4,5,3,6','AZ,BY,KZ,KG,LV,LT,MD,RU,TJ,TM,UZ,UA,EE','8,3,6,1,7,2,4,5,9,10,11,12,13',11,'<br />','moneybookers.jpg','Sys',0)");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_shop_zahlungsmethoden` VALUES (12,1,'������ ����� ������� Worldpay','','','������ ����� ������� <strong>Worldpay</strong><br />','','','<div style=\"text-align: justify\">���������� ��������� ������� <strong><a href=\"http://www.rbsworldpay.com/\">RBS WorldPay</a></strong> ������������� ����������� ��������� ������� � �������� ������ Royal Bank of Scotland Group.</div>','','',0,'','','','-',0,'pro','7,1,2,4,5,3,6','AZ,BY,KZ,KG,LV,LT,MD,RU,TJ,TM,UZ,UA,EE','8,3,6,1,7,2,4,5,9,10,11,12,13',12,'<br />','worldpay.jpg','Sys',0)");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_shop_zahlungsmethoden` VALUES (13,1,'������ ����� ������� Z-Payment','','','������ ����� ������� <strong>Z-Payment<br />\r\n</strong>','','','<div style=\"text-align: justify\"><strong><a href=\"http://www.z-payment.ru/\">Z-PAYMENT</a></strong> - ��� ������������� ��������� ����������, ������������� ��������� ����� ������ � ������ ��������������� ��������. ������� �������� ������ � �������� �������, � ������� ������� ����� ����������� ����� ������ �������: �������� ����� �������� ������ ������ �������, ��������; ��������� ������� ����������� �����; ������ ���; ������ ����������� ������ � ������ ������ <br />\r\nZ-PAYMENT - ������������ �������, ���������� ���� ������ � 2003 ����, ����� ���� ����������� ������ ��������������� ��������� TRANSACTOR. ��� ���������������������� ���������� ��� ������������� ������ �������� ��������� �������. ��� ��� ����� ������� ������������������ � �����������, �������������� ���������, ������� ������������, ���������� � ������������. ���� ����������� ����, ����������� � ��������������� ������� ���� ������������ ��� �������� Z-PAYMENT.<br />\r\n����� ������������ �������� ����� ��������� ������� ���� ����������� ���� � Z-PAYMENT � �������� ����������� ������������� ��������� �������� ������� - ��� ��������, ���������� � ��������� �� ����. ��� �������� � Z-PAYMENT ���������� � ������ on-line � �������������� �� ����� ����������� ������� �� �������, �.�. ���������. <br />\r\n������������ ������������ ��������� Z-PAYMENT �� �������������� ������ ������������ ����� �������, �� �������� � ���� ������������ ����� ������� ������� � ����� �������.<br />\r\n������� ������� �������� � ������� �������� ����� ���������� ���������, � ��������� �� �������. 1 zp = 1 �����. ��� ��������� ������� ������� �������� ������� ������� � ������ ������������ ����� ������.<br />\r\n&nbsp;</div>','','',0,'','','','-',0,'pro','7,1,2,4,5,3,6','AZ,BY,KZ,KG,LV,LT,MD,RU,TJ,TM,UZ,UA,EE','8,3,6,1,7,2,4,5,9,10,11,12,13',13,'<br />','zpayment.jpg','Sys',0)");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_shop_zahlungsmethoden` VALUES (14,1,'������ ����� ������� ASSIST','','','������ ����� ������� <strong>ASSIST</strong><br />','','','<p style=\"text-align: justify\"><strong><a href=\"http://www.assist.ru/\">ASSIST</a></strong> - ��� ���������������� ������� �������� �� ����������� � ����������� ������ ����� ��������, ����������� � �������� ������� ����������� ����������� � ��������� ����c�����.</p>\r\n<p style=\"text-align: justify\">� ���������� � ������������ ������ ���� VISA, MasterCard, <strong>ASSIST</strong> ����� ������������� ����������� ������ ����������� ����������� &ndash; WebMoney, ������.������, e-port, Kredit Pilot � ������ ������� ����������������� ����������.</p>\r\n<p style=\"text-align: justify\">�������������� <strong>ASSIST</strong> �������� ��������-�������� � ����������. ��������-������� �������� ����������� ��������� ����� �������� ����������� ����� � ����������� ���������� ��� ������������ ������������ ����������-����������� ���������. ���������� ����������� �������� � ���������� ������ ������.</p>\r\n<p style=\"text-align: justify\">�������, ���������� � �������������� ������� <strong>ASSIST</strong>, ��������� ������������� ���������������� �� � ������������ ���������������� �������� ������������ ������� ���������� ��������� (�� ��). ������� � �������������� ���������� ��������� �������� ���������� �� ����� MOTO (Mail Order Telephone Order) � ������� ������������ �������� ��������� ������ (VISA, Europay � ��.).<br />\r\n&nbsp;</p>','','',0,'','','','-',0,'pro','7,1,2,4,5,3,6','AZ,BY,KZ,KG,LV,LT,MD,RU,TJ,TM,UZ,UA,EE','8,3,6,1,7,2,4,5,9,10,11,12,13',14,'<br />','assist.jpg','Sys',0)");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_shop_zahlungsmethoden` VALUES (15,1,'������ ����� ������� RBK Money','','','<p style=\"text-align: justify\">������ ����� ������� <strong>RBK Money</strong></p>','','','<p style=\"text-align: justify\"><strong><a href=\"https://rbkmoney.ru/\">RBK Money</a></strong> (� ������� &mdash; RUpay) &mdash; ����������� �������� �������, ������� ����� ������� �������� ���������� � ���������� ���������� �������� �������� � ��������� ����������� ������. ������� ���������� �� ����������� ������������.<br />\r\n<br />\r\n����� ������� ���������������� RBK Money ������ ��� �������� ������: ����� �������� ����� ������� ���� � ���� �������, ���������� ������ �������, �������� ������������ ����� ����������� ��� ������ ������� ������� � ������ ����� � ��������� (��� �������, ��� ������� �������� �������� � �������� RBK Money). ������������ ����� ��������� ���� ���������� ��������� �������� (���������, ���������� ���������, ��������� �� ������ ����������� �������� ������, ��������� ������� ����� ������� RBK Money (��������, ������� ���������� ��������-��������); �������� �������� �� ������ �����, ����� ���������� ��������. �� �������� �� ���������� ��������� ��������.</p>\r\n<p style=\"text-align: justify\">�������� ��������� �������� ������� � RBK Money �������� ����� �������� �������, ������������� �����.</p>\r\n<p style=\"text-align: justify\">����� ������������ �������� �����/������ ������� ����������� ���� ����������� RBK Money ������� �������� �������, � ������� ��� ����������/����� �������� ������������ ���������� �������� ��������� ����� ���� ����.</p>\r\n<p style=\"text-align: justify\">������ � RBK Money ��������� ����������� ����� ���-���������, ��� ��������������� ��� ������������ �������, ��������� �� ��������� ������������� ����������� ����������� ��������, � ������� � ������ ������� �������� ��� ���� ������������ ������.</p>','','',0,'','RUR','','-',0,'pro','7,1,2,4,5,3,6','AZ,BY,KZ,KG,LV,LT,MD,RU,TJ,TM,UZ,UA,EE','8,3,6,1,7,2,4,5,9,10,11,12,13',15,'<br />','rbk.jpg','Sys',0)");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_shop_zahlungsmethoden` VALUES (16,1,'������ ����� ������� ROBOKASSA','','','������ ����� ������� <strong>ROBOKASSA<br />\r\n</strong>','','','<p style=\"text-align: justify\"><strong><a href=\"http://www.robokassa.ru/\">ROBOKASSA</a></strong> - ��� ������, ����������� ��������� (��������-���������, ����������� �����) ��������� ������� �� �������� � ����� ����������� ������, � ������� sms-���������, ����� ������� �������� ��������� Contact, � ����� ��������� ���������� ������. ��� ���� ��������-��������� ����� �� ����� ���������� � ���� ��������� ������� �������������� - �� ��� ������� ��� �� ���. �� ������ ������ �������� �� ����� �������� � ���������� �� �� ��� ��������� ����.</p>\r\n<p style=\"text-align: justify\"><strong>ROBOKASSA</strong> - ������ ������� ROBOXchange.com, �������� ������ � ����� ����������� ������ � ���������� ����������� �����. � ������� ������� ������� � ROBOKASS� ������������ ����� 6500 ����������� ��������-���������.<br />\r\n&nbsp;</p>','','',0,'','','','-',0,'pro','7,1,2,4,5,3,6','AZ,BY,KZ,KG,LV,LT,MD,RU,TJ,TM,UZ,UA,EE','8,3,6,1,7,2,4,5,9,10,11,12,13',16,'<br />','robokassa.jpg','Sys',0)");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_shop_zahlungsmethoden` VALUES (17,1,'������ ����� ������� LiqPAY','','','������ ����� ������� <strong>LiqPAY<br />\r\n</strong>','','','<div style=\"text-align: justify\"><strong><a href=\"http://www.liqpay.com/\">LiqPAY</a></strong> &mdash; �������� ��������� �������, ������� ��������� ��������� ������ � ������� ���������� ��������, ��������� � ��������� ���� �� ��� ����.<br />\r\n<br />\r\n������������ ����������� ����������� OTP (One-time Password - ����������� ������), � ����� ����������� 3D secure code. �������� �������������� ������������ ����������� �������, ������� ���������� � SMS-���������.</div>','','',0,'','RUR','','-',0,'pro','7,1,2,4,5,3,6','AZ,BY,KZ,KG,LV,LT,MD,RU,TJ,TM,UZ,UA,EE','8,3,6,1,7,2,4,5,9,10,11,12,13',17,'<br />','liqpay.jpg','Sys',0)");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_shop_zahlungsmethoden` VALUES (18,1,'������ ����� ������� Pay2Pay','','','������ ����� ������� <strong>Pay2Pay<br /></strong>','','','<div style=\"text-align: justify\"><b>������� ����������� �������� <a href=\"http://www.pay2pay.com/\">Pay2Pay</a></b> � ��� ������� ������ ������ �������� ������, ����� ��� ���������� �����, ������� ����������� �����, ��������� ����������������, ���������� ��������� �������. ��������� ������ �������� �� ���������� ����� ������, ��� ����������� ������������������ ������������ ����������.</div>','','',0,'','1','','-',0,'pro','7,1,2,4,5,3,6','AZ,BY,KZ,KG,LV,LT,MD,RU,TJ,TM,UZ,UA,EE','8,3,6,1,7,2,4,5,9,10,11,12,13',18,'<br />','pay2pay.jpg','Sys',0)");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_shop_zahlungsmethoden` VALUES (19,1,'������ ����� ������� ������ �������','','','������ ����� ������� <strong>������ �������</strong><br />','','','<div style=\"text-align: justify\"><b>��������� ������ <a href=\"http://www.walletone.com/?ref=157063999778\">������ �������</a></b> &ndash; ��� ������� � ������� ������ ������ ����� � ���������� �������� ��� ���������� ����� ��������. �������� ����� ��������-��������, ������������� � ������ �����, ����� ����� ��� � 300 000 ������� ������ �������� &mdash; � ����������, ������� �����, ������������� �����������, �������� �����, ����������, ���������� ����� ������, ��������� � ����� ���������� ������.</div>','','',0,'','643','','-',0,'pro','7,1,2,4,5,3,6','AZ,BY,KZ,KG,LV,LT,MD,RU,TJ,TM,UZ,UA,EE','8,3,6,1,7,2,4,5,9,10,11,12,13',19,'<br />','w1.jpg','Sys',0)");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_shop_zahlungsmethoden` VALUES (20,1,'������ ����� ������� EasyPay.by','','','������ ����� ������� <strong>EasyPay.by<br /></strong>','','','<div style=\"text-align: justify\"><b>EasyPay</b> � ������ ����������� ������� ����������� �����, ��������������� ��� ������������� �������� � ��������� � � ������� SMS-���������. ����������� ������ EasyPay ������������� ��� ������������� ��������� ����� � ����������� ��������-��������� ��� ������� �������� �������� ����� ��������. ������ ���������� ����� ������� � ������ ������ �������� � ��������, ��� ������ � ������� ����� ��������� � ������� ����� � � ����� ����� � �� ����� www.easypay.by. �������� �������� � ������� �������� ����������� �����.</div>','','',0,'','','','-',0,'pro','7,1,2,4,5,3,6','AZ,BY,KZ,KG,LV,LT,MD,RU,TJ,TM,UZ,UA,EE','8,3,6,1,7,2,4,5,9,10,11,12,13',20,'<br />','easypay.jpg','Sys', '0')");
        break;

    case '1.04': // ������ ��� ���� ������� ������ 1.05
        $DB->query("CREATE TABLE IF NOT EXISTS `" . PREFIX . "_shop_eigenschaften` (
  `Id` smallint(2) NOT NULL AUTO_INCREMENT,
  `Name_1` VARCHAR(100) NOT NULL DEFAULT '',
  `Name_2` VARCHAR(100) NOT NULL DEFAULT '',
  `Name_3` VARCHAR(100) NOT NULL DEFAULT '',
  `StartText_1` text,
  `StartText_2` text,
  `StartText_3` text,
  `StartText_1_zeigen` enum('0','1') NOT NULL DEFAULT '0',
  `StartText_2_zeigen` enum('0','1') NOT NULL DEFAULT '0',
  `StartText_3_zeigen` enum('0','1') NOT NULL DEFAULT '0',
  `Name_1_zeigen` enum('0','1') NOT NULL DEFAULT '0',
  `Name_2_zeigen` enum('0','1') NOT NULL DEFAULT '0',
  `Name_3_zeigen` enum('0','1') NOT NULL DEFAULT '0',
  `Sektion` smallint(2) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`)
  ) ENGINE=MyISAM DEFAULT CHARSET=cp1251");

        $eigenschaften = $DB->fetch_assoc("SELECT * FROM " . PREFIX . "_shop_eigenschaften LIMIT 1");
        if (empty($eigenschaften['Id'])) {
            $DB->query("INSERT IGNORE INTO `" . PREFIX . "_shop_eigenschaften` (`Id`) VALUES ('1')");
        }

        $DB->query("CREATE TABLE IF NOT EXISTS `" . PREFIX . "_shop_tracking` (
  `Id` smallint(3) unsigned NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(100) NOT NULL,
  `Hyperlink` VARCHAR(255) NOT NULL,
  `TrNr` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`)
  ) ENGINE=MyISAM DEFAULT CHARSET=cp1251");

        $DB->query("CREATE TABLE IF NOT EXISTS `" . PREFIX . "_shop_kundendownloads` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `Bestellung` int(10) unsigned NOT NULL,
  `Kunde` int(10) unsigned NOT NULL,
  `Datum` int(14) unsigned NOT NULL,
  `Datei` VARCHAR(255) NOT NULL,
  `Titel` VARCHAR(200) NOT NULL DEFAULT '',
  `Beschreibung` text,
  `Downloads` int(5) unsigned NOT NULL,
  KEY Id (`Id`)
  ) ENGINE=MyISAM DEFAULT CHARSET=cp1251");

        $DB->query("CREATE TABLE IF NOT EXISTS `" . PREFIX . "_shop_warenkorb` (
  `Id` int(14) unsigned NOT NULL AUTO_INCREMENT,
  `Benutzer` int(14) unsigned NOT NULL,
  `ZeitBis` int(14) unsigned NOT NULL,
  `ZeitBisRaw` VARCHAR(20) NOT NULL,
  `Inhalt` text,
  `InhaltKonf` longtext,
  `Gesperrt` enum('0','1') NOT NULL DEFAULT '0',
  `EingeloestAm` int(14) NOT NULL,
  `EingeloestAmRaw` VARCHAR(20) NOT NULL,
  `Code` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`Id`)
  ) ENGINE=MyISAM DEFAULT CHARSET=cp1251");

        $DB->query("CREATE TABLE IF NOT EXISTS `" . PREFIX . "_bookmarks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT '0',
  `document` VARCHAR(200) DEFAULT NULL,
  `doc_name` VARCHAR(200) DEFAULT NULL,
  `bookmark_time` int(14) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
  ) ENGINE=MyISAM DEFAULT CHARSET=cp1251");

        $DB->query("CREATE TABLE IF NOT EXISTS `" . PREFIX . "_banned` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `User_id` VARCHAR(10) DEFAULT NULL,
  `Reson` VARCHAR(255) DEFAULT NULL,
  `Type` enum('bann','autobann') NOT NULL DEFAULT 'bann',
  `TimeStart` int(11) unsigned NOT NULL DEFAULT '0',
  `TimeEnd` int(11) unsigned NOT NULL DEFAULT '0',
  `Name` VARCHAR(255) DEFAULT NULL,
  `Email` VARCHAR(200) DEFAULT NULL,
  `Ip` VARCHAR(25) DEFAULT NULL,
  `Aktiv` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`)
  ) ENGINE=MyISAM DEFAULT CHARSET=cp1251");

        $DB->query("CREATE TABLE IF NOT EXISTS `" . PREFIX . "_phrases` (
  `id` mediumint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL DEFAULT '',
  `phrase` text,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
  ) ENGINE=MyISAM DEFAULT CHARSET=cp1251");

        $DB->query("CREATE TABLE IF NOT EXISTS `" . PREFIX . "_description` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Text` VARCHAR(70) DEFAULT NULL,
  `Aktiv` enum('1','0') NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`)
  ) ENGINE=MyISAM DEFAULT CHARSET=cp1251");

        $DB->query("CREATE TABLE IF NOT EXISTS `" . PREFIX . "_ping` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Dokument` varchar(255) NOT NULL,
  `Aktiv` enum('1','0') NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`)
  ) ENGINE=MyISAM DEFAULT CHARSET=cp1251");

        $DB->query("CREATE TABLE IF NOT EXISTS `" . PREFIX . "_navi_flashtag` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(100) NOT NULL DEFAULT '',
  `Size` smallint(2) unsigned NOT NULL DEFAULT '10',
  `Dokument` varchar(255) NOT NULL,
  `Aktiv` enum('1','0') NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`)
  ) ENGINE=MyISAM DEFAULT CHARSET=cp1251");

        $DB->query("CREATE TABLE IF NOT EXISTS `" . PREFIX . "_sitemap_items` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL DEFAULT '',
  `active` varchar(250) NOT NULL DEFAULT '1',
  `prio` varchar(250) NOT NULL DEFAULT '0.5',
  `changef` varchar(255) NOT NULL DEFAULT 'always',
  PRIMARY KEY (`Id`)
  ) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=cp1251");

        $DB->query("CREATE TABLE IF NOT EXISTS `" . PREFIX . "_roadmap` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL default '',
  `Beschreibung` text,
  `Aktiv` tinyint(1) unsigned NOT NULL default '1',
  `Pos` varchar(250) NOT NULL default '',
  PRIMARY KEY  (`Id`)
  ) ENGINE=MyISAM DEFAULT CHARSET=cp1251");

        $DB->query("CREATE TABLE IF NOT EXISTS `" . PREFIX . "_roadmap_tickets` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Rid` varchar(10) NOT NULL default '',
  `Beschreibung` text,
  `Datum` varchar(250) NOT NULL default '',
  `Fertig` tinyint(1) unsigned NOT NULL default '0',
  `Uid` varchar(250) NOT NULL default '',
  `pr` varchar(10) NOT NULL default '',
  PRIMARY KEY  (`Id`)
  ) ENGINE=MyISAM DEFAULT CHARSET=cp1251");

        $DB->query("CREATE TABLE IF NOT EXISTS `" . PREFIX . "_admin_notes` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `UserId` int(10) unsigned NOT NULL,
  `Datum` int(14) unsigned NOT NULL,
  `Text` text,
  `Type` enum('main','pub') NOT NULL DEFAULT 'main',
  PRIMARY KEY (`Id`)
  ) ENGINE=MyISAM DEFAULT CHARSET=cp1251");

        $DB->query("CREATE TABLE IF NOT EXISTS `" . PREFIX . "_user_friends` (
  `Id` int(10) unsigned NOT NULL auto_increment,
  `BenutzerId` varchar(15) default NULL,
  `FreundId` varchar(25) NOT NULL default '0',
  `Aktiv` varchar(5) NOT NULL,
  PRIMARY KEY (`Id`)
  ) ENGINE=MyISAM DEFAULT CHARSET=cp1251");

        $DB->query("CREATE TABLE IF NOT EXISTS `" . PREFIX . "_user_gallery` (
  `Id` int(10) unsigned NOT NULL auto_increment,
  `BenutzerId` varchar(12) NOT NULL,
  `Datum` varchar(25) NOT NULL,
  `Name` varchar(250) NOT NULL,
  `Beschreibung` text,
  PRIMARY KEY (`Id`)
  ) ENGINE=MyISAM DEFAULT CHARSET=cp1251");

        $DB->query("CREATE TABLE IF NOT EXISTS `" . PREFIX . "_user_images` (
  `Id` int(10) unsigned NOT NULL auto_increment,
  `GalerieId` varchar(12) NOT NULL,
  `Datum` varchar(25) NOT NULL,
  `Name` varchar(150) NOT NULL,
  `Datei` varchar(150) NOT NULL,
  PRIMARY KEY (`Id`)
  ) ENGINE=MyISAM DEFAULT CHARSET=cp1251");

        $DB->query("CREATE TABLE IF NOT EXISTS `" . PREFIX . "_user_values` (
  `Id` int(10) unsigned NOT NULL auto_increment,
  `BenutzerId` varchar(15) NOT NULL default '',
  `Besucher` varchar(250) NOT NULL default '',
  PRIMARY KEY (`Id`)
  ) ENGINE=MyISAM DEFAULT CHARSET=cp1251");

        $DB->query("CREATE TABLE IF NOT EXISTS `" . PREFIX . "_faq_kategorie` (
  `Id` smallint(2) unsigned NOT NULL AUTO_INCREMENT,
  `Parent_Id` smallint(2) unsigned NOT NULL DEFAULT '0',
  `Name_1` varchar(150) NOT NULL,
  `Name_2` varchar(150) DEFAULT NULL,
  `Name_3` varchar(150) DEFAULT NULL,
  `Sektion` smallint(2) unsigned NOT NULL DEFAULT '1',
  `Posi` smallint(2) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`)
  ) ENGINE=MyISAM DEFAULT CHARSET=cp1251");

        $DB->query("CREATE TABLE IF NOT EXISTS `" . PREFIX . "_shop_webpayment` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `order_number` varchar(25) NOT NULL DEFAULT '',
  `price_order` decimal(10,2),
  `user_order_date` varchar(40) NOT NULL DEFAULT '',
  `hashcode` varchar(40) NOT NULL DEFAULT '',
  `check_call` varchar(40) NOT NULL DEFAULT '',
  `system` varchar(40) NOT NULL DEFAULT '',
  `info` longtext NOT NULL,
  PRIMARY KEY (`Id`)
  ) ENGINE=MyISAM DEFAULT CHARSET=cp1251");

        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_bereiche` DROP COLUMN `Posi`");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_bereiche` DROP COLUMN `Aktiv`");

        $query = $DB->query("SELECT * FROM " . PREFIX . "_bereiche");
        while ($row = $query->fetch_object()) {
            $row->Link = str_replace('area=1', 'area=__SECTION__', $row->Link);
            $DB->query("UPDATE " . PREFIX . "_bereiche SET Link = '" . $DB->escape($row->Link) . "' WHERE Id = '" . $DB->escape($row->Id) . "'");
        }

        $phrases = $DB->fetch_assoc("SELECT * FROM " . PREFIX . "_bereiche WHERE Name = 'phrases' LIMIT 1");
        if (!is_array($phrases)) {
            $DB->query("INSERT IGNORE INTO `" . PREFIX . "_bereiche` (`Name`, `Link`, `Type`) VALUES ('phrases','','modul')");
        }
        $banned = $DB->fetch_assoc("SELECT * FROM " . PREFIX . "_bereiche WHERE Name = 'banned' LIMIT 1");
        if (!is_array($banned)) {
            $DB->query("INSERT IGNORE INTO `" . PREFIX . "_bereiche` (`Name`, `Link`, `Type`) VALUES ('banned','','modul')");
        }
        $seo = $DB->fetch_assoc("SELECT * FROM " . PREFIX . "_bereiche WHERE Name = 'seomod' LIMIT 1");
        if (!is_array($seo)) {
            $DB->query("INSERT IGNORE INTO `" . PREFIX . "_bereiche` (`Name`, `Link`, `Type`) VALUES ('seomod','','modul')");
        }
        $ping = $DB->fetch_assoc("SELECT * FROM " . PREFIX . "_bereiche WHERE Name = 'ping' LIMIT 1");
        if (!is_array($ping)) {
            $DB->query("INSERT IGNORE INTO `" . PREFIX . "_bereiche` (`Name`, `Link`, `Type`) VALUES ('ping','','modul')");
        }
        $flashtag = $DB->fetch_assoc("SELECT * FROM " . PREFIX . "_bereiche WHERE Name = 'flashtag' LIMIT 1");
        if (!is_array($flashtag)) {
            $DB->query("INSERT IGNORE INTO `" . PREFIX . "_bereiche` (`Name`, `Link`, `Type`) VALUES ('flashtag','','modul')");
        }
        $roadmap = $DB->fetch_assoc("SELECT * FROM " . PREFIX . "_bereiche WHERE Name = 'roadmap' LIMIT 1");
        if (!is_array($roadmap)) {
            $DB->query("INSERT IGNORE INTO `" . PREFIX . "_bereiche` (`Name`,`Link`, `Type`) VALUES ('roadmap','index.php?p=roadmap&area=__SECTION__','modul')");
        }
        $highlighter = $DB->fetch_assoc("SELECT * FROM " . PREFIX . "_bereiche WHERE Name = 'highlighter' LIMIT 1");
        if (!is_array($highlighter)) {
            $DB->query("INSERT IGNORE INTO `" . PREFIX . "_bereiche` (`Name`,`Link`, `Type`) VALUES ('highlighter','','modul')");
        }
        $forums_topicstartpage = $DB->fetch_assoc("SELECT * FROM " . PREFIX . "_bereiche WHERE Name = 'forums_topicstartpage' LIMIT 1");
        if (!is_array($forums_topicstartpage)) {
            $DB->query("INSERT IGNORE INTO `" . PREFIX . "_bereiche` (`Name`,`Link`, `Type`) VALUES ('forums_topicstartpage','','modul')");
        }

        $query = $DB->query("SELECT Id FROM " . PREFIX . "_sektionen");
        while ($row = $query->fetch_object()) {
            $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_bereiche` ADD `Aktiv_Section_" . $DB->escape($row->Id) . "` ENUM('0','1') NOT NULL DEFAULT '1'");
        }

        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_roadmap` ADD `Sektion` SMALLINT(3) NOT NULL DEFAULT '1'");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_roadmap_tickets` ADD `Sektion` SMALLINT(3) NOT NULL DEFAULT '1'");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_benutzer` ADD `MiddleName` VARCHAR(100) NOT NULL DEFAULT ''");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_benutzer` ADD `BankName` text");

        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_produkte` ADD `Gruppen` TEXT NOT NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_produkte` ADD `Frei_1` VARCHAR(200) NOT NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_produkte` ADD `Frei_2` VARCHAR(200) NOT NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_produkte` ADD `Frei_3` VARCHAR(200) NOT NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_produkte` ADD `Frei_1_Pflicht` ENUM('0','1') NOT NULL DEFAULT '0'");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_produkte` ADD `Frei_2_Pflicht` ENUM('0','1') NOT NULL DEFAULT '0'");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_produkte` ADD `Frei_3_Pflicht` ENUM('0','1') NOT NULL DEFAULT '0'");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_produkte` ADD `EinheitBezug` DECIMAL(8, 3) NOT NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_produkte` ADD `EAN_Nr` VARCHAR(75) NOT NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_produkte` ADD `ISBN_Nr` VARCHAR(75) NOT NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_produkte` ADD `Fsk18` ENUM('0','1') NOT NULL DEFAULT '0'");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_produkte` ADD `SeitenTitel` VARCHAR(255) NOT NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_produkte` ADD `Template` VARCHAR(75) NOT NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_produkte` ADD `Sektion` SMALLINT(3) NOT NULL DEFAULT '1'");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_produkte` ADD `Preis_EK` DECIMAL(10, 2) NOT NULL AFTER `Preis_Liste_Gueltig`");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_produkte` ADD `PrCountry` VARCHAR(75) NOT NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_produkte` ADD `Yml` ENUM('0','1') NOT NULL DEFAULT '1'");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_gutscheine` ADD `CommentCupon` text");

        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_bestellungen` ADD `UStId` VARCHAR(20) NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_bestellungen` ADD `Tracking_Id` SMALLINT(3) UNSIGNED NOT NULL ");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_bestellungen` ADD `Tracking_Code` VARCHAR(255) NOT NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_bestellungen` ADD `Rng_MiddleName` VARCHAR(100) NOT NULL DEFAULT ''");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_bestellungen` ADD `Rng_BankName` text");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_bestellungen` ADD `Lief_MiddleName` VARCHAR(100) NOT NULL DEFAULT ''");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_bestellungen` ADD `Order_Type` longtext");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_cheats` ADD `DefektGemeldet` VARCHAR(255) DEFAULT NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_cheats` ADD `DEmail` VARCHAR(255) DEFAULT NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_cheats` ADD `DName` VARCHAR(255) DEFAULT NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_cheats` ADD `DDatum` INT(14) UNSIGNED NOT NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_navi` ADD `Link_Titel_1` VARCHAR(255) NOT NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_navi` ADD `Link_Titel_2` VARCHAR(255) NOT NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_navi` ADD `Link_Titel_3` VARCHAR(255) NOT NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_navi` ADD `Aktiv` ENUM('0','1') NOT NULL DEFAULT '1'");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_laender` ADD `VersandFreiAb` DECIMAL(8, 2) NOT NULL DEFAULT '0'");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_benutzer_online` ADD `Link` VARCHAR(255) DEFAULT NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_benutzer_online` ADD `Bots` ENUM('1','0') NOT NULL DEFAULT '0'");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_kategorie` ADD `Aktiv` ENUM('0','1') NOT NULL DEFAULT '1'");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_kategorie` ADD `Sektion` SMALLINT(3) NOT NULL DEFAULT '1'");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_kategorie` ADD `Search` ENUM('0','1') NOT NULL DEFAULT '1'");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_f_post` ADD `thanks` TINYTEXT NOT NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_glossar` ADD `Typ` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0'");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_posticons` ADD `title` VARCHAR(25) DEFAULT NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_sessions` ADD `Ip` VARCHAR(35) NOT NULL DEFAULT ''");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_smileys` ADD `title` VARCHAR(25) DEFAULT NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_quicknavi` ADD `Gruppe` VARCHAR(25) NOT NULL ");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_varianten` ADD `Bestand` INT(10) UNSIGNED NOT NULL DEFAULT '1000'");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_varianten` ADD `GewichtOperant` enum('+','-')  NOT NULL DEFAULT '+'");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_kategorie_zubehoer` ADD `Sektion` SMALLINT(3) NOT NULL DEFAULT '1'");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_content` ADD `Gruppen` TINYTEXT NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_benutzer` ADD `Fsk18` ENUM('0','1') NOT NULL DEFAULT '0'");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_sektionen` ADD `LimitLastThreads` smallint(2) unsigned NOT NULL DEFAULT '4'");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_sektionen` ADD `Domains` VARCHAR(50) NOT NULL DEFAULT ''");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_faq` ADD `Sender` VARCHAR(150) DEFAULT NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_faq` ADD `NewCat` VARCHAR(150) DEFAULT NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_faq` ADD `Kategorie` smallint(3) unsigned NOT NULL DEFAULT '1'");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_faq` DROP COLUMN `Parent_Id`");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_benutzer` ADD `Films` VARCHAR(255) DEFAULT NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_benutzer` ADD `Tele` VARCHAR(255) DEFAULT NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_benutzer` ADD `Book` VARCHAR(255) DEFAULT NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_benutzer` ADD `Game` VARCHAR(255) DEFAULT NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_benutzer` ADD `Citat` VARCHAR(255) DEFAULT NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_benutzer` ADD `Other` VARCHAR(255) DEFAULT NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_benutzer` ADD `Status` VARCHAR(255) DEFAULT NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_benutzer` ADD `Gravatar` ENUM('0','1') NOT NULL DEFAULT '0'");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_benutzer` ADD `Vkontakte` VARCHAR(255) NOT NULL DEFAULT ''");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_benutzer` ADD `Odnoklassniki` VARCHAR(255) NOT NULL DEFAULT ''");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_benutzer` ADD `Facebook` VARCHAR(255) NOT NULL DEFAULT ''");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_benutzer` ADD `Twitter` VARCHAR(255) NOT NULL DEFAULT ''");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_benutzer` ADD `Google` VARCHAR(255) NOT NULL DEFAULT ''");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_benutzer` ADD `Mymail` VARCHAR(255) NOT NULL DEFAULT ''");

        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_log` CHANGE `Aktion` `Aktion` text");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_zahlungsmethoden` CHANGE `Kosten` `Kosten` DECIMAL(8,2) UNSIGNED DEFAULT '0.00'");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_versandarten` CHANGE `Gebuehr_Pauschal` `Gebuehr_Pauschal` DECIMAL(8,2) UNSIGNED NOT NULL DEFAULT '0.00'");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_varianten` CHANGE `Wert` `Wert` DECIMAL(9,2) UNSIGNED NOT NULL DEFAULT '0.00'");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_counter_referer` CHANGE `Ua` `Ua` VARCHAR(35) DEFAULT NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_news` CHANGE `Intro1` `Intro1` text");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_news` CHANGE `Intro2` `Intro2` text");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_news` CHANGE `Intro3` `Intro3` text");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_benutzer` CHANGE `Ort_Public` `Ort_Public` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1'");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_benutzer` CHANGE `Gaestebuch` `Gaestebuch` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1'");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_sprachen` CHANGE `Zeitformat` `Zeitformat` VARCHAR(30) NOT NULL DEFAULT '%d.%m.%Y, %H:%M'");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_produkte` CHANGE `EinheitCount` `EinheitCount` DECIMAL(8, 3) UNSIGNED NULL DEFAULT '1.000'");

        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_benutzer` DROP COLUMN `Geloescht_Email`");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_bestellungen` DROP COLUMN `Rng_Anrede`");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_bestellungen` DROP COLUMN `Lief_Anrede`");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_bestellungen` DROP COLUMN `BankName`");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_bestellungen` DROP COLUMN `BankLeitzahl`");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_bestellungen` DROP COLUMN `BankKonto`");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_bestellungen` DROP COLUMN `BankKontoInhaber`");

        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_sitemap_items` VALUES (1,'News','1','0.5','always')");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_sitemap_items` VALUES (2,'Articles','1','0.5','always')");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_sitemap_items` VALUES (3,'Global_Shop','1','0.5','always')");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_sitemap_items` VALUES (4,'Products','1','0.5','always')");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_sitemap_items` VALUES (5,'Downloads','1','0.5','always')");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_sitemap_items` VALUES (6,'Forums_nt','1','0.5','always')");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_sitemap_items` VALUES (7,'Gallery','1','0.5','always')");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_sitemap_items` VALUES (8,'Faq','1','0.5','always')");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_sitemap_items` VALUES (9,'Content','1','0.5','always')");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_sitemap_items` VALUES (10,'Polls','1','0.5','always')");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_sitemap_items` VALUES (11,'User_nameS','1','0.5','always')");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_sitemap_items` VALUES (12,'Links','1','0.5','always')");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_sitemap_items` VALUES (13,'Gaming_cheats','1','0.5','always')");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_sitemap_items` VALUES (14,'Manufacturer','1','0.5','always')");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_sitemap_items` VALUES (15,'Calendar','1','0.5','always')");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_sitemap_items` VALUES (16,'Roadmaps','1','0.5','always')");

        $DB->query("DROP TABLE IF EXISTS `" . PREFIX . "_cheats_einstellungen`");
        $DB->query("DROP TABLE IF EXISTS `" . PREFIX . "_links_einstellungen`");
        $DB->query("DROP TABLE IF EXISTS `" . PREFIX . "_downloads_einstellungen`");
        $DB->query("DROP TABLE IF EXISTS `" . PREFIX . "_produkte_einstellungen`");
        $DB->query("DROP TABLE IF EXISTS `" . PREFIX . "_gaestebuch_einstellungen`");
        $DB->query("DROP TABLE IF EXISTS `" . PREFIX . "_fckinserts`");

        $DB->query("CREATE TABLE IF NOT EXISTS `" . PREFIX . "_shop_bestellungen_historie` (
  `Id` mediumint(10) unsigned NOT NULL auto_increment,
  `BestellNummer` int(10) NOT NULL,
  `Datum` int(14) unsigned NOT NULL,
  `Subjekt` varchar(255) NOT NULL,
  `Kommentar` text,
  `StatusText` text,
  PRIMARY KEY  (`Id`)
  ) ENGINE=MyISAM DEFAULT CHARSET=cp1251");

        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_produkte` ADD `MetaTags` VARCHAR(255) NOT NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_produkte` ADD `MetaTags` VARCHAR(255) NOT NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_produkte` ADD `MetaDescription` VARCHAR(255) NOT NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_bestellungen` ADD `Verschickt` TEXT NOT NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_bestellungen` CHANGE `Status` `Status` ENUM('wait', 'progress', 'ok', 'failed', 'oksend', 'oksendparts') NULL DEFAULT 'wait'");

        $DB->query("CREATE TABLE IF NOT EXISTS `" . PREFIX . "_shop_produkte_downloads` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ProduktId` int(10) unsigned NOT NULL,
  `Datei` varchar(255) NOT NULL,
  `Datum` int(14) unsigned NOT NULL,
  `DlName` varchar(200) NOT NULL,
  `Beschreibung` text,
  PRIMARY KEY (`Id`)
  ) ENGINE=MyISAM DEFAULT CHARSET=cp1251");

        $DB->query("CREATE TABLE IF NOT EXISTS `" . PREFIX . "_schedule` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Datum` int(14) unsigned NOT NULL,
  `PrevTime` int(14) unsigned NOT NULL,
  `NextTime` int(14) unsigned NOT NULL,
  `Type` enum('one','more','sys') NOT NULL DEFAULT 'sys',
  `Modul` varchar(50) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Func` varchar(255) NOT NULL,
  `Options` text,
  `Aktiv` enum('0','1') NOT NULL DEFAULT '1',
  `Error` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`,`Datum`)
  ) ENGINE=MyISAM DEFAULT CHARSET=cp1251");

        $DB->query("CREATE TABLE IF NOT EXISTS `" . PREFIX . "_audios` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `Sektion` smallint(3) unsigned NOT NULL DEFAULT '1',
  `Name` varchar(255) NOT NULL,
  `Audio` varchar(200) DEFAULT NULL,
  `Width` varchar(10) NOT NULL DEFAULT '400',
  `Datum` int(12) unsigned NOT NULL,
  `Benutzer` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`)
  ) ENGINE=MyISAM DEFAULT CHARSET=cp1251");

        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_zahlungsmethoden` CHANGE `Install_Id` `Install_Id` VARCHAR(255) DEFAULT NULL");

        $res = $DB->fetch_assoc("SELECT * FROM " . PREFIX . "_shop_verfuegbarkeit WHERE Id = '5' LIMIT 1");
        if (!is_array($res)) {
            $DB->query("INSERT IGNORE INTO `" . PREFIX . "_shop_verfuegbarkeit` VALUES (5,'������������ �������� �� �����','','','','','')");
        }

        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_counter_referer` ADD `Words` VARCHAR(255) NOT NULL DEFAULT ''");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_counter_referer` ADD `Url` varchar(255) NOT NULL DEFAULT ''");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_f_topic` ADD `top_first_post` ENUM('0','1') NOT NULL DEFAULT '0'");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_f_topic` ADD `first_post_id` INT(11) DEFAULT NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_counter_ips` CHANGE `ip` `ip` INT(10) unsigned NOT NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_benutzer_online` CHANGE `Ip` `Ip` INT(10) unsigned NOT NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_benutzer_online` ADD `Type` ENUM('site','admin') NOT NULL DEFAULT 'site'");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_suche_log` ADD `UserId` int(10) unsigned NOT NULL DEFAULT '0'");

        $DB->query("CREATE TABLE IF NOT EXISTS `" . PREFIX . "_shop_warenkorb_gaeste` (
  `Id` int(14) unsigned NOT NULL AUTO_INCREMENT,
  `BenutzerId` varchar(50) NOT NULL,
  `Ablauf` int(14) unsigned NOT NULL,
  `Inhalt` longtext NOT NULL,
  `InhaltConfig` text,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `BenutzerId` (`BenutzerId`)
  ) ENGINE=MyISAM DEFAULT CHARSET=cp1251");

        $query = $DB->query("SELECT id FROM " . PREFIX . "_f_topic");
        while ($row = $query->fetch_object()) {
            $res = $DB->fetch_object("SELECT id FROM " . PREFIX . "_f_post WHERE topic_id = '" . $row->id . "' ORDER BY id ASC LIMIT 1");
            $DB->query("UPDATE " . PREFIX . "_f_topic SET first_post_id = '" . $res->id . "' WHERE id = '" . $row->id . "'");
        }

        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_staffelpreise` DROP COLUMN `Prozent`");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_staffelpreise` ADD `Wert` DECIMAL(6,2) NULL DEFAULT NULL");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_staffelpreise` ADD `Operand` ENUM('pro','wert') NOT NULL DEFAULT 'pro'");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_shop_zahlungsmethoden` ADD `MaxWert` DECIMAL(8,2) NOT NULL DEFAULT '0.00'");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_kontakt_form` ADD `Email2` VARCHAR(150) NOT NULL AFTER `Email`");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_counter_referer` ADD `UserId` int(10) unsigned NOT NULL DEFAULT '0'");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_counter_referer` ADD `UserName` VARCHAR(100) NOT NULL DEFAULT ''");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_sektionen` DROP COLUMN `Tpl_denied`");

        $DB->query("CREATE TABLE IF NOT EXISTS `" . PREFIX . "_seotags` (
  `id` int(14) unsigned NOT NULL AUTO_INCREMENT,
  `page` varchar(255) NOT NULL default '',
  `title` varchar(255) NOT NULL default '',
  `keywords` varchar(255) NOT NULL default '',
  `description` varchar(255) NOT NULL default '',
  `canonical` varchar(255) NOT NULL DEFAULT '',
  `aktiv` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `page` (`page`)
  ) ENGINE=MyISAM DEFAULT CHARSET=cp1251");

        $DB->query("CREATE TABLE IF NOT EXISTS `" . PREFIX . "_settings` (
  `Id` varchar(100) NOT NULL,
  `Modul` varchar(50) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Type` enum('int','string') NOT NULL DEFAULT 'string',
  `Value` text,
  PRIMARY KEY (`Id`)
  ) ENGINE=MyISAM DEFAULT CHARSET=cp1251");

        $tables = array();
        $sql = $DB->query("SHOW TABLES LIKE '" . PREFIX . "_%'");
        while ($row = $sql->fetch_array()) {
            $tables[] = $row[0];
        }

        if (in_array(PREFIX . '_admin_settings', $tables)) {
            SX::save('admin', $DB->fetch_assoc("SELECT * FROM " . PREFIX . "_admin_settings LIMIT 1"));
        }
        if (in_array(PREFIX . '_einstellungen', $tables)) {
            SX::save('system', $DB->fetch_assoc("SELECT * FROM " . PREFIX . "_einstellungen LIMIT 1"));
        }
        if (in_array(PREFIX . '_galerie_einstellungen', $tables)) {
            SX::save('galerie', $DB->fetch_assoc("SELECT * FROM " . PREFIX . "_galerie_einstellungen LIMIT 1"));
        }
        if (in_array(PREFIX . '_shop_einstellungen', $tables)) {
            SX::save('shop', $DB->fetch_assoc("SELECT * FROM " . PREFIX . "_shop_einstellungen LIMIT 1"));
        }
        if (in_array(PREFIX . '_f_allowed_files', $tables)) {
            SX::save('forum', $DB->fetch_assoc("SELECT * FROM " . PREFIX . "_f_allowed_files LIMIT 1"));
        }
        if (in_array(PREFIX . '_sitemap', $tables)) {
            SX::save('sitemap', $DB->fetch_assoc("SELECT * FROM " . PREFIX . "_sitemap LIMIT 1"));
        }
        if (in_array(PREFIX . '_modul_settings', $tables)) {
            $query = $DB->query("SELECT * FROM " . PREFIX . "_modul_settings");
            while ($res = $query->fetch_assoc()) {
                SX::save($res['Modul'], $res);
            }
        }

        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('admin_editarea', 'admin', 'EditArea', 'int', '1')");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('shop_popup_product', 'shop', 'popup_product', 'int', '0')");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('shop_menu_low_amount', 'shop', 'menu_low_amount', 'int', '1')");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('shop_seen_cat', 'shop', 'seen_cat', 'int', '1')");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('shop_vat_info_cat', 'shop', 'vat_info_cat', 'int', '1')");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('shop_similar_product', 'shop', 'similar_product', 'int', '1')");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('shop_vat_info_product', 'shop', 'vat_info_product', 'int', '1')");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('shop_sortable_produkte', 'shop', 'Sortable_Produkte', 'int', 'date_asc')");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('system_update', 'system', 'Update', 'int', '0')");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('system_reg_address', 'system', 'Reg_Address', 'int', '1')");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('system_reg_addressfill', 'system', 'Reg_AddressFill', 'int', '0')");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('system_reg_datapflichtfill', 'system', 'Reg_DataPflichtFill', 'int', '0')");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('system_error_email', 'system', 'Error_Email', 'int', '0')");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('shop_availtype', 'shop', 'AvailType', 'int', '1')");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('shop_onlyfhrase', 'shop', 'OnlyFhrase', 'int', '1')");

        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('system_counttitle', 'system', 'CountTitle', 'int', '150')");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('system_countkeywords', 'system', 'CountKeywords', 'int', '250')");
        $DB->query("INSERT IGNORE INTO `" . PREFIX . "_settings` VALUES ('system_countdescription', 'system', 'CountDescription', 'int', '250')");

        $DB->query("UPDATE `" . PREFIX . "_settings` SET Type = 'string' WHERE Name = 'Inn' OR Name = 'Kpp' OR Name = 'Bik' OR Name = 'Kschet' OR Name = 'Rschet'");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_newsletter_archiv` ADD `Sys` ENUM('one','later','more') NOT NULL DEFAULT 'one'");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_newsletter_archiv` ADD `Noheader` ENUM('0','1') NOT NULL DEFAULT '1'");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_banner` ADD `Click` int(11) unsigned NOT NULL DEFAULT '0'");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_seotags` ADD `canonical` VARCHAR(255) NOT NULL DEFAULT ''");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_partner` ADD `Nofollow` tinyint(1) NOT NULL DEFAULT '1'");

        $array = array(
            'Max_Groesse'    => 6144,
            'Typen'          => '.3gp|.7z|.aif|.avi|.bmp|.bzip|.cfg|.doc|.gif|.gz|.gzip|.htm|.html|.jpg|.mov|.mp3|.pdf|.pdf|.png|.psd|.rar|.sql|.tar|.tgz|.tpl|.txt|.vmf|.wav|.wmv|.xls|.xml|.zip',
            'TypenMoegliche' => '.jpg|.png|.gif|.zip|.rar|.exe|.pdf|.txt|.php|.tar|.wav|.mp3|.mp4|.3gp|.psd|.xls|.doc|.bmp|.bz2|.tgz|.7z|.gzip|.bzip|.gz|.inc|.cfg|.aif|.mov|.rmp|.vmf|.avi|.ram|.wmv|.asf|.tpl|.htm|.html|.cgi|.asp|.cfm|.xml|.sql|.8bi|.pdd|.pdf|.cd4');
        SX::save('forum', $array);

        $DB->query("DROP TABLE IF EXISTS `" . PREFIX . "_admin_settings`");
        $DB->query("DROP TABLE IF EXISTS `" . PREFIX . "_einstellungen`");
        $DB->query("DROP TABLE IF EXISTS `" . PREFIX . "_galerie_einstellungen`");
        $DB->query("DROP TABLE IF EXISTS `" . PREFIX . "_shop_einstellungen`");
        $DB->query("DROP TABLE IF EXISTS `" . PREFIX . "_modul_settings`");
        $DB->query("DROP TABLE IF EXISTS `" . PREFIX . "_f_allowed_files`");
        $DB->query("DROP TABLE IF EXISTS `" . PREFIX . "_sitemap`");

        $DB->query("CREATE TABLE IF NOT EXISTS `" . PREFIX . "_antivirus` (
  `Datum` int(11) unsigned NOT NULL DEFAULT '0',
  `DatumCheck` int(11) unsigned NOT NULL DEFAULT '0',
  `StatusCheck` enum('ok','error') NOT NULL DEFAULT 'error',
  `Value` longtext,
  `Scan` longtext,
  `Infected` longtext,
  `SnapError` longtext,
  `VersionBase` int(9) unsigned NOT NULL DEFAULT '0',
  `VirusBase` longtext,
  `Folders` longtext,
  `ExtScan` text,
  `ExtVir` text,
  `ExtAll` text,
  `Ignore` longtext
  ) ENGINE=MyISAM DEFAULT CHARSET=cp1251");


        $antivirus = $DB->fetch_assoc("SELECT * FROM " . PREFIX . "_antivirus LIMIT 1");
        if (!is_array($antivirus)) {
            $DB->query("INSERT IGNORE INTO `" . PREFIX . "_antivirus` VALUES (0,0,'error','','','','',0,'','a:2:{i:0;s:5:\"/temp\";i:1;s:8:\"/uploads\";}','a:50:{i:0;s:3:\"3gp\";i:1;s:2:\"7z\";i:2;s:3:\"avi\";i:3;s:3:\"bmp\";i:4;s:2:\"bz\";i:5;s:3:\"bz2\";i:6;s:3:\"dbf\";i:7;s:3:\"djv\";i:8;s:4:\"djvu\";i:9;s:3:\"doc\";i:10;s:3:\"dwg\";i:11;s:3:\"exe\";i:12;s:3:\"f4v\";i:13;s:3:\"flv\";i:14;s:3:\"gif\";i:15;s:3:\"ico\";i:16;s:3:\"iso\";i:17;s:3:\"jpe\";i:18;s:4:\"jpeg\";i:19;s:3:\"jpg\";i:20;s:3:\"m3u\";i:21;s:3:\"m4a\";i:22;s:3:\"m4v\";i:23;s:3:\"mkv\";i:24;s:3:\"mod\";i:25;s:4:\"moov\";i:26;s:3:\"mov\";i:27;s:5:\"movie\";i:28;s:3:\"mp2\";i:29;s:3:\"mp3\";i:30;s:3:\"mp4\";i:31;s:3:\"mpe\";i:32;s:4:\"mpeg\";i:33;s:3:\"mpg\";i:34;s:3:\"pdf\";i:35;s:3:\"png\";i:36;s:3:\"psd\";i:37;s:3:\"rar\";i:38;s:3:\"rtf\";i:39;s:3:\"swf\";i:40;s:3:\"tar\";i:41;s:3:\"tga\";i:42;s:3:\"tmp\";i:43;s:3:\"ttf\";i:44;s:3:\"wav\";i:45;s:3:\"wmv\";i:46;s:3:\"xla\";i:47;s:3:\"xls\";i:48;s:3:\"xsl\";i:49;s:3:\"zip\";}','a:50:{i:0;s:3:\"3gp\";i:1;s:2:\"7z\";i:2;s:3:\"avi\";i:3;s:3:\"bmp\";i:4;s:2:\"bz\";i:5;s:3:\"bz2\";i:6;s:3:\"dbf\";i:7;s:3:\"djv\";i:8;s:4:\"djvu\";i:9;s:3:\"doc\";i:10;s:3:\"dwg\";i:11;s:3:\"exe\";i:12;s:3:\"f4v\";i:13;s:3:\"flv\";i:14;s:3:\"gif\";i:15;s:3:\"ico\";i:16;s:3:\"iso\";i:17;s:3:\"jpe\";i:18;s:4:\"jpeg\";i:19;s:3:\"jpg\";i:20;s:3:\"m3u\";i:21;s:3:\"m4a\";i:22;s:3:\"m4v\";i:23;s:3:\"mkv\";i:24;s:3:\"mod\";i:25;s:4:\"moov\";i:26;s:3:\"mov\";i:27;s:5:\"movie\";i:28;s:3:\"mp2\";i:29;s:3:\"mp3\";i:30;s:3:\"mp4\";i:31;s:3:\"mpe\";i:32;s:4:\"mpeg\";i:33;s:3:\"mpg\";i:34;s:3:\"pdf\";i:35;s:3:\"png\";i:36;s:3:\"psd\";i:37;s:3:\"rar\";i:38;s:3:\"rtf\";i:39;s:3:\"swf\";i:40;s:3:\"tar\";i:41;s:3:\"tga\";i:42;s:3:\"tmp\";i:43;s:3:\"ttf\";i:44;s:3:\"wav\";i:45;s:3:\"wmv\";i:46;s:3:\"xla\";i:47;s:3:\"xls\";i:48;s:3:\"xsl\";i:49;s:3:\"zip\";}','a:55:{i:0;s:3:\"3gp\";i:1;s:2:\"7z\";i:2;s:3:\"avi\";i:3;s:3:\"bmp\";i:4;s:2:\"bz\";i:5;s:3:\"bz2\";i:6;s:3:\"cgi\";i:7;s:3:\"css\";i:8;s:3:\"dbf\";i:9;s:3:\"djv\";i:10;s:4:\"djvu\";i:11;s:3:\"doc\";i:12;s:3:\"dwg\";i:13;s:3:\"exe\";i:14;s:3:\"f4v\";i:15;s:3:\"flv\";i:16;s:3:\"gif\";i:17;s:2:\"gz\";i:18;s:3:\"ico\";i:19;s:3:\"inc\";i:20;s:3:\"iso\";i:21;s:3:\"jpe\";i:22;s:4:\"jpeg\";i:23;s:3:\"jpg\";i:24;s:3:\"m3u\";i:25;s:3:\"m4a\";i:26;s:3:\"m4v\";i:27;s:3:\"mkv\";i:28;s:3:\"mod\";i:29;s:4:\"moov\";i:30;s:3:\"mov\";i:31;s:5:\"movie\";i:32;s:3:\"mp2\";i:33;s:3:\"mp3\";i:34;s:3:\"mp4\";i:35;s:3:\"mpe\";i:36;s:4:\"mpeg\";i:37;s:3:\"mpg\";i:38;s:3:\"pdf\";i:39;s:3:\"png\";i:40;s:3:\"psd\";i:41;s:3:\"rar\";i:42;s:3:\"rtf\";i:43;s:3:\"swf\";i:44;s:3:\"tar\";i:45;s:3:\"tga\";i:46;s:3:\"tmp\";i:47;s:3:\"ttf\";i:48;s:3:\"wav\";i:49;s:3:\"wmv\";i:50;s:3:\"xla\";i:51;s:3:\"xls\";i:52;s:3:\"xml\";i:53;s:3:\"xsl\";i:54;s:3:\"zip\";}','')");
        }
        $pay2pay = $DB->fetch_assoc("SELECT * FROM " . PREFIX . "_shop_zahlungsmethoden WHERE Id = '18' LIMIT 1");
        if (!is_array($pay2pay)) {
            $DB->query("INSERT IGNORE INTO `" . PREFIX . "_shop_zahlungsmethoden` VALUES (18,1,'������ ����� ������� Pay2Pay','','','������ ����� ������� <strong>Pay2Pay<br /></strong>','','','<div style=\"text-align: justify\"><b>������� ����������� �������� <a href=\"http://www.pay2pay.com/\">Pay2Pay</a></b> � ��� ������� ������ ������ �������� ������, ����� ��� ���������� �����, ������� ����������� �����, ��������� ����������������, ���������� ��������� �������. ��������� ������ �������� �� ���������� ����� ������, ��� ����������� ������������������ ������������ ����������.</div>','','',0,'','1','','-',0,'pro','7,1,2,4,5,3,6','AZ,BY,KZ,KG,LV,LT,MD,RU,TJ,TM,UZ,UA,EE','8,3,6,1,7,2,4,5,9,10,11,12,13',18,'<br />','pay2pay.jpg','Sys',0)");
        }
        $w1 = $DB->fetch_assoc("SELECT * FROM " . PREFIX . "_shop_zahlungsmethoden WHERE Id = '19' LIMIT 1");
        if (!is_array($w1)) {
            $DB->query("INSERT IGNORE INTO `" . PREFIX . "_shop_zahlungsmethoden` VALUES (19,1,'������ ����� ������� ������ �������','','','������ ����� ������� <strong>������ �������</strong><br />','','','<div style=\"text-align: justify\"><b>��������� ������ <a href=\"http://www.walletone.com/?ref=157063999778\">������ �������</a></b> &ndash; ��� ������� � ������� ������ ������ ����� � ���������� �������� ��� ���������� ����� ��������. �������� ����� ��������-��������, ������������� � ������ �����, ����� ����� ��� � 300 000 ������� ������ �������� &mdash; � ����������, ������� �����, ������������� �����������, �������� �����, ����������, ���������� ����� ������, ��������� � ����� ���������� ������.</div>','','',0,'','643','','-',0,'pro','7,1,2,4,5,3,6','AZ,BY,KZ,KG,LV,LT,MD,RU,TJ,TM,UZ,UA,EE','8,3,6,1,7,2,4,5,9,10,11,12,13',19,'<br />','w1.jpg','Sys',0)");
        }

        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_f_topic` ADD INDEX `datum` (`datum`)");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_f_topic` ADD INDEX `uid` (`uid`)");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_f_post` ADD INDEX `datum` (`datum`)");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_f_post` ADD INDEX `uid` (`uid`)");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_pn` ADD INDEX `count` (`to_uid`,`typ`)");
        $DB->query("ALTER IGNORE TABLE `" . PREFIX . "_antivirus` ADD `Ignore` longtext");

        $DB->query("DELETE FROM `" . PREFIX . "_settings` WHERE Modul = 'shop' AND (Name = 'Email_Antwort' OR Name = 'Email_Wertung' OR Name = 'Email_Produktanfrage')");
        break;
}

SX::output('<script type="text/javascript"> alert("' . SX::$lang['GlobalUpdateOk'] . '"); location.href = "index.php";</script>', true);
