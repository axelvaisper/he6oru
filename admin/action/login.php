<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (!defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}

switch (Arr::getRequest('action')) {
    default:
    case 'form':
        SX::object('AdminLogin')->formLogin();
        break;

    case 'login':
        SX::object('AdminLogin')->newLogin();
        break;

    case 'sectionswitch':
        SX::object('AdminLogin')->sectionSwitch();
        break;

    case 'themeswitch':
        SX::object('AdminLogin')->themeSwitch();
        break;
}
