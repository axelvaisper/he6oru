<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (!defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}

if (!perm('settings')) {
    SX::object('AdminCore')->noAccess();
}

function importUpload() {
    $options = array(
        'type'   => 'load',
        'rand'   => true,
        'result' => 'data',
        'upload' => '/temp/cache/',
        'input'  => 'csvfile',
    );
    $object = SX::object('Upload');
    $object->extensions('load', array('txt', 'csv', 'xls', 'xlsx'));
    $array = $object->load($options);
    return !empty($array) ? $array : '';
}

$array_fields = array(
    'Id'                     => 'ID ������������ | � ���� Id',
    'Gruppe'                 => '�������� ������ ������������ | � ���� Gruppe',
    'Team'                   => '��������� � ������������� �����| � ���� Team',
    'Regdatum'               => '���� �����������| � ���� Regdatum',
    'RegCode'                => '��������������� ���| � ���� RegCode',
    'Email'                  => 'E-mail | � ���� Email',
    'Kennwort'               => '������ | � ���� Kennwort',
    'KennwortTemp'           => '��������� ������ ��� ������������� | � ���� KennwortTemp',
    'Benutzername'           => '��� ������������ (�����)| � ���� Benutzername',
    'Vorname'                => '��� | � ���� Vorname',
    'Nachname'               => '������� | � ���� Nachname',
    'Strasse_Nr'             => '�����, ���, ���� | � ���� Strasse_Nr',
    'Postleitzahl'           => '�������� ������| � ���� Postleitzahl',
    'Ort'                    => '����� | � ���� Ort',
    'Firma'                  => '����������� | � ���� Firma',
    'UStId'                  => 'UStId | � ���� UStId',
    'Telefon'                => '������� | � ���� Telefon',
    'Telefax'                => '���� | � ���� Telefax',
    'Geburtstag'             => '���� �������� | � ���� Geburtstag',
    'Land'                   => '������ | � ���� Land',
    'LandCode'               => '��� ������| � ���� LandCode',
    'Aktiv'                  => '���������� ������������ | � ���� Aktiv',
    'Logins'                 => '���������� ����������� �� ����� | � ���� Logins',
    'Profil_public'          => '������������ ������� | � ���� Profil_public',
    'Profil_Alle'            => '����������� �� �������� ������� �������| � ���� Profil_Alle',
    'Geburtstag_public'      => '���������� ���� �������� ������������ | � ���� Geburtstag_public',
    'Ort_Public'             => '���������� ����� ������������| � ���� Ort_Public',
    'Unsichtbar'             => '�������� ����������� ������������ | � ���� Unsichtbar',
    'Newsletter'             => '��������� ��������� ������������� | � ���� Newsletter',
    'Emailempfang'           => '�������� ������ �� email �� ������������� | � ���� Emailempfang',
    'Pnempfang'              => '�������� �.�. �� ������������� | � ���� Pnempfang',
    'PnEmail'                => '����������� �� ����� � ����� �.�. | � ���� PnEmail',
    'PnPopup'                => '����������� ���� ��� ����� �.�. | � ���� PnPopup',
    'Gaestebuch'             => '�������� ����� ������������ | � ���� Gaestebuch',
    'Gaestebuch_KeineGaeste' => '��������� �������� ����� ������������ | � ���� Gaestebuch_KeineGaeste',
    'Gaestebuch_Moderiert'   => '��������� �������� ����� ������������� | � ���� Gaestebuch_Moderiert',
    'Gaestebuch_Zeichen'     => '���������� �������� � �������� ������������ | � ���� Gaestebuch_Zeichen',
    'Gaestebuch_imgcode'     => '��������� IMG-��� � �������� ������������ | � ���� Gaestebuch_imgcode',
    'Gaestebuch_smilies'     => '��������� ������ � �������� ������������ | � ���� Gaestebuch_smilies',
    'Gaestebuch_bbcode'      => '��������� BB-��� � �������� ������������ | � ���� Gaestebuch_bbcode',
    'msn'                    => 'MSN ������������ | � ���� msn',
    'aim'                    => 'AIM ������������ | � ���� aim',
    'icq'                    => 'ICQ ������������ | � ���� icq',
    'skype'                  => '����� ������������ | � ���� skype',
    'Webseite'               => '����� ����� ������������ | � ���� Webseite',
    'Signatur'               => '������� ������������ | � ���� Signatur',
    'Interessen'             => '�������� ������������ | � ���� Interessen',
    'Avatar_Default'         => '����������� ������ ������ | � ���� Avatar_Default',
    'Avatar'                 => '������ ������������ | � ���� Avatar',
    'Beitraege'              => '���������� ��������� ������������ �� ������| � ���� Beitraege',
    'Profil_Hits'            => '��������� ������� ������������ | � ���� Profil_Hits',
    'Zuletzt_Aktiv'          => '��������� ��������� ����� ������������� | � ���� Zuletzt_Aktiv',
    'Geschlecht'             => '��� ������������ | � ���� Geschlecht',
    'Beruf'                  => '��������� ������������ | � ���� Beruf',
    'Hobbys'                 => '��������� ������������ | � ���� Hobbys',
    'Essen'                  => '������� ����� ������������ | � ���� Essen',
    'Musik'                  => '������� ������ ������������ | � ���� Musik',
    'Forum_Beitraege_Limit'  => '���������� ��������� �� �������� ������ | � ���� Forum_Beitraege_Limit',
    'Forum_Themen_Limit'     => '���������� ��� �� �������� ������ | � ���� Forum_Themen_Limit',
    'Geloescht'              => '������ �������������� ������ �� ����� | � ���� Geloescht',
    'Fsk18'                  => '������ � ������� ��� �������� | � ���� Fsk18',
    'MiddleName'             => '�������� ������������ | � ���� MiddleName',
    'BankName'               => '���������� ��������� | � ���� BankName',
    'Films'                  => '������� ������ | � ���� Films',
    'Tele'                   => '������� ������� | � ���� Tele',
    'Book'                   => '������� ����� | � ���� Book',
    'Game'                   => '������� ���� | � ���� Game',
    'Citat'                  => '������� ������ | � ���� Citat',
    'Other'                  => '� ���� | � ���� Other',
    'Status'                 => 'C�����-��������� | � ���� Status',
    'Gravatar'               => '������������ ������ � �������  Gravatar | � ���� Gravatar',
    'Vkontakte'              => '����� ��������� � ���������� ���� ��������� | � ���� Vkontakte',
    'Odnoklassniki'          => '����� ��������� � ���������� ���� ������������� | � ���� Odnoklassniki',
    'Facebook'               => '����� ��������� � ���������� ���� ������� | � ���� Facebook',
    'Twitter'                => '����� ��������� � ���������� ���� ������� | � ���� Twitter',
    'Google'                 => '����� ��������� � ���������� ���� ���� | � ���� Google',
    'Mymail'                 => '����� ��������� � ���������� ��� ��� | � ���� Mymail',
    'UloginId'               => '����������� ����� ������� Ulogin | � ���� UloginId',
);

$CS = View::get();
$TempDir = TEMP_DIR . '/cache/';

switch (Arr::getRequest('action')) {
    case 'userexp':
        new AdminDataExport('����_�������������_' . date('d_m_y'), Arr::getRequest('format'), Arr::getRequest('groups'));
        break;

    case 'importcsv':
        if (Arr::getRequest('send') == '1') {
            $fileid = importUpload();
            if (empty($fileid)) {
                SX::object('Redir')->redirect('index.php?do=expimp&action=error&error=upload');
            }

            $types = Tool::extension($fileid);
            if ($types == 'xls' || $types == 'xlsx') {
                require_once (STATUS_DIR . '/admin/class/class.XLS.php');
                $fp = fopen($TempDir . $fileid, 'rb');
                $csv = new XLSReader($fp, $TempDir . $fileid);
            } else {
                $fp = fopen($TempDir . $fileid, 'r');
                $csv = new AdminCSVReader($fp);
            }
            $fields = $csv->fields();
            $count = $csv->count();
            fclose($fp);

            if ($count < 1) {
                SX::object('Redir')->redirect('index.php?do=expimp&action=error&error=empty');
            }

            $field_table = array();
            foreach ($fields as $csv_field) {
                $my_field = isset($csv_assoc[$csv_field]) ? $csv_assoc[$csv_field] : '';
                $field_table[] = array('id' => md5($csv_field), 'csv_field' => $csv_field, 'my_field' => $my_field);
            }

            $CS->assign('types', $types);
            $CS->assign('fileid', $fileid);
            $CS->assign('field_table', $field_table);
            $CS->assign('available_fields', $array_fields);
            $CS->assign('datas', $count);
        }
        $CS->assign('action', 'importcsv');
        break;

    case 'importcsv2':
        $fileid = Tool::cleanString(Arr::getRequest('fileid'), '.');

        if (!is_file($TempDir . $fileid)) {
            SX::object('Redir')->redirect('index.php?do=expimp&action=error&error=cache');
        }

        $existing = Arr::getRequest('existing') == 'ignore' ? 'ignore' : 'replace';
        $types = Arr::getRequest('types');
        if ($types == 'xls' || $types == 'xlsx') {
            $fp = fopen($TempDir . $fileid, 'rb');
            require_once (STATUS_DIR . '/admin/class/class.XLS.php');
            $csv = new XLSReader($fp, $TempDir . $fileid);
        } else {
            $fp = fopen($TempDir . $fileid, 'r');
            $csv = new AdminCSVReader($fp);
        }

        while ($row = $csv->fetch()) {
            $save_array = array();
            foreach ($row as $key => $value) {
                $field = Arr::getRequest('field_' . md5($key));
                if (isset($array_fields[$field])) {
                    $save_array[$field] = $value;
                }
            }

            if (!empty($save_array['Email'])) {
                $db = DB::get();
                $row = $db->fetch_assoc("SELECT Id FROM " . PREFIX . "_benutzer WHERE Id='" . intval($save_array['Id']) . "' AND Email='" . $db->escape($save_array['Email']) . "' LIMIT 1");

                $save_array['Geburtstag'] = Tool::formatDate($save_array['Geburtstag']);
                if (!empty($row['Id']) && $existing == 'replace') {
                    $now = $db->update_query('benutzer', $save_array, "Id='" . $db->escape($save_array['Id']) . "' AND Email='" . $db->escape($save_array['Email']) . "'");
                } elseif (empty($row['Id'])) {
                    $now = $db->insert_query('benutzer', $save_array);
                }
            }
        }
        fclose($fp);

        File::delete($TempDir . $fileid);
        SX::object('Redir')->redirect('index.php?do=user&sub=showusers');
        break;

    case 'error':
        $array = array(
            'upload' => SX::$lang['UploadFileError'],
            'cache'  => '������ ������ � ������� /temp/cache/ ��������� ����� �������...',
            'empty'  => '� ����� ��� ������...',
            'error'  => '�������� ������...',
        );
        $error = Arr::getRequest('error');
        $CS->assign('error', (isset($array[$error]) ? $array[$error] : ''));
        $CS->assign('action', 'error');
        break;

    default:
        $groups = DB::get()->fetch_assoc_all("SELECT * FROM " . PREFIX . "_benutzer_gruppen WHERE Id != 2");
        $CS->assign('groups', $groups);
        $CS->assign('db_fields', SX::object('AdminMain')->load(PREFIX));
        $CS->assign('action', 'start');
        break;
}
$CS->content('/exportimport/expimp.tpl');
