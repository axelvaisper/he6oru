<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (!defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}

if (!perm('settings')) {
    SX::object('AdminCore')->noAccess();
}

ignore_user_abort(true);
set_time_limit(600);
function importUmlaut($text) {
    return str_replace(array('�', '�', '�', '�', '�', '�', '�', '�'), array('&euro;', '&raquo;', '&laquo;', '&copy;', '&reg;', '&trade;', '&bdquo;', '&ldquo;'), $text);
}

function importUpload() {
    $options = array(
        'type'   => 'load',
        'rand'   => true,
        'result' => 'data',
        'upload' => '/temp/cache/',
        'input'  => 'csvfile',
    );
    $object = SX::object('Upload');
    $object->extensions('load', array('txt', 'csv', 'xls', 'xlsx'));
    $array = $object->load($options);
    return !empty($array) ? $array : '';
}

function importCateg($db) {
    static $result = false;
    if ($result === false) {
        $dn = '��������� ������� ��� ������� ������� ' . date('d.m.Y H:i:s') . ' (���������)';
        $insert_array = array(
            'Parent_Id' => '',
            'Name_1'    => $dn,
            'Name_2'    => $dn,
            'Name_3'    => $dn,
            'UstId'     => 1,
            'Sektion'   => 1,
            'Aktiv'     => 0);
        $db->insert_query('shop_kategorie', $insert_array);
        $result = $db->insert_id();
    }
    return $result;
}

function importPrise($value) {
    static $result = false;
    if ($result === false) {
        if (Arr::getRequest('netto_to_brutto') == 1) {
            if (Arr::getRequest('operand') == '-') {
                $mp1 = Tool::cleanDigit(Arr::getRequest('mpli'));
                $mp2 = '0.' . $mp1;
                $result = 1 - $mp2;
            } else {
                $mp1 = '1.';
                $mp2 = Tool::cleanDigit(Arr::getRequest('mpli'));
                $result = $mp1 . $mp2;
            }
        } else {
            $result = 'none';
        }
    }
    $value = str_replace(',', '.', $value);
    if ($result != 'none') {
        $value = numf($value * $result);
    }
    return $value;
}

$TempDir = TEMP_DIR . '/cache/';
$array_fields = array(
    'Id'                  => 'ID ������ (�� ������!) | � ���� Id',
    'Artikelnummer'       => '������� ������ | � ���� Artikelnummer',
    'Kategorie'           => '�������� ��������� | � ���� Kategorie',
    'Kategorie_Multi'     => '�������������� ��������� | � ���� Kategorie_Multi',
    'Schlagwoerter'       => '�������� ����� ������ | � ���� Schlagwoerter',
    'Zub_a'               => '���������� (1 - �����) | � ���� Zub_a',
    'Zub_b'               => '���������� (2 - �����) | � ���� Zub_b',
    'Zub_c'               => '���������� (3 - �����l) | � ���� Zub_c',
    'Preis_Liste'         => '��������� ������ | � ���� Preis_Liste',
    'Preis'               => '��������� ������ (����������� �����������) | � ���� Preis',
    'Preis_Liste_Gueltig' => '���� ���������� ������������ ����������� | � ���� Preis_Liste_Gueltig',
    'Preis_EK'            => '���������� ����, ������������ ������ � ����� ������ | � ���� Preis_EK',
    'Titel_1'             => '������������ ������ (1. ����) | � ���� Titel_1',
    'Titel_2'             => '������������ ������ (2. ����) | � ���� Titel_2',
    'Titel_3'             => '������������ ������ (3. ����) | � ���� Titel_3',
    'Beschreibung_1'      => '������� �������� ������ (1. ����) | � ���� Beschreibung_1',
    'Beschreibung_2'      => '������� �������� ������ (2. ����) | � ���� Beschreibung_2',
    'Beschreibung_3'      => '������� �������� ������ (3. ����) | � ���� Beschreibung_3',
    'Beschreibung_lang_1' => '������ �������� ������ (1. ����) | � ���� Beschreibung_lang_1',
    'Beschreibung_lang_2' => '������ �������� ������ (2. ����) | � ���� Beschreibung_lang_2',
    'Beschreibung_lang_3' => '������ �������� ������ (3. ����) | � ���� Beschreibung_lang_3',
    'Hat_ESD'             => '����� ����� ����� ��� ���������� | � ���� Hat_ESD',
    'Aktiv'               => '����� ������� ��� ������� (1/0) | � ���� Aktiv',
    'Erstellt'            => '���� ���������� ������ | � ���� Erstellt',
    'Klicks'              => '���������� ���������� ������ | � ���� Klicks',
    'Bild'                => '�������� ����������� ������ | � ���� Bild',
    'Bilder'              => '�������������� ����������� ������ | � ���� Bilder',
    'Gewicht'             => '��� ������ � ������� | � ���� Gewicht',
    'Gewicht_Ohne'        => '��� ������ ��� �������� | � ���� Gewicht_Ohne',
    'Abmessungen'         => '�������� ������ (������ / ������ / �����) | � ���� Abmessungen',
    'Hersteller'          => '������������� ������ | � ���� Hersteller',
    'EinheitCount'        => '���������� ������ � ������ | � ���� EinheitCount',
    'EinheitId'           => '������� ��������� ������ | � ���� EinheitId',
    'Startseite'          => '���������� ����� �� ������� �������� (1/0) | � ���� Startseite',
    'Lagerbestand'        => '���������� ������ �� ������ | � ���� Lagerbestand',
    'Bestellt'            => '����� ������������ ��� ����� (1/0) | � ���� Bestellt',
    'Verfuegbar'          => '������ ������� ������ | � ���� Verfuegbar',
    'EinzelBestellung'    => '����������� ����� (1/0) | � ���� EinzelBestellung',
    'Verkauft'            => '���������� ������ ������ | � ���� Verkauft',
    'MaxBestellung'       => '������������ ���������� ��� ������ | � ���� MaxBestellung',
    'MinBestellung'       => '����������� ���������� ��� ������ | � ���� MinBestellung',
    'Lieferzeit'          => '���� �������� ������ | � ���� Lieferzeit',
    'Spez_1'              => '������������ 1 (1.����) | � ���� Spez_1',
    'Spez_2'              => '������������ 2 (1.����) | � ���� Spez_2',
    'Spez_3'              => '������������ 3 (1.����) | � ���� Spez_3',
    'Spez_4'              => '������������ 4 (1.����) | � ���� Spez_4',
    'Spez_5'              => '������������ 5 (1.����) | � ���� Spez_5',
    'Spez_6'              => '������������ 6 (1.����) | � ���� Spez_6',
    'Spez_7'              => '������������ 7 (1.����) | � ���� Spez_7',
    'Spez_8'              => '������������ 8 (1.����) | � ���� Spez_8',
    'Spez_9'              => '������������ 9 (1.����) | � ���� Spez_9',
    'Spez_10'             => '������������ 10 (1.����) | � ���� Spez_10',
    'Spez_11'             => '������������ 11 (1.����) | � ���� Spez_11',
    'Spez_12'             => '������������ 12 (1.����) | � ���� Spez_12',
    'Spez_13'             => '������������ 13 (1.����) | � ���� Spez_13',
    'Spez_14'             => '������������ 14 (1.����) | � ���� Spez_14',
    'Spez_15'             => '������������ 15 (1.����) | � ���� Spez_15',
    'Spez_1_2'            => '������������ 1 (2.����) | � ���� Spez_1_2',
    'Spez_2_2'            => '������������ 2 (2.����) | � ���� Spez_2_2',
    'Spez_3_2'            => '������������ 3 (2.����) | � ���� Spez_3_2',
    'Spez_4_2'            => '������������ 4 (2.����) | � ���� Spez_4_2',
    'Spez_5_2'            => '������������ 5 (2.����) | � ���� Spez_5_2',
    'Spez_6_2'            => '������������ 6 (2.����) | � ���� Spez_6_2',
    'Spez_7_2'            => '������������ 7 (2.����) | � ���� Spez_7_2',
    'Spez_8_2'            => '������������ 8 (2.����) | � ���� Spez_8_2',
    'Spez_9_2'            => '������������ 9 (2.����) | � ���� Spez_9_2',
    'Spez_10_2'           => '������������ 10 (2.����) | � ���� Spez_10_2',
    'Spez_11_2'           => '������������ 11 (2.����) | � ���� Spez_11_2',
    'Spez_12_2'           => '������������ 12 (2.����) | � ���� Spez_12_2',
    'Spez_13_2'           => '������������ 13 (2.����) | � ���� Spez_13_2',
    'Spez_14_2'           => '������������ 14 (2.����) | � ���� Spez_14_2',
    'Spez_15_2'           => '������������ 15 (2.����) | � ���� Spez_15_2',
    'Spez_1_3'            => '������������ 1 (3.����) | � ���� Spez_1_3',
    'Spez_2_3'            => '������������ 2 (3.����) | � ���� Spez_2_3',
    'Spez_3_3'            => '������������ 3 (3.����) | � ���� Spez_3_3',
    'Spez_4_3'            => '������������ 4 (3.����) | � ���� Spez_4_3',
    'Spez_5_3'            => '������������ 5 (3.����) | � ���� Spez_5_3',
    'Spez_6_3'            => '������������ 6 (3.����) | � ���� Spez_6_3',
    'Spez_7_3'            => '������������ 7 (3.����) | � ���� Spez_7_3',
    'Spez_8_3'            => '������������ 8 (3.����) | � ���� Spez_8_3',
    'Spez_9_3'            => '������������ 9 (3.����) | � ���� Spez_9_3',
    'Spez_10_3'           => '������������ 10 (3.����) | � ���� Spez_10_3',
    'Spez_11_3'           => '������������ 11 (3.����) | � ���� Spez_11_3',
    'Spez_12_3'           => '������������ 12 (3.����) | � ���� Spez_12_3',
    'Spez_13_3'           => '������������ 13 (3.����) | � ���� Spez_13_3',
    'Spez_14_3'           => '������������ 14 (3.����) | � ���� Spez_14_3',
    'Spez_15_3'           => '������������ 15 (3.����) | � ���� Spez_15_3',
    'Fsk18'               => '����� ��� �������� (0/1) | � ���� Fsk18',
    'Frei_1'              => '�������������� ��������� 1 | � ���� Frei_1',
    'Frei_2'              => '�������������� ��������� 2 | � ���� Frei_2',
    'Frei_3'              => '�������������� ��������� 3 | � ���� Frei_3',
    'Frei_1_Pflicht'      => '������������ �������������� ��������� 1 (0/1) | � ���� Frei_1_Pflicht',
    'Frei_2_Pflicht'      => '������������ �������������� ��������� 2 (0/1) | � ���� Frei_2_Pflicht',
    'Frei_3_Pflicht'      => '������������ �������������� ��������� 3 (0/1) | � ���� Frei_3_Pflicht',
    'Gruppen'             => '����������� ������ ������� ������������� | � ���� Gruppen',
    'EinheitBezug'        => '���������� � ������� ������ | � ���� EinheitBezug',
    'EAN_Nr'              => 'EAN ����� ������ | � ���� EAN_Nr',
    'ISBN_Nr'             => 'ISBN ����� ������ | � ���� ISBN_Nr',
    'SeitenTitel'         => '�������� �������� | � ���� SeitenTitel',
    'Template'            => '������ ������ ������ | � ���� Template',
    'Sektion'             => '������ ����� ������ ������ | � ���� Sektion',
    'PrCountry'           => '������ ������������ ������ | � ���� PrCountry',
    'Yml'                 => '�������� � ������.������ | � ���� Yml',
    'MetaTags'            => '������� keywords | � ���� MetaTags',
    'MetaDescription'     => '������� description | � ���� MetaDescription');

$CS = View::get();
$CS->assign('form', '?do=shopimport');
$CS->assign('method', 'shop');
$CS->assign('next', 0);

$_REQUEST['action'] = !empty($_REQUEST['action']) ? $_REQUEST['action'] : '';
switch ($_REQUEST['action']) {
    case 'importcsv':
        if (Arr::getRequest('send') == '1') {
            $fileid = importUpload();
            if (empty($fileid)) {
                SX::object('Redir')->redirect('index.php?do=shopimport&action=error&error=upload');
            }

            $types = Tool::extension($fileid);
            if ($types == 'xls' || $types == 'xlsx') {
                require_once (STATUS_DIR . '/admin/class/class.XLS.php');
                $fp = fopen($TempDir . $fileid, 'rb');
                $csv = new XLSReader($fp, $TempDir . $fileid);
            } else {
                $fp = fopen($TempDir . $fileid, 'r');
                $csv = new AdminCSVReader($fp);
            }

            if ($csv->count() < 1) {
                SX::object('Redir')->redirect('index.php?do=shopimport&action=error&error=empty');
            }
            $fields = $csv->fields();
            $field_table = array();
            foreach ($fields as $csv_field) {
                $my_field = isset($csv_assoc[$csv_field]) ? $csv_assoc[$csv_field] : '';
                $field_table[] = array('id' => md5($csv_field), 'csv_field' => $csv_field, 'my_field' => $my_field);
            }
            $CS->assign('types', $types);
            $CS->assign('fileid', $fileid);
            $CS->assign('field_table', $field_table);
            $CS->assign('available_fields', $array_fields);
            $CS->assign('next', 1);
            $CS->assign('datas', $csv->count());
            $CS->content('/exportimport/import.tpl');
            fclose($fp);
        }
        break;

    case 'importcsv2':
        $fileid = Tool::cleanString(Arr::getRequest('fileid'), '.');

        if (!is_file($TempDir . $fileid)) {
            SX::object('Redir')->redirect('index.php?do=shopimport&action=error&error=cache');
        }

        $existing = Arr::getRequest('existing') == 'ignore' ? 'ignore' : 'replace';
        $types = Arr::getRequest('types');
        if ($types == 'xls' || $types == 'xlsx') {
            $fp = fopen($TempDir . $fileid, 'rb');
            require_once (STATUS_DIR . '/admin/class/class.XLS.php');
            $csv = new XLSReader($fp, $TempDir . $fileid);
        } else {
            $fp = fopen($TempDir . $fileid, 'r');
            $csv = new AdminCSVReader($fp);
        }

        while ($row = $csv->fetch()) {
            $save_array = array();
            foreach ($row as $key => $value) {
                $field = Arr::getRequest('field_' . md5($key));
                if (isset($array_fields[$field])) {
                    $save_array[$field] = importUmlaut($value);
                }
            }

            if (!empty($save_array['Artikelnummer'])) {
                $save_array['Preis'] = importPrise($save_array['Preis']);
                $save_array['Preis_EK'] = str_replace(',', '.', $save_array['Preis_EK']);
                $save_array['Preis_Liste'] = importPrise($save_array['Preis_Liste']);
                $save_array['Preis_Liste_Gueltig'] = str_replace(',', '.', $save_array['Preis_Liste_Gueltig']);
                $save_array['Gewicht'] = $save_array['Gewicht'] == '' ? 100 : $save_array['Gewicht'];
                $save_array['Lagerbestand'] = $save_array['Lagerbestand'] == '' ? 100 : $save_array['Lagerbestand'];
                $save_array['MinBestellung'] = empty($save_array['MinBestellung']) ? 1 : $save_array['MinBestellung'];
                $save_array['MaxBestellung'] = empty($save_array['MaxBestellung']) ? 10 : $save_array['MaxBestellung'];
                $save_array['Verfuegbar'] = $save_array['Verfuegbar'] == '' ? 1 : $save_array['Verfuegbar'];
                $save_array['Aktiv'] = $save_array['Aktiv'] == '' ? 1 : $save_array['Aktiv'];
                $save_array['Titel_2'] = empty($save_array['Titel_2']) ? $save_array['Titel_1'] : $save_array['Titel_2'];
                $save_array['Titel_3'] = empty($save_array['Titel_2']) ? $save_array['Titel_1'] : $save_array['Titel_3'];
                $save_array['Beschreibung_1'] = str_replace('\n', '', $save_array['Beschreibung_1']);
                $save_array['Beschreibung_2'] = str_replace('\n', '', $save_array['Beschreibung_2']);
                $save_array['Beschreibung_3'] = str_replace('\n', '', $save_array['Beschreibung_3']);
                $save_array['Beschreibung_lang_1'] = str_replace('\n', '', $save_array['Beschreibung_lang_1']);
                $save_array['Beschreibung_lang_2'] = str_replace('\n', '', $save_array['Beschreibung_lang_2']);
                $save_array['Beschreibung_lang_3'] = str_replace('\n', '', $save_array['Beschreibung_lang_3']);
                $save_array['Beschreibung_2'] = empty($save_array['Beschreibung_2']) ? $save_array['Beschreibung_1'] : $save_array['Beschreibung_2'];
                $save_array['Beschreibung_3'] = empty($save_array['Beschreibung_3']) ? $save_array['Beschreibung_1'] : $save_array['Beschreibung_3'];
                $save_array['Beschreibung_lang_2'] = empty($save_array['Beschreibung_lang_2']) ? $save_array['Beschreibung_lang_1'] : $save_array['Beschreibung_lang_2'];
                $save_array['Beschreibung_lang_3'] = empty($save_array['Beschreibung_lang_3']) ? $save_array['Beschreibung_lang_1'] : $save_array['Beschreibung_lang_3'];
                $save_array['Erstellt'] = $save_array['Erstellt'] == '' ? time() : $save_array['Erstellt'];
                $save_array['Sektion'] = empty($save_array['Sektion']) ? 1 : $save_array['Sektion'];

                $db = DB::get();
                if (empty($save_array['Kategorie'])) {
                    $save_array['Kategorie'] = importCateg($db);
                }

                $row = $db->fetch_assoc("SELECT Id FROM " . PREFIX . "_shop_produkte WHERE Artikelnummer='" . $db->escape($save_array['Artikelnummer']) . "' LIMIT 1");
                if (!empty($row['Id']) && $existing == 'replace') {
                    $now = $db->update_query('shop_produkte', $save_array, "Artikelnummer='" . $db->escape($save_array['Artikelnummer']) . "'");
                } elseif (empty($row['Id'])) {
                    $now = $db->insert_query('shop_produkte', $save_array);
                }
            }
        }
        fclose($fp);

        File::delete($TempDir . $fileid);
        SX::object('Redir')->redirect('index.php?do=shop&sub=articles');
        break;

    case 'error':
        $array = array(
            'upload' => SX::$lang['UploadFileError'],
            'cache'  => '������ ������ � ������� /temp/cache/ ��������� ����� �������...',
            'empty'  => '� ����� ��� ������...',
            'error'  => '�������� ������...',
        );
        $error = Arr::getRequest('error');
        $CS->assign('error', (isset($array[$error]) ? $array[$error] : ''));
        break;
}
$CS->content('/exportimport/import.tpl');