<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (!defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}

if (!perm('templates')) {
    SX::object('AdminCore')->noAccess();
}

switch (Arr::getRequest('sub')) {
    default:
    case 'show_all_tpl':
        SX::object('AdminThemes')->loadTpl();
        break;

    case 'show_tpl':
        SX::object('AdminThemes')->showTpl();
        break;

    case 'show_all_css':
        SX::object('AdminThemes')->loadCss();
        break;

    case 'show_css':
        SX::object('AdminThemes')->showCss();
        break;
}
