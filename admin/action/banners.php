<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (!defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}

if (!perm('bannerperm')) {
    SX::object('AdminCore')->noAccess();
}

switch (Arr::getRequest('sub')) {
    default:
    case 'overview':
        SX::object('AdminBanners')->show();
        break;

    case 'delete':
        SX::object('AdminBanners')->delete(Arr::getRequest('id'));
        break;

    case 'edit':
        SX::object('AdminBanners')->edit(Arr::getRequest('id'));
        break;

    case 'new':
        SX::object('AdminBanners')->add();
        break;

    case 'categs':
        SX::object('AdminBanners')->showCateg();
        break;

    case 'delcateg':
        SX::object('AdminBanners')->delCateg(Arr::getRequest('id'));
        break;
}
