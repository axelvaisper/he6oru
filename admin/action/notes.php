<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (!defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}

if (!perm('notes') || SX::get('admin.Aktiv_Notes') != '1') {
    SX::object('AdminCore')->noAccess();
}

$_REQUEST['noout'] = 1;
switch (Arr::getRequest('sub')) {
    case 'addnotes':
        SX::object('AdminNotes')->add();
        break;

    case 'delnotes':
        SX::object('AdminNotes')->delete(intval(Arr::getRequest('notid')));
        break;

    case 'editnotes':
        SX::object('AdminNotes')->edit(intval(Arr::getRequest('notid')));
        break;

    default:
    case 'shownotes':
        SX::object('AdminNotes')->show();
        break;
}
