<?php
function smarty_modifier_slice($string, $length = 80, $etc = '...', $break_words = true) {
    if ($length == 0)
        return '';
    if (strlen($string) > $length) {
        $bit_len = floor($length / 2) - floor(strlen($etc) / 2);
        if ($break_words) {
            $ret_string = substr($string, 0, $bit_len) . $etc . substr($string, -($bit_len - 1));
        } else {
            $ret_string = wordwrap($string, $bit_len + 1, "\0") . $etc . strrev(wordwrap(strrev($string), $bit_len, "\0"));
        }
        return $ret_string;
    } else {
        return $string;
    }
}

?>