<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}
error_reporting(0);                              // ����� ������ php: 0 - ���������, E_ALL - ��������
define('STATUS_DIR', realpath(dirname(dirname(__FILE__))));
require_once STATUS_DIR . '/class/class.SX.php'; // ���������� �������� ����� �������
SX::initStatus('user');                          // �������������� �������

$_REQUEST['action'] = !empty($_REQUEST['action']) ? $_REQUEST['action'] : '';
switch ($_REQUEST['action']) {
    case 'checkcode':
        SX::object('Captcha')->validate();
        break;

    case 'reload':
        SX::object('Captcha')->ajax();
        break;

    default:
        SX::object('Captcha')->load();
        break;
}
