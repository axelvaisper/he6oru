<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################

error_reporting(0);
if (!defined('STATUS_DIR')) {
    define('STATUS_DIR', realpath(dirname(dirname(__FILE__))));
    require_once STATUS_DIR . '/class/class.SX.php'; // ���������� �������� ����� �������
    SX::initStatus('user');                          // �������������� �������
    SX::getLocale($_SESSION['lang']);                // ������������� ������ PHP
}

header('Content-type: text/html; charset=' . $_SESSION['Charset']);
SX::setDefine('AJAX_OUTPUT', 1);

switch (Arr::getRequest('action')) {
    case 'shop';
        $value = NULL;
        $query = Tool::win1251(urldecode($_REQUEST['q']));
        if (!empty($query) && strlen($query) >= 2) {
            $LC = intval(Arr::getSession('Langcode', 1));
            $group = $_SESSION['user_group'];

            $like = DB::get()->escape($query);
            $result = DB::get()->query("SELECT
	    a.Titel_{$LC} AS Name,
	    a.Artikelnummer
	FROM
	    " . PREFIX . "_shop_produkte AS a,
	    " . PREFIX . "_shop_kategorie AS b
	WHERE
            a.Kategorie = b.Id
	AND
	    b.Aktiv = '1'
	AND
	    b.Search = '1'
	AND
	    (a.Titel_{$LC} LIKE '%" . $like . "%' OR a.Artikelnummer LIKE '%" . $like . "%')
	AND
	    a.Aktiv = '1'
	AND
	    (a.Gruppen = '' OR a.Gruppen LIKE '%,$group' OR a.Gruppen LIKE '$group,%' OR a.Gruppen LIKE '%,$group,%' OR a.Gruppen = '$group')
	ORDER BY a.Titel_{$LC} ASC LIMIT 50");
            while ($row = $result->fetch_assoc()) {
                if (stripos($row['Name'], $query) !== false) {
                    $value .= '"' . sanitize($row['Name']) . '"' . PE;
                }
                if (stripos($row['Artikelnummer'], $query) !== false) {
                    $value .= '"' . sanitize($row['Artikelnummer']) . '"' . PE;
                }
            }
            $result->close();
        }
        SX::output($value, true);
        break;

    case 'other';
        // ��� ����� ���� ����
        break;
}
