<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (!defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}

/* ����� �������������� �������, ������: ���� = 1234.56 ����� = 1234.56 */
function numf($num) {
    return number_format((double) $num, '2', '.', '');
}

/* ����� �������������� � �������� */
function translit($text) {
    static $cache = array();
    static $array = array(
        'replace' => array('quote', '&amp;', '&lt;', '&gt;', '&quot;', '&euro;', '&raquo;', '&laquo;', '&copy;', '&reg;', '&trade;', '&bdquo;', '&ldquo;', '&nbsp;'),
        'letters' => array('�' => 'a', '�' => 'b', '�' => 'v', '�' => 'g', '�' => 'd', '�' => 'e', '�' => 'e', '�' => 'zh', '�' => 'z', '�' => 'i', '�' => 'i', '�' => 'k', '�' => 'l', '�' => 'm', '�' => 'n', '�' => 'o', '�' => 'p', '�' => 'r', '�' => 's', '�' => 't', '�' => 'u', '�' => 'f', '�' => 'h', '�' => 'ts', '�' => 'ch', '�' => 'sh', '�' => 'sch', '�' => '', '�' => 'y', '�' => '', '�' => 'e', '�' => 'yu', '�' => 'ya')
    );
    if (!isset($cache[$text])) {
        $result = str_replace($array['replace'], '-', strtolower($text));
        $result = strtr($result, $array['letters']);
        $result = preg_replace(array('/[^a-z0-9-]/', '/-+/'), '-', $result);
        $cache[$text] = !empty($result) ? trim($result, '-') : '-';
    }
    return $cache[$text];
}

/* ����� ������ �������� ��� RSS */
function sanitizeRss($text) {
    static $array = array(
        'search'  => array('&bdquo;', '&ldquo;', '&ndash;', '&copy;', '&reg;', '&pound;', '&laquo;', '&raquo;', '&quot;', '&', '<o:p>', '</o:p>', '&nbsp;', '&trade;', '&#8482;', '&#8364;', '&euro;', '&hellip;', '&bull;'),
        'replace' => array('�', '�', '-', '�', '�', '�', '�', '�', '"', '&amp;', '<p>', '</p>', ' ', '�', '�', '�', '�', '�', '�'),
    );
    $text = preg_replace('#\[sx_code lang=(.*?)\](.*?)\[\/sx_code\]#si', '\\2', $text);
    return str_replace($array['search'], $array['replace'], $text);
}

/* ����� ������ �������� �� �������� */
function sanitize($text) {
    static $cache = array();
    static $array = array(
        '\''  => '&#039;',
        ' & ' => ' &amp; ',
        '<'   => '&lt;',
        '>'   => '&gt;',
        '"'   => '&quot;',
        '�'   => '&euro;',
        '�'   => '&raquo;',
        '�'   => '&laquo;',
        '�'   => '&copy;',
        '�'   => '&reg;',
        '�'   => '&trade;',
        '�'   => '&bdquo;',
        '�'   => '&ldquo;',
    );
    if (!isset($cache[$text])) {
        $cache[$text] = strtr($text, $array);
    }
    return $cache[$text];
}

/* ����� ��������� ����������������� ���� ������ */
function perm($action) {
    static $arr = NULL;
    if ($arr === NULL) {
        $arr['p'] = Arr::getSession('perm_admin');
        $arr['a'] = $_SESSION['a_area'];
        $arr['a'] = is_numeric($arr['a']) ? $arr['a'] : 1;
    }
    if (isset($arr['p'][$action . $arr['a']]) && $arr['p'][$action . $arr['a']] == 1) {
        return true;
    }
    if (isset($arr['p']['all' . $arr['a']]) && $arr['p']['all' . $arr['a']] == 1) {
        return true;
    }
    if (Arr::getSession('user_group') == 1) {
        return true;
    }
    return false;
}

/* ����� ��������� ���������������� ���� ������ */
function permission($action) {
    static $arr = NULL;
    if ($arr === NULL) {
        $arr['p'] = Arr::getSession('perm');
        $arr['a'] = Arr::getSession('area');
        $arr['a'] = is_numeric($arr['a']) ? $arr['a'] : 1;
    }
    if (isset($arr['p'][$action . $arr['a']]) && $arr['p'][$action . $arr['a']] == 1) {
        return true;
    }
    if (isset($arr['p']['all' . $arr['a']]) && $arr['p']['all' . $arr['a']] == 1) {
        return true;
    }
    if (Arr::getSession('user_group') == 1) {
        return true;
    }
    return false;
}

/* ����� ��������� ���������� ������� � ����� ������ */
function admin_active($param) {
    static $active = NULL;
    if ($active === NULL) {
        $active = SX::get('admin_active');
        $active['Aktiv_Modul'] = SX::get('admin.Aktiv_Modul');
    }
    if ((isset($active[$param]) && $active[$param] == 1) || $active['Aktiv_Modul'] == 1) {
        return true;
    }
    return false;
}

/* ����� �������� ���������� ������� */
function get_active($param) {
    static $active = NULL;
    if ($active === NULL) {
        $active = SX::get('active');
    }
    if (isset($active[$param]) && $active[$param] == 1) {
        return true;
    }
    return false;
}

/* �������� ��� ������ */
if (!function_exists('mb_split')) {
    function mb_split($pattern, $string, $limit = -1) {
        return split($pattern, $string, $limit);
    }

}
