<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}
error_reporting(0);                                                                     // ����� ������ php: 0 - ���������, E_ALL - ��������
define('STATUS_DIR', realpath(dirname(dirname(__FILE__))));
require_once STATUS_DIR . '/class/class.SX.php';                                        // ���������� �������� ����� �������
SX::initStatus('user');                                                                 // �������������� �������
$Core = SX::object('Core');
$Core->getSection();                                                                    // �������� � ��������� � $_REQUEST['area'] � $_SESSION['area'] ������ ������
$Core->section();                                                                       // ���������� ������������ ������ ��� ���������� ��������� ������
$Core->modules();                                                                       // ������������� ��� �������� ������
$Core->aktiveLangs();                                                                   // �������� ������ �������� ������
$Core->selectLangs();                                                                   // ������������� ������ �� �����
$Core->getLangcode();                                                                   // ������������� ��� ����� � $_SESSION['Langcode']
$lang_settings = $Core->langSettings();                                                 // �������� ��������� �������� �����
SX::getLocale($lang_settings['Sprachcode']);                                            // ������������� ������ PHP
$Core->selectTemplate();                                                                // ������������� ������ � ������� � ������ �������� ������� �������
SX::setDefine('THEME', STATUS_DIR . '/theme/' . SX::get('options.theme'));              // ������������� ���� � �������
SX::loadLang(LANG_DIR . '/' . $_SESSION['lang'] . '/main.txt');                         // ��������� ������ �� ��������� ���� �����
SX::loadLang(LANG_DIR . '/' . $_SESSION['lang'] . '/mail.txt');                         // ��������� ������ �� ���� ����� ������� �����
$_SESSION['Charset'] = SX::$lang['Charset'];                                            // ������������� ������ � ������� ����������
header('Content-type: text/html; charset=' . $_SESSION['Charset']);                     // ������������� ��������� � ������� ����������
SX::object('Cron')->get('cron');
SX::output('ok', true);
