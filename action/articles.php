<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (!defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}
if (!permission('articles')) {
    SX::object('Core')->noAccess();
}
if (!get_active('articles')) {
    SX::object('Core')->notActive();
}

switch (Arr::getRequest('action')) {
    default:
        SX::object('Articles')->all();
        break;

    case 'displayarticle':
        SX::object('Articles')->get(Arr::getRequest('id'));
        break;
}
