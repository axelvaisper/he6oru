<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (!defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}

switch (Arr::getRequest('action')) {
    default:
        $seo_array = array(
            'headernav' => SX::$lang['Sitemap'],
            'pagetitle' => SX::$lang['Sitemap'],
            'content'   => SX::object('Navigation')->sitemap());
        View::get()->finish($seo_array);
        break;

    case 'full':
        SX::object('Navigation')->fullmap();
        break;
}
