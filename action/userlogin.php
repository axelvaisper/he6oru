<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (!defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}
if (!get_active('Login')) {
    SX::object('Core')->notActive();
}

switch (Arr::getRequest('action')) {
    case 'newlogin':
        SX::object('Login')->newLogin();
        break;

    case 'logout':
        SX::object('Login')->logout();
        break;

    case 'ajaxlogin':
        SX::setDefine('AJAX_OUTPUT', '1');
        SX::object('Login')->ajaxLogin();
        break;

    case 'login':
        SX::object('Login')->newLogin(1);
        SX::object('Login')->pageLogin();
        break;

    default:
        SX::object('Login')->pageLogin();
        break;
}
