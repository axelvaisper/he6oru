<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (!defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}

if (!get_active('Register')) {
    SX::object('Core')->notActive();
}
if (Arr::getRequest('sub') == 'ok') {
    SX::object('Login')->success();
} else {
    switch (Arr::getRequest('do')) {
        default:
        case 'register':
            SX::object('Login')->register();
            break;

        case 'checkuserdata':
            SX::setDefine('AJAX_OUTPUT', 1);
            SX::object('Login')->�heck();
            break;

        case 'activate':
            SX::object('Login')->activate();
            break;
    }
}
