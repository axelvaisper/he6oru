<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (!defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}
$CS = View::get();
$contact = SX::get('system');
$link = 'http://' . $_SERVER['HTTP_HOST'];
$array = array(
    '%%COMPANY%%'  => $contact['Firma'],
    '%%TOWN%%'     => $contact['Stadt'],
    '%%ZIP%%'      => $contact['Zip'],
    '%%ADRESS%%'   => $contact['Strasse'],
    '%%MAIL%%'     => str_replace('@', ' [AT] ', $contact['Mail_Absender']),
    '%%TELEFON%%'  => $contact['Telefon'],
    '%%FAX%%'      => $contact['Fax'],
    '%%HTTP%%'     => '<a href="' . $link . '">' . $link . '</a>',
    '%%INN%%'      => $contact['Inn'],
    '%%KPP%%'      => $contact['Kpp'],
    '%%BIK%%'      => $contact['Bik'],
    '%%BANK%%'     => $contact['Bank'],
    '%%KSCHET%%'   => $contact['Kschet'],
    '%%RSCHET%%'   => $contact['Rschet'],
    '%%DIREKTOR%%' => $contact['Seitenbetreiber'],
    '%%BUH%%'      => $contact['Buh']);
$Imprint = Tool::replace($contact['Impressum'], $array);
$CS->assign('Imprint', $Imprint);

$seo_array = array(
    'headernav' => SX::$lang['Imprint'],
    'pagetitle' => SX::$lang['Imprint'],
    'content'   => View::get()->fetch(THEME . '/other/imprint.tpl'));
View::get()->finish($seo_array);
