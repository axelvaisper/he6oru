<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (!defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}
if (!get_active('links')) {
    SX::object('Core')->notActive();
}
if (!permission('links')) {
    SX::object('Core')->noAccess();
}

SX::setDefine('AJAX_OUTPUT', 1);
SX::object('Banner')->click(Arr::getRequest('click'));
