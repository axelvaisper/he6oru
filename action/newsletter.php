<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (!defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}
if (!get_active('newsletter')) {
    SX::object('Core')->notActive();
}

switch (Arr::getRequest('action')) {
    default:
    case 'abonew':
        SX::object('Newsletter')->create();
        break;

    case 'activate':
        SX::object('Newsletter')->activate();
        break;

    case 'unsubscribe':
        SX::object('Newsletter')->delete();
        break;
}
