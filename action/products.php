<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (!defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}
if (!get_active('products')) {
    SX::object('Core')->notActive();
}
if (!permission('products')) {
    SX::object('Core')->noAccess();
}

switch (Arr::getRequest('action')) {
    default:
    case 'overview':
        SX::object('Products')->show();
        break;

    case 'showproduct':
        SX::object('Products')->get(Arr::getRequest('id'));
        break;

    case 'rate':
        break;

    case 'quicksearch':
        SX::setDefine('AJAX_OUTPUT', 1);
        SX::object('Products')->search(Arr::getRequest('q'));
        break;
}
