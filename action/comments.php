<?php
#############################################################################
# *****************  CONTENT MANAGEMENT SYSTEM STATUS-X  ****************** #
# *              Copyright � 2009 - 2017  Alexander Voloshin              * #
# ************************************************************************* #
# * cms@status-x.ru | GNU GENERAL PUBLIC LICENSE | http://www.status-x.ru * #
# ************************************************************************* #
#############################################################################
if (!defined('STATUS_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}

SX::setDefine('AJAX_OUTPUT', 1);
switch (Arr::getRequest('action')) {
    case 'change':
        SX::object('Comments')->change(Arr::getRequest('id'));
        break;

    case 'delete':
        SX::object('Comments')->delete(Arr::getRequest('id'));
        break;
}
