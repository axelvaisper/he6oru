<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="{$langcode}" lang="{$langcode}" dir="ltr">
<head>
{include file="$incpath/other/header_head.tpl"}
{include file="$incpath/other/header_scripts.tpl"}
</head>
<body id="body">
  <img src="{$imgpath}/page/printlogo.png" alt="" />
  <br />
  <h2>{$settings.Seitenname|sanitize}</h2>
  <hr noshade="noshade" size="1" />
  {no_print_link}
  <hr noshade="noshade" size="1" />
  <div id="hide_this" style="">
    <h2>{#PrintVersion#} / <a href="{no_print_link}">{#PrintOrig#}</a></h2>
    <br />
    <br />
    <hr noshade="noshade" size="1" />
    <br />
  </div>
  {$content}
  <hr noshade="noshade" size="1" />
  {include file="$incpath/other/outlinks.tpl"}
  <br />
  <p align="center"> {#copyright_text#} | {version}</p>
  <hr noshade="noshade" size="1" />
  <p align="center">
    <input class="button" type="button" onclick="document.getElementById('hide_this').style.display='none';window.print();" value="{#PrintNow#}" />&nbsp;
    <input class="button" type="button" onclick="window.close();" value="{#WinClose#}" />
  </p>
  {include file="$incpath/other/google.tpl"}
</body>
</html>
