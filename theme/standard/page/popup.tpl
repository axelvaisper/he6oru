<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="{$langcode}" lang="{$langcode}" dir="ltr">
<head>
{include file="$incpath/other/header_head.tpl"}
{include file="$incpath/other/header_scripts.tpl"}
</head>
<body id="popbody" style="padding: 5px">
  <div class="main_content"> {$content} </div>
  <div align="center" style="display: none"> {#copyright_text#} | {version}</div>
  {include file="$incpath/other/google.tpl"}
</body>
</html>
