{if $use_code{$uniq_key} == 1}
<script type="text/javascript">
<!-- //
$(document).ready(function() {
    $('#secure_reload{$uniq_key}').on('click', function() {
        var options = {
            target: '#secure_code{$uniq_key}',
            url: '{$baseurl}/lib/secure.php?action=reload&uniq_key={$uniq_key}&key=' + Math.random(),
            timeout: 3000
        };
        $(this).ajaxSubmit(options);
        return true;
    });
});
//-->
</script>

<fieldset>
  <legend>{#SecureText#}</legend>
  <a class="stip" title="{#ReloadCode#}" id="secure_reload{$uniq_key}" href="javascript: void(0);"><img class="absmiddle" src="{$imgpath_page}reload.png" alt="" /></a>
  {#Secure#}&nbsp;&nbsp;&nbsp;<span id="secure_code{$uniq_key}">{$captcha_img{$uniq_key}}</span>
  <input class="input" name="scode{$uniq_key}" id="scode" type="text" style="width: 80px;font-size: 18px;font-weight: bold" value="" />&nbsp;
  <input type="hidden" name="uniq_key" value="{$uniq_key}" />
</fieldset>
{else}
  <span id="secure_code{$uniq_key}">&nbsp;</span>
{/if}
