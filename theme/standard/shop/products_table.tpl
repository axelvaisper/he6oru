<script type="text/javascript">
<!-- //
$(document).ready(function() {
    $('.product_table').on('click', function() {
        $('#slide_' + $(this).attr('id')).slideToggle('slow');
    });

    var options = { target: '#ajaxbasket', timeout: 3000 };
    $('.ajax_products').submit(function() {
        var id = '#mylist_' + $(this).attr('id');
        if ($(id).val() == 1) {
            showNotice('<br /><p class="h3">{#Shop_ProdAddedToList#}</p><br />', 2000);
        } else {
            showNotice($('#ajax_prodmessage'), 10000);
        }
        $(this).ajaxSubmit(options);
        $(id).val(0);
        return false;
    });
    $('#ajax_yes').on('click', function() {
        document.location = 'index.php?action=showbasket&p=shop';
        $.unblockUI();
        return false;
    });
    $('#ajax_no').on('click', function() {
        $.unblockUI();
        return false;
    });
});
//-->
</script>

<div id="ajax_prodmessage" style="display: none">
  <br />
  <p class="h3">{#Shop_ProdAddedToBasket#}</p>
  <p>{#LoginExternActions#}</p>
  <input class="shop_buttons_big" type="button" id="ajax_yes" value="{#Shop_go_basket#}" />
  <input class="shop_buttons_big_second" type="button" id="ajax_no" value="{#WinClose#}" />
  <br />
  <br />
</div>
{assign var=shop_q value=$smarty.request.shop_q|urlencode|default:'empty'}
{if $shopsettings->TopNewOffersPos == 'top' && !$is_print}
{include file="$incpath/shop/categ_tabs.tpl"}
{/if}
{if $smarty.request.s == 1}
{include file="$incpath/shop/search_extended.tpl"} <br />
{/if}
{if $cat_desc}
<div class="shop_cat_desc">{$cat_desc}</div>
{/if}
<div class="shop_headers">{#Shop_productOverview#}</div>
{if !$products}
<div class="shop_empty_categ">
  {if !empty($smarty.request.shop_q)}
  {#Shop_searchNull#}
  {else}
  {#Shop_noProducts#}
  {/if} </div>
<br />
{else}
{if !$is_print}
<div class="shop_header_inf">
  <table width="100%" cellspacing="0" cellpadding="0">
    <tr>
      <td class="shop_header_inf_pages"> {#DataRecords#}:
        {if !empty($smarty.request.limit)} <a class="{if $smarty.request.limit == 6}page_active{else}page_navigation{/if}" href="{page_link|regex_replace: "/limit=([a-zA-Z0-9]*)/": "limit=6"|regex_replace: "/page=([a-zA-Z0-9]*)/": "page=1"}">6</a> <a class="{if $smarty.request.limit == 10}page_active{else}page_navigation{/if}" href="{page_link|regex_replace: "/limit=([a-zA-Z0-9]*)/": "limit=10"|regex_replace: "/page=([a-zA-Z0-9]*)/": "page=1"}">10</a> <a class="{if $smarty.request.limit == 20}page_active{else}page_navigation{/if}" href="{page_link|regex_replace: "/limit=([a-zA-Z0-9]*)/": "limit=20"|regex_replace: "/page=([a-zA-Z0-9]*)/": "page=1"}">20</a> <a class="{if $smarty.request.limit == 50}page_active{else}page_navigation{/if}" href="{page_link|regex_replace: "/limit=([a-zA-Z0-9]*)/": "limit=50"|regex_replace: "/page=([a-zA-Z0-9]*)/": "page=1"}">50</a> <a class="{if $smarty.request.limit == 100}page_active{else}page_navigation{/if}" href="{page_link|regex_replace: "/limit=([a-zA-Z0-9]*)/": "limit=100"|regex_replace: "/page=([a-zA-Z0-9]*)/": "page=1"}">100</a> <a class="{if $smarty.request.limit == 200}page_active{else}page_navigation{/if}" href="{page_link|regex_replace: "/limit=([a-zA-Z0-9]*)/": "limit=200"|regex_replace: "/page=([a-zA-Z0-9]*)/": "page=1"}">200</a> {else} <a class="page_navigation" href="index.php?shop_q={$shop_q}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit=6&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$smarty.request.list|default:$title_sort}{if $smarty.request.s == 1}&amp;s=1{/if}">6</a> <a class="page_navigation" href="index.php?shop_q={$shop_q}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit=10&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$smarty.request.list|default:$title_sort}{if $smarty.request.s == 1}&amp;s=1{/if}">10</a> <a class="page_navigation" href="index.php?shop_q={$shop_q}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit=20&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$smarty.request.list|default:$title_sort}{if $smarty.request.s == 1}&amp;s=1{/if}">20</a> <a class="page_navigation" href="index.php?shop_q={$shop_q}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit=50&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$smarty.request.list|default:$title_sort}{if $smarty.request.s == 1}&amp;s=1{/if}">50</a> <a class="page_navigation" href="index.php?shop_q={$shop_q}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit=100&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$smarty.request.list|default:$title_sort}{if $smarty.request.s == 1}&amp;s=1{/if}">100</a> <a class="page_navigation" href="index.php?shop_q={$shop_q}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit=100&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$smarty.request.list|default:$title_sort}{if $smarty.request.s == 1}&amp;s=1{/if}">200</a> {/if} </td>
      <td align="right" class="shop_header_inf_pages"> {if !empty($pages)}
        {$pages}
        {/if} </td>
    </tr>
  </table>
</div>
{/if}
<table width="100%" border="0" cellpadding="3" cellspacing="1">
  <tr class="shop_merge_second">
    <td align="center" nowrap="nowrap"><a class="page_navigation" href="index.php?shop_q={$shop_q}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit={$smarty.request.limit|default:$shopsettings->Produkt_Limit_Seite}&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$art_sort}{if $smarty.request.s == 1}&amp;s=1{/if}">{#Shop_ArticleNumber#} <img src="{$imgpath_page}{$img_art_sort|default:"sorter_none.png"}" alt="" /></a></td>
    <td nowrap="nowrap" align="center"><a class="page_navigation" href="index.php?shop_q={$shop_q}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit={$smarty.request.limit|default:$shopsettings->Produkt_Limit_Seite}&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$title_sort}{if $smarty.request.s == 1}&amp;s=1{/if}">{#Shop_title2#} <img src="{$imgpath_page}{$img_title_sort|default:"sorter_none.png"}" alt="" /></a></td>
    <td nowrap="nowrap" align="center"><a class="page_navigation" href="index.php?shop_q={$shop_q}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit={$smarty.request.limit|default:$shopsettings->Produkt_Limit_Seite}&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$kat_sort}{if $smarty.request.s == 1}&amp;s=1{/if}">{#Global_Categ#} <img src="{$imgpath_page}{$img_kat_sort|default:"sorter_none.png"}" alt="" /></a></td>
    <td nowrap="nowrap" align="center"><a class="page_navigation" href="index.php?shop_q={$shop_q}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit={$smarty.request.limit|default:$shopsettings->Produkt_Limit_Seite}&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$klick_sort}{if $smarty.request.s == 1}&amp;s=1{/if}">{#Forums_Header_hits#} <img src="{$imgpath_page}{$img_klick_sort|default:"sorter_none.png"}" alt="" /></a></td>
    <td nowrap="nowrap" align="center"><a class="page_navigation" href="index.php?shop_q={$shop_q}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit={$smarty.request.limit|default:$shopsettings->Produkt_Limit_Seite}&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$date_sort}{if $smarty.request.s == 1}&amp;s=1{/if}">{#Date#} <img src="{$imgpath_page}{$img_date_sort|default:"sorter_none.png"}" alt="" /></a></td>
    <td nowrap="nowrap" align="center"><a class="page_navigation" href="index.php?shop_q={$shop_q}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit={$smarty.request.limit|default:$shopsettings->Produkt_Limit_Seite}&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$price_sort}{if $smarty.request.s == 1}&amp;s=1{/if}">{#Products_price#} <img src="{$imgpath_page}{$img_price_sort|default:"sorter_none.png"}" alt="" /></a></td>
  </tr>
  {foreach from=$products item=p name=pro}
  <tr id="product_table_{$p.Id}" class="product_table {cycle name='fs' values='shop_merge_first,shop_merge_second'}" style="cursor: pointer">
    <td nowrap="nowrap">{$p.Artikelnummer|sanitize}</td>
    <td colspan="4">{$p.Titel|sanitize}</td>
    <td align="center" nowrap="nowrap">{if $p.Preis > 0}{$p.Preis|numformat} {$currency_symbol}{else}{#Zvonite#}{/if}</td>
  </tr>
  <tr>
    <td colspan="13"><div id="slide_product_table_{$p.Id}" style="display: none" class="shop_products_simple_first">
        {assign var=count value=$count+1} <a name="prod_anchor_{$p.Id}"></a>
        <form method="post" name="products_{$p.Id}" id="ajax_{$p.Id}" class="ajax_products" action="{if empty($p.Vars)}index.php?p=shop&amp;area={$area}{else}index.php?p=shop&amp;area={$area}&amp;action=showproduct&amp;id={$p.Id}{/if}">
          <table width="100%" cellspacing="0" cellpadding="0">
            <tr>
              <td width="150" valign="top"> {if $shopsettings->popup_product == 1} <a class="colorbox" title="{$p.Titel|sanitize}" href="{$p.ProdLink}&amp;blanc=1"><img class="shop_productimage_list" src="{$p.Bild_Mittel}" alt="{$p.Titel|sanitize}" /></a> {else} <a title="{$p.Titel|sanitize} - {$p.Beschreibung|striptags|truncate: 500|sanitize}" href="{$p.ProdLink}"><img class="shop_productimage_list" src="{$p.Bild_Mittel}" alt="{$p.Titel|sanitize}" /></a> {/if} <br />
                {if $p.Fsk18 == 1}
                <p align="center"> <img src="{$imgpath_page}usk_small.gif" alt="{#Shop_isFSKWarning#}" /> <br />
                  <strong>{#Shop_isFSKWarning#}</strong> </p>
                {/if} </td>
              <td valign="top"> {if $shopsettings->popup_product == 1}
                <h3><a class="colorbox stip" title="{$p.Beschreibung|tooltip:500}" href="{$p.ProdLink}&amp;blanc=1">{$p.Titel|sanitize}</a></h3>
                {else}
                <h3><a class="stip" title="{$p.Beschreibung|tooltip:500}" href="{$p.ProdLink}">{$p.Titel|sanitize}</a></h3>
                {/if}
                {if $shopsettings->Zeige_Text == 1 && !empty($p.Beschreibung)}
                {$p.Beschreibung|striptags|truncate: $shopsettings->Prodtext_Laenge|sanitize} <br />
                <br />
                {/if}
                {if $shopsettings->Zeige_Verfuegbarkeit == 1 || $shopsettings->Zeige_Lagerbestand == 1 || $shopsettings->Zeige_Lieferzeit == 1 || $shopsettings->Zeige_ArtNr == 1 || $shopsettings->Zeige_Hersteller == 1 || $shopsettings->Zeige_ErschienAm == 1}
                <table cellpadding="0" cellspacing="0">
                  {if $shopsettings->Zeige_Verfuegbarkeit == 1 || $shopsettings->Zeige_Lagerbestand == 1 || $shopsettings->Zeige_Lieferzeit == 1 || $shopsettings->Zeige_ArtNr == 1 || $shopsettings->Zeige_Hersteller == 1 || $shopsettings->Zeige_ErschienAm == 1}
                  <tr>
                    <td width="150">{#Global_Categ#}: </td>
                    <td align="right">{$p.KategorieName}</td>
                  </tr>
                  {/if}
                  {if $shopsettings->Zeige_Verfuegbarkeit == 1}
                  <tr>
                    <td width="150">{#Shop_Availablility#}: </td>
                    <td align="right">{$p.VIcon}</td>
                  </tr>
                  {/if}
                  {if $p.Lieferzeit && $p.Lagerbestand>0}
                  {if $shopsettings->Zeige_Lagerbestand == 1}
                  <tr>
                    <td width="150">{#Shop_av_store#}: </td>
                    <td align="right">{#Shop_av_storeAv#} {$p.Lagerbestand}</td>
                  </tr>
                  {/if}
                  {if $shopsettings->Zeige_Lieferzeit == 1}
                  <tr>
                    <td width="150">{#Shop_shipping_timeinf#}</td>
                    <td align="right">{$p.Lieferzeit|sanitize}</td>
                  </tr>
                  {/if}
                  {/if}
                  {if $shopsettings->Zeige_ArtNr == 1}
                  <tr>
                    <td width="150">{#Shop_ArticleNumber#}: </td>
                    <td align="right"><strong>{$p.Artikelnummer}</strong></td>
                  </tr>
                  {/if}
                  {if $p.man->Id >= 1 && $shopsettings->Zeige_Hersteller == 1}
                  <tr>
                    <td width="150">{#Manufacturer#}: </td>
                    <td align="right"><a href="index.php?p=shop&amp;action=showproducts&amp;man={$p.man->Id}">{$p.man->Name}</a></td>
                  </tr>
                  {/if}
                  {if $shopsettings->Zeige_ErschienAm == 1}
                  <tr>
                    <td width="150">{#Added#}</td>
                    <td align="right">{$p.Erstellt|date_format: $lang.DateFormatSimple}</td>
                  </tr>
                  {/if}
                </table>
                <br />
                {/if}
                <div class="shop_products_list_bboxheader" style="margin-top: 5px">
                  <div style="float: left">
                    {if $shopsettings->PreiseGaeste == 1 || $loggedin}
                    {if $p.Preis_Liste != $p.Preis}
                    {#Shop_instead#}&nbsp;<span class="shop_price_old">{$p.Preis_Liste|numformat} {$currency_symbol}</span> <br />
                    {/if}
                    {if !empty($p.Vars)}
                    {#Shop_priceFrom#}
                    {/if}
                    {if $p.Preis > 0} <span class="shop_price">{$p.Preis|numformat} {$currency_symbol}</span> {else} <span class="shop_price">{#Zvonite#}</span> {/if}
                    {else} <strong>{#Shop_prices_justforUsers#}</strong> {/if} </div>
                  <div style="float: right">
                    {if empty($p.Vars) && $p.Lagerbestand > 0 && $p.Preis > 0 && empty($p.Frei_1) && empty($p.Frei_2) && empty($p.Frei_3)}
                    {if $p.Fsk18 == 1 && $fsk_user != 1}
                    {else}
                    {#Shop_amount#}&nbsp;
                    <input class="input" name="amount" type="text" style="width: 40px" value="1" maxlength="3" />
                    {/if}
                    {/if}
                  </div>
                  <div class="clear"></div>
                </div>
                <div class="shop_products_list_bbox">
                  <div class="shop_products_list_bcontent">
                    {if $p.shipping_free == 1}
                    <div class="product_important_noshipping small">{#Shop_freeshipping#}</div>
                    {/if}
                    {if $p.diffpro > 0}
                    <div class="product_important_cheaper small">{#Shop_Billiger#}{$p.diffpro|numformat}%</div>
                    {/if}
                    {if $shopsettings->PreiseGaeste == 1 || $loggedin}
                    <div style="float: right"> {if $p.Fsk18 == 1 && $fsk_user != 1}
                      {if $shopsettings->popup_product == 1}
                      <button class="shop_buttons_big_second" type="button" onclick="newWindow('{$p.ProdLink}&amp;blanc=1', '90%', '97%');"><img src="{$imgpath}/shop/basket_simple.png" alt="" /> {#buttonDetails#}</button>
                      {else}
                      <button class="shop_buttons_big_second" type="button" onclick="location.href='{$p.ProdLink}';"><img src="{$imgpath}/shop/basket_simple.png" alt="" /> {#buttonDetails#}</button>
                      {/if}
                      {else}
                      {if empty($p.Vars) && $p.Lagerbestand > 0 && $p.Preis > 0 && empty($p.Frei_1) && empty($p.Frei_2) && empty($p.Frei_3)}
                      <input type="hidden" name="action" value="to_cart" />
                      <input type="hidden" name="redir" value="{page_link}{*#prod_anchor_{$p.Id}*}" />
                      <input type="hidden" name="product_id" value="{$p.Id}" />
                      <input type="hidden" name="mylist" id="mylist_ajax_{$p.Id}" value="0" />
                      <input type="hidden" name="ajax" value="1" />
                      <noscript>
                      <input type="hidden" name="ajax" value="0" />
                      </noscript>
                      <button class="shop_buttons_big" type="submit"><img src="{$imgpath}/shop/basket_simple.png" alt="" /> {#Shop_toBasket#}</button>
                      <button class="shop_buttons_big_second" onclick="document.getElementById('mylist_ajax_{$p.Id}').value='1';" type="submit"><img src="{$imgpath}/shop/wishlist.png" alt="" />{#Shop_WishList#}</button>
                      {else}
                      <input type="hidden" name="parent" value="{$p.Parent}" />
                      <input type="hidden" name="navop" value="{$p.Navop}" />
                      <input type="hidden" name="cid" value="{$p.Kategorie}" />
                      {if $shopsettings->popup_product == 1}
                      <button class="shop_buttons_big" type="button" onclick="newWindow('{$p.ProdLink}&amp;blanc=1', '90%', '97%');"><img src="{$imgpath}/shop/basket_simple.png" alt="" /> {#buttonDetails#}</button>
                      {else}
                      <button class="shop_buttons_big" type="button" onclick="location.href='{$p.ProdLink}';"><img src="{$imgpath}/shop/basket_simple.png" alt="" /> {#buttonDetails#}</button>
                      {/if}
                      {/if}
                      {/if} </div>
                    {/if}
                    <div class="clear"></div>
                  </div>
                </div></td>
            </tr>
          </table>
        </form>
      </div></td>
  </tr>
  {/foreach}
  <tr class="shop_merge_second">
    <td align="center" nowrap="nowrap"><a class="page_navigation" href="index.php?shop_q={$shop_q}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit={$smarty.request.limit|default:$shopsettings->Produkt_Limit_Seite}&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$art_sort}{if $smarty.request.s == 1}&amp;s=1{/if}">{#Shop_ArticleNumber#} <img src="{$imgpath_page}{$img_art_sort|default:"sorter_none.png"}" alt="" /></a></td>
    <td nowrap="nowrap" align="center"><a class="page_navigation" href="index.php?shop_q={$shop_q}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit={$smarty.request.limit|default:$shopsettings->Produkt_Limit_Seite}&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$title_sort}{if $smarty.request.s == 1}&amp;s=1{/if}">{#Shop_title2#} <img src="{$imgpath_page}{$img_title_sort|default:"sorter_none.png"}" alt="" /></a></td>
    <td nowrap="nowrap" align="center"><a class="page_navigation" href="index.php?shop_q={$shop_q}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit={$smarty.request.limit|default:$shopsettings->Produkt_Limit_Seite}&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$kat_sort}{if $smarty.request.s == 1}&amp;s=1{/if}">{#Global_Categ#} <img src="{$imgpath_page}{$img_kat_sort|default:"sorter_none.png"}" alt="" /></a></td>
    <td nowrap="nowrap" align="center"><a class="page_navigation" href="index.php?shop_q={$shop_q}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit={$smarty.request.limit|default:$shopsettings->Produkt_Limit_Seite}&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$klick_sort}{if $smarty.request.s == 1}&amp;s=1{/if}">{#Forums_Header_hits#} <img src="{$imgpath_page}{$img_klick_sort|default:"sorter_none.png"}" alt="" /></a></td>
    <td nowrap="nowrap" align="center"><a class="page_navigation" href="index.php?shop_q={$shop_q}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit={$smarty.request.limit|default:$shopsettings->Produkt_Limit_Seite}&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$date_sort}{if $smarty.request.s == 1}&amp;s=1{/if}">{#Date#} <img src="{$imgpath_page}{$img_date_sort|default:"sorter_none.png"}" alt="" /></a></td>
    <td nowrap="nowrap" align="center"><a class="page_navigation" href="index.php?shop_q={$shop_q}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit={$smarty.request.limit|default:$shopsettings->Produkt_Limit_Seite}&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$price_sort}{if $smarty.request.s == 1}&amp;s=1{/if}">{#Products_price#} <img src="{$imgpath_page}{$img_price_sort|default:"sorter_none.png"}" alt="" /></a></td>
  </tr>
</table>
{if !$is_print}
<div class="shop_header_inf">
  <table width="100%" cellspacing="0" cellpadding="0">
    <tr>
      <td class="shop_header_inf_pages"> {#DataRecords#}:
        {if !empty($smarty.request.limit)} <a class="{if $smarty.request.limit == 6}page_active{else}page_navigation{/if}" href="{page_link|regex_replace: "/limit=([a-zA-Z0-9]*)/": "limit=6"|regex_replace: "/page=([a-zA-Z0-9]*)/": "page=1"}">6</a> <a class="{if $smarty.request.limit == 10}page_active{else}page_navigation{/if}" href="{page_link|regex_replace: "/limit=([a-zA-Z0-9]*)/": "limit=10"|regex_replace: "/page=([a-zA-Z0-9]*)/": "page=1"}">10</a> <a class="{if $smarty.request.limit == 20}page_active{else}page_navigation{/if}" href="{page_link|regex_replace: "/limit=([a-zA-Z0-9]*)/": "limit=20"|regex_replace: "/page=([a-zA-Z0-9]*)/": "page=1"}">20</a> <a class="{if $smarty.request.limit == 50}page_active{else}page_navigation{/if}" href="{page_link|regex_replace: "/limit=([a-zA-Z0-9]*)/": "limit=50"|regex_replace: "/page=([a-zA-Z0-9]*)/": "page=1"}">50</a> <a class="{if $smarty.request.limit == 100}page_active{else}page_navigation{/if}" href="{page_link|regex_replace: "/limit=([a-zA-Z0-9]*)/": "limit=100"|regex_replace: "/page=([a-zA-Z0-9]*)/": "page=1"}">100</a> <a class="{if $smarty.request.limit == 200}page_active{else}page_navigation{/if}" href="{page_link|regex_replace: "/limit=([a-zA-Z0-9]*)/": "limit=200"|regex_replace: "/page=([a-zA-Z0-9]*)/": "page=1"}">200</a> {else} <a class="page_navigation" href="index.php?shop_q={$shop_q}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit=6&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$smarty.request.list|default:$title_sort}{if $smarty.request.s == 1}&amp;s=1{/if}">6</a> <a class="page_navigation" href="index.php?shop_q={$shop_q}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit=10&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$smarty.request.list|default:$title_sort}{if $smarty.request.s == 1}&amp;s=1{/if}">10</a> <a class="page_navigation" href="index.php?shop_q={$shop_q}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit=20&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$smarty.request.list|default:$title_sort}{if $smarty.request.s == 1}&amp;s=1{/if}">20</a> <a class="page_navigation" href="index.php?shop_q={$shop_q}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit=50&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$smarty.request.list|default:$title_sort}{if $smarty.request.s == 1}&amp;s=1{/if}">50</a> <a class="page_navigation" href="index.php?shop_q={$shop_q}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit=100&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$smarty.request.list|default:$title_sort}{if $smarty.request.s == 1}&amp;s=1{/if}">100</a> <a class="page_navigation" href="index.php?shop_q={$shop_q}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit=100&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$smarty.request.list|default:$title_sort}{if $smarty.request.s == 1}&amp;s=1{/if}">200</a> {/if} </td>
      <td align="right" class="shop_header_inf_pages"> {if !empty($pages)}
        {$pages}
        {/if} </td>
    </tr>
  </table>
</div>
{if $shopsettings->TopNewOffersPos == 'bottom'}
{include file="$incpath/shop/categ_tabs.tpl"}
{/if}
{/if}
{/if}
{if $smarty.request.s != '1'}
{include file="$incpath/shop/products_navi_bottom.tpl"}
{/if}
{if $shopsettings->seen_cat == 1}
{$small_seen_products}
{/if}
{if $shopsettings->vat_info_cat == 1}
{include file="$incpath/shop/vat_info.tpl"}
{/if}
