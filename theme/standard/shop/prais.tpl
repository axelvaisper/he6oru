<div class="box_innerhead">{#Shop_Preis#}</div>
<table class="box_inner" width="100%" cellspacing="2" cellpadding="0">
  <tr>
    <td width="10%" align="center"><strong>{#Shop_ArticleNumber#}</strong></td>
    <td width="80%" align="center"><strong>{#GlobalTitle#}</strong></td>
    <td width="10%" align="center"><strong>{#Products_price#}</strong></td>
  </tr>
  {foreach from=$categs item=cat}
    <tr class="shop_specification_left">
      <td colspan="3">{#Global_Categ#} - <a href="index.php?p=shop&amp;action=showproducts&amp;cid={$cat->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$cat->Name|translit}">{$cat->Name|sanitize}</a></td>
    </tr>
    {foreach from=$prais item=tovar}
      {if $tovar->Kategorie == $cat->Id}
        <tr class="shop_specification_left">
          <td align="center" valign="middle">{$tovar->Artikelnummer|sanitize}</td>
          <td valign="middle"><a href="index.php?p=shop&amp;action=showproduct&amp;id={$tovar->Id}&amp;cid={$tovar->Kategorie}&amp;pname={$tovar->Titel|translit}">{$tovar->Titel|sanitize}</a><br /><small>{$tovar->Beschreibung|striptags|truncate: 200|sanitize}</small></td>
          <td align="center" valign="middle">
            {if $shopsettings->PreiseGaeste == 1 || $loggedin}
              {if $tovar->Preis_Liste > 0}
                {$tovar->Preis_Liste|numformat} {$currency_symbol}
              {else}
                {#Zvonite#}
              {/if}
            {else}
              <strong>{#Shop_prices_justforUsers#}</strong>
            {/if}
          </td>
        </tr>
      {/if}
    {/foreach}
  {/foreach}
</table>
<br />
{if !empty($pages)}
  {$pages}
{/if}
