{include file="$incpath/forums/user_panel_forums.tpl"}
<table width="100%" cellspacing="0" cellpadding="0">
  <tr>
    <td> {include file="$incpath/forums/tree.tpl"} </td>
    <td>
      <div align="right">
        {if $topic->status == 1}
          <img src="{$imgpath_forums}closed.png" alt="{#Forums_TopicClosed#}" class="absmiddle" /><strong>{#Forums_TopicClosed#}</strong>
          {/if}
      </div>
    </td>
  </tr>
</table>
<p>
<h1><a href="{$origlink}">{$topic->title|escape|sslash}</a></h1>
({$origlink})
</p>
<br />
<br />
{foreach from=$postings item=post name=postings}
  <div class="forum_posts_print">
    <table width="100%" cellpadding="4" cellspacing="1">
      <tr>
        <td>
          <h2>{$post->poster->uname}</h2>
          (
          {if $post->datum|date_format: $lang.DateFormatSimple == $smarty.now|date_format: $lang.DateFormatSimple}
            {#today#},&nbsp;{$post->datum|date_format: '%H:%M'}
          {else}
            {$post->datum|date_format: $lang.DateFormatExtended}
          {/if}
          )
          <hr />
          {if !empty($post->title)}
            <strong> {$post->title|escape} </strong>
            <br />
          {/if}
          {$post->message}
        </td>
      </tr>
    </table>
  </div>
{/foreach}
{include file="$incpath/forums/forums_footer.tpl"}
