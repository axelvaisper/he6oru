{if !$is_print}
  <br />
  {if get_active('whosonline')}
    <div class="infobox">
      <h3><img src="{$imgpath_forums}users.png" alt="" class="absmiddle" />&nbsp;{#Forums_WhosOnline#}</h3>
      <br />
      {useronline}
    </div>
  {/if}
  <div class="infobox">
    <h3><img src="{$imgpath_forums}stats.png" alt="" class="absmiddle" />&nbsp;{#Forums_Stats#}</h3>
    <br />
    {forumstats}
  </div>
  <div class="infobox">
    <h3><img src="{$imgpath_forums}birthday.png" alt="" class="absmiddle" />&nbsp;{#Birthdays_Today#}</h3>
    <br />
    {birthdays}
  </div>
{/if}
